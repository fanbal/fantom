﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitFantom
{
	[TestClass]
	public class SameFiles
	{
		[TestMethod]
		public void DifferentPathStringToFileInfo()
		{
			var path1 = @"C:\Users\dell\source\repos\fantom\TestConsole\OpenXml测试.pptx";
			var path2 = @"    C:\Users\dell\source\repos\fantom\TestConsole\OpenXml测试.pptx           ";
			var path3 = @"C:\ Users\ dell \source \repos \fantom \TestConsole \OpenXml测试";
			Assert.IsTrue(File.Exists(path1), "标准路径");
			Assert.IsTrue(File.Exists(path2.Trim()), "杂项路径");
			Assert.IsTrue(File.Exists(path3), "空白符路径");
		}
	}
}
