﻿using Fantom;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitFantom
{
	[TestClass]
	public class PresentationTest
	{
		[TestMethod]
		public void RunPresentation()
		{
			try
			{
				var pathstr = @"C:\Users\fanbal\Source\Repos\fantom_mas\TestCore\OpenXml测试.pptx";
				var app = Application.Create();
				var pres = app.Presentations;
				var pre1 = pres.Load(pathstr);

				foreach (ISlide item in pre1.Slides)
				{
					var s = item.ToString();
					foreach (Shape shape in item.Shapes)
					{
						var s1 = shape.ToString();
						var s2 = string.Format(" Preview Text: {0}", shape.ReadOnlyText.Shorten());
					}
				}
			}
			catch
			{
				Assert.IsTrue(false);
			}

			Assert.IsTrue(true);

		}


	}
}
