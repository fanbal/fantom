﻿using System;
using DocumentFormat.OpenXml;
using D = DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;
using PK = DocumentFormat.OpenXml.Packaging;
using System.IO;
using Fantom;
using Fantom.Models;
using System.Linq;
using System.Text;
using static Fantom.Models.Shape;
using Fantom.Animation;

namespace TestCore
{
    class Program
    {

        static void Main(string[] args)
        {
            var pathstr = Path.Combine(Environment.CurrentDirectory, @"测试文件.pptx");
            //var pathstr = @"I:\repo\Gravity\Gravity\Test.pptx";
            var pathstr2 = Path.Combine(Environment.CurrentDirectory, @"测试文件2.pptx");
            //ReadTest(pathstr);
            //ExportDetail(pathstr);
            //ExportTest(pathstr, pathstr2);
            ExportMarkdownTest(pathstr);
        }


        /// <summary>
        /// 测试核心库的解析功能。
        /// </summary>
        /// <param name="path"></param>
        static void ReadTest(string pathstr)
        {
            var app = Application.Create();

            Console.WriteLine("解析容器构建完成");

            var pres = app.Presentations;
            pres.OnLoadingSlide += (_, e) =>
            {
                var s = string.Empty;

                switch (e.LoadingState)
                {
                    case PresentationsLoadingState.PackageLoadCompleted:
                        s = "包体导入已完成。";
                        break;
                    case PresentationsLoadingState.SlidesReorderCompleted:
                        s = "序列初始化已完成。";
                        break;
                    case PresentationsLoadingState.SlideLoadCompleted:
                        s = $"幻灯片[{e.RaiseLoadingSlideArguments.Index}]已导入完成。幻灯片解析进度：{e.RaiseLoadingSlideArguments.Index}/{e.RaiseLoadingSlideArguments.Count}";
                        break;
                    case PresentationsLoadingState.PresentationLoadCompleted:
                        s = "演示文稿导入完成。";
                        break;
                    case PresentationsLoadingState.ShapeLoadCompleted:
                        s = $"图形对象 {e.RaiseLoadingShapeArguments.Shape.Name} 导入完成。图形解析进度：{e.RaiseLoadingShapeArguments.Index}/{e.RaiseLoadingShapeArguments.Count}。";
                        break;
                    case PresentationsLoadingState.BundleLoadCompleted:
                        s = $"动画效果 {e.Info} 导入完成。";
                        break;
                    default:
                        break;
                }
                Console.WriteLine(s);
            };

            var pre1 = pres.Load(pathstr);

            Console.WriteLine();
            Console.WriteLine("文件路径: {0}", pre1.Path);

            Console.WriteLine();
            Console.WriteLine("主题色：");


            Console.WriteLine();
            Console.WriteLine("幻灯片尺寸: {0}", pre1.SlideSize);

            Console.WriteLine("==============================================");
            Console.WriteLine("幻灯片信息：");
            foreach (Slide slide in pre1.Slides)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("编号：{0}", slide.Index);
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.WriteLine();
                Console.WriteLine("幻灯片自定义数据：");
                foreach (var xmlpart in slide.CustomContainer.CustomXmlList)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(xmlpart.XDocument.Root.Name);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }

                Console.WriteLine();
                Console.WriteLine("图形信息：");
                foreach (var shape in slide.Shapes)
                {
                    PrintShape(shape);
                }

                Console.WriteLine();
                Console.WriteLine("动画信息：");
                WritelineSeq(slide.TimeLine.MainSequence);
                foreach (var seq in slide.TimeLine.InteractiveSequences)
                {
                    WritelineSeq(seq);
                }
                Console.WriteLine("==============================================");
            }
        }

        private static void PrintShape(ShapeBase shapeBase)
        {
            Console.WriteLine($"{shapeBase}");

            switch (shapeBase.Type)
            {
                case ShapeType.Unknown:
                    break;
                case ShapeType.Shape:
                    {
                        //var shape = shapeBase as Shape;
                        //Console.WriteLine($"name: {shape.Name}");
                        var shape = shapeBase as Shape;
                        Console.WriteLine(shape.Fill.Color);
  
                    }
                    break;
                case ShapeType.Group:
                    {

                    }
                    break;
                case ShapeType.Connection:
                    {

                    }
                    break;
                case ShapeType.Picture:
                    {
                        //var picture = shapeBase as Picture;
                        //var media = picture.PictureFill.Media;
                    }
                    break;
                default:
                    break;
            }

        }

        private static void WritelineSeq(AnimationSequence seq)
        {

            foreach (var fra in seq)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Green;
                Console.Write("片段：");
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();

                foreach (var bun in fra)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.Write("集束：");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.WriteLine();
                    foreach (var eff in bun)
                    {
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.Write("效果：");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine(eff);
                        foreach (var bhv in eff)
                        {
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.BackgroundColor = ConsoleColor.DarkYellow;
                            Console.Write("行为：");
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine();
                            Console.WriteLine(bhv);
                        }
                    }
                }
            }
        }

        private static void ExportTest(string path, string outputPath)
        {
            var app = Application.Create();
            var pres = app.Presentations.Load(path);
            
            pres.Save(outputPath);
        }

        private static void ExportMarkdownTest(string path)
        {
            var app = Application.Create();
            var pres = app.Presentations.Load(path);

            using (var filestream = new FileStream("test.md", FileMode.Create))
            {
                using (var markdownStream = new StreamWriter(filestream))
                {
                    pres.ExportMarkdown(markdownStream);
                }

            }
           
           
          
        }

        /// <summary>
        /// 提取 pptx 文件中的重要信息，不借助自己的封装。
        /// </summary>
        static void ExportDetail(string pathstr)
        {

            var predoc = PK.PresentationDocument.Open(pathstr, false);
            var slides = predoc.PresentationPart.SlideParts;
            var masters = predoc.PresentationPart.SlideMasterParts;
            foreach (var mst in masters)
            {

                //foreach (var item in sld.SlideLayoutPart)
                //{
                Console.WriteLine(mst.Uri);
                //}
                foreach (P.SlideLayoutId layoutid in mst.SlideMaster.SlideLayoutIdList)
                {
                    var layout = mst.GetPartById(layoutid.RelationshipId) as PK.SlideLayoutPart;
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine(layout.Uri);
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine(layout.SlideMasterPart.Uri);
                    Console.BackgroundColor = ConsoleColor.Black;
                }
            }

            foreach (var slide in slides)
            {

                //foreach (var item in sld.SlideLayoutPart)
                //{
                Console.WriteLine(slide.Uri);
                //}
              
            }


        }
    }

}
