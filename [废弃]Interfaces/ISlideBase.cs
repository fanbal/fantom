﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 幻灯片对象接口，承载图形对象与动画。
	/// 子对象为 <see cref="IShapes"/> 图形集合对象，父对象为 <see cref="IPresentation"/> 演示文稿对象。
	/// </summary>
	public interface ISlideBase : INotifyPropertyChanged, IFantomObject, ISelectable, ICustomContainer
	{

		/// <summary>
		/// 幻灯片在演示文稿中的编号，编号从0开始。
		/// </summary>
		int Index { get; }

		/// <summary>
		/// 幻灯片的名称。
		/// </summary>
		string Name { get; set; }

		/// <summary>
		/// 图形集合对象。
		/// </summary>
		IShapes Shapes { get; }

		/// <summary>
		/// 动画时间轴对象。
		/// </summary>
		ITimeLine TimeLine { get; }



	}
}
