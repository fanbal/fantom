﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 承载动画效果的动画片段。
	/// </summary>
	public interface IAnimationFragment : IFantomObject, IList<IAnimationBundle>
	{
		/// <summary>
		/// 动画片段的名称。
		/// </summary>
		string Name { get; set; }
	}
}
