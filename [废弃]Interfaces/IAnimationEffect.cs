﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 动画效果，该对象由若干 <see cref="IAnimationBehavior"/> 动画行为构成。每个 <see cref="IAnimationEffect"/> 动画效果至少具有一条 <see cref="IAnimationBehavior"/> 动画行为。
	/// </summary>
	public interface IAnimationEffect : IFantomObject, IList<IAnimationBehavior>, ITiming
	{
		/// <summary>
		/// 动画的作用图形，该属性允许为空。
		/// 空值时，该动画将不具有任何效用，但是在输出保存时，空宿主的动画将无法被导出。
		/// </summary>
		IShape Shape { get; set; } 

		/// <summary>
		/// 动画效果的名称。
		/// </summary>
		string Name { get; set; }
	}
}
