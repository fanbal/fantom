﻿using System.ComponentModel;

namespace Fantom
{

	/// <summary>
	/// 幻灯片对象接口，承载图形对象与动画。
	/// 子对象为 <see cref="IShapes"/> 图形集合对象，父对象为 <see cref="IPresentation"/> 演示文稿对象。
	/// </summary>
	public interface ISlide : ISlideBase
	{
		/// <summary>
		/// 样式布局。
		/// </summary>
		ISlideLayout Layout { get; set; }
	}

}
