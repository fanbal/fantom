﻿using System.ComponentModel;
using Fantom.Drawing;

namespace Fantom
{
	/// <summary>
	/// 幻灯片对象基本接口。
	/// </summary>
	public interface IPresentation : INotifyPropertyChanged, IFantomObject
	{
		/// <summary>
		/// 当演示文稿被激活时触发该事件。
		/// </summary>
		event PresentationEventHandler Actived;

		/// <summary>
		/// 演示文稿关闭前触发该事件。
		/// </summary>
		event PresentationEventHandler BeforeClose;

		/// <summary>
		/// 演示文稿关闭后触发该事件。
		/// </summary>
		event PresentationEventHandler AfterClose;

		/// <summary>
		/// 当演示文稿保存前触发该事件。
		/// </summary>
		event PresentationEventHandler BeforeSave;

		/// <summary>
		/// 当演示文稿保存后触发该事件。
		/// </summary>
		event PresentationEventHandler AfterSave;

		/// <summary>
		/// 当演示文稿解除编辑时触发该事件。
		/// </summary>
		event PresentationEventHandler AllowEdit;

		/// <summary>
		/// 幻灯片尺寸发生变化时触发该事件。
		/// </summary>
		event PresentationSizeChangeEventHandler SlideSizeChanged;

		/// <summary>
		/// 备注页尺寸发生变化时触发该事件。
		/// </summary>
		event PresentationSizeChangeEventHandler NoteSizeChanged;

		int RevisionCount { get; }

		/// <summary>
		/// 是否被激活。
		/// </summary>
		bool IsActived { get; }

		/// <summary>
		/// 作者名。
		/// </summary>
		string Author { get; }

		/// <summary>
		/// 演示文稿名。
		/// </summary>
		string Name { get; }

		/// <summary>
		/// 是否允许编辑。
		/// </summary>
		bool AllowedEdit { get; }

		/// <summary>
		/// 演示文稿路径。
		/// </summary>
		string Path { get; }


		/// <summary>
		/// 幻灯片主页的主题。
		/// </summary>
		ITheme Theme { get; }

		/// <summary>
		/// 幻灯片集合。
		/// </summary>
		ISlides Slides { get; }

		/// <summary>
		/// 幻灯片母版集合。
		/// </summary>
		ISlideMasters SlideMasters { get; }

		/// <summary>
		/// 备注母版集合。
		/// </summary>
		INoteMasters NoteMasters { get; }

		/// <summary>
		/// 幻灯片尺寸属性。
		/// </summary>
		Size SlideSize { get; set; }

		/// <summary>
		/// 备注页属性。
		/// </summary>
		Size NoteSize { get; set; }

		/// <summary>
		/// 媒体控制管理相关。
		/// </summary>
		IMediaManager MediaManager { get; }

		/// <summary>
		/// 激活。
		/// </summary>
		void Activate();

		/// <summary>
		/// 强行关闭演示文稿，并释放内存。
		/// </summary>
		void Close();

		/// <summary>
		/// 保存文件。
		/// </summary>
		void Save();

		/// <summary>
		/// 另存为文件。
		/// </summary>
		/// <param name="saveAsPath">另存为路径</param>
		void Save(string saveAsPath);

		/// <summary>
		/// 允许编辑，开启AllowedEdit状态。
		/// </summary>
		void PremitEdit();

		/*
		/// <summary>
		/// 开始放映模式，从当前幻灯片开始放映。
		/// </summary>
		void Run();
		*/


	}
}
