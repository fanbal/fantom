﻿namespace Fantom
{
	/// <summary>
	/// 表明对象是否可被选择。
	/// </summary>
	public interface ISelectable
	{
		/// <summary>
		/// 是否被选中，该属性被修改时会触发<see cref="ISelection"/> 中的<see cref="ISelection.OnSelectionChanged"/> 事件与 <see cref="OnSelect"/> 或 <see cref="LostSelect"/> 事件。
		/// </summary>
		bool IsSelected { get; set; }

	}
}
