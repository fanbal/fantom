﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Fantom
{
	/// <summary>
	/// 可视化集合接口。
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IObserveCollection<T> : INotifyPropertyChanged, IList<T>, INotifyCollectionChanged
	{
	}
}
