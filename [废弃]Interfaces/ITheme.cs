﻿using System.Collections.Generic;
using System.ComponentModel;
using Fantom.Drawing;

namespace Fantom
{

	/// <summary>
	/// 主题。
	/// </summary>
	public interface ITheme : INotifyPropertyChanged, IFantomObject
	{

		/// <summary>
		/// 默认颜色。
		/// </summary>
		IReadOnlyDictionary<ThemeColorType, BaseColor> Colors { get; }



	}
}