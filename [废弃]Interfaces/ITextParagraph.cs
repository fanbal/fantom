﻿namespace Fantom
{
	/// <summary>
	/// 文本段落。可存放一段文本。段落之间存在换行。
	/// </summary>
	public interface ITextParagraph : IFantomObject, IObserveCollection<ITextBlock>
	{
		/// <summary>
		/// 新建字块，并附加在文本末尾。
		/// </summary>
		/// <returns>字块对象。</returns>
		ITextBlock New();

		/// <summary>
		/// 只读的文本。
		/// </summary>
		string ReadOnlyText { get; }
	}
}
