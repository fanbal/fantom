﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 时间轴对象，用于描述PPT的多条动画与交互子序列。
	/// </summary>
	public interface ITimeLine: IFantomObject
	{
		/// <summary>
		/// 主动画序列，每页幻灯片中均存在。
		/// </summary>
		IAnimationSequence MainSequence { get; }

		/// <summary>
		/// 交互序列列表，此处用于描述触发器序列，默认序列为空。
		/// </summary>
		IList<IAnimationSequence> InteractiveSequences { get; }

	}
}
