﻿namespace Fantom
{
	/// <summary>
	/// 文本主干对象，里面通过存放文本段落描述文本的细节，如果您只是希望读取图片，您可以使用 <see cref="ITextBody.Parent"/> 父对象中的 <see cref="IShape.ReadOnlyText"/> 图形对象只读文本属性。
	/// </summary>
	public interface ITextBody : IObserveCollection<ITextParagraph>, IFantomObject
	{
		/// <summary>
		/// 主文本颜色的填充。
		/// </summary>
		IFillFormat Fill { get; set; }

		/// <summary>
		/// 文本内容全局的字体大小。
		/// </summary>
		Emu FontSize { get; set; }

		/// <summary>
		/// 文本内容全局的字体类型。
		/// </summary>
		string FontFamily { get; set; }

		/// <summary>
		/// 创建新文本块。
		/// </summary>
		/// <returns></returns>
		ITextParagraph New();

		/// <summary>
		/// 只读的文本。
		/// </summary>
		string ReadOnlyText { get; }
	}
}
