﻿using System.Xml.Linq;
namespace Fantom
{
	/// <summary>
	/// 自定义Xml内容。
	/// </summary>
	public interface ICustomXml : IFantomObject
	{
		/// <summary>
		/// Xml文件对象。
		/// </summary>
		XDocument XDocument { get; }
	}
}
