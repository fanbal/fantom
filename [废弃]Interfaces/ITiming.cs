﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 具有时间描述的对象，如动画，行为等，都具备该接口。
	/// </summary>
	public interface ITiming
	{
		/// <summary>
		/// 效果持续时间。
		/// </summary>
		Emu Duration { get; set; }

		/// <summary>
		/// 动画延迟开始时间。
		/// </summary>
		Emu Delay { get; set; }

		/// <summary>
		/// 是否播放反向恢复动画。
		/// 在设计一些动画效果如弹跳、摇晃等时，您可以设置该值以实现自动逆转。
		/// </summary>
		bool AutoReverse { get; set; }

		/// <summary>
		/// 加速比，参考单位为 <see cref="Emu.Percent1"/> 。如果您希望获得其值，您需要调用 <see cref="Emu.ToPercent"/> 。
		/// <para>例如数值 0.9 意味着动画将在前 90% 的时间内逐渐加速到默认速度，并在最后 10% 的时间内保持默认速度。</para>
		/// <para>如果您希望在动画结束时减慢动画的速度，请使用 <see cref="Decelerate"/> 属性。</para>
		/// </summary>
		Emu Accelerate { get; set; }

		/// <summary>
		/// 减速比，参考单位为 <see cref="Emu.Percent1"/> 。如果您希望获得其值，您需要调用 <see cref="Emu.ToPercent"/> 。
		/// <para>例如数值 0.9 表示减速过程以默认速度启动，经过动画持续时间的 10% 之后开始减速。</para>
		/// <para>如果您希望在动画结束时加速动画的速度，请使用 <see cref="Accelerate"/> 属性。</para>
		/// </summary>
		Emu Decelerate { get; set; }


		/// <summary>
		/// 弹跳结束，参考单位为 <see cref="Emu.Percent1"/>。如果您希望获得其值，您需要调用 <see cref="Emu.ToPercent"/> 。
		/// <para>例如数值 0.9 表示弹跳结束过程以默认速度启动，经过动画持续时间的 10% 之后开始弹跳结束。</para>
		/// 该属性最早出现在 Office2010 版本中，2003 与 2007 不支持该动画。
		/// </summary>
		Emu BounceEnd { get; set; }

		/// <summary>
		/// 重复次数，参考单位为 <see cref="Emu.Double1"/> 。当您希望有重复多次的动画时，如旋转多次的需求时，您可以设置重复次数。
		/// <para>重复次数允许填入小数，如果您填入小数如 0.5（Emu 500）时，动画可能会出现在某一时刻被唐突截断的生硬效果。</para>
		/// </summary>
		Emu RepeatCount { get; set; }

		/// <summary>
		/// 是否动画结束时重置动画。
		/// </summary>
		bool IsRewindAtEnd { get; set; } 


	}
}
