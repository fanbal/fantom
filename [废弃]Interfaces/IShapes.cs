﻿namespace Fantom
{

	/// <summary>
	/// 图形集合接口，承载图形对象，父级对象为 <see cref="ISlide"/> 幻灯片对象。子级对象为 <see cref="IShape"/> 图形对象。
	/// </summary>
	public interface IShapes : IObserveCollection<IShape>, IFantomObject, ISelection
	{
		/// <summary>
		/// 按指定名称索引集合中的 <see cref="IShape"/> 图形对象。
		/// </summary>
		/// <param name="shapeName">图形对象的名称。</param>
		/// <returns>指定名称的 <see cref="IShape"/> 图形对象。</returns>
		IShape this[string shapeName] { get; }


		/// <summary>
		/// 新建图形。
		/// </summary>
		/// <returns>新的 <see cref="ISlide"/> 实例。</returns>
		IShape Add();


	}
}
