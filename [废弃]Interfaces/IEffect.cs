﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Interfaces
{
	/// <summary>
	/// 可视元素的特殊效果集合对象。内部具体描述了可视元素的阴影、倒影等特殊效果。
	/// </summary>
	interface IEffect
	{
	}
}
