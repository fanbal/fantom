﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 动画束，由效果构成。
	/// </summary>
	public interface IAnimationBundle : IFantomObject, IList<IAnimationEffect>
	{
		/// <summary>
		/// 动画束的名称。
		/// </summary>
		string Name { get; set; }
	}
}
