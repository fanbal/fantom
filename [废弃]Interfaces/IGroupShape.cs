﻿using System.ComponentModel;
using System.Collections.Generic;
using Fantom.Drawing;

namespace Fantom
{

	/// <summary>
	/// 图形对象。父对象为 <see cref="IShapes"/> 图形集合对象实例。
	/// </summary>
	public interface IGroupShape : IShape, IObserveCollection<IShape>
	{
	}
}
