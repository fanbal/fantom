﻿namespace Fantom
{
	/// <summary>
	/// 文本段落块。里面会存放单个相同语言的文本。
	/// </summary>
	public interface ITextBlock : IFantomObject
	{

		/// <summary>
		/// 颜色填充类型。
		/// </summary>
		IFillFormat Fill { get; set; }


		/// <summary>
		/// 文本块中的文本。
		/// </summary>
		string Text { get; set; }


		/// <summary>
		/// 字体尺寸。
		/// </summary>
		Emu FontSize { get; set; }

		/// <summary>
		/// 字型。
		/// </summary>
		string FontFamily { get; set; }
	}
}
