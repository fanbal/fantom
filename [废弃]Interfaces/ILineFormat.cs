﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{

	/// <summary>
	/// 线条格式。
	/// </summary>
	public interface ILineFormat:IFillFormat
	{
		/// <summary>
		/// 线宽粗细。
		/// </summary>
		Emu Thickness { get; set; }
	}
}
