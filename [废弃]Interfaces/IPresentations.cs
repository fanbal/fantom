﻿namespace Fantom
{
	/// <summary>
	/// 幻灯片集合接口。
	/// </summary>
	public interface IPresentations : IObserveCollection<IPresentation>, IFantomObject
	{

		/// <summary>
		/// 在幻灯片导入过程中中触发多次该事件，以此向外汇报进度。完成时也会触发该事件。
		/// </summary>
		event PresentationsLoadingDocumentEventHandler OnLoadingSlide;

		/// <summary>
		/// 当前默认的 <see cref="IPresentation"/> 演示文稿 对象。
		/// </summary>
		IPresentation CurrentPresentation { get; }

		/// <summary>
		/// 通过指定路径 <see cref="string"/> 字符串 导入 <see cref="IPresentation"/> 演示文稿 对象。默认只读模式，不允许编辑。
		/// </summary>
		/// <returns><see cref="IPresentation"/> 演示文稿 对象。</returns>
		IPresentation Load(string uristr);

		/// <summary>
		/// 通过指定路径 <see cref="string"/> 字符串 导入 <see cref="IPresentation"/> 演示文稿 对象。
		/// </summary>
		/// <returns><see cref="IPresentation"/> 演示文稿 对象。</returns>
		IPresentation Load(string uristr, bool allowEdit);


		/// <summary>
		/// 新建 <see cref="IPresentation"/> 演示文稿 对象。
		/// </summary>
		/// <returns><see cref="IPresentation"/> 演示文稿 对象。</returns>
		IPresentation New();

		/// <summary>
		/// 关闭指定的 <see cref="IPresentation"/> 演示文稿 对象。
		/// </summary>
		void Close(IPresentation presentation);



	}
}
