﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 媒体文件，其中容纳了PPTX中全部的资源文件，
	/// Fantom 会将其全部读入内存，因此它可能会占用您大量内存。
	/// </summary>
	public interface IMedia
	{
		/// <summary>
		/// 该媒体文件位于图中的位置，其可以指向源文件包体中的具体位置，也可以指向当前系统文件资源管理器中的位置。
		/// </summary>
		Uri Uri { get; }


		/// <summary>
		/// 媒体文件数据。如果您希望导出该文件数据，您可以使用 <see cref="System.IO.FileStream"/> 文件流对象或更多流对象实现数据的导出与编辑。
		/// </summary>
		byte[] Bytes { get; }
	}
}
