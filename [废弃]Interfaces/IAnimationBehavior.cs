﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 动画行为，该对象集合组成 <see cref="IAnimationEffect"/> 动画效果。每个 <see cref="IAnimationEffect"/> 动画效果至少具有一条 <see cref="IAnimationBehavior"/> 动画行为。
	/// </summary>
	public interface IAnimationBehavior : IFantomObject, ITiming
	{
		/// <summary>
		/// 与动画绑定的图形，该属性可为空。
		/// </summary>
		IShape Shape { get; set; }

		/// <summary>
		/// 在 <see cref="IAnimationEffect"/> 对象集合中移除该对象。
		/// </summary>
		void Delete();
	}
}
