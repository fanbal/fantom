﻿using System.ComponentModel;
using Fantom.Drawing;
using Fantom.Effect;

namespace Fantom
{

	/// <summary>
	/// 图形对象。父对象为 <see cref="IShapes"/> 图形集合对象实例。
	/// </summary>
	public interface IShape : INotifyPropertyChanged, IFantomObject, ISelectable, ICustomContainer
	{

		/// <summary>
		/// 是否为文本框，其影响默认的文字填充颜色。
		/// </summary>
		bool IsTextBox { get; }

		/// <summary>
		/// 调节控点的集合，该集合长度固定，每种不同预设具有不同的控点数量。
		/// </summary>
		IAdjustments Adjustments { get; }

		/// <summary>
		/// 图形的预设类型，如 <see cref="ShapePresetType.Rectangle"/> 表示矩形。不同的预设类型应该呈现结果不同，不同的预设类型具有不同的调节控点细节数据 <see cref="Adjustments"/> 详情请见 <seealso cref="Adjustments"/>。
		/// </summary>
		ShapePresetType PresetType { get; }

		/// <summary>
		/// 线框相关属性，具有图形线框外观的描述。如果您在寻找尺寸等相关设置，另请参阅 <seealso cref="Transform"/> 形变属性。
		/// </summary>
		ILineFormat Line { get; }

		/// <summary>
		/// 填充相关属性，具有图形填充外观的描述。如果您在寻找尺寸等相关设置，另请参阅 <seealso cref="Transform"/> 形变属性。
		/// </summary>
		IFillFormat Fill { get; }

		/// <summary>
		/// 图形编号。该编号唯一标识一个图形。
		/// </summary>
		int Id { get; }

		/// <summary>
		/// 图形对象在选择窗格中的名称。
		/// </summary>
		string Name { get; set; }

		/// <summary>
		/// 图形对象在图形集合中的顺序，下标从0开始，下标越小，说明越底层。
		/// </summary>
		int ZOrderPosition { get; }

		/// <summary>
		/// 图形的类型，如 <see cref="ShapeType.Group"/> 组合图形，<see cref="ShapeType.Common"/> 基本矢量图形等。
		/// </summary>
		ShapeType Type { get; }
		
		/// <summary>
		/// 阴影效果，在其中可以描述阴影的具体细节。
		/// </summary>
		Shadow Shadow { get; }
		

		/// <summary>
		/// 调整图形对象的顺序。
		/// </summary>
		void ZOrder(ZOrderCommandEnum zordercmd);

		/// <summary>
		/// 图形的锁定设置。
		/// </summary>
		ILocks Locks { get; }

		/// <summary>
		/// 形变属性，具有对象的位置坐标、锚点比例、旋转角度等描述。
		/// </summary>
		RectTransform Transform { get; }

		/// <summary>
		/// 文本段落的具体样式与内容。
		/// </summary>
		ITextBody TextBody { get; }

		/// <summary>
		/// 只读文本。
		/// </summary>
		string ReadOnlyText { get; }

		/// <summary>
		/// 可见性。
		/// </summary>
		bool Visibility { get; set; }

	}
}
