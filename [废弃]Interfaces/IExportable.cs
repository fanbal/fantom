﻿using DocumentFormat.OpenXml;

namespace Fantom.Interface
{
	/// <summary>
	/// 用于实现Xml节点导出的接口
	/// </summary>
	public interface IExportable
	{
		/// <summary>
		/// 由对象导出对应的xml节点。
		/// </summary>
		/// <returns>导出节点。</returns>
		OpenXmlCompositeElement Export();
	}
}
