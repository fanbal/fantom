﻿using System;
using System.Collections.Generic;
using System.Text;
using Fantom.Drawing;

namespace Fantom
{
	/// <summary>
	/// 颜色填充格式。
	/// </summary>
	public interface IFillFormat
	{
		/// <summary>
		/// 填充的颜色，具体类型可以是 <see cref="SolidColor"/>, <see cref="SchemeColor"/>。
		/// </summary>
		BaseColor Color { get; set; }

	}
}
