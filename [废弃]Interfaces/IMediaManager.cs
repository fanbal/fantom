﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 媒体资源文件管理器。
	/// </summary>
	public interface IMediaManager
	{
		/// <summary>
		/// 文件本地媒体字典。key值为图片源的uri。
		/// </summary>
		IReadOnlyDictionary<Uri, IMedia> LocalMediaDic { get; }

		/// <summary>
		/// 创建并分配媒体。
		/// </summary>
		/// <param name="uri"></param>
		/// <returns></returns>
		IMedia CreateMedia(Uri uri);

	}
}
