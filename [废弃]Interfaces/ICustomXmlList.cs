﻿namespace Fantom
{

	/// <summary>
	/// 自定义Xml数据集合。
	/// </summary>
	public interface ICustomXmlList : IObserveCollection<ICustomXml>, IFantomObject
	{

	}
}
