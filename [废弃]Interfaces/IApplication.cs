﻿namespace Fantom
{
	/// <summary>
	/// 整个 Fantom 的入口接口，是整个 SDK 的顶层对象。在每个 Fantom 类体调用结构树中，每个结点均可指向根节点 Application 。
	/// </summary>
	public interface IApplication : IFantomObject
	{
		/// <summary>
		/// Fantom 的版本信息。
		/// </summary>
		string Version { get; }

		/// <summary>
		/// Fantom 的细节信息。
		/// </summary>
		string Detail { get; }

		/// <summary>
		/// 幻灯片集合。
		/// </summary>
		IPresentations Presentations { get; }

	}
}
