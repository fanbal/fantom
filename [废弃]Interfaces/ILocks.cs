﻿namespace Fantom
{
	/// <summary>
	/// 图形对象的锁定设置。
	/// </summary>
	public interface ILocks
	{
		#region Methods

		/// <summary>
		/// 所有锁定项完全锁定。
		/// </summary>
		void LockAll();

		/// <summary>
		/// 所有锁定项完全解锁。
		/// </summary>
		void UnlockAll();


		#endregion

		#region 基本设置。

		/// <summary>
		/// 无法为图形设置绑组。
		/// </summary>
		bool NoGrouping { get; set; }

		/// <summary>
		/// 无法被选中。
		/// </summary>
		bool NoSelection { get; set; }

		/// <summary>
		/// 无法旋转。
		/// </summary>
		bool NoRotation { get; set; }

		/// <summary>
		/// 无法修改纵横比。
		/// </summary>
		bool NoChangeAspect { get; set; }

		/// <summary>
		/// 无法移动。
		/// </summary>
		bool NoMove { get; set; }

		/// <summary>
		/// 无法调整大小。
		/// </summary>
		bool NoResize { get; set; }

		#endregion

		#region 独立矢量图形专属设置。

		/// <summary>
		/// 面向连接符与矢量图形。无法编辑顶点。
		/// </summary>
		bool NoEditPoints { get; set; }

		/// <summary>
		/// 面向连接符与矢量图形。无法编辑控点。
		/// </summary>
		bool NoAdjustHandles { get; set; }

		/// <summary>
		/// 面向连接符与矢量图形。无法改变箭头。
		/// </summary>
		bool NoChangeArrowheads { get; set; }

		/// <summary>
		/// 面向连接符与矢量图形。无法编辑图形样式。
		/// </summary>
		bool NoChangeShapeType { get; set; }

		#endregion

		#region 常规矢量图形专属设置

		/// <summary>
		/// 仅限常规矢量图形。无法编辑内文本。
		/// </summary>
		bool NoTextEdit { get; set; }

		#endregion

		#region 图形组合

		/// <summary>
		/// 仅限组合图形。无法解除组合。
		/// </summary>
		bool NoUnGrouping { get; set; }

		#endregion

		#region 图片图形

		/// <summary>
		/// 仅限图片图形。无法裁剪。
		/// </summary>
		bool NoCorp { get; set; }

		#endregion

	}
}
