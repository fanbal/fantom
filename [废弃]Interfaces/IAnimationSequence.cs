﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom
{
	/// <summary>
	/// 动画序列，构成 <see cref="ITimeLine"/> 时间轴对象，一个<see cref="ITimeLine"/> 时间轴对象必须具有一条主动画序列，用于描述页面图形的动画行为。
	/// </summary>
	public interface IAnimationSequence : IFantomObject, IList<IAnimationFragment>
	{
		/// <summary>
		/// 动画序列的名称。
		/// </summary>
		string Name{ get; set; }
	}
}
