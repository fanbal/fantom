﻿namespace Fantom
{
	/// <summary>
	/// Fantom 的基接口。所有在Fantom对象逻辑树中的类节点必须具有该接口。
	/// </summary>
	public interface IFantomObject
	{
		/// <summary>
		/// Fantom 的顶层对象，该实例在 Fantom 中唯一存在。
		/// 所有 <see cref="IFantomObject"/> 对象具有统一的 <see cref="IApplication"/> 根对象，
		/// 您在编写底层对象如 <see cref="IShape"/> 时，可以通过访问根节点获得整个解析环境的全貌，如获得当前演示文稿的页数等。
		/// </summary>
		IApplication Application { get; }

		/// <summary>
		/// 当前对象的父级对象，请通过使用 as 语句或强制类型转换将其变成正确的类型。
		/// 如对于幻灯片对象 sld，访问其父级 <see cref="ISlides"/> 幻灯片集合对象的正确方式为 sld.Parent as ISlides。
		/// </summary>
		IFantomObject Parent { get; }

	}
}
