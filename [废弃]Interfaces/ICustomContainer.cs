﻿namespace Fantom
{
	/// <summary>
	/// 自定义内容容器。在PPT中，一些内容具有自定义数据，比如 <see cref="IShape"/> 图形对象。这些类可以具有此接口。
	/// </summary>
	public interface ICustomContainer : IFantomObject
	{
		/// <summary>
		/// 自定义文档Xml内容。
		/// </summary>
		ICustomXmlList CustomXmlList { get; }

		/// <summary>
		/// 自定义文档Xml标签。
		/// </summary>
		Tags Tags { get; }
	}
}
