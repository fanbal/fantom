﻿using System.ComponentModel;
using System.Collections.Generic;

namespace Fantom
{
	/// <summary>
	/// 演示文稿的主题位于此处，所有的幻灯片均需从幻灯片母版出发进行构造。
	/// </summary>
	public interface ISlideMaster : IFantomObject, ISelection
	{

		/// <summary>
		/// 母版的布局子样式。
		/// </summary>
		IList<ISlideLayout> Layouts { get; }

		/// <summary>
		/// 主题颜色。
		/// </summary>
		ITheme Theme { get; }

		/// <summary>
		/// 母版的主题名称。
		/// </summary>
		string Name { get; }

	}
}
