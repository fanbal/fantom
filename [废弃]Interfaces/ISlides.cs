﻿using System.Collections.Generic;

namespace Fantom
{
	public interface ISlides : IObserveCollection<ISlide>, IFantomObject, ISelection
	{
		/// <summary>
		/// 向演示文稿最后新建幻灯片。
		/// </summary>
		/// <returns><see cref="ISlide"/> 幻灯片 对象。</returns>
		ISlide New();

		/// <summary>
		/// 获得选中的幻灯片集合。
		/// </summary>
		/// <returns><see cref="IEnumerable{ISlide}"/> 幻灯片集合 对象。</returns>
		IEnumerable<ISlide> SelectedSlides { get; }

		/// <summary>
		/// 图形集合对象。
		/// </summary>
		IShapes Shapes { get; }

	}
}
