# <b>此为当前开发分支。</b>
# Fantom

## 介绍
【这是一个有生之年缓慢推进的项目，我们欢迎所有人参与项目的建设】

一个对 PML 的封装项目，在未来将是一个支持大部分 PPT 的浏览显示编辑功能的集成演示设计环境，实装 LOVE2D 游戏引擎，实装 Rosyln 编译器与支持高亮的富文本编辑器，实装像素的编辑模块，动画模块等功能……有生之年能够实现。

## 原理
这是一个基于 .NET 和 OpenXml 对 PowerPoint 的 PML 文档的非 COM 封装。
PPT 的 COM 对象使用起来极为限制，与其忍受 Office 的限制，倒不如选择自己通过 OpenXml 制作一个更加强大的 PPT 解析库。


## 目标

### PPT 基础放映（基础模块）

1. 完成 PML 的读取，包括演示文稿、幻灯片、图形、文本、动画、备注、母版以及 Tag 和自定义内容对象的简单读取。
2. 精细建模。为对象加入足够的事件，完善 MVVM 消息通讯模型。
3. 建立不会被 PPT 覆盖掉的自定义格式（保存在自定义节点中）进行保存。
4. 为图形对象实装更丰富的滤镜预设。

### PPT 动画放映（FANBOX 模块）
1. 加入能够实时预览动画的时间轴系统。 
2. 加入高性能动画的呈现。
3. 支持兼容 PPT 的动画导出。

### 游戏引擎模块（LOVE2D 模块）
1. 加入游戏引擎 LOVE2D，允许 Fantom 能够导出含可执行文件的 Windows 桌面端游戏项目。不考虑跨平台。
2. PPT 对象刚体化与物理化。

### 开发环境模块（Fantom PowerV 框架）
1. 借助 Roslyn 编译器为 Fantom 实现编译类库的功能，用于为 LOVE2D 模块服务。
2. 采用 MEF 实现类似 Office VBA 的功能。

### 自定义图形界面
1. 高度的定制界面，希望能够呈现 Unity3D，Office 与 Adobe 产品三种不同软件的 UI。并且支持用户在三种模式间进行切换。
 
## 暂未实现的内容
动画的细节描述。如动画的触发方式、动画的默认预设。
多边形。暂不支持多边形的读取。
滤镜与效果，如阴影，投影等。
备注页与母版的解析。

## 正在实装的内容
母版与版式的解析，这是 PPT 保存功能实现的基础。

## Fantom.Partner 进度
https://gitee.com/fanbal/fantom/issues/I48C59
![输入图片说明](https://images.gitee.com/uploads/images/2021/0831/181843_39484fee_7372773.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0831/181826_98e4772c_7372773.png "屏幕截图.png")

## Fantom.Core 进度
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/095529_ef184e8e_7372773.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0724/230549_8b87a7a3_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0724/200829_93709dd3_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/185720_f5bef0e7_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/192133_fcf0631c_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/110848_1c260bef_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0718/154100_b8f2b76b_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/105809_b2259e86_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/001804_4bd35cad_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0716/184237_b4decbff_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/230324_3f0d77e6_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/125722_3550516b_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0714/225826_a1b19ca7_7372773.png "屏幕截图.png")

![图片的解析](https://images.gitee.com/uploads/images/2021/0711/233650_580bd092_7372773.png "屏幕截图.png")

![图片与选框能够正常运作](https://images.gitee.com/uploads/images/2021/0711/131411_d7e47afa_7372773.png "屏幕截图.png")

![图片解析](https://images.gitee.com/uploads/images/2021/0710/081623_958f918d_7372773.png "解析图片.png")

![加入了颜色解析](https://images.gitee.com/uploads/images/2021/0704/092611_10b36eb9_7372773.png "屏幕截图.png")

![加入了选择窗格](https://images.gitee.com/uploads/images/2021/0627/091525_8d340509_7372773.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0626/191755_da8a9274_7372773.png "屏幕截图.png")


