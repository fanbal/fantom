﻿using System;
using ft = Fantom;

class Program
{
	static void Main(string[] args)
	{
		// 文档路径任意。
		var path = @"D:\Code\Fantom\FantomUI\OpenXml测试.pptx";
		var app = ft.Application.Create(); // 创建fantom的顶层对象。
		var pres = app.Presentations.Load(path); // 载入演示文档。

		// 遍历所有幻灯片。
		foreach (var sld in pres.Slides)
		{
			// 遍历所有图形对象。
			foreach (var shp in sld.Shapes)
			{
				Console.WriteLine(shp.Name);
			}
		}

	}
}
