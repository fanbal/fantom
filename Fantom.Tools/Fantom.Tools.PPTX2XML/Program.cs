﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace Fantom.Tools.PPTX2XML
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args.Length != 1)
				throw new Exception("输入参数有误");
			var file = args[0];

			try
			{
				ExportDir(file);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}
			

		}


		static void ExportDir(string filepath)
		{
			var path = Path.Combine(GetParent(filepath), GetName(filepath));
			if (Directory.Exists(path))
				Directory.Delete(path);
			ZipFile.ExtractToDirectory(filepath, path);
		}

		static string GetParent(string file)
		{
			int i = file.Length - 1;
			while (file[i] != '\\' && file[i] != '/' && i >= 0)
			{
				i--;
			}

			return file.Substring(0, i);
		}

		static string GetName(string file)
		{
			int i = file.Length - 1;
			int j = 0;
			while (file[i] != '\\' && file[i] != '/' && i >= 0)
			{
				if (file[i] == '.')
					j = i;
				i--;
			}
			return file.Substring(i + 1, j - i - 1);
		}


	}
}
