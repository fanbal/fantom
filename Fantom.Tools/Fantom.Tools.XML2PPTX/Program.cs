﻿using System;
using System.IO;
using System.IO.Compression;

namespace Fantom.Tools.XML2PPTX
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				if (args.Length != 1) return;
				var dirpath = args[0];
				dirpath = GetRoot(dirpath);
				Compression(dirpath);
				Console.WriteLine("导出解析完成。");
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		/// <summary>
		/// 压缩文件。
		/// </summary>
		static void Compression(string dirpath)
		{
			var parent = GetParentDir(dirpath);
			var export = Path.Combine(parent, GetDirName(dirpath) + ".pptx");
			if (File.Exists(export))
				File.Delete(export);

			ZipFile.CreateFromDirectory(dirpath, export);
		}

		/// <summary>
		/// 获得根目录位置。
		/// </summary>
		/// <param name="dirpath"></param>
		/// <returns></returns>
		static string GetRoot(string dirpath)
		{
			var dir = new DirectoryInfo(dirpath);
			int i = 0, count = 6;
			bool isfind = false;
			for (i = 0; i < count; i++)
			{
				if (dir == null)
				{
					break;
				}
				else if (Array.Exists(dir.GetFiles(), (file) => file.Name.EndsWith("[Content_Types].xml")))
				{
					isfind = true; break;
				}
				else
				{
					dir = Directory.GetParent(dirpath);
				}

			}

			if (isfind)
				return dir.FullName;
			else
				throw new Exception("未找到 PPT 源文件根目录，请确保您的解压包完整。");

		}

		//static bool IsExist(DirectoryInfo dir, string filename)
		//{
		//	foreach (var item in dir.GetFiles())
		//	{

		//	}
		//}

		/// <summary>
		/// 获得父级的目录。
		/// </summary>
		/// <param name="dirpath"></param>
		/// <returns></returns>
		static string GetParentDir(string dirpath)
		{
			int i = 0;
			for (i = dirpath.Length - 1; i >= 0; i--)
			{
				if (dirpath[i] == '\\' || dirpath[i] == '/')
					break;
			}
			return dirpath.Substring(0, i);
		}

		/// <summary>
		/// 获得文件夹名称。
		/// </summary>
		/// <param name="dirpath"></param>
		/// <returns></returns>
		static string GetDirName(string dirpath)
		{
			int i = 0;
			for (i = dirpath.Length - 1; i >= 0; i--)
			{
				if (dirpath[i] == '\\' || dirpath[i] == '/')
					break;
			}
			return dirpath.Substring(i + 1);
		}

	}
}
