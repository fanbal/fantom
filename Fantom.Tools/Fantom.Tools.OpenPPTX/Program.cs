﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Diagnostics;

namespace Fantom.Tools.OpenPPTX
{
	class Program
	{
		const string SAV_NAME = "entry.sav";
		static string savpath = Path.Combine(Environment.CurrentDirectory, SAV_NAME);
		static void Main(string[] args)
		{
			try
			{
				if (args.Length != 1)
					throw new Exception("输入参数有误");
				var file = args[0];
				// var path = GetPPTLocation();

				Process.Start(string.Format(@"{0}", file));

				Console.WriteLine("PPT打开成功");

			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			// Console.ReadKey();
		}

		/// <summary>
		/// 获得PPT的文件位置。
		/// </summary>
		/// <returns></returns>
		static string GetPPTLocation()
		{
			string path;
			if (!File.Exists(savpath))
			{
				path = FindPPTAppLocation();
				SavePPTLocation(path);
				if (path == null || path.Length < 3)
					throw new Exception("找不到PPT文件，请自行创建 entry.sav 文件并添加定位");
			}
			else
			{
				var fs = new FileStream(savpath, FileMode.Open);
				var bytes = new byte[1024];
				fs.Read(bytes, 0, bytes.Length);
				fs.Close();
				fs.Dispose();
				path = Encoding.Default.GetString(bytes).TrimEnd();
			}

			return path;
		}

		/// <summary>
		/// 保存PPT的文件位置。
		/// </summary>
		/// <param name="path"></param>
		static void SavePPTLocation(string path)
		{
			var fs = new FileStream(savpath, FileMode.Create);
			var bytes = Encoding.Default.GetBytes(path);
			fs.Write(bytes, 0, bytes.Length);
			fs.Close();
			fs.Dispose();
		}


		/// <summary>
		/// 暴力遍历的方法获得POWERPNT.exe的位置。
		/// </summary>
		/// <returns></returns>
		static string FindPPTAppLocation()
		{

			var viewarr = new RegistryView[]
			{
				RegistryView.Registry32,
				RegistryView.Registry64
			};


			var keyarr = new string[]
			{
				@"SOFTWARE\Microsoft\Office\16.0\PowerPoint\InstallRoot",
				@"SOFTWARE\Microsoft\Office\15.0\PowerPoint\InstallRoot",
				@"SOFTWARE\Microsoft\Office\14.0\PowerPoint\InstallRoot",
			};

			// 遍历获得 PPT 程序的位置。
			foreach (var view in viewarr)
			{
				var localMachine = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, view);

				foreach (var key in keyarr)
				{
					var reg = localMachine.OpenSubKey(key);
					if (reg != null && reg.GetValue("Path") != null)
						return reg.GetValue("Path").ToString();
				}

			}

			return null;
		}

	}
}
