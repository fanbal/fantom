﻿using System.ComponentModel;
using Fantom.Partner.Mobile.ViewModels;
using Xamarin.Forms;

namespace Fantom.Partner.Mobile.Views
{
	public partial class ItemDetailPage : ContentPage
	{
		public ItemDetailPage()
		{
			InitializeComponent();
			BindingContext = new ItemDetailViewModel();
		}
	}
}