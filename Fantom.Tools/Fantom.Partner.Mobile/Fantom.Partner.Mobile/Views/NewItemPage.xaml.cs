﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Fantom.Partner.Mobile.Models;
using Fantom.Partner.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fantom.Partner.Mobile.Views
{
	public partial class NewItemPage : ContentPage
	{
		public Item Item { get; set; }

		public NewItemPage()
		{
			InitializeComponent();
			BindingContext = new NewItemViewModel();
		}
	}
}