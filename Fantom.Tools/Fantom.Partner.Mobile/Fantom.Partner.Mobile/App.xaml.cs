﻿using System;
using Fantom.Partner.Mobile.Services;
using Fantom.Partner.Mobile.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fantom.Partner.Mobile
{
	public partial class App : Application
	{

		public App()
		{
			InitializeComponent();

			DependencyService.Register<MockDataStore>();
			MainPage = new AppShell();
		}

		protected override void OnStart()
		{
		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
		}
	}
}
