﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using Fantom.Partner.Models;
using Fantom.Partner.Xml;

namespace Fantom.Partner
{

	/// <summary>
	/// TestWindow.xaml 的交互逻辑
	/// </summary>
	public partial class TestWindow : Window
	{
		XmlDocument _xmlDocument;
		string xmltext = Properties.Resources.TestXml;
		public TestWindow()
		{
			InitializeComponent();
			_xmlDocument = new XmlDocument();
			// (_xmlDocument as XmlDocument).LoadXml(xmltext);
			//D:\Code\fantom\Fantom.Tools\Fantom.Partner\MainWindow.xaml

			 _xmlDocument.Load(@"C:\Users\fanbal\Desktop\OpenXml测试_WorkArea0\xmlproject\ppt\slides\slide1.xml");
			//_xmlDocument.Load(@"C:\Users\dell\Desktop\OurPartner_WorkArea0\xmlproject\ppt\slides\slide1.xml");

			treeView.LoadXmlDocument(_xmlDocument);
		}


	}
}
