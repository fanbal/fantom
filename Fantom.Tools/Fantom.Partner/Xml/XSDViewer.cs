﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Resolvers;
using System.Xml.Schema;
using Fantom.Partner.Controls;

namespace Fantom.Partner.Xml
{
	public static class XSDViewer
	{
		// XSD 的相对路径。
		public static string XSD_PATH = Path.Combine(Environment.CurrentDirectory, "XSD");


		/// <summary>
		/// 语法集合。
		/// </summary>
		public static XmlSchemaSet SchemaSet { get; }
		static XSDViewer()
		{
			SchemaSet = new XmlSchemaSet();

			var xsds = XSD_PATH;

			// 规定 XSD 的相对路径，避免自动定位失效。
			var xmlresolver = new XmlPreloadedResolver();
			xmlresolver.ResolveUri(new Uri(xsds), null); ;
			SchemaSet.XmlResolver = xmlresolver;

			// 遍历。
			foreach (var file in from f in Directory.GetFiles(xsds) where Path.GetExtension(f) == ".xsd" select f)
			{
				using (var xmlreader = new FileStream(file, FileMode.Open))
				{
					var xmlschema = XmlSchema.Read(xmlreader, null);
					SchemaSet.Add(xmlschema);
				}
			}

			// 编译。
			SchemaSet.Compile();
		}

		/// <summary>
		/// 获得 XmlSchemaType。
		/// </summary>
		public static XmlSchemaType GetXmlSchemaType(XmlNode node)
		{
			var nodeTrace = new Stack<XmlNode>();
			XmlSchemaType schemaType = null;
			var parent = node;
			while (true)
			{
				if (node == null)
					break;
				nodeTrace.Push(node);
				node = node.ParentNode;
				if (node is XmlDocument)
					break;
			}
			while (nodeTrace.Count != 0)
			{
				var xmlnode = nodeTrace.Pop();
				XmlSchemaElement elem = null;
				if (schemaType == null)
				{
					var qname = new XmlQualifiedName(xmlnode.LocalName, xmlnode.NamespaceURI);
					elem = XSDViewer.SchemaSet.GlobalElements[qname] as XmlSchemaElement;
				}
				else
				{
					if (schemaType is XmlSchemaComplexType)
					{
						var ctype = schemaType as XmlSchemaComplexType;
						var par = ctype.Particle;
						var outpar = GetSchemaParticle(xmlnode.LocalName, par);

						elem = outpar != null ? outpar as XmlSchemaElement : null;
					}
				}
				if (elem == null)
					return null;
				schemaType = elem.ElementSchemaType;
			}
			return schemaType;
		}

		/// <summary>
		/// 获得子成员。
		/// </summary>
		/// <param name="targetName">目标类型名。</param>
		/// <param name="par">XSD 节点。</param>
		/// <returns></returns>
		public static XmlSchemaParticle GetSchemaParticle(string targetName, XmlSchemaParticle par)
		{
			if (par is XmlSchemaSequence)
			{
				var seq = par as XmlSchemaSequence;

				foreach (XmlSchemaParticle item in seq.Items)
				{
					if (item is XmlSchemaElement && (item as XmlSchemaElement).Name == targetName)
						return item;
					else if (item is XmlSchemaChoice || item is XmlSchemaSequence || item is XmlSchemaGroupRef)
					{
						var outitem = GetSchemaParticle(targetName, item);
						if (outitem != null) return outitem;
					}
				}
				return null;
			}
			else if (par is XmlSchemaChoice)
			{
				var chos = par as XmlSchemaChoice;

				foreach (XmlSchemaParticle item in chos.Items)
				{
					if (item is XmlSchemaElement && (item as XmlSchemaElement).Name == targetName)
						return item;
					else if (item is XmlSchemaChoice || item is XmlSchemaSequence || item is XmlSchemaGroupRef)
					{
						var outitem = GetSchemaParticle(targetName, item);
						if (outitem != null) return outitem;
					}
				}
				return null;
			}
			else if (par is XmlSchemaGroupRef)
			{
				var grp = par as XmlSchemaGroupRef;
				return GetSchemaParticle(targetName, grp.Particle);
			}
			return null;
		}

		/// <summary>
		/// 从节点中获得子成员。
		/// </summary>
		/// <param name="node">节点对象。</param>
		public static IEnumerable<XmlSchemaParticle> GetSchemaTypeMembers(XmlNode node)
		{
			return GetSchemaTypeMembers(GetXmlSchemaType(node));
		}

		public static IEnumerable<XmlSchemaParticle> TryGetElementInChoiceFromNode(XmlSchemaGroupBase par)
		{
			if (par == null)
				return new XmlSchemaParticle[0] { };
			var chos = par.Items.OfType<XmlSchemaChoice>().FirstOrDefault();
			if (chos == null)
				return new XmlSchemaParticle[0] { };

			return chos.Items.OfType<XmlSchemaElement>();
		}

		/// <summary>
		/// 通过类型获得语法的类型成员。
		/// </summary>
		public static IEnumerable<XmlSchemaParticle> GetSchemaTypeMembers(XmlSchemaType schemaType)
		{
			if (schemaType == null) return null;
			if (schemaType is XmlSchemaComplexType)
			{
				var ctype = schemaType as XmlSchemaComplexType;
				var par = ctype.Particle;

				if (par is XmlSchemaSequence)
				{
					var seq = par as XmlSchemaSequence;
					return seq.Items.OfType<XmlSchemaParticle>();
				}
				else if (par is XmlSchemaChoice)
				{
					var chos = par as XmlSchemaChoice;
					return chos.Items.OfType<XmlSchemaParticle>();
				}
				else if (par is XmlSchemaGroupRef)
				{
					var grp = par as XmlSchemaGroupRef;
					return null;
				}

			}
			return null;
		}

		/// <summary>
		/// 从 XSD 规范中查找符合条件的成员节点。
		/// </summary>
		/// <param name="node"></param>
		/// <param name="parsRef"></param>
		/// <returns></returns>
		public static IEnumerable<XmlSchemaElement> GetSchemaAvailableTypeMembers(XmlElement node, IEnumerable<XmlSchemaParticle> parsRef)
		{
			var hashset = new HashSet<string>(from cn in node.ChildNodes.OfType<XmlElement>() select cn.LocalName);

			return from cn in parsRef.OfType<XmlSchemaElement>() where !hashset.Contains(cn.Name) select cn;

		}


		

		public static XmlElement CreateNode(XmlSchemaElement element)
		{
			return null;
		}

	}
}
