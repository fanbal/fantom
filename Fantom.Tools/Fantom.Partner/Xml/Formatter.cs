﻿using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Fantom.Partner.Xml
{
	/// <summary>
	/// Xml 格式化解析。
	/// 来源：https://github.com/mikoskinen/NXMLFormatter/blob/master/NXMLFormatter/Formatter.cs
	/// 作者：mikoskinen，https://github.com/mikoskinen 。
	/// 但是这个东西非常局限，并没有语义分析，以后得自己做一个专门的模块，用于未来高亮的实现。
	/// </summary>
	public class Formatter
	{
		private static readonly XmlWriterSettings settings = new XmlWriterSettings()
		{
			OmitXmlDeclaration = true,
			Indent = true,
			NewLineOnAttributes = false,
			Encoding = Encoding.UTF8,
		};

		public static string Format(string source, XmlWriterSettings customSettings = null)
		{
			var stringBuilder = new StringBuilder();

			var element = XElement.Parse(source);

			using (var xmlWriter = XmlWriter.Create(stringBuilder, customSettings ?? settings))
			{
				element.Save(xmlWriter);
			}

			return stringBuilder.ToString();


		}

		public static void FormatInFile(string path, XmlWriterSettings customSettings = null)
		{
			var stringBuilder = new StringBuilder();

			var element = XElement.Load(path);

			using (var xmlWriter = XmlWriter.Create(stringBuilder, customSettings ?? settings))
			{
				element.Save(xmlWriter);
			}
		}
	}
}
