﻿using System;
using wpf = System.Windows;
using Fantom.Partner.Models;
using System.Windows.Threading;

namespace Fantom.Partner
{
	/// <summary>
	/// App.xaml 的交互逻辑
	/// </summary>
	public partial class App : wpf.Application
	{
		#region properties
		public static WorkArea MainWorkArea { get; set; }

		public static MainWindow EditWindow { get; set; }

		public static HomeWindow HomeWindow { get; set; }

		public static MiniWindow MiniWindow { get; set; }
		#endregion

		#region ctors
		public App()
		{
//#if DEBUG
//			return;
//#endif
			this.DispatcherUnhandledException += App_DispatcherUnhandledException;
		}
		#endregion

		#region events
		private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			e.Handled = true;
		}
		#endregion

		#region methods

		#endregion


		#region system
		// 来源 https://www.cnblogs.com/FengShenMeng/p/6054571.html
		private static DispatcherOperationCallback exitFrameCallback = new DispatcherOperationCallback(ExitFrame);
		public static void DoEvents()
		{
			DispatcherFrame nestedFrame = new DispatcherFrame();
			DispatcherOperation exitOperation = Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, exitFrameCallback, nestedFrame);
			Dispatcher.PushFrame(nestedFrame);
			if (exitOperation.Status !=
			DispatcherOperationStatus.Completed)
			{
				exitOperation.Abort();
			}
		}

		private static Object ExitFrame(Object state)
		{
			DispatcherFrame frame = state as
			DispatcherFrame;
			frame.Continue = false;
			return null;
		}


	}


	#endregion

}
