﻿using System;
using System.Windows;
using ft = Fantom.Partner.Controls;

namespace Fantom.Partner
{
	/// <summary>
	/// MiniWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MiniWindow : ft.FantomWindow
	{
		public MiniWindow()
		{
			InitializeComponent();
		}


		private void Button_OpenEditWindow(object sender, RoutedEventArgs e)
		{
			this.Hide();
			var mainwin = App.EditWindow;
			if (mainwin == null)
			{
				mainwin = new MainWindow();
				App.EditWindow = mainwin;
			}
			App.Current.MainWindow = mainwin;
			mainwin.Show();
		}

		private void Button_OpenVSCode(object sender, RoutedEventArgs e)
		{
			App.MainWorkArea.OpenInVisualStudioCode();
		}

		/// <summary>
		/// 打开文件按钮事件。
		/// </summary>
		private void Button_OpenFile(object sender, RoutedEventArgs e)
		{
			App.MainWorkArea.ExportPPTXFile();
			App.MainWorkArea.OpenPPTXFileInBrowser();
		}
	}
}
