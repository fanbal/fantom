﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ft = Fantom.Partner.Controls;

namespace Fantom.Partner
{
	public static class Global
	{

		/// <summary>
		/// 弹出错误提示。
		/// </summary>
		public static void ThrowExceptionMessage(string message, bool isError = true)
		{
			ft.MessageBox.Show(message);
			if (isError) throw new Exception(message);
		}
	}
}
