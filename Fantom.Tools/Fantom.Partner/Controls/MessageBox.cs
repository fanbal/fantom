﻿using System.Windows;
using System.Windows.Input;

namespace Fantom.Partner.Controls
{
	public class MessageBox : Window
	{
		#region properties

		/// <summary>
		/// 消息框的信息。
		/// </summary>
		public string Message { get; set; }


		#endregion properties

		#region ctors
		private MessageBox()
		{
			
		}

		#endregion

		#region events
		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			DragMove();
		}

		#endregion

		#region static
		static MessageBox()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(MessageBox), new FrameworkPropertyMetadata(typeof(MessageBox)));
		}

		/// <summary>
		/// 显示。
		/// </summary>
		public static void Show(string message, bool isDialog = true)
		{
			var width = SystemParameters.WorkArea.Width;

			var height = SystemParameters.WorkArea.Height;

			var msgbox = new MessageBox() { Message = message };
			msgbox.DataContext = msgbox;

			msgbox.Left = width / 2 - 300 / 2;
			msgbox.Top = height / 2 - 150 / 2;

			if (isDialog)
				msgbox.ShowDialog();
			else
				msgbox.Show();

			

			//textBlock.Text = message;
		}

		#endregion


	}
}
