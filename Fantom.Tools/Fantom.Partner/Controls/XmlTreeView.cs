﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Xml;
using System.Xml.Schema;
using Fantom.Partner.Xml;

namespace Fantom.Partner.Controls
{
	/// <summary>
	/// Xml 树视图控件。
	/// </summary>
	public class XmlTreeView : ItemsControl
	{
		#region events
		/// <summary>
		/// 可视化树节点的鼠标移入时触发该事件。
		/// </summary>
		public event RoutedEventHandler ItemEnter = null;

		/// <summary>
		/// 可视化树节点的鼠标移出时触发该事件。
		/// </summary>
		public event RoutedEventHandler ItemLeave = null;

		/// <summary>
		/// 可视化树节点的鼠标点击时触发该事件。
		/// </summary>
		public event RoutedEventHandler ItemClick = null;

		#endregion

		#region properties

		/// <summary>
		/// 树容器当前所描述的 Xml 文档对象。
		/// </summary>
		public XmlDocument XmlDocument { get; set; }

		/// <summary>
		/// 最近被选中的图形。
		/// </summary>
		public XmlElement CheckedElement { get; private set; } = null;

		private XmlTreeView CheckedView;
		private ToggleButton CheckedToggleButton;


		#endregion

		#region ctors
		public XmlTreeView()
		{
			// 注册节点的鼠标移过事件。
			this.AddHandler(XmlTreeViewItem.ItemMouseEnterEvent, new RoutedEventHandler(OnItemEnter));

			// 注册节点的鼠标移出事件，
			this.AddHandler(XmlTreeViewItem.ItemMouseLeaveEvent, new RoutedEventHandler(OnItemLeave));

			// 注册节点的鼠标点击事件，
			this.AddHandler(XmlTreeViewItem.ItemClickedEvent, new RoutedEventHandler(OnItemClick));

			ItemClick += XmlTreeView_ItemClickDefault; // 注册默认事件。
		}


		#endregion

		#region methods

		/// <summary>
		/// 进入编辑器选项时时触发该事件。
		/// </summary>
		protected virtual void OnItemEnter(object sender, RoutedEventArgs e)
		{
			ItemEnter?.Invoke(sender, e);
		}

		/// <summary>
		/// 进入编辑器选项时时触发该事件。
		/// </summary>
		protected virtual void OnItemLeave(object sender, RoutedEventArgs e)
		{
			ItemLeave?.Invoke(sender, e);
		}

		/// <summary>
		/// 选中子项时触发该事件。
		/// </summary>
		protected virtual void OnItemClick(object sender, RoutedEventArgs e)
		{
			ItemClick?.Invoke(sender, e);
		}

		/// <summary>
		/// 鼠标点击树节点的默认事件。
		/// </summary>
		private void XmlTreeView_ItemClickDefault(object sender, RoutedEventArgs e)
		{
			// 选中子项时能够保存该项，用于快速复原对象信息。
			var os = e.OriginalSource as FrameworkElement;
			CheckedElement = os.DataContext as XmlElement;
			var grid = os.Parent as Grid;
			CheckedView = grid.FindName("xmlTreeView") as XmlTreeView;
			CheckedToggleButton = os as ToggleButton;
			// Controls.NoticeBar.Show(CheckedView.ItemsSource);
		}

		/// <summary>
		/// 通过检索树的节点查询到具有限制信息的节点信息。用于可视化编辑 Xml 的语法规范检查。
		/// </summary>
		/// <param name="item">树节点。</param>
		/// <param name="min">最小可能的值。</param>
		/// <param name="max">最大可能的值。</param>
		/// <param name="isNotLimit">是否不限制上限。</param>
		public static void GetSchemaRestrictNode(FrameworkElement item, out int min, out int max)
		{
			min = 1;
			max = 1;

			var xmlelement = item.DataContext as System.Xml.Schema.XmlSchemaElement;
			if (xmlelement == null) return;

			// 对于 Choice 内部项目需要参考其父节点，也就是 Choice 节点。
			if (xmlelement.Parent is XmlSchemaChoice)
			{
				var cho = xmlelement.Parent as System.Xml.Schema.XmlSchemaChoice;

				min = (int)cho.MinOccurs;
				max = cho.MaxOccursString == "unbounded" ? int.MaxValue : (int)cho.MaxOccurs;
			}

			// 对于一般节点，直接参考自身。
			else
			{
				min = (int)xmlelement.MinOccurs;
				max = xmlelement.MaxOccursString == "unbounded" ? int.MaxValue : (int)xmlelement.MaxOccurs;
			}
		}

		public void AddNodeFromXSDElement(XmlSchemaElement xsdElement)
		{
			// 前缀。
			var pfix = this.CheckedElement.GetPrefixOfNamespace(xsdElement.QualifiedName.Namespace);
			var nspace = xsdElement.QualifiedName.Namespace;
			var name = nspace == string.Empty ? xsdElement.Name : pfix + ":" + xsdElement.Name;
			var node = XmlDocument.CreateElement(name);
			CheckedElement.AppendChild(node);
			// Controls.NoticeBar.Show("{0}, {1}", name, nspace);

			//this.ItemsSource = XmlDocument;// XmlDocument;
			CheckedView.ItemsSource = CheckedElement.ChildNodes;
			// CheckedToggleButton.SetValue(ToggleButton.IsCheckedProperty, true);
			// be.UpdateSource();

		}

		/// <summary>
		/// 导入 Xml 文档。
		/// </summary>
		public void LoadXmlDocument(XmlDocument xmldoc)
		{
			xmldoc.Schemas = XSDViewer.SchemaSet;
			XmlDocument = xmldoc;
			ItemsSource = xmldoc;
		}

		/// <summary>
		/// 导入 Xml 文档。
		/// </summary>
		public void LoadXmlDocument(string xmlpath)
		{
			XmlDocument xmldoc = new XmlDocument();
			xmldoc.Load(xmlpath);
			LoadXmlDocument(xmldoc);
		}



		#endregion

	}
}
