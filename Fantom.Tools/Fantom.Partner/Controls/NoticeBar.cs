﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media.Animation;

namespace Fantom.Partner.Controls
{

	/// <summary>
	/// 消息弹出信息条。
	/// </summary>
	public class NoticeBar : Window
	{
		#region properties
		public static double NoticeBarHeight { get; } = 100;

		/// <summary>
		/// 可视化位置，用于在侧边中呈现位置。
		/// </summary>
		private int VisualPositionIndex = 0;

		public static int debugnum = 0;

		/// <summary>
		/// 框体的延迟。
		/// </summary>
		public TimeSpan DisplayDuration { get; set; } = TimeSpan.FromMilliseconds(4000);

		#endregion
		#region ctors
		public NoticeBar()
		{
			Style = (Style)Application.Current.FindResource("Fantom.Controls.Themes.NoticeBar.NormalStyle");
		}
		#endregion

		#region static
		static NoticeBar()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(NoticeBar), new FrameworkPropertyMetadata(typeof(NoticeBar)));
		}

		/// <summary>
		/// 显示侧边栏弹出提示框。
		/// </summary>
		/// <param name="message">可在消息框中显示的文本消息。</param>
		public static void Show(string message)
		{
			// 为了解决多个消息框在短时间内会叠在一起的问题
			// 消息框需要在弹出前计算可显示的位置。
			// 以下代码用于计算消息框可显示的位置。
			var avaindex = GetAvailableVisualPositionIndex();
			var maxindex = (int)(SystemParameters.FullPrimaryScreenHeight / NoticeBarHeight);
			if (avaindex >= maxindex) return;

			var top = avaindex * NoticeBarHeight;

			var noticebar = new NoticeBar();
			noticebar.ShowInTaskbar = false;
			noticebar.Title = message;
			noticebar.Left = 0;
			noticebar.Top = top;
			noticebar.VisualPositionIndex = avaindex;

			Application.Current.Dispatcher.Invoke(() => { noticebar.Show(); });

		}

		public static void Show(object message)
		{
			Show( message.ToString());
		}

		public static void Show(string format, params object[] paraArr)
		{
			Show(string.Format(format, paraArr));
		}

		/// <summary>
		/// 获得上一个消息条。
		/// </summary>
		private static int GetAvailableVisualPositionIndex()
		{
			int lastIndex = -1;
			foreach (NoticeBar win in from NoticeBar nwin in Application.Current.Windows.OfType<NoticeBar>() orderby nwin.VisualPositionIndex select nwin)
			{
				lastIndex++;
				var index = win.VisualPositionIndex;
				if (index != lastIndex)
					return lastIndex;
			}
			return lastIndex + 1;
		}


		#endregion


		#region events
		/// <summary>
		/// 初始化时配置其动画。
		/// </summary>
		protected override void OnInitialized(EventArgs e)
		{
			var anim = new DoubleAnimationUsingKeyFrames();
			anim.Duration = new Duration(DisplayDuration);
			anim.KeyFrames.Add(new LinearDoubleKeyFrame(1, KeyTime.FromPercent(.8)));
			anim.KeyFrames.Add(new LinearDoubleKeyFrame(0, KeyTime.FromPercent(1)));
			anim.Completed += (s, args) => { this.Close(); };
			this.BeginAnimation(OpacityProperty, anim);
		}

		#endregion


	}





}
