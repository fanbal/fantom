﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace Fantom.Partner.Controls
{
	/// <summary>
	/// 本质 CheckBox，就是 Office 自动保存的那种开关切换控件。
	/// </summary>
	public class SwitchButton : CheckBox
	{
		#region ctors
		public SwitchButton() { }

		#endregion

		#region static
		static SwitchButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(SwitchButton), new FrameworkPropertyMetadata(typeof(SwitchButton)));
		}
		#endregion
	}
}
