﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Fantom.Partner.Controls
{
	/// <summary>
	/// 主窗体对象。
	/// </summary>
	public class FantomWindow : Window
	{

		#region depencency properties

		/// <summary>
		/// 标题栏的内容属性。
		/// </summary>
		public ControlTemplate TitleContent
		{
			get { return (ControlTemplate)GetValue(TitleContentProperty); }
			set { SetValue(TitleContentProperty, value); }
		}

		// 标题栏的内容属性。
		public static readonly DependencyProperty TitleContentProperty =
			DependencyProperty.Register("TitleContent", typeof(ControlTemplate), typeof(FantomWindow), null);


		/// <summary>
		/// 位于小按钮区域的模板内容。
		/// </summary>
		public ControlTemplate MiniContent
		{
			get { return (ControlTemplate)GetValue(MiniContentProperty); }
			set { SetValue(MiniContentProperty, value); }
		}

		// 小按钮的内容。
		public static readonly DependencyProperty MiniContentProperty =
			DependencyProperty.Register("MiniContent", typeof(ControlTemplate), typeof(FantomWindow), null);



		/// <summary>
		/// 是否允许通过双击修改窗体的最大化/正常状态。
		/// </summary>
		public bool AllowDoubleClickResizeWindow
		{
			get { return (bool)GetValue(AllowDoubleClickResizeWindowProperty); }
			set { SetValue(AllowDoubleClickResizeWindowProperty, value); }
		}

		/// <summary>
		/// 是否允许通过双击修改窗体的最大化/正常状态的依赖属性。
		/// </summary>
		public static readonly DependencyProperty AllowDoubleClickResizeWindowProperty =
			DependencyProperty.Register("AllowDoubleClickResizeWindow", typeof(bool), typeof(FantomWindow), new PropertyMetadata(true));


		#endregion

		#region ctors

		public FantomWindow()
		{
			DataContext = this;
		}

		static FantomWindow()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(FantomWindow), new FrameworkPropertyMetadata(typeof(FantomWindow)));
		}

		#endregion

		#region events

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed)
				DragMove();
		}


		#endregion


	}
}
