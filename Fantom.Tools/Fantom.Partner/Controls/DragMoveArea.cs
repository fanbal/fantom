﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Fantom.Partner.Controls
{
	/// <summary>
	/// 专门用于文件拖动放入的控件，应该具有文件拖入成功时的触发效果、点击区域时的效果。其继承按钮控件。
	/// </summary>
	public class DragMoveArea : Button
	{
	}
}
