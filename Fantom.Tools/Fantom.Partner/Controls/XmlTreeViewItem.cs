﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using Fantom.Partner.Xml;

namespace Fantom.Partner.Controls
{
	/// <summary>
	/// Xml 树视图控件。
	/// </summary>
	public class XmlTreeViewItem : HeaderedItemsControl
	{
		/// <summary>
		/// 点击时触发该事件。
		/// </summary>
		public static readonly RoutedEvent ItemClickedEvent = EventManager.RegisterRoutedEvent("ItemClicked", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(XmlTreeViewItem));

		/// <summary>
		/// 鼠标移上时触发该事件。
		/// </summary>
		public static readonly RoutedEvent ItemMouseEnterEvent = EventManager.RegisterRoutedEvent("ItemMouseEnter", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(XmlTreeViewItem));

		/// <summary>
		/// 鼠标移出时触发该事件。
		/// </summary>
		public static readonly RoutedEvent ItemMouseLeaveEvent = EventManager.RegisterRoutedEvent("ItemMouseLeave", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(XmlTreeViewItem));

		public bool IsExpanded
		{
			get { return (bool)GetValue(IsExpandedProperty); }
			set { SetValue(IsExpandedProperty, value); }
		}

		// Using a DependencyProperty as the backing store for IsExpanded.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty IsExpandedProperty =
			DependencyProperty.Register("IsExpanded", typeof(bool), typeof(XmlTreeViewItem), new PropertyMetadata(false));

		public XmlTreeViewItem()
		{
		}
	}
}
