﻿using System;
using System.Diagnostics;
using System.IO.Compression;
using System.Text;
using System.Windows.Threading;
using Microsoft.Win32;
using System.Xml.Linq;
using System.Collections.Generic;
using io = System.IO;
using ft = Fantom.Partner.Controls;
using fs = Fantom.Partner.FileSystem;
using wpf = System.Windows;

namespace Fantom.Partner.Models
{
	/// <summary>
	/// 工作区对象，每个项目能够创建若干工作区对象（但目前仅支持一个）。
	/// </summary>
	public class WorkArea
	{
		/// <summary>
		/// 工作区目录。
		/// </summary>
		private string _workarea = string.Empty;
		public string WorkAreaPath => _workarea;


		/// <summary>
		/// 目标 PPTX 文件路径。
		/// </summary>
		private string _targetFilePath = string.Empty;
		public string PPTXFilePath => _targetFilePath;

		/// <summary>
		/// 项目文件夹路径。
		/// </summary>
		private string _xmlProjectPath = string.Empty;
		public string XmlProjectPath => _xmlProjectPath;

		/// <summary>
		/// OpenXml 项目文件夹结构对象。
		/// </summary>
		private fs.Folder _xmlProjectFolder = null;
		public fs.Folder XmlProjectTreeView => _xmlProjectFolder;


		/// <summary>
		/// 文件系统监听器。
		/// </summary>
		private io.FileSystemWatcher _fileSystemWatcher;

		/// <summary>
		/// 当前 Xml 文档路径。
		/// </summary>
		private string _currentXmlFilePath = null;
		public string CurrentXmlFilePath { get => _currentXmlFilePath; set => _currentXmlFilePath = value; }

		/// <summary>
		/// 主线程代理器。
		/// </summary>
		private Dispatcher _dispatcher = null;


		/// <summary>
		/// 文件创建时触发该事件。
		/// </summary>
		public event io.FileSystemEventHandler FileCreated;

		/// <summary>
		/// 文件移除时触发该事件。
		/// </summary>
		public event io.FileSystemEventHandler FileRemoved;

		/// <summary>
		/// 文件更改时触发该事件。
		/// </summary>
		public event io.FileSystemEventHandler FileChanged;

		public delegate void FileFormatHandler(WorkArea workArea, string fileName);
		public event FileFormatHandler FileFormating;

		/// <summary>
		/// 操作检查，基本上所有方法都要在最开始调用此方法。
		/// </summary>
		private void OperatorCheck()
		{
			if (_workarea == string.Empty || !io.Directory.Exists(_workarea))
				Global.ThrowExceptionMessage("未设定工作区。");
		}


		/// <summary>
		/// 从拖拽信息中得到 PPTX 文件。
		/// </summary>
		private void LoadPPTXFromDrag(wpf.DragEventArgs e)
		{
			var datas = e.Data.GetData(wpf.DataFormats.FileDrop) as string[];

			if (datas == null ||
				datas.Length == 0 ||
				!datas[0].ToLower().EndsWith(".pptx"))
				Global.ThrowExceptionMessage("未知文件，请重新导入。");

			_targetFilePath = datas[0];
			_workarea = fs.Folder.GetParentFullPath(_targetFilePath);
			_xmlProjectPath = GetProjectFolder(_workarea);

			if (_xmlProjectPath == null)
			{
				_workarea = CreateWorkArea(_workarea, _targetFilePath, out _targetFilePath);
			}

		}

		/// <summary>
		/// 由 PPTX 文件创建工作区。
		/// </summary>
		public static WorkArea FromPPTX(string filePath)
		{

			var targetfile = filePath;
			var workarea = fs.Folder.GetParentFullPath(targetfile);
			var xmlproject = GetProjectFolder(workarea);

			if (xmlproject == null)
			{
				workarea = CreateWorkArea(workarea, targetfile, out targetfile);
				xmlproject = io.Path.Combine(workarea, "xmlproject");
				ZipFile.ExtractToDirectory(targetfile, xmlproject);
			}

			return new WorkArea()
			{
				_workarea = workarea,
				_targetFilePath = targetfile,
				_xmlProjectPath = xmlproject,
				_xmlProjectFolder = fs.Folder.LoadFolderFromFullPath(xmlproject)
			};

		}

		/// <summary>
		/// 从拖拽信息中得到 PPTX 文件。
		/// </summary>
		public static WorkArea FromDragPPTX(wpf.DragEventArgs e)
		{

			var datas = e.Data.GetData(wpf.DataFormats.FileDrop) as string[];

			if (datas == null ||
				datas.Length == 0 ||
				!datas[0].ToLower().EndsWith(".pptx"))
				Global.ThrowExceptionMessage("未知文件，请重新导入。");


			return FromPPTX(datas[0]);

		}

		#region 工作区存档相关操作，用于下次启动时自动载入原先工作区



		/// <summary>
		/// 保存当前工作区目录。
		/// </summary>
		public void SetConfigSave()
		{
			if (!io.Directory.Exists(_workarea) || !io.Directory.Exists(_xmlProjectPath) || !io.File.Exists(_targetFilePath))
				Global.ThrowExceptionMessage("存档时，工作区、PPTX 文件、Xml 项目文件缺一不可。");

			var sb = new StringBuilder();
			sb.AppendLine("[Config]");
			sb.AppendLine($"WorkArea = {_workarea}");
			sb.AppendLine($"TargetFile = {_targetFilePath}");
			sb.AppendLine($"XmlProject = {_xmlProjectPath}");

			var filestream = new io.FileStream(SavePath, io.FileMode.Create);
			var bytes = Encoding.Default.GetBytes(sb.ToString());
			filestream.Write(bytes, 0, bytes.Length);
			filestream.Close();
			filestream.Dispose();

		}

		/// <summary>
		/// 读取当前工作区目录。
		/// </summary>
		private bool GetConfigSave(out string workarea, out string targetfile, out string xmlproject)
		{
			workarea = null;
			targetfile = null;
			xmlproject = null;

			if (!io.File.Exists(SavePath))
				return false;

			var filestream = new io.FileStream(SavePath, io.FileMode.Open);
			var bytes = new byte[1024];
			filestream.Read(bytes, 0, bytes.Length);
			filestream.Close();
			filestream.Dispose();

			var initext = Encoding.Default.GetString(bytes).Trim();
			workarea = GetIniKeyValue(initext, "WorkArea").Trim(new char[] { ' ', '\r', '\n' });
			targetfile = GetIniKeyValue(initext, "TargetFile").Trim(new char[] { ' ', '\r', '\n' });
			xmlproject = GetIniKeyValue(initext, "XmlProject").Trim(new char[] { ' ', '\r', '\n' });
			return true;
		}

		/// <summary>
		/// 读取当前工作区目录。
		/// </summary>
		public static WorkArea FromConfigSave()
		{
			var workarea = string.Empty;
			var targetfile = string.Empty;
			var xmlproject = string.Empty;

			if (!io.File.Exists(SavePath))
				return null;

			var filestream = new io.FileStream(SavePath, io.FileMode.Open);
			var bytes = new byte[1024];
			filestream.Read(bytes, 0, bytes.Length);
			filestream.Close();
			filestream.Dispose();

			var initext = Encoding.Default.GetString(bytes).Trim();
			workarea = GetIniKeyValue(initext, "WorkArea").Trim(new char[] { ' ', '\r', '\n' });
			targetfile = GetIniKeyValue(initext, "TargetFile").Trim(new char[] { ' ', '\r', '\n' });
			xmlproject = GetIniKeyValue(initext, "XmlProject").Trim(new char[] { ' ', '\r', '\n' });

			if (!io.File.Exists(workarea))
				return null;

			if (!io.File.Exists(targetfile))
				return null;

			if (!io.File.Exists(xmlproject))
				return null;

			var folder = fs.Folder.LoadFolderFromFullPath(xmlproject);

			return new WorkArea()
			{
				_workarea = workarea,
				_targetFilePath = targetfile,
				_xmlProjectPath = xmlproject,
				_xmlProjectFolder = folder
			};
		}

		/// <summary>
		/// 获得 Ini 中指定键值对数据。
		/// </summary>
		private static string GetIniKeyValue(string initext, string key)
		{
			int start = initext.IndexOf(key);
			int equal = initext.IndexOf('=', start);
			int end = initext.IndexOf(Environment.NewLine, equal);
			if (end == -1)
				end = initext.Length - 1;
			return initext.Substring(equal + 1, end - equal);
		}
		#endregion


		#region 工作区载入相关，围绕环境的载入过程


		/// <summary>
		/// 结合 PPTX 文件上下文创建工作区。
		/// </summary>
		/// <returns>返回创建的工作区位置，并在 out 位输出新的 PPTX 文件位置。</returns>
		private static string CreateWorkArea(string oldpath, string filepath, out string newFilePath)
		{
			var name = fs.File.GetFileShortName(filepath);
			string newpath;
			int i = 0;
			while (true)
			{
				newpath = io.Path.Combine(oldpath, name + "_WorkArea" + i.ToString());
				if (!io.Directory.Exists(newpath))
					break;
				i++;
			}

			io.Directory.CreateDirectory(newpath);

			newFilePath = io.Path.Combine(newpath, name + ".pptx");
			io.File.Copy(filepath, newFilePath);

			return newpath;
		}


		/// <summary>
		/// 自动定位项目文件夹，如果没有找到，则创建文件夹。
		/// </summary>
		/// <returns>返回项目文件夹位置。如果存在项目文件夹，则直接返回项目文件夹。</returns>
		private static string GetProjectFolder(string path)
		{
			// 检索当前目录下是否存在 Xml 项目目录。
			foreach (var folder in io.Directory.GetDirectories(path))
			{
				if (IsXmlProjectFolder(folder)) return folder;
			}
			return null;
		}

		/// <summary>
		/// 是否为 Xml 工程目录。
		/// </summary>
		private static bool IsXmlProjectFolder(string path)
		{
			return io.File.Exists(io.Path.Combine(path, ProjectEntryFile));
		}

		#endregion


		/// <summary>
		/// 导出 Xml 源项目。
		/// </summary>
		public void ExportXmlProject()
		{
			OperatorCheck();

			if (!io.File.Exists(_targetFilePath))
				Global.ThrowExceptionMessage("未找到 PPTX 包体，请确保工作区内存在 PPTX 文件。");

			// 一定会为 _xmlProjectPath 赋值，确保具有导出路径。
			if (_xmlProjectPath == null || !io.Directory.Exists(_xmlProjectPath))
				_xmlProjectPath = io.Path.Combine(_workarea, fs.File.GetFileShortName(_targetFilePath));

			if (io.Directory.Exists(_xmlProjectPath))
			{
				_fileSystemWatcher.EnableRaisingEvents = false;
				io.Directory.Delete(_xmlProjectPath, true);
			}

			ZipFile.ExtractToDirectory(_targetFilePath, _xmlProjectPath);
		}

		/// <summary>
		/// 委托文件资源管理器打开 PPTX 文件，完全按照您的默认方式打开，所以可能是 PowerPoint 也可能是 WPS。
		/// </summary>
		public void OpenPPTXFileInBrowser()
		{
			OperatorCheck();
			if (!io.File.Exists(_targetFilePath))
				Global.ThrowExceptionMessage("未找到 PPTX 包体，我们无法打开不存在的东西。");
			Process.Start(_targetFilePath);
		}

		/// <summary>
		/// 从文件资源管理器中打开工作区。
		/// </summary>
		public void OpenWorkAreaInBrowser()
		{
			OperatorCheck(); // 此处由OperatorCheck处理，当出现错误时会提示 未设定工作区。
			Process.Start(_workarea);
		}

		/// <summary>
		/// 将项目文件压缩，并导出覆盖原先的 PPTX 文件。
		/// </summary>
		public void ExportPPTXFile()
		{
			OperatorCheck();

			var export = _targetFilePath;

			if (!io.Directory.Exists(_xmlProjectPath))
				Global.ThrowExceptionMessage("您需要先提供 Xml 源项目才能更新 PPT");

			if (io.File.Exists(export))
				io.File.Delete(export);

			ZipFile.CreateFromDirectory(_xmlProjectPath, export);
		}


		/// <summary>
		/// 生成 PPTX 文件。
		/// </summary>
		public void BuildPPTXFile()
		{
			ExportPPTXFile();
		}


		/// <summary>
		/// 导入路径启动文件系统监视器。
		/// </summary>
		public void OpenFileSystemWatcher(Dispatcher mainThreadDispacher)
		{
			_dispatcher = mainThreadDispacher;

			// 如果已经开启了监视，则先释放原先的监视器，释放旧内存。
			if (_fileSystemWatcher != null)
			{
				_fileSystemWatcher.EnableRaisingEvents = false;
				_fileSystemWatcher.Dispose();
			}


			_fileSystemWatcher = new io.FileSystemWatcher(_xmlProjectPath);
			_fileSystemWatcher.NotifyFilter = io.NotifyFilters.LastWrite;
			_fileSystemWatcher.Created += (s, e) =>
			{ _dispatcher.Invoke(() => { FileCreated?.Invoke(s, e); }); };
			_fileSystemWatcher.Deleted += (s, e) =>
			{ _dispatcher.Invoke(() => { FileRemoved?.Invoke(s, e); }); };
			_fileSystemWatcher.Changed += (s, e) =>
			{ _dispatcher.Invoke(() => { FileChanged?.Invoke(s, e); }); };

			_fileSystemWatcher.IncludeSubdirectories = true;
			_fileSystemWatcher.EnableRaisingEvents = true;
			_fileSystemWatcher.Filter = "";
		}

		/// <summary>
		/// 关闭路径启动文件系统监视器。
		/// </summary>
		public void CloseFileSystemWatcher()
		{
			_fileSystemWatcher.EnableRaisingEvents = false;
			_fileSystemWatcher.Dispose();
			_fileSystemWatcher = null;
		}


		/// <summary>
		/// 从 Visual Studio Code 打开项目。
		/// </summary>
		public void OpenInVisualStudioCode()
		{
			var path = fs.VSCodeFinder.GetVisualStudioCodePath();
			if (path == null)
				Global.ThrowExceptionMessage("找不到 VS Code");
			
			var info = new ProcessStartInfo();
			info.FileName = path;
			info.Arguments = $"{_xmlProjectPath}";
			Process.Start(info);
			// 计算机\HKEY_USERS\S-1-5-21-2864470408-2210959852-1108548295-1001\Software\Microsoft\Windows\CurrentVersion\Uninstall\{771FD6B0-FA20-440A-A002-3B3BAC16DC50}_is1
		}

		/// <summary>
		/// 从 Visual Studio Code 打开项目。
		/// </summary>
		public void OpenInDefault()
		{
			Process.Start(_currentXmlFilePath);

			// 计算机\HKEY_USERS\S-1-5-21-2864470408-2210959852-1108548295-1001\Software\Microsoft\Windows\CurrentVersion\Uninstall\{771FD6B0-FA20-440A-A002-3B3BAC16DC50}_is1
		}

		/// <summary>
		/// 格式化文件。
		/// </summary>
		private void FormatOriginXmlFile(string path)
		{
			var filestream = new io.FileStream(path, io.FileMode.Open);
			var bytes = new List<byte>();
			int outbyte = 0;
			while (true)
			{
				outbyte = filestream.ReadByte();
				if (outbyte == -1)
					break;
				else
					bytes.Add((byte)outbyte);
			}
			filestream.Close();
			filestream.Dispose();
			var str = Encoding.UTF8.GetString(bytes.ToArray());
			str = Xml.Formatter.Format(str);

			filestream = io.File.OpenWrite(path);
			var bytes2 = Encoding.UTF8.GetBytes(str);
			filestream.Write(bytes2, 0, bytes2.Length);
			filestream.Close();
			filestream.Dispose();
		}

		/// <summary>
		/// 格式化整理原先的 Xml 文档。
		/// </summary>
		private void FormatOriginXml(fs.Folder fsnode)
		{
			foreach (var file in fsnode.Files)
			{
				if (file.FullPath.EndsWith("xml", StringComparison.CurrentCultureIgnoreCase) || file.FullPath.EndsWith(".rels", StringComparison.CurrentCultureIgnoreCase))
				{
					FileFormating?.Invoke(this, file.Name);
					FormatOriginXmlFile(file.FullPath);
					//Xml.Formatter.FormatInFile(file.FullPath);
				}
					
			}

			foreach (var folder in fsnode.Subfolders)
			{
				FormatOriginXml(folder);
			}
		}

		/// <summary>
		/// 格式化。
		/// </summary>
		public void FormatOriginXml()
		{
			FormatOriginXml(XmlProjectTreeView);
		}

		#region static

		/// <summary>
		/// 相关配置信息存档路径。
		/// </summary>
		public static string SavePath { get; } = io.Path.Combine(Environment.CurrentDirectory, "SaveConfig.ini");

		/// <summary>
		/// 项目入口文件名。由于每个包体都有这个文件，Partner在检索匹配项目目录时，便采用检索是否存在这个文件作为检索标准。
		/// </summary>
		public static string ProjectEntryFile { get; } = "[Content_Types].xml";

		/// <summary>
		/// 访问 MSDN 的 OpenXml 文档部分。
		/// </summary>
		public static void VisitMSDN()
		{
			Process.Start(@"https://docs.microsoft.com/zh-cn/office/open-xml/open-xml-sdk");
		}

		/// <summary>
		/// 访问 Fantom 的 主页部分。
		/// </summary>
		public static void VisitFantomHomePage()
		{
			Process.Start(@"https://gitee.com/fanbal/fantom");
		}

		#endregion

	}
}
