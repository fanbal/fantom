﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Xml;

namespace Fantom.Partner.Models
{
	/// <summary>
	/// Xml树内容的代理视图，具有控制着整个树节点的状态，如折叠、标题名、。
	/// </summary>
	public class XmlTreeItemProxyViewer : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public XmlDocument XmlDocument { get; }

		public XmlElement XmlElement { get; }

		public bool IsExpand
		{
			get
			{
				return _isExpand;
			}
			set
			{
				_isExpand = value;
				PropertyChangedInvoke();
			}
		}
		private bool _isExpand = false;

		public XmlTreeItemProxyViewer(XmlDocument xmldoc, XmlElement xmlele)
		{
			XmlDocument = xmldoc;
			XmlElement = xmlele;
		}

		public void PropertyChangedInvoke([CallerMemberName] string callerName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(callerName));
		}

	}
}
