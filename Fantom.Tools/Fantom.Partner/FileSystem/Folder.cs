﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Fantom.Partner.FileSystem
{
	public class Folder : FileSystemNode
	{
		public List<Folder> Subfolders { get; set; }
		public List<File> Files { get; set; }

		/// <summary>
		/// 由路径导入生成逻辑树结构。
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static Folder LoadFolderFromFullPath(string path)
		{
			var folder = new Folder()
			{
				FullPath = path,
				Name = GetShortName(path),
				Subfolders = new List<Folder>(),
				Files = new List<File>(),
			};

			foreach (var file in Directory.GetFiles(path))
				folder.Files.Add(new File()
				{ Name = GetShortName(file), FullPath = file });


			foreach (var subfolder in Directory.GetDirectories(path))
				folder.Subfolders.Add(LoadFolderFromFullPath(subfolder));

			return folder;
		}

		public override IEnumerable<FileSystemNode> SubNodes => Subfolders.Concat<FileSystemNode>(from f in Files orderby f.Name.Length, f.Name select f);
	}
}
