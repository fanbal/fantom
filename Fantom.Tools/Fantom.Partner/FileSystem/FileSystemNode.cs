﻿using System.Collections.Generic;
using System.IO;

namespace Fantom.Partner.FileSystem
{
	public abstract class FileSystemNode
	{
		public string FullPath { get; set; }
		public string Name { get; set; }

		/// <summary>
		/// 文件节点的子节点。
		/// </summary>
		public virtual IEnumerable<FileSystemNode> SubNodes { get; } = new List<FileSystemNode>();

		/// <summary>
		/// 获得父级的目录完整路径。
		/// </summary>
		public static string GetParentFullPath(string path)
		{
			int i;
			for (i = path.Length - 1; i >= 0; i--)
			{
				if (path[i] == '\\' || path[i] == '/')
					break;
			}
			return path.Substring(0, i);
		}

		/// <summary>
		/// 获得当前路径的短名称。
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static string GetShortName(string path)
		{
			int i = path.Length - 1;
			while (i >= 0)
			{
				if (path[i] == '\\' || path[i] == '/')
					break;
				i--;
			}
			i++;
			return path.Substring(i, path.Length - i);
		}


		/// <summary>
		/// 去除拓展名的短文件名。
		/// </summary>
		/// <param name="file"></param>
		public static string GetFileShortName(string file)
		{
			int i = file.Length - 1;
			int j = 0;
			while (file[i] != '\\' && file[i] != '/' && i >= 0)
			{
				if (file[i] == '.')
					j = i;
				i--;
			}
			return file.Substring(i + 1, j - i - 1);
		}

		/// <summary>
		/// 动态获得指定目录下的 PPTX 文件。
		/// </summary>
		public static string GetPPTX(string path)
		{
			foreach (var file in Directory.GetFiles(path))
			{
				if (file.EndsWith("pptx") || file.EndsWith("PPTX")) return file;
			}
			return null;
		}


	}
}
