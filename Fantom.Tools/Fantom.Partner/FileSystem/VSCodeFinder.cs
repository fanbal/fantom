﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using io = System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Windows.Media;

namespace Fantom.Partner.FileSystem
{
	/// <summary>
	/// 用于查询本机上的 VS Code 可执行文件。
	/// </summary>
	public static class VSCodeFinder
	{
		delegate string MethodHandler();

		private static List<MethodHandler> funcs;
		static VSCodeFinder()
		{
			funcs = new List<MethodHandler>(); ;
			funcs.Add(FromIcon);
			funcs.Add(FromUnistall);
			funcs.Add(FromUserLocal);
			funcs.Add(FromProgramFiles);
			funcs.Add(FromProgramFiles_x86);
			funcs.Add(FromUserEnvironmentVar);
			funcs.Add(FromSysEnvironmentVar);
		}

		/// <summary>
		/// 由注册表中的图标项查询。
		/// </summary>
		/// <returns></returns>
		private static string FromIcon()
		{
			var reg = Registry.ClassesRoot;
			var vscode = reg.OpenSubKey(@"*\shell\VSCode", false);
			if (vscode == null) return null;
			return vscode.GetValue("Icon") as string;
		}

		/// <summary>
		/// 由卸载信息中查询。
		/// </summary>
		/// <returns></returns>
		private static string FromUnistall()
		{
			// 计算机\HKEY_USERS\S - 1 - 5 - 21 - 2864470408 - 2210959852 - 1108548295 - 1001\Software\Microsoft\Windows\CurrentVersion\Uninstall\{ 771FD6B0 - FA20 - 440A - A002 - 3B3BAC16DC50}_is1
			var reg = Registry.Users;
			var subPath = @"S - 1 - 5 - 21 - 2864470408 - 2210959852 - 1108548295 - 1001\Software\Microsoft\Windows\CurrentVersion\Uninstall\{ 771FD6B0 - FA20 - 440A - A002 - 3B3BAC16DC50}_is1";
			var vscode = reg.OpenSubKey(subPath, false);
			if (vscode == null) return null;
			return vscode.GetValue("InstallLocation") as string;
		}


		/// <summary>
		/// 从用户列表中查询文件。
		/// </summary>
		private static string FromUserLocal()
		{
			var users = @"C:\Users";
			var subpath = @"AppData\Local\Programs\Microsoft VS Code\Code.exe";
			foreach (var user in io.Directory.GetDirectories(users))
			{
				var path = io.Path.Combine(user, subpath);
				if (io.File.Exists(path))
					return path;
			}
			return null;
		}

		/// <summary>
		/// 从 Program Files 中提取。
		/// </summary>
		private static string FromProgramFiles()
		{
			var path = @"C:\Programs Files\Microsoft VS Code\Code.exe";
			if (io.File.Exists(path))
				return path;
			else return null;
		}


		/// <summary>
		/// 从 Program Files x86 中提取。
		/// </summary>
		private static string FromProgramFiles_x86()
		{
			var path = @"C:\Program Files (x86)\Microsoft VS Code\Code.exe";
			if (io.File.Exists(path))
				return path;
			else return null;
		}

		/// <summary>
		/// 从 环境变量 中提取。
		/// </summary>
		private static string FromUserEnvironmentVar()
		{
			var userPath = Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.User);
			var paths = from path in userPath.Split(';') where path.EndsWith("Microsoft VS Code\bin") select path;
			if (paths.Count() == 0)
				return null;
			else
				return paths.First();

		}


		/// <summary>
		/// 从 环境变量 中提取。
		/// </summary>
		private static string FromSysEnvironmentVar()
		{
			var userPath = Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.Machine);
			var paths = from path in userPath.Split(';') where path.EndsWith("Microsoft VS Code\bin") select path;
			if (paths.Count() == 0)
				return null;
			else
				return paths.First();
		}

		private static string GetVisualStudioCodeRawPath()
		{
			string path;
			foreach (var fun in funcs)
			{
				path = fun();
				if (path != null) return path;
			}
			return null;
		}

		public static string GetVisualStudioCodePath()
		{
			string path = GetVisualStudioCodeRawPath();
			if (path == null) return null;

			if (!path.EndsWith("exe"))
				path = path + @"\Code.exe";

			if (io.File.Exists(path))
				return path;
			else
				return null;


		}
	}
}
