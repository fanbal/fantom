﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;
using System.Text;
using wpf = System.Windows;
using io = System.IO;
using fs = Fantom.Partner.FileSystem;
using Fantom.Partner.Models;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Linq;
using System.Xml.Schema;

namespace Fantom.Partner
{
	// MainWindow.xaml 的交互逻辑
	public partial class MainWindow : Controls.FantomWindow
	{
		#region properties

		#endregion

		#region ctors
		public MainWindow()
		{
			InitializeComponent();

			// 载入项目目录结构。
			LoadXmlProjectTreeView();

			// 定位入口文件位置。
			LoadXmlText(io.Path.Combine(App.MainWorkArea.XmlProjectPath, @"ppt\slides\slide1.xml"));

			// 用于 Xml 编辑器的初始化。
			XmlEditor_Initialize();
		}


		#endregion

		#region events

		/// <summary>
		/// 编辑器初始化。
		/// </summary>
		private void XmlEditor_Initialize()
		{
			xmlEditor.ItemEnter += XmlEditor_ItemEnter;
			xmlEditor.ItemLeave += XmlEditor_ItemLeave;
		}

		/// <summary>
		/// 离开编辑器选项时时触发该事件。
		/// </summary>
		private void XmlEditor_ItemLeave(object sender, RoutedEventArgs e)
		{
			UpdateMembersList(xmlEditor.CheckedElement);
		}

		/// <summary>
		/// 进入编辑器选项时触发该事件。
		/// </summary>
		private void XmlEditor_ItemEnter(object sender, RoutedEventArgs e)
		{
			var os = e.OriginalSource as FrameworkElement;
			UpdateMembersList(os.DataContext as System.Xml.XmlElement);
		}

		/// <summary>
		/// 更新成员集合列表。
		/// </summary>
		private void UpdateMembersList(System.Xml.XmlElement xmlelem)
		{
			if (xmlelem == null)
			{
				membersList.ItemsSource = null;
				return;
			}

			// 因为获得了 Xml 元素，我们可以尝试通过 XSDViewer 检索其对应的语法类型。
			// Xml.XSDViewer.GetSchemaTypeMembers() 获得 Xml 节点的成员集合。
			var typeMembers = Xml.XSDViewer.GetSchemaTypeMembers(xmlelem);

			// 如果没有找到则直接退出。
			if (typeMembers == null) return;

			// 选择 XmlSchemaElement 和 XmlSchemaChoice 两类成员。
			var members =
				from m in typeMembers
				where m is XmlSchemaElement || m is XmlSchemaChoice
				select m;

			if (members == null || members.Count() == 0) return;

			// 更新数据源。
			membersList.ItemsSource = members;

		}

		/// <summary>
		/// 按钮添加成员时触发该事件。
		/// 目前尚未实现添加成员，因为显示方面存在一定问题。
		/// </summary>
		private void Button_AddMember(object sender, RoutedEventArgs e)
		{
			var control = sender as FrameworkElement;
			var elem = control.DataContext as System.Xml.Schema.XmlSchemaElement;
			var sb = new StringBuilder();

			sb.AppendLine("name: " + elem.Name);
			sb.AppendLine("max: " + elem.MaxOccurs);
			sb.AppendLine("min: " + elem.MinOccurs);

			xmlEditor.AddNodeFromXSDElement(elem);
			//Controls.XmlTreeView.GetSchemaRestrictNode(control, out int m, out int M);

			xmlEditor.UpdateLayout();
		}

		/// <summary>
		/// 打开文件按钮事件。
		/// </summary>
		private void Button_OpenWorkArea(object sender, wpf.RoutedEventArgs e)
		{
			App.MainWorkArea.OpenWorkAreaInBrowser();
		}

		/// <summary>
		/// 提取文件源代码按钮事件。
		/// </summary>
		private void Button_ExtractFile(object sender, wpf.RoutedEventArgs e)
		{
			App.MainWorkArea.ExportXmlProject();
			LoadXmlProjectTreeView();
		}

		/// <summary>
		/// 打开文件按钮事件。
		/// </summary>
		private void Button_OpenFile(object sender, wpf.RoutedEventArgs e)
		{
			App.MainWorkArea.OpenPPTXFileInBrowser();
		}

		/// <summary>
		/// 生成文件按钮事件。
		/// </summary>
		private void Button_BuildFile(object sender, wpf.RoutedEventArgs e)
		{
			SaveXmlFileInEditor();
			App.MainWorkArea.ExportPPTXFile();
			App.MainWorkArea.OpenPPTXFileInBrowser();
		}

		/// <summary>
		/// 生成PPT文件。
		/// </summary>
		private void Button_ExportFile(object sender, wpf.RoutedEventArgs e)
		{
			App.MainWorkArea.ExportPPTXFile();
		}

		/// <summary>
		/// 导出工作区。
		/// </summary>
		private void Button_SaveWorkArea(object sender, wpf.RoutedEventArgs e)
		{
			App.MainWorkArea.SetConfigSave();
		}

		/// <summary>
		/// 由指定节点打开文件或者文件夹。
		/// </summary>
		private void Button_OpenPathInBrowser(object sender, wpf.RoutedEventArgs e)
		{
			var node = (sender as wpf.FrameworkElement).DataContext as fs.FileSystemNode;
			if (node is fs.File)
			{
				if (!node.FullPath.EndsWith("xml", StringComparison.CurrentCultureIgnoreCase) &&
					!node.FullPath.EndsWith(".rels", StringComparison.CurrentCultureIgnoreCase)) return;
				LoadXmlText(node.FullPath);
			}
		}

		/// <summary>
		/// 保存 Xml 文本文件的事件。
		/// </summary>
		private void Button_SaveXmlFile(object sender, wpf.RoutedEventArgs e)
		{
			SaveXmlFileInEditor();
		}

		#endregion

		/// <summary>
		/// 保存位于编辑器的 Xml 文件。
		/// </summary>
		void SaveXmlFileInEditor()
		{
			xmlEditor.XmlDocument.Save(App.MainWorkArea.CurrentXmlFilePath);
		} 

		/// <summary>
		/// 载入 Xml 项目的目录树结构，并激活文件资源监听器，用以实现文件的监听跟踪。
		/// </summary>
		void LoadXmlProjectTreeView()
		{
			treeView.ItemsSource = App.MainWorkArea.XmlProjectTreeView.SubNodes; // 数据绑定，具体布局见 XAML。

			App.MainWorkArea.OpenFileSystemWatcher(Dispatcher);
		}

		/// <summary>
		/// 加载 Xml 文本。
		/// </summary>
		void LoadXmlText(string path)
		{
			xmlEditor.LoadXmlDocument(path);
			App.MainWorkArea.CurrentXmlFilePath = path;
		}

		private void HierarchicalDataTemplate_Expanded(object sender, wpf.RoutedEventArgs e)
		{
			var treeView = sender as TreeView;
			Controls.MessageBox.Show("test");
		}

		private void Button_OpenMiniWindow(object sender, wpf.RoutedEventArgs e)
		{
			Hide();
			var miniwin = App.MiniWindow;
			if (miniwin == null)
			{
				miniwin = new MiniWindow();
				App.MiniWindow = miniwin;
			}
			wpf.Application.Current.MainWindow = miniwin;
			miniwin.Show();
		}

		private void Button_OpenDefault(object sender, wpf.RoutedEventArgs e)
		{
			App.MainWorkArea.OpenInDefault();
		}

		private void Button_SaveAs(object sender, wpf.RoutedEventArgs e)
		{
			xmlEditor.XmlDocument.Save(App.MainWorkArea.CurrentXmlFilePath);
			Controls.MessageBox.Show("另存为成功");
		}


	}
}
