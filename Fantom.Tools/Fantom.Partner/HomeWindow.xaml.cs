﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Fantom.Partner.Models;
using ft = Fantom.Partner.Controls;
using fs = Fantom.Partner.FileSystem;
using io = System.IO;
using System.Diagnostics;

namespace Fantom.Partner
{
	/// <summary>
	/// Home.xaml 的交互逻辑
	/// </summary>
	public partial class HomeWindow : ft.FantomWindow
	{

		static SolidColorBrush _noHighlightBrush = new SolidColorBrush(Colors.Black);
		public static string GetHeightlightText(DependencyObject obj)
		{
			return (string)obj.GetValue(HeightlightTextProperty);
		}

		public static void SetHeightlightText(DependencyObject obj, string value)
		{
			obj.SetValue(HeightlightTextProperty, value);
		}

		// Using a DependencyProperty as the backing store for HeightlightText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty HeightlightTextProperty =
			DependencyProperty.RegisterAttached("HeightlightText", typeof(string), typeof(TextBox), new PropertyMetadata(""));


		public HomeWindow()
		{
			InitializeComponent();
			App.HomeWindow = this;

		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			// Controls.NoticeBar.Show("");
			var workArea = WorkArea.FromConfigSave();
			App.MainWorkArea = workArea;
			OpenMainWindow(workArea);
		}


		/// <summary>
		/// 打开示例文件 OurPartner
		/// </summary>
		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			var ourPartner = io.Path.Combine(Environment.CurrentDirectory, @"OurPartner_WorkArea0\OurPartner.pptx");
			var created = true;
			if (!io.File.Exists(ourPartner))
			{
				ourPartner = io.Path.Combine(Environment.CurrentDirectory, @"OurPartner.pptx");
				created = false;
			}

			if (!io.File.Exists(ourPartner))
				Global.ThrowExceptionMessage("找不到 OurPartner.pptx ");
			else
			{

				var workArea = WorkArea.FromPPTX(ourPartner);
				App.MainWorkArea = workArea;

				if (created == false)
				{
					// workArea.FileFormating += MainWorkArea_FileFormating;
					// workArea.FormatOriginXml();
				}

				OpenMainWindow(workArea);

			}
		}

		/// <summary>
		/// 拖拽加载工作区。
		/// </summary>
		private void DragMoveArea_Drop(object sender, DragEventArgs e)
		{
			var workArea = WorkArea.FromDragPPTX(e);
			if (workArea == null)
			{
				Global.ThrowExceptionMessage("导入文件错误。");
			}
			else
			{
				App.MainWorkArea = workArea;
				// workArea.FileFormating += MainWorkArea_FileFormating;
				// workArea.FormatOriginXml();
				OpenMainWindow(workArea);
			}

		}

		/// <summary>
		/// 打开编辑窗体。
		/// </summary>
		/// <param name="workArea"></param>
		private void OpenMainWindow(WorkArea workArea)
		{
			this.Hide();
			var mainwin = App.EditWindow;
			if (mainwin == null)
			{
				mainwin = new MainWindow();
				App.EditWindow = mainwin;
			}
			App.Current.MainWindow = mainwin;
			mainwin.Show();


		}

		private void MainWorkArea_FileFormating(WorkArea workArea, string fileName)
		{
			// statusBar.Text = fileName;
		}

		private void Button_Click_2(object sender, RoutedEventArgs e)
		{
#if DEBUG
			var root = GetFantomPartnerRoot();
			var ppt = io.Path.Combine(root, @"UIDesign\Fantom 设计讨论版.pptx");

			Process.Start(ppt);
#else
			Global.ThrowExceptionMessage("该功能仅限 Debug 版本使用！\r\n需要结合源码编译。独立发布版无法使用此功能。");
#endif
		}

		private string GetFantomPartnerRoot()
		{
			var path = Environment.CurrentDirectory;
			while (!path.EndsWith("Fantom.Partner"))
			{
				path = fs.Folder.GetParentFullPath(path);
			}
			return path;
		}
	}
}
