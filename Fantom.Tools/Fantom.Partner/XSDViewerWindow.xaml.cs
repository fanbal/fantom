﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using Fantom.Partner.Models;
using Fantom.Partner.Xml;

namespace Fantom.Partner
{
	/// <summary>
	/// XSDViewerWindow.xaml 的交互逻辑
	/// </summary>
	public partial class XSDViewerWindow : Window
	{
		public XSDViewerWindow()
		{
			InitializeComponent();

			PART_elemList.ItemsSource = XSDViewer.SchemaSet.GlobalElements.Names;
			PART_typeList.ItemsSource = XSDViewer.SchemaSet.GlobalTypes.Names;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			
		}
	}
}
