﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml;
using Fantom.Partner.Xml;

namespace Fantom.Partner.Converters
{
	public class ElementToTypeTipsConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var node = value as XmlElement;
			// var name = XSDViewer.GetXmlSchemaType(node)?.Name;
			var typeNode = XSDViewer.GetXmlSchemaType(node);
			
			return XSDViewer.GetSchemaTypeMembers(typeNode);
		}

		// 这个不管。
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
