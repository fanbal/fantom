﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Fantom.Partner.Converters
{
	public class TreeViewItemToFolderStateConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var isExpand = (bool)value;
			if (isExpand)
				return new SolidColorBrush(Colors.Pink);
			else
				return new SolidColorBrush(Colors.Green);

		}

		// 这个我们不管。
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
