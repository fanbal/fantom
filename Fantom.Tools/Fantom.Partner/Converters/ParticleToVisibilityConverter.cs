﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml;
using Fantom.Partner.Xml;

namespace Fantom.Partner.Converters
{
	public class ParticleToVisibilityConverterA : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var node = value as System.Xml.Schema.XmlSchemaElement;
			return node != null ? Visibility.Visible : Visibility.Hidden;
		}

		// 这个不管。
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
	public class ParticleToVisibilityConverterB : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var node = value as System.Xml.Schema.XmlSchemaChoice;
			return node != null ? Visibility.Visible : Visibility.Hidden;
		}

		// 这个不管。
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

}
