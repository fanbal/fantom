﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml;
using System.Xml.Schema;
using Fantom.Partner.Xml;

namespace Fantom.Partner.Converters
{
	public class ParticleToItemsSourceConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is XmlSchemaChoice)
			{
				return (value as XmlSchemaChoice).Items.OfType<XmlSchemaElement>();
			}
			else return null;


		}

		// 这个不管。
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
