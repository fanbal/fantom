﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media.Animation;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Input;
using Fantom.Partner.Xml;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace Fantom.Partner.Events
{
	/// <summary>
	/// XmlTreeView 的事件相关。
	/// </summary>
	public partial class XmlTreeViewItem : ResourceDictionary
	{

		private void Node_Click(object sender, EventArgs eventArgs)
		{
			var dpobject = sender as FrameworkElement;
			var xmlnode = dpobject.DataContext as XmlNode;

			var type = XSDViewer.GetXmlSchemaType(xmlnode);

			Controls.MessageBox.Show(type != null ? type.Name : "missing");
		}

		private void Node_ExportTips(object sender, EventArgs eventArgs)
		{
			var dpobject = sender as FrameworkElement;
			var xmlnode = dpobject.DataContext as XmlNode;

			var type = XSDViewer.GetXmlSchemaType(xmlnode);

			Controls.MessageBox.Show(type != null ? type.Name : "missing");
		}

		private void Node_PopupTips(object sender, EventArgs eventArgs)
		{
			var treeViewItem = sender as DependencyObject;
			var parent = VisualTreeHelper.GetParent(treeViewItem);
		}

		private void Node_RaiseItemClickedEvent(object sender, EventArgs eventArgs)
		{
			var treeViewItem = sender as FrameworkElement;
			var args = new RoutedEventArgs(Controls.XmlTreeViewItem.ItemClickedEvent, treeViewItem);
			treeViewItem.RaiseEvent(args);
			
		}

		private void Node_ItemMouseEnterEvent(object sender, EventArgs eventArgs)
		{
			var treeViewItem = sender as FrameworkElement;
			var args = new RoutedEventArgs(Controls.XmlTreeViewItem.ItemMouseEnterEvent, treeViewItem);
			treeViewItem.RaiseEvent(args);
			
		}

		private void Node_ItemMouseLeaveEvent(object sender, EventArgs eventArgs)
		{
			var treeViewItem = sender as FrameworkElement;
			var args = new RoutedEventArgs(Controls.XmlTreeViewItem.ItemMouseLeaveEvent, treeViewItem);
			treeViewItem.RaiseEvent(args);

		}

	}
}
