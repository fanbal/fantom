﻿using System.Windows;
using System.Windows.Input;
using wpf = System.Windows.Controls;

namespace Fantom.Partner.Events
{
	public partial class MessageBox : ResourceDictionary
	{

		/// <summary>
		/// 捕获 空格 与 ESC，用于快速关闭消息框。
		/// </summary>
		private void MessageBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Escape)
			{
				var btn = sender as DependencyObject;
				var win = Window.GetWindow(btn);
				win.Close();
			}

		}

		/// <summary>
		/// 鼠标按下关闭窗体。
		/// </summary>
		private void Button_CloseWindow(object sender, RoutedEventArgs e)
		{
			var btn = sender as wpf.Button;
			var win = Window.GetWindow(btn);
			win.Close();
		}

		/// <summary>
		/// 放大窗体。
		/// </summary>
		private void Button_MaxWindow(object sender, RoutedEventArgs e)
		{
			var btn = sender as DependencyObject;
			var win = Window.GetWindow(btn);
			var windowstate = win.WindowState;
			if (windowstate == WindowState.Normal)
				windowstate = WindowState.Maximized;
			else
				windowstate = WindowState.Normal;
		}

		/// <summary>
		/// 最小化窗体。
		/// </summary>
		private void Button_MinWindow(object sender, RoutedEventArgs e)
		{
			var btn = sender as DependencyObject;
			var win = Window.GetWindow(btn);
			var windowstate = win.WindowState;

			windowstate = WindowState.Minimized;
		}



	}
}
