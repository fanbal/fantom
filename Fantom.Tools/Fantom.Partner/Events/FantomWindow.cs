﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media.Animation;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Input;

namespace Fantom.Partner.Events
{
	public partial class FantomWindow : ResourceDictionary
	{

		private DateTime? _lastTime = null;
		private Point _lastCursorPoint = new Point(0, 0);

		/// <summary>
		/// 关闭程序。
		/// </summary>
		private void Button_CloseWindow(object sender, RoutedEventArgs e)
		{
			// var win = Window.GetWindow(sender as DependencyObject);
			Application.Current.Shutdown();
		}

		/// <summary>
		/// 最大化按钮。
		/// </summary>
		private void Button_MaxWindow(object sender, RoutedEventArgs e)
		{
			var win = Window.GetWindow(sender as DependencyObject) as Controls.FantomWindow;

			var grid = win.Template.FindName("PART_windowMainGrid", win) as Grid;
			var path = win.Template.FindName("PART_maxPath", win) as Path;

			// 由小变大
			if (win.WindowState == WindowState.Normal)
			{
				win.WindowState = WindowState.Maximized;
				var anim = new ThicknessAnimation(new Thickness(30), new Thickness(5), new Duration(TimeSpan.FromMilliseconds(300)));
				anim.EasingFunction = new CubicEase() { EasingMode = EasingMode.EaseOut };
				grid.BeginAnimation(Window.MarginProperty, anim);
				path.Data = Geometry.Parse("M3.00035 10.3562 3.00035 43.984 36.8093 43.984 36.8093 10.3562 3.00035 10.3562ZM11.1908 3.01614 11.1908 7.34004 39.8097 7.34004 39.8097 36.6439 44.9998 36.6439 44.9998 3.01614 11.1908 3.01614ZM8.19044 0 48.0001 0 48.0001 39.6601 39.8097 39.6601 39.8097 47.0001 0 47.0001 0 7.34004 8.19044 7.34004 8.19044 0Z");
			}
			// 由大变小
			else
			{
				var anim = new ThicknessAnimation(new Thickness(-50), new Thickness(0), new Duration(TimeSpan.FromMilliseconds(400)));
				anim.EasingFunction = new CubicEase() { EasingMode = EasingMode.EaseOut };
				grid.BeginAnimation(Window.MarginProperty, anim);
				path.Data = Geometry.Parse("M0 0 48 0 48 48 0 48 Z M 3 3 3 45 45 45 45 3Z");
				win.WindowState = WindowState.Normal;
			}

		}

		/// <summary>
		/// 最小化按钮。
		/// </summary>
		private void Button_MinWindow(object sender, RoutedEventArgs e)
		{
			var win = Window.GetWindow(sender as DependencyObject) as Controls.FantomWindow;
			win.WindowState = WindowState.Minimized;
		}

		/// <summary>
		/// 双击切换。
		/// </summary>
		private void Title_DoubleClick(object sender, EventArgs e)
		{
			var win = Window.GetWindow(sender as DependencyObject) as Controls.FantomWindow;
			
			if (win.AllowDoubleClickResizeWindow == false)	return;

			// var textblock = win.Template.FindName("PART_debugTextBlock", win) as TextBlock;
			
			if (_lastTime == null)
			{
				_lastTime = DateTime.Now;
				_lastCursorPoint = Mouse.GetPosition(win);
			}
			else
			{
				var nowTime = DateTime.Now;
				var nowCursorPoint = Mouse.GetPosition(win);

				var delta = (nowTime - _lastTime.Value).Milliseconds;
				var distance = nowCursorPoint - _lastCursorPoint;
				// textblock.Text = $"{nowTime} \r\n{_lastTime.Value}\r\n{delta}\r\n{distance.Length}";
				if (delta < 800 && distance.Length < 5)
				{
					Button_MaxWindow(sender, null);
					_lastTime = null;
				}
				else
				{
					_lastTime = nowTime;
				}
				_lastCursorPoint = nowCursorPoint;

			}


		}

	}
}
