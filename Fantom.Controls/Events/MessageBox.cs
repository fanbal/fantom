﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using wpf = System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fantom.Controls.Events
{
	public partial class MessageBox : ResourceDictionary
	{
		private void PART_ok_Click(object sender, RoutedEventArgs e)
		{
			var btn = sender as wpf.Button;
			var win = Window.GetWindow(btn);
			win.Close();
		}

		private void MessageBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Escape)
			{
				var btn = sender as DependencyObject;
				var win = Window.GetWindow(btn);
				win.Close();
			}

		}

	}
}
