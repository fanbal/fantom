﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using wpf = System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fantom.Controls.Common
{
	public class MessageBox : Window
	{
		#region properties

		/// <summary>
		/// 消息框的信息。
		/// </summary>
		public string Message { get; set; }


		#endregion properties

		#region ctors
		private MessageBox()
		{
		}

		#endregion

		#region events

		
		#endregion

		#region static
		static MessageBox()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(MessageBox), new FrameworkPropertyMetadata(typeof(MessageBox)));
		}

		/// <summary>
		/// 显示。
		/// </summary>
		public static void Show(string message)
		{
			var msgbox = new MessageBox() { Message = message };
			msgbox.DataContext = msgbox;

			msgbox.WindowStartupLocation = WindowStartupLocation.CenterScreen;

			msgbox.ShowDialog();
			//textBlock.Text = message;
		}


		#endregion


		#region properties

		#endregion

	}
}
