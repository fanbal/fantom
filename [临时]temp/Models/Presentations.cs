﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using D = DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;

using Fantom.Builders;

namespace Fantom.Models
{
	/// <summary>
	/// 演示文稿集合。
	/// </summary>
	public class Presentations : ObservableCollection<IPresentation>, IPresentations
	{
		#region events
		public event PresentationsLoadingDocumentEventHandler OnLoadingSlide;
		#endregion

		#region properties

		internal HashSet<string> _filePathHashSet = new HashSet<string>();


		private IPresentation _currentPresentation = null;
		public IPresentation CurrentPresentation
		{
			get
			{
				if (_currentPresentation == null)
					return this.Items.FirstOrDefault();
				return _currentPresentation;
			}
			set { _currentPresentation = value; }
		}

		public IApplication Application { get; private set; } = null;

		public IFantomObject Parent { get; private set; } = null;

		#endregion

		#region ctors

		public Presentations(IFantomObject parent, IApplication application)
		{
			Application = application;
			Parent = parent;
		}

		#endregion

		#region methods

		public void Close(IPresentation presentation)
		{
			if (presentation != null)
				this.Remove(presentation);
		}

		public IPresentation Load(string uristr)
		{
			return Load(uristr, false);
		}

		public IPresentation Load(string uristr, bool allowEdit)
		{
			return PresentationLoader.Load(this, uristr, allowEdit);
		}

		// 载入自定义信息。
		// 自定义信息包括自定义Xml与自定义Tag。
		private void LoadCustomInfo
			(ICustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, SlidePart sldpart)
		{

			// 自定义列表。
			var customList = container.CustomXmlList as CustomXmlList;
			var tags = container.Tags; // 获得父对象的Tags容器并使用它，添加字段。

			customList.Application = Application;
			customList.Parent = container;

			if (xlist == null) return;

			// 遍历Xml节点。
			foreach (var customXmlPart in xlist)
			{

				// 处理自定义Xml节点。
				if (customXmlPart is P.CustomerData)
				{
					var xmlpart = CustomXmlBuilder.Build(
						customXmlPart as P.CustomerData, package, uridic) as CustomXml;
					xmlpart.Application = Application;
					xmlpart.Parent = customList;
					customList.Add(xmlpart);
				}

				// 处理自定义Tag节点。
				else if (customXmlPart is P.CustomerDataTags)
				{
					string rId = (customXmlPart as P.CustomerDataTags).Id;
					var t =
						from tagpair in sldpart.UserDefinedTagsParts
						where tagpair.Uri == uridic[rId]
						select tagpair;
					if (t.Count() != 0)
					{
						var taglist = t.First().TagList;
						foreach (P.Tag tag in taglist)
						{
							container.Tags.Add(tag.Name, tag.Val);
						}
					}
				}

			}
		}

		public IPresentation New()
		{
			string guid = Guid.NewGuid().ToString();
			_filePathHashSet.Add(guid);
			return PresentationBuilder.Build();
		}

		// 触发事件。
		public void RaiseLoadingSlideEvent(PresentationsLoadingState loadingState, int steps, int step)
		{
			OnLoadingSlide?.Invoke(this, new PresentationsLoadingArgs(loadingState, steps, step));
		}

		public void RaiseLoadingSlideEvent(PresentationsLoadingState loadingState, int steps, int step, string info)
		{
			OnLoadingSlide?.Invoke(this, new PresentationsLoadingArgs(loadingState, steps, step, info));
		}

		#endregion
	}
}
