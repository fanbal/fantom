﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	public class Media : IMedia
	{
		public Media(Uri uri, System.IO.Packaging.Package pack)
		{
			Uri = uri;

			var pk = pack.GetPart(Uri);
			var bytes = new List<byte>();
			var stream = pk.GetStream();
			int b = 0;

			while (b != -1)
			{
				b = stream.ReadByte();
				if (b == -1) break;
				bytes.Add((byte)b);
			}
			stream.Dispose();

			_bytes = bytes.ToArray();
		}


		public Uri Uri { get; }

		private byte[] _bytes;
		public byte[] Bytes => _bytes;

	}
}
