﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Fantom.Models
{
	/// <summary>
	/// 主题。
	/// </summary>
	public class SlideMasters : ObservableCollection<ISlideMaster>, ISlideMasters
	{
		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }
	}
}
