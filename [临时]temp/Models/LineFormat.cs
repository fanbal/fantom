﻿using System;
using System.Collections.Generic;
using System.Text;
using Fantom.Drawing;

namespace Fantom.Models
{
	public class LineFormat : FillFormat, ILineFormat
	{
		private Emu _thickness = Emu.Num0;
		public Emu Thickness { get => _thickness; set { _thickness = value; PropertyChangedInvoke(); } }


	}
}
