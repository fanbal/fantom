﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Fantom.Models
{
	public class Shapes : ObservableCollection<IShape>, IShapes
	{
		#region events

		public event SelectionEventHandler OnSelectionChanged;

		#endregion

		/// <summary>
		/// 用于管理图形的编号，用于实现组合内部的动画提取。
		/// </summary>
		public Dictionary<int, IShape> ShapeIndexDic = new Dictionary<int, IShape>();

		public IShape this[string shapeName] => (from shp in Items where shp.Name == shapeName select shp).First();

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public IEnumerable<ISelectable> Selection => from shp in Items where shp.IsSelected select shp;

		// TODO 待加入图形对象的添加模块。
		public IShape Add()
		{
			throw new NotImplementedException();
		}

		#region selection implement


		public void Select(ISelectable obj)
		{
			Select(new[] { obj });
		}

		public void Select(IEnumerable<ISelectable> objRange)
		{
			foreach (var item in this)
			{
				item.IsSelected = false;
			}

			foreach (var item in objRange)
			{
				item.IsSelected = true;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
		}


		public void SelectAdditional(ISelectable obj)
		{
			Select(new[] { obj });
		}

		public void SelectAdditional(IEnumerable<ISelectable> objRange)
		{
			foreach (var item in objRange)
			{
				item.IsSelected = true;
			}
			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
		}

		public void UnSelect(ISelectable obj)
		{
			UnSelect(new[] { obj });
		}

		public void UnSelect(IEnumerable<ISelectable> objRange)
		{

			foreach (var item in objRange)
			{
				item.IsSelected = false;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(objRange, new ISelectable[] { }));
		}

		public void SelectAll()
		{
			foreach (var item in this)
			{
				item.IsSelected = true;
			}
			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, Items));
		}

		public void UnSelectAll()
		{

			foreach (var item in this)
			{
				item.IsSelected = false;
			}

			OnSelectionChanged?.Invoke(this,  new SelectionEventArgs(Items, new ISelectable[] { }));
		}


		#endregion

	}
}
