﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	/// <summary>
	/// 调节控点对象。不同 <see cref="ShapePresetType"/> 类型的图形具有不同类型的控点，
	/// 对应描述的数据也存在差异，如 <see cref="ShapePresetType.RoundRectangle"/> 圆角矩形类型，
	/// 其具有一个控点，其描述值为圆角半径与最短边之间的比例。
	/// </summary>
	public class Adjustments : List<Emu>
	{
		public Adjustments():base()
		{

		}

		public Adjustments(params Emu[] p) : base(p)
		{

		}

		public Adjustments(Adjustments adjs):base(adjs)
		{

		}
	}
}
