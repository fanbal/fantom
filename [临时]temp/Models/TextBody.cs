﻿using System;
using System.Collections.ObjectModel;
using Fantom.Drawing;
namespace Fantom.Models
{
	public class TextBody : ObservableCollection<ITextParagraph>, ITextBody
	{
		// 默认填充。
		private static FillFormat _whiteFill = new FillFormat() { Color = new SolidColor(255, 255, 255) };
		private static FillFormat _blackFill = new FillFormat() { Color = new SolidColor(0, 0, 0) };


		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public string ReadOnlyText
		{
			get
			{
				var temp = string.Empty;
				foreach (var item in this.Items)
				{
					temp += item.ReadOnlyText + Environment.NewLine;
				}
				return temp;
			}
		}

		public IFillFormat Fill
		{
			get
			{
				foreach (var para in Items)
				{
					foreach (var textblock in para)
					{
						if (textblock.Fill.Color != null)
							return textblock.Fill;
					}
				}

				if ((Parent as IShape).IsTextBox)
					return _blackFill;
				else
					return _whiteFill;
			}
			set
			{
				;
			}
		}
		public Emu FontSize
		{
			get
			{
				foreach (var para in Items)
				{
					foreach (var textblock in para)
					{
						return textblock.FontSize;
					}
				}
				return Emu.FontSize1 * 18;
			}
			set
			{
				;
			}
		}
		public string FontFamily
		{
			get
			{
				foreach (var para in Items)
				{
					foreach (var textblock in para)
					{
						return textblock.FontFamily;
					}
				}
				return "微软雅黑";
			}
			set
			{
				;
			}
		}

		public ITextParagraph New()
		{
			throw new NotImplementedException();
		}
	}
}
