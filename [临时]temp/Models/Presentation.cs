﻿using System;
using Fantom.Drawing;
using OX = DocumentFormat.OpenXml;
using Pack = DocumentFormat.OpenXml.Packaging;

namespace Fantom.Models
{
	public class Presentation : EchoBaseObject, IPresentation
	{
		#region properties

		public event PresentationEventHandler Actived;
		public event PresentationEventHandler BeforeClose;
		public event PresentationEventHandler AfterClose;
		public event PresentationEventHandler BeforeSave;
		public event PresentationEventHandler AfterSave;
		public event PresentationEventHandler AllowEdit;
		public event PresentationSizeChangeEventHandler SlideSizeChanged;
		public event PresentationSizeChangeEventHandler NoteSizeChanged;

		public bool IsActived { get; set; } = false;

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public string Author { get; set; }

		public string Name { get; set; } = "untitle";

		public bool AllowedEdit { get; set; } = true;

		public string Path { get; set; }

		public ISlides Slides { get; set; } = new Slides();

		public ISlideMasters SlideMasters { get; set; } // TODO SlideMasters

		public INoteMasters NoteMasters { get; set; } // TODO MoteMasters

		public int RevisionCount { get; set; } = 0;


		public ITheme Theme { get; set; }


		private Size _slideSize = Size.Zero;
		public Size SlideSize
		{
			get => _slideSize;
			set
			{
				var oldsize = _slideSize;
				_slideSize = value;

				if (oldsize != value)
				{
					SlideSizeChanged?.Invoke(this, new PresentationSizeChangeEventArgs(oldsize, value));
					PropertyChangedInvoke();
				}

			}
		}

		private Size _noteSize = Size.Zero;
		public Size NoteSize
		{
			get => _noteSize;
			set
			{
				var oldsize = _slideSize;
				_noteSize = value;

				if (oldsize != value)
				{
					NoteSizeChanged?.Invoke(this, new PresentationSizeChangeEventArgs(oldsize, value));
					PropertyChangedInvoke();
				}

			}
		}

		public IMediaManager MediaManager { get; set; }


		#endregion

		#region ctors
		public Presentation()
		{

		}

		#endregion

		#region methods

		public override string ToString()
		{
			return string.Format("Type: {0}\nName: {1}\nAuthor: {2}\nRevision: {3}\n", GetType(), Name, Author, RevisionCount);

		}
		public void Activate()
		{
			if (!IsActived)
			{
				var parent = Parent as Presentations;

				var cntPres = parent.CurrentPresentation as Presentation;
				cntPres.IsActived = false;
				IsActived = true;
				parent.CurrentPresentation = this;
			}

			Actived?.Invoke(this, EventArgs.Empty);
		}

		public void Close()
		{
			BeforeClose?.Invoke(this, EventArgs.Empty);
			var parent = Parent as Presentations;
			if (IsActived)
				parent.CurrentPresentation = null;

			if (parent.Contains(this))
				parent.Remove(this);

			AfterClose?.Invoke(this, EventArgs.Empty);
		}

		public void Save()
		{
			if (RevisionCount == 0)
				throw new ArgumentException("警告：新建演示文稿未指定存档目录。");

			if (!AllowedEdit) return;//只读下不进行存档
			Save(Path);
		}

		public void Save(string saveAsPath)
		{
			if (!AllowedEdit && Path == saveAsPath)
				return;

			BeforeSave?.Invoke(this, EventArgs.Empty);

			var newpresdoc = Pack.PresentationDocument.Create
				(saveAsPath, OX.PresentationDocumentType.Presentation);

			var prespart = newpresdoc.AddPresentationPart();

			newpresdoc.Save();

			newpresdoc.Close();

			AfterSave?.Invoke(this, EventArgs.Empty);
			throw new NotImplementedException();
		}

		public void PremitEdit()
		{
			if (AllowedEdit == false)
			{
				AllowedEdit = true;
				AllowEdit?.Invoke(this, EventArgs.Empty);
			}
		}


		#endregion
	}
}
