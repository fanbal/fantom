﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	/// <summary>
	/// 时间轴对象。
	/// </summary>
	public class TimeLine : ITimeLine
	{
		public IAnimationSequence MainSequence { get; set; } = new AnimationSequence();

		public IList<IAnimationSequence> InteractiveSequences { get; set; } = new List<IAnimationSequence>();

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }
	}
}
