﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Fantom.Drawing;
using System.Runtime.CompilerServices;
using Fantom.Effect;

namespace Fantom.Models
{
	public class GroupShape : ObservableCollection<IShape>, IGroupShape
	{
		#region events
		public event SelectableEventHandler OnSelect;
		public event SelectableEventHandler LostSelect;
		public new event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region properties
		public bool IsTextBox => false;

		public IAdjustments Adjustments => null;

		public ShapePresetType PresetType => ShapePresetType.None;

		public ILineFormat Line { get; } = null;
		public IFillFormat Fill { get; } = null;

		public Shadow Shadow { get; set; } = null;

		public int Id { get; set; }

		public string Name { get; set; }

		public int ZOrderPosition { get; set; }

		public ShapeType Type => ShapeType.Group;

		public ILocks Locks { get; set; }

		public RectTransform Transform { get; set; }

		public ITextBody TextBody => null;

		public string ReadOnlyText => string.Empty;

		public bool Visibility
		{
			get => _visibility;
			set
			{
				if (_visibility != value)
				{
					_visibility = value;
					PropertyChangedInvoke();
				}

			}
		}
		private bool _visibility = true;

		protected void PropertyChangedInvoke([CallerMemberName] string name = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
		}



		public bool IsSelected
		{
			get => _isSelected;
			set
			{
				var temp = _isSelected;

				if (Parent != null)
				{
					if (value)
					{
						(Parent as Shapes).UnSelectAll();
						(Parent as Shapes).Select(this as IShape);
						OnSelect?.Invoke(this.Parent as IShapes, new[] { this });
					}

					else
					{
						(Parent as Shapes).UnSelect(this as IShape);
						LostSelect?.Invoke(this.Parent as IShapes, new[] { this });
					}
				}

				PropertyChangedInvoke();
			}
		}
		private bool _isSelected = false;

		public ICustomXmlList CustomXmlList { get; set; }

		public Tags Tags { get; set; }

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }
		#endregion

		#region methods
		public void ZOrder(ZOrderCommandEnum zordercmd)
		{
			throw new System.NotImplementedException();
		}

		public void Select()
		{
			if (Locks.NoSelection) return;

			IsSelected = true;

		}

		public void UnSelect()
		{
			if (Locks.NoSelection) return;

			IsSelected = false;
		}

		#endregion

		#region ctors
		public GroupShape(Shape shape)
		{
			this.Application = shape.Application;
			this.Parent = shape.Parent;
			this.Locks = shape.Locks;
			this.Name = shape.Name;
			this.Transform = shape.Transform;
			this.CustomXmlList = shape.CustomXmlList;
			this.Tags = shape.Tags;
			this.Visibility = shape.Visibility;
			this.Id = shape.Id;

			this.Transform.Shape = this;
			(this.Locks as Locks).Shape = this;

			this.Shadow = shape.Shadow;
		}
		#endregion

	}
}
