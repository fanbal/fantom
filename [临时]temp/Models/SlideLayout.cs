﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	/// <summary>
	/// 幻灯片母版的子样式。
	/// </summary>
	public class SlideLayout : EchoBaseObject, ISlideLayout
	{
		#region properties

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public string Name { get => _name; set { _name = value; PropertyChangedInvoke(); } }
		private string _name = string.Empty;

		public IShapes Shapes { get; } = new Shapes();

		public int Index => (Parent as SlideMaster).Layouts.IndexOf(this);

		public Tags Tags { get; private set; } = new Tags();

		public bool IsSelected
		{
			get => _isSelected;
			set { _isSelected = value; PropertyChangedInvoke(); }
		}
		private bool _isSelected = false;

		public ICustomXmlList CustomXmlList { get; } = new CustomXmlList();

		public ITimeLine TimeLine { get; set; } = null;

		#endregion

		#region methods
		public override string ToString()
		{
			return string.Format("SlideId: {0}", Index);
		}

		#endregion

	}
}
