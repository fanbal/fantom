﻿using System.Collections.ObjectModel;

namespace Fantom.Models
{
	public class CustomXmlList : ObservableCollection<ICustomXml>, ICustomXmlList
	{
		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }
	}
}
