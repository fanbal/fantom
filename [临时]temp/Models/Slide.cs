﻿namespace Fantom.Models
{
	public class Slide : EchoBaseObject, ISlide
	{

		#region properties

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public string Name { get => _name; set { _name = value; PropertyChangedInvoke(); } }
		private string _name = string.Empty;

		public IShapes Shapes { get; } = new Shapes();

		public int Index => (Parent as Slides).IndexOf(this);

		public Tags Tags { get; private set; } = new Tags();

		public bool IsSelected
		{
			get => _isSelected;
			set { _isSelected = value; PropertyChangedInvoke(); }
		}
		private bool _isSelected = false;

		public ICustomXmlList CustomXmlList { get; } = new CustomXmlList();

		public ITimeLine TimeLine { get; set; } = null;

		public ISlideLayout Layout { get => _layout; set { _layout = value; PropertyChangedInvoke(); } }
		private ISlideLayout _layout = null;

		#endregion

		#region methods
		public override string ToString()
		{
			return string.Format("SlideId: {0}", Index);
		}

		public void Select()
		{
			IsSelected = true;
		}

		public void UnSelect()
		{
			IsSelected = false;
		}
		#endregion

	}
}
