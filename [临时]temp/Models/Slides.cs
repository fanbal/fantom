﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fantom.Builders;

namespace Fantom.Models
{
	public class Slides : ObservableCollection<ISlide>, ISlides
	{
		#region events

		public event SelectionEventHandler OnSelectionChanged;

		#endregion

		#region properties

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public IEnumerable<ISlide> SelectedSlides => from sld in this.Items where sld.IsSelected select sld;

		public IShapes Shapes { get; private set; } = new Shapes();

		public IEnumerable<ISelectable> Selection => from sld in Items
													 where sld.IsSelected
													 select sld;



		#endregion

		#region ctors

		#endregion

		#region methods

		public ISlide New()
		{
			var sld = BaseSlideBuilder.BuildSlide();
			return sld;
		}


		#region selection implement

		public void Select(ISelectable obj)
		{
			Select(new[] { obj });
		}

		public void Select(IEnumerable<ISelectable> objRange)
		{
			foreach (var item in this)
			{
				item.IsSelected = false;
			}

			foreach (var item in objRange)
			{
				item.IsSelected = true;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
		}


		public void SelectAdditional(ISelectable obj)
		{
			Select(new[] { obj });
		}

		public void SelectAdditional(IEnumerable<ISelectable> objRange)
		{
			foreach (var item in objRange)
			{
				item.IsSelected = true;
			}
			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
		}

		public void UnSelect(ISelectable obj)
		{
			UnSelect(new[] { obj });
		}

		public void UnSelect(IEnumerable<ISelectable> objRange)
		{

			foreach (var item in objRange)
			{
				item.IsSelected = false;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(objRange, new ISelectable[] { }));
		}

		public void SelectAll()
		{
			foreach (var item in this)
			{
				item.IsSelected = true;
			}
			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, Items));
		}

		public void UnSelectAll()
		{

			foreach (var item in this)
			{
				item.IsSelected = false;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(Items, new ISelectable[] { }));
		}


		#endregion

		#endregion
	}
}
