﻿using System;
using System.Collections.ObjectModel;

namespace Fantom.Models
{
	public class TextParagraph : ObservableCollection<ITextBlock>, ITextParagraph
	{
		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public ITextBlock New()
		{
			throw new NotImplementedException();
		}

		public string ReadOnlyText
		{
			get
			{
				var temp = string.Empty;
				foreach (var item in this.Items)
				{
					temp += item.Text;
				}
				return temp;
			}
		}
	}
}
