﻿namespace Fantom.Models
{
	/// <summary>
	/// 文本块。
	/// </summary>
	public class TextBlock : EchoBaseObject, ITextBlock
	{
		private string _text;
		public string Text
		{
			get => _text;
			set
			{
				_text = value;
				PropertyChangedInvoke();
			}
		}

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public IFillFormat Fill { get; set; } = new FillFormat();

		public Emu FontSize { get; set; } = Emu.FontSize1 * 18;
		public string FontFamily { get; set; } = "微软雅黑";

		public TextBlock(string text)
		{
			_text = text;
		}

	}
}
