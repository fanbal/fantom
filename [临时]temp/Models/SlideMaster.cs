﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Fantom.Models
{
	/// <summary>
	/// 演示文稿的母版对象。
	/// </summary>
	public class SlideMaster : EchoBaseObject, ISlideMaster
	{

		public event SelectionEventHandler OnSelectionChanged;

		public IApplication Application => throw new NotImplementedException();

		public IFantomObject Parent => throw new NotImplementedException();

		public string Name { get => _name; set { _name = value; PropertyChangedInvoke(); } }
		private string _name = string.Empty;

		public IList<ISlideLayout> Layouts => new List<ISlideLayout>();

		public ITheme Theme { get; set; }

		public IEnumerable<ISelectable> Selection => from layout in Layouts
													 where layout.IsSelected
													 select layout;

		#region selection implement

		public void Select(ISelectable obj)
		{
			Select(new[] { obj });
		}

		public void Select(IEnumerable<ISelectable> objRange)
		{
			foreach (var item in Layouts)
			{
				item.IsSelected = false;
			}

			foreach (var item in objRange)
			{
				item.IsSelected = true;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
		}


		public void SelectAdditional(ISelectable obj)
		{
			Select(new[] { obj });
		}

		public void SelectAdditional(IEnumerable<ISelectable> objRange)
		{
			foreach (var item in objRange)
			{
				item.IsSelected = true;
			}
			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
		}

		public void UnSelect(ISelectable obj)
		{
			UnSelect(new[] { obj });
		}

		public void UnSelect(IEnumerable<ISelectable> objRange)
		{

			foreach (var item in objRange)
			{
				item.IsSelected = false;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(objRange, new ISelectable[] { }));
		}

		public void SelectAll()
		{
			foreach (var item in Layouts)
			{
				item.IsSelected = true;
			}
			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, Layouts));
		}

		public void UnSelectAll()
		{

			foreach (var item in Layouts)
			{
				item.IsSelected = false;
			}

			OnSelectionChanged?.Invoke(this, new SelectionEventArgs(Layouts, new ISelectable[] { }));
		}


		#endregion
	}
}
