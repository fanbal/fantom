﻿using Fantom.Drawing;
using Fantom.Effect;

namespace Fantom.Models
{
	public class Shape : EchoBaseObject, IShape
	{
		#region events
		public event SelectableEventHandler OnSelect;
		public event SelectableEventHandler LostSelect;
		#endregion

		#region properties

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public bool IsSelected
		{
			get => _isSelected;
			set
			{
				_isSelected = value;
				PropertyChangedInvoke();
			}
		}
		private bool _isSelected = false;

		public int Id { get; set; }
		
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				PropertyChangedInvoke();
			}
		}
		private string _name = "unknown";

		public int ZOrderPosition => (Parent as Shapes).IndexOf(this);

		public ShapeType Type { get; set; } = ShapeType.Common;

		public ILocks Locks { get; set; }

		public RectTransform Transform { get; set; }

		public ITextBody TextBody { get; set; } = null;

		public string ReadOnlyText
		{
			get
			{
				if (Type == ShapeType.Common)
					return TextBody.ReadOnlyText;
				return "";
			}
		}

		public ICustomXmlList CustomXmlList { get; set; } = new CustomXmlList();

		public Tags Tags { get; set; } = new Tags();

		
		public bool Visibility
		{
			get => _visibility;
			set
			{
				if (_visibility != value)
				{
					_visibility = value;
					PropertyChangedInvoke();
				}

			}
		}
		private bool _visibility = true;

		public ILineFormat Line { get; set; } = new LineFormat();
		public IFillFormat Fill { get; set; } = new FillFormat();

		public Shadow Shadow { get; set; } = null;

		public IAdjustments Adjustments { get; set; }

		public ShapePresetType PresetType { get; set; }

		public bool IsTextBox { get; set; } = false;

		#endregion

		#region ctors
		public Shape()
		{
			Transform = new RectTransform(this);
			Locks = new Locks() { Shape = this };
		}
		#endregion

		#region methods

		public override string ToString()
		{
			return string.Format("Type: {1, -10},Name: {0,-20}", Name.Shorten(), Type.ToString());
		}



		public void ZOrder(ZOrderCommandEnum zordercmd)
		{
			if (Locks.NoMove) return;

			int id = ZOrderPosition;
			var shps = Parent as Shapes;
			switch (zordercmd)
			{
				case ZOrderCommandEnum.BringForward:
					shps.Move(id, id + 1);
					break;
				case ZOrderCommandEnum.BringToFront:
					shps.Move(id, shps.Count - 1);
					break;
				case ZOrderCommandEnum.msoSendBackward:
					shps.Move(id, id - 1);
					break;
				case ZOrderCommandEnum.msoSendToBack:
					shps.Move(id, 0);
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// 触发渲染更新事件，更新Transform。
		/// </summary>
		public void RaiseRenderUpdate()
		{
			PropertyChangedInvoke("Transform");
		}


		#endregion
	}
}
