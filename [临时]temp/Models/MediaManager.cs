﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	public class MediaManager : IMediaManager
	{
		private Dictionary<Uri, IMedia> _mediaDic = new Dictionary<Uri, IMedia>();

		public IReadOnlyDictionary<Uri, IMedia> LocalMediaDic => _mediaDic;

		private System.IO.Packaging.Package _pack;

		public MediaManager(System.IO.Packaging.Package pack)
		{
			_pack = pack;
		}

		public IMedia CreateMedia(Uri uri)
		{
			if (!_mediaDic.ContainsKey(uri))
			{
				var media = new Media(uri, _pack);
				_mediaDic.Add(uri, media);

			}
			return _mediaDic[uri];

		}
	}
}
