﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	public class AnimationEffect : EchoList<AnimationBehavior>
	{
		public string Name { get; set; }
		
		public Shape Shape { get; set; } = null;
		public Emu Duration { get; set; } = Emu.Seconds1 * 2;
		public Emu Delay { get; set; } = Emu.Seconds0;
		public bool AutoReverse { get; set; } = false;
		public Emu Accelerate { get; set; } = Emu.Num0;
		public Emu Decelerate { get; set; } = Emu.Num0;
		public Emu BounceEnd { get; set; } = Emu.Num0;
		public Emu RepeatCount { get; set; } = Emu.Double1;
		public bool IsRewindAtEnd { get; set; } = false;
	}
}
