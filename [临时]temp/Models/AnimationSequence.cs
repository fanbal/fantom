﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	public class AnimationSequence : List<IAnimationFragment>, IAnimationSequence 
	{
		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		public string Name { get; set; }

	}
}
