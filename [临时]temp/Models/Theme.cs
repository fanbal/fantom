﻿using System.Collections.Generic;
using Fantom.Drawing;

namespace Fantom.Models
{
	public class Theme : EchoBaseObject, ITheme
	{
		#region properties
		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }

		// 颜色集合。
		internal Dictionary<ThemeColorType, BaseColor> _colors = new Dictionary<ThemeColorType, BaseColor>();
		public IReadOnlyDictionary<ThemeColorType, BaseColor> Colors => _colors;


		#endregion

	}
}
