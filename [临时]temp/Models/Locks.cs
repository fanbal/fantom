﻿namespace Fantom.Models
{
	public class Locks : ILocks
	{
		/// <summary>
		/// 父级图形对象。
		/// </summary>
		public IShape Shape { get; set; }

		public bool NoGrouping { get; set; } = false;

		private bool _noSelect = false;
		public bool NoSelection
		{
			get => _noSelect;
			set
			{
				_noSelect = value;
				(Shape as Shape).IsSelected = false;
			}
		}
		public bool NoRotation { get; set; } = false;
		public bool NoChangeAspect { get; set; } = false;
		public bool NoMove { get; set; } = false;
		public bool NoResize { get; set; } = false;
		public bool NoEditPoints { get; set; } = false;
		public bool NoAdjustHandles { get; set; } = false;
		public bool NoChangeArrowheads { get; set; } = false;
		public bool NoChangeShapeType { get; set; } = false;
		public bool NoTextEdit { get; set; } = false;
		public bool NoUnGrouping { get; set; } = false;
		public bool NoCorp { get; set; } = false;

		public void LockAll()
		{
			bool state = true;
			NoGrouping = state;
			NoSelection = state;
			NoRotation = state;
			NoChangeAspect = state;
			NoMove = state;
			NoResize = state;
			NoEditPoints = state;
			NoAdjustHandles = state;
			NoChangeArrowheads = state;
			NoChangeShapeType = state;
			NoTextEdit = state;
			NoUnGrouping = state;
			NoCorp = state;
		}

		public void UnlockAll()
		{
			bool state = false;
			NoGrouping = state;
			NoSelection = state;
			NoRotation = state;
			NoChangeAspect = state;
			NoMove = state;
			NoResize = state;
			NoEditPoints = state;
			NoAdjustHandles = state;
			NoChangeArrowheads = state;
			NoChangeShapeType = state;
			NoTextEdit = state;
			NoUnGrouping = state;
			NoCorp = state;
		}
	}
}
