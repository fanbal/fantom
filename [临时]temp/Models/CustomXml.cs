﻿using System.Xml.Linq;

namespace Fantom.Models
{

	/// <summary>
	/// 自定义Xml对象，用于安全可靠地存储用户的自定义信息。
	/// </summary>
	public class CustomXml : ICustomXml
	{
		public XDocument XDocument { get; set; }

		public IApplication Application { get; set; }

		public IFantomObject Parent { get; set; }
	}
}
