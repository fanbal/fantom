﻿using Fantom.Models;

namespace Fantom
{
	public class Application : IApplication
	{

		string IApplication.Version => Properties.Resources.Version;

		public string Detail => Properties.Resources.Detail;

		public IPresentations Presentations { get; private set; }

		IApplication IFantomObject.Application => this;

		public IFantomObject Parent => this;

		public Application()
		{
			Presentations = new Presentations(this, this);
		}

		/// <summary>
		/// 静态创建应用对象实例。
		/// </summary>
		/// <returns><see cref="IApplication"/> 应用 对象</returns>
		public static IApplication Create()
		{
			return new Application();
		}
	}
}
