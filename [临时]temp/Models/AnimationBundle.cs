﻿using Fantom.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Models
{
	public class AnimationBundle : EchoList<AnimationBase>
	{
		
		public string Name { get; set; }

	}
}
