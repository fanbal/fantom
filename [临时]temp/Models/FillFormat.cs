﻿using System;
using System.Collections.Generic;
using System.Text;
using Fantom.Drawing;

namespace Fantom.Models
{
	/// <summary>
	/// 填充格式，包括图片，纯色等。
	/// </summary>
	public class FillFormat:EchoBaseObject, IFillFormat
	{
		private BaseColor _color;

		/// <summary>
		/// 填充的颜色，具体类型可以是 <see cref="SolidColor"/>, <see cref="SchemeColor"/>, <see cref="BitmapColor"/>。
		/// </summary>
		public BaseColor Color { get => _color; set { _color = value; PropertyChangedInvoke(); } }
	}
}
