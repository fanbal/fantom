﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Effect
{
	/// <summary>
	/// 阴影的种类。
	/// </summary>
	public enum ShadowType
	{
		/// <summary>
		/// 内部阴影。
		/// </summary>
		Inner,

		/// <summary>
		/// 外部阴影。
		/// </summary>
		Outer,

		/// <summary>
		/// 预设阴影。
		/// </summary>
		Preset,
	}
}
