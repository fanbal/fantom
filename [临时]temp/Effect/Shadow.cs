﻿using System;
using System.Collections.Generic;
using System.Text;
using Fantom.Drawing;

namespace Fantom.Effect
{
	/// <summary>
	/// 阴影效果。
	/// </summary>
	public abstract class Shadow : EchoBaseObject
	{
		#region properties

		/// <summary>
		/// 阴影的模糊磅数。PowerPoint 默认的编辑上限为 1270000 emu (100 磅)，但是在 OpenXml 中上不封顶。
		/// </summary>
		public Emu Blur { get => _blur; set { _blur = value; PropertyChangedInvoke(); } }
		protected Emu _blur = Emu.Num0;

		/// <summary>
		/// 阴影颜色。
		/// </summary>
		public BaseColor ForeColor { get => _foreColor; set { _foreColor = value; PropertyChangedInvoke(); } }
		protected BaseColor _foreColor = SolidColor.FromRGB(0, 0, 0);

		/// <summary>
		/// 是否在图形没有填充时仍然能显示对应的阴影。
		/// </summary>
		public bool IsObscured { get => _isObscured; set { _isObscured = value; PropertyChangedInvoke(); } }
		protected bool _isObscured = false;



		/// <summary>
		/// 阴影的偏移方向量。
		/// </summary>
		public Emu Direction { get => _direction; set { _direction = value; PropertyChangedInvoke(); } }
		protected Emu _direction = Emu.Num0;

		/// <summary>
		/// 阴影的偏移方向量。
		/// </summary>
		public Emu Distance { get => _distance; set { _distance = value; PropertyChangedInvoke(); } }
		protected Emu _distance = Emu.Num0;

		/// <summary>
		/// 阴影是否与图形一起旋转。
		/// </summary>
		public bool IsRotateWithShape { get => _isRotateWithShape; set { _isRotateWithShape = true; PropertyChangedInvoke(); } }
		protected bool _isRotateWithShape = false;

		/// <summary>
		/// 透明度。 PowerPoint 中允许范围在 0% ~ 100%，0% 表示完全可见，100% 表示完全透明，默认值为 100%。
		/// </summary>
		public Emu Transparency { get => _transparency; set { _transparency = value; PropertyChangedInvoke(); } }
		protected Emu _transparency = Emu.Percent100; 

		/// <summary>
		/// 阴影是否可见。
		/// </summary>
		public bool IsVisible { get => _isVisible; set { _isVisible = value; PropertyChangedInvoke(); } }
		protected bool _isVisible = true;

		/// <summary>
		/// 预设类型，一旦创建就无法被修改。
		/// </summary>
		public ShadowPresetType PresetType { get; }

		#endregion

		#region ctors

		public Shadow(ShadowPresetType presetType)
		{
			PresetType = presetType;
		}

		internal Shadow()
		{
			PresetType = ShadowPresetType.Custom;
		}

		#endregion

		#region methods

		
		#endregion

	}
}
