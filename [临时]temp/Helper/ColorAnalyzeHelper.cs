﻿using D = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml;
using Fantom.Drawing;

namespace Fantom.Helper
{

	/// <summary>
	/// 颜色分析。
	/// </summary>
	public static class ColorAnalyzeHelper
	{

		/// <summary>
		/// 获得颜色对象。
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		public static ColorBrushType GetColorBrushType(D.Color2Type element)
		{
			if (element.PresetColor != null)
				return ColorBrushType.Preset;
			if (element.HslColor != null)
				return ColorBrushType.HSL;
			if (element.RgbColorModelHex != null)
				return ColorBrushType.SRGB;
			if (element.SystemColor != null)
				return ColorBrushType.System;

			return ColorBrushType.Unknown;
		}

		// TODO 需要重构颜色系统。 
		public static BaseColorBrush GetColorBrush(OpenXmlElement element)
		{
			return null;
		}

	}

	/// <summary>
	/// 主题色的解析。
	/// </summary>
	public static class ThemeColorTypeHelper
	{
		public static ThemeColorType GetColorType(string str)
		{
			switch (str)
			{
				case "accent1":
					return ThemeColorType.Accent1;
				case "accent2":
					return ThemeColorType.Accent2;
				case "accent3":
					return ThemeColorType.Accent3;
				case "accent4":
					return ThemeColorType.Accent4;
				case "accent5":
					return ThemeColorType.Accent5;
				case "accent6":
					return ThemeColorType.Accent6;
				case "tx1":
				case "dk1":
					return ThemeColorType.Dark1;
				case "tx2":
				case "dk2":
					return ThemeColorType.Dark2;
				case "bg1":
				case "lt1":
					return ThemeColorType.Light1;
				case "bg2":
				case "lt2":
					return ThemeColorType.Light2;
				case "hlink":
					return ThemeColorType.Hyperlink;
				case "folHlink":
					return ThemeColorType.FollowedHyperlink;
				default:
					return ThemeColorType.Unknown;
			}
		}
	}


}
