﻿using System.Collections.Generic;

using Fantom.Models;
namespace Fantom.Helper
{
	internal static class PresetTypeHelper
	{

		private static Dictionary<ShapePresetType, Emu[]> _typePresetAdjustmentsDic = new Dictionary<ShapePresetType, Emu[]>();

		public static ShapePresetType GetPresetType(string presetName)
		{
			if (presetName == null) return ShapePresetType.Rectangle;
			switch (presetName)
			{
				case "Rectangle": return ShapePresetType.Rectangle;
				case "RoundRectangle":
					return ShapePresetType.RoundRectangle;
				case "Ellipse":
					return ShapePresetType.Ellipse;
				default:
					return ShapePresetType.Rectangle;
			}
		}

		public static Emu[] GetDefaultAdjustmentsArray(ShapePresetType type)
		{
			if (_typePresetAdjustmentsDic.ContainsKey(type)) return _typePresetAdjustmentsDic[type];
			else return null;
		}

		public static Adjustments GetDefaultAdjustments(ShapePresetType type)
		{
			var arr = GetDefaultAdjustmentsArray(type);
			if(arr != null)
				return new Adjustments(arr);
			else
				return new Adjustments();
		}


		private static void AddKey(ShapePresetType type, params Emu[] p)
		{
			if (p == null || p.Length == 0) return;
			_typePresetAdjustmentsDic.Add(type, p);
		}

		static PresetTypeHelper()
		{
			AddKey(ShapePresetType.RoundRectangle, 50000);
		}
	}
}
