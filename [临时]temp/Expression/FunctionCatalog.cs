﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
	/// <summary>
	/// 函数清单。
	/// </summary>
	public static class FunctionCatalog
	{

		private static Dictionary<string, FunctionNode> _functionDic = new Dictionary<string, FunctionNode>();

		private static Random _rand = new Random();

		private static void AddFunc(string name, int para = 2, FunctionNode.FuncHandler func = null)
		{
			_functionDic.Add(name, new FunctionNode() { Name = name, ParameterCount = para, Func = func });
		}

		/// <summary>
		/// 函数清单目录，您可以在此处注册自定义函数。
		/// </summary>
		public static Dictionary<string, FunctionNode> Functions => _functionDic;

		static FunctionCatalog()
		{
			AddFunc("rand", 0, (n1, n2, n3, n4, n5, n6) => _rand.NextDouble());
			AddFunc("sin", 1, (num, n2, n3, n4, n5, n6) => Math.Sin(num));
			AddFunc("cos", 1, (num, n2, n3, n4, n5, n6) => Math.Cos(num));
			AddFunc("hello", 2, (num1, num2, n3, n4, n5, n6) => num1 + num2);
			AddFunc("fanbal", 2, (num1, num2, n3, n4, n5, n6) => num1 * num2);
			AddFunc("test", 6, (num1, num2, num3, num4, num5, num6) => num1 + num2);

		}
	}
}
