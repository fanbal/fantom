﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
	[Flags]
	public enum MetaNodeType : int
	{
		/// <summary>
		/// 错误节点，可能是因为小数点不合法的问题。
		/// </summary>
		Error = 00,

		/// <summary>
		/// 空节点，表示一个括号域，具体在关键值中。
		/// </summary>
		Empty = 01,

		/// <summary>
		/// 操作符。
		/// </summary>
		Operator = 0x10,

		/// <summary>
		/// 单目操作符。一般地，我们将单目操作符开头的树视为一个数字。
		/// </summary>
		OperatorSingle = 0x11,

		/// <summary>
		/// 双目操作符。
		/// </summary>
		OperatorBinary = 0x12,

		/// <summary>
		/// 单值类型，单值类型不允许有子节点。
		/// </summary>
		Value = 04,

		/// <summary>
		/// 数字，如0, 1，该节点只包含正值，前缀符号由单目运算符单独表示。
		/// </summary>
		ValueNum = 05,

		/// <summary>
		/// 常量类型，如 pi、e 等。
		/// </summary>
		ValueConst = 06,

		/// <summary>
		/// 变量类型，如时间差值 $。
		/// </summary>
		ValueVar = 07,

		/// <summary>
		/// 函数类型。
		/// </summary>
		Function = 08,

		/// <summary>
		/// 无参函数。
		/// </summary>
		FunctionPara0 = 09,

		/// <summary>
		/// 一元函数。
		/// </summary>
		FunctionPara1 = 0x0A,

		/// <summary>
		/// 二元函数。
		/// </summary>
		FunctionPara2 = 0x0B,

		/// <summary>
		/// 三元函数。
		/// </summary>
		FunctionPara3 = 0x0C,

		/// <summary>
		/// 四元函数。
		/// </summary>
		FunctionPara4 = 0x0D,

		/// <summary>
		/// 五元函数。
		/// </summary>
		FunctionPara5 = 0x0E,

		/// <summary>
		/// 六元函数。
		/// </summary>
		FunctionPara6 = 0x0F,


	}
}
