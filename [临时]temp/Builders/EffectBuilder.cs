﻿using System;
using System.Collections.Generic;
using System.Text;
using O = DocumentFormat.OpenXml;
using P = DocumentFormat.OpenXml.Presentation;
using D = DocumentFormat.OpenXml.Drawing;
using Fantom.Effect;
using Fantom.Drawing;
using Fantom.Primitive;

namespace Fantom.Builders
{
	/// <summary>
	/// 效果载入相关方法。
	/// </summary>
	internal static class EffectBuilder
	{
		/// <summary>
		/// 内部阴影
		/// </summary>
		public static Shadow BuildShadow(D.InnerShadow shadow, ColorLoadArgs loadArgs)
		{
			var sh = new InnerShadow()
			{
				Blur = shadow.BlurRadius ?? 0,
				Direction = shadow.Direction ?? 0,
				Distance = shadow.Distance ?? 0,
				ForeColor = ColorLoader.GetColorFromNodeWithColor(shadow, loadArgs),
			};

			return sh;
		}

		public static Shadow BuildShadow(D.OuterShadow shadow, ColorLoadArgs loadArgs)
		{
			var sh = new OuterShadow()
			{
				Blur = shadow.BlurRadius ?? 0,
				Direction = shadow.Direction ?? 0,
				Distance = shadow.Distance ?? 0,
				Alignment = AlignmentHelper.GetAlignment(shadow.Alignment),
				SizeX = shadow.HorizontalRatio ?? Emu.Percent100,
				SizeY = shadow.VerticalRatio ?? Emu.Percent100,
				SkewX = shadow.HorizontalSkew ?? Emu.Percent100,
				SkewY = shadow.VerticalSkew ?? Emu.Percent100,
				ForeColor = ColorLoader.GetColorFromNodeWithColor(shadow, loadArgs),
				IsRotateWithShape = shadow.RotateWithShape ?? false,
			};

			return sh;
		}

		public static Shadow BuildShadow(D.PresetShadow shadow, ColorLoadArgs loadArgs)
		{
			throw new NotImplementedException("在实际 PPT 中无法找到该节点。该节点的解析暂未实现。");
		}


	}
}
