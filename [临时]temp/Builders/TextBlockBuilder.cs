﻿using System;
using Fantom.Drawing;
using PK = DocumentFormat.OpenXml.Packaging;
using D = DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;
using Fantom.Models;
using Fantom.Primitive;

namespace Fantom.Builders
{
	/// <summary>
	/// 解析run。
	/// </summary>
	internal static class TextBlockBuilder
	{
		[Obsolete("过时的方法", true)]
		public static ITextBlock Build(PK.SlidePart sldpart, D.Run element, Theme theme, IMediaManager mediaManager)
		{
			var tb = new TextBlock(element.Text.InnerText);

			var rpr = element.RunProperties;

			tb.Fill.Color = ColorLoader.GetCustomFill(sldpart, theme, rpr, mediaManager);

			SetFontSize(tb, rpr);

			SetFontFamily(tb, rpr);

			return tb;
		}

		public static ITextBlock Build( D.Run element, ColorLoadArgs loadArgs)
		{
			var tb = new TextBlock(element.Text.InnerText);

			var rpr = element.RunProperties;

			tb.Fill.Color = ColorLoader.GetCustomFill(rpr, loadArgs);

			SetFontSize(tb, rpr);

			SetFontFamily(tb, rpr);

			return tb;
		}

		private static void SetFontSize(TextBlock tb, D.RunProperties rpr)
		{
			if (rpr.FontSize != null)
				tb.FontSize = rpr.FontSize.Value;
		}

		// 设置并覆盖性地导入字体样式
		private static void SetFontFamily(TextBlock tb, D.RunProperties rpr)
		{
			SetFontFamily<D.LatinFont>(tb, rpr);
			SetFontFamily<D.EastAsianFont>(tb, rpr);
			//SetFontFamily<D.BulletFont>(tb, rpr);
			//SetFontFamily<D.SymbolFont>(tb, rpr);
			//SetFontFamily<D.ComplexScriptFont>(tb, rpr);

		}

		private static void SetFontFamily<T>(TextBlock tb, D.RunProperties rpr) where T : D.TextFontType
		{
			var font = rpr.GetFirstChild<T>();
			if (font == null) return;
			tb.FontFamily = font.Typeface;

		}

		public static ITextBlock Build()
		{
			return new TextBlock("");
		}

		public static D.Run Export(ITextBlock model)
		{
			throw new NotImplementedException();
		}


	}
}
