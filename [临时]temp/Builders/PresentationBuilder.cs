﻿using System;
using System.IO;
using OX = DocumentFormat.OpenXml;
using PK = DocumentFormat.OpenXml.Packaging;
using Fantom.Drawing;
using Fantom.Models;
namespace Fantom.Builders
{
	/// <summary>
	/// 演示文稿构建器。
	/// </summary>
	internal static class PresentationBuilder
	{
		/// <summary>
		/// 用于构建演示文稿。
		/// </summary>
		public static IPresentation Build
			(PK.PresentationDocument presdoc, string packagePath, bool allowEdit)
		{
			var package = presdoc.Package;
			var prespart = presdoc.PresentationPart.Presentation;
			var prespr = package.PackageProperties;
			var author = prespr.Creator;
			var revision = int.Parse(prespr.Revision);
			var name = Path.GetFileNameWithoutExtension(packagePath);

			var sldsize = prespart.SlideSize;

			var pres = new Presentation()
			{
				Path = packagePath,
				Name = name,
				AllowedEdit = allowEdit,
				Author = author,
				RevisionCount = revision,
				SlideSize = new Size(sldsize.Cx.Value, sldsize.Cy.Value),
				MediaManager = new MediaManager(package),
			};

			return pres;
		}

		/// <summary>
		/// 用于构建演示文稿。
		/// </summary>
		public static IPresentation Build(string packagePath, bool allowEdit)
		{
			var presdoc = PK.PresentationDocument.Open(packagePath, !allowEdit);
			return Build(presdoc, packagePath, allowEdit);

		}

		public static IPresentation Build(string packagePath)
		{
			return Build(packagePath, false);
		}

		public static IPresentation Build()
		{
			return new Presentation() { AllowedEdit = true };
		}

		public static PK.PresentationDocument Export(IPresentation model)
		{
			if (model.Name.Length == 0)
				return Export(model, Path.Combine(Directory.GetCurrentDirectory(), $"{Guid.NewGuid()}.pptx"));
			else
				return Export(model, model.Path);
		}

		/// <summary>
		/// 另存为指定文件。
		/// </summary>
		public static PK.PresentationDocument Export(IPresentation model, string packPath)
		{
			var newPack = PK.PresentationDocument.Create
				(packPath, OX.PresentationDocumentType.Presentation);

			//演示文稿容器对象。
			var presPart = newPack.AddPresentationPart();
			
			// Template 的名字为第一个主题。
			//newPack.ExtendedFilePropertiesPart.Properties.Template = new OX.ExtendedProperties.Template() { Text = "凤舞九天"};
			// newPack.ExtendedFilePropertiesPart.Properties.Template;


			return null;
		}

	}
}
