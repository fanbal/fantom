﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.IO;
using System.Text;
using System.Linq;
using DocumentFormat.OpenXml;
using PK = DocumentFormat.OpenXml.Packaging;
using D = DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;
using Fantom.Models;
using Fantom.Primitive;

namespace Fantom.Builders
{

	/// <summary>
	/// 用于演示文稿的整体导入。
	/// </summary>
	internal static class PresentationLoader
	{
		/// <summary>
		/// 导入图形时传递的消息句柄。
		/// </summary>
		/// <param name="info"></param>
		public delegate void RaiseLoadingShapeEventHandler(string info);

		/// <summary>
		/// 导入动画束时传递的消息句柄。
		/// </summary>
		/// <param name="info"></param>
		public delegate void RaiseLoadingBundleEventHandler(string info);

		/// <summary>
		/// 基本步骤数。基本步骤包括: 1. 包体导入 2. 页面排序 3. 主题导入 4. 解析完成。 
		/// </summary>
		const int BASE_STEPS = 4;


		/// <summary>
		/// 触发响应事件。
		/// </summary>
		/// <param name="press">作为容器的演示文稿集合对象。</param>
		/// <param name="loadingState">导入状态。</param>
		/// <param name="steps">总步骤。</param>
		/// <param name="step">当前步骤。</param>
		private static void RaiseLoadingSlideEvent(Presentations press, PresentationsLoadingState loadingState, int steps, int step)
		{
			press.RaiseLoadingSlideEvent(loadingState, steps, step);
		}

		/// <summary>
		/// 触发响应事件。
		/// </summary>
		/// <param name="press">作为容器的演示文稿集合对象。</param>
		/// <param name="loadingState">导入状态。</param>
		/// <param name="steps">总步骤。</param>
		/// <param name="step">当前步骤。</param>
		/// <param name="info">传递的附加消息。</param>
		private static void RaiseLoadingSlideEvent(Presentations press, PresentationsLoadingState loadingState, int steps, int step, string info)
		{
			press.RaiseLoadingSlideEvent(loadingState, steps, step, info);
		}

		/// <summary>
		/// 检查当前导入路径是否合法。宿主不允许重复导入同一演示文稿。
		/// </summary>
		/// <param name="press">演示文稿集合宿主。</param>
		/// <param name="uristr">当前路径。</param>
		/// <returns>如果路径不存在或者路径导入宿主，则返回 <see cref="false"/> 。否则返回 <see cref="true"/> 。</returns>
		private static bool IsUriValid(Presentations press, string uristr)
		{
			return File.Exists(uristr) && !press._filePathHashSet.Contains(uristr);
		}

		/// <summary>
		/// 导入并返回演示文稿。是导入模块的入口。
		/// </summary>
		/// <param name="press">演示文稿集合。需要存在此对象检查当前导入路径是否合法。</param>
		/// <param name="uristr">导入路径。</param>
		/// <param name="allowEdit">是否允许编辑。</param>
		/// <returns>成功导入后，将会在集合宿主中注册该对象，并返回该对象。如果路径不合法，则返回 <see cref="null"/> 。</returns>
		public static IPresentation Load(Presentations press, string uristr, bool allowEdit)
		{
			uristr = uristr.Trim();
			if (File.Exists(uristr))
				return LoadMain(press, uristr, allowEdit);
			else
				return null;
		}

		/// <summary>
		/// 导入的核心模块。
		/// </summary>
		/// <param name="press">演示文稿集合。生成完成的演示文稿对象需要在演示文稿集合中进行注册。</param>
		/// <param name="uristr">合法的路径需要在演示文稿集合对象中进行注册。</param>
		/// <param name="allowEdit">是否允许编辑。</param>
		/// <returns>新建立的演示文稿对象。</returns>
		private static IPresentation LoadMain(Presentations press, string uristr, bool allowEdit)
		{
			var app = press.Application as Application;
			var stepcount = 0;
			var step = 0;

			var presdoc = PK.PresentationDocument.Open(uristr, !allowEdit);
			// 截止目前为止，演示文稿包体的打开已完成。

			var package = presdoc.Package;

			var pres = PresentationBuilder.Build(presdoc, uristr, allowEdit) as Presentation;

			var sldps = presdoc.PresentationPart.SlideParts;

			press._filePathHashSet.Add(uristr); // 在集合的哈希表中注册该路径。

			stepcount = sldps.Count() + BASE_STEPS;

			// 截止目前为止，演示文稿的导入已经完成。
			RaiseLoadingSlideEvent(press, PresentationsLoadingState.PackageLoadCompleted, stepcount, ++step);

			// 导入主题。
			var theme = pres.Theme =
				ThemeBuilder.Build(presdoc.PresentationPart.ThemePart.Theme);

			// 导入主题完成。
			RaiseLoadingSlideEvent(press, PresentationsLoadingState.ThemeLoadCompleted, stepcount, ++step);

			// 此处用于导入母版。
			LoadMasters(package, app, pres, presdoc.PresentationPart, new BuildingCallbackLoadArgs());


			// TODO 幻灯片重排。
			var sldrange = from sld in sldps
						   orderby BaseSlideBuilder.GetSlideId(sld.Uri.OriginalString)
						   select sld;

			// 获得空的幻灯片集合。
			var slides = pres.Slides as Slides;

			// 幻灯片的重排已完成。
			RaiseLoadingSlideEvent(press, PresentationsLoadingState.SlidesReorderCompleted, stepcount, ++step);

			// 幻灯片处理。
			if (sldrange != null)
				foreach (var sldp in sldrange)
				{
					var buildingArgs = new BuildingCallbackLoadArgs();
					buildingArgs.ShapeHandler = (msg) =>
					{ RaiseLoadingSlideEvent(press, PresentationsLoadingState.ShapeLoadCompleted, stepcount, step, msg); };
					buildingArgs.BundleHandler = (msg) =>
					{ RaiseLoadingSlideEvent(press, PresentationsLoadingState.BundleLoadCompleted, stepcount, step, msg); };


					LoadSlide(package, app, pres, slides, sldp, buildingArgs);

					// 单页幻灯片导入完成。
					RaiseLoadingSlideEvent(press, PresentationsLoadingState.SlideLoadCompleted, stepcount, ++step);
				}

			presdoc.Close();
			presdoc.Dispose();

			(pres as Presentation).Parent = press; // 绑定父子集合关系。

			// 此时文档全部导入完成。
			RaiseLoadingSlideEvent(press, PresentationsLoadingState.PresentationLoadCompleted, stepcount, ++step);

			return pres;
		}


		/// <summary>
		/// 导入母版。
		/// </summary>
		/// <param name="package">幻灯片的包体。</param>
		/// <param name="app">顶层对象。</param>
		/// <param name="pres">演示文稿对象。</param>
		private static void LoadMasters(Package package, Application app, Presentation pres, PK.PresentationPart prespart, BuildingCallbackLoadArgs loadArgs)
		{
			pres.SlideMasters = new SlideMasters();
			if (prespart.SlideMasterParts != null)
				foreach (var mstpart in prespart.SlideMasterParts)
				{
					var master = new SlideMaster();
					if (mstpart.SlideLayoutParts != null)
						foreach (var lopart in mstpart.SlideLayoutParts)
						{
							LoadLayout(package, app, pres, master, lopart, loadArgs);
						}
					pres.SlideMasters.Add(master);
					//LoadMaster(package, app, pres, pres.SlideMasters as SlideMasters, );
				}
		}

		/// <summary>
		/// 导入幻灯片。
		/// </summary>
		/// <param name="package">幻灯片的包体。</param>
		/// <param name="app">顶层对象。</param>
		/// <param name="pres">演示文稿对象。</param>
		/// <param name="masters">幻灯片集合对象。</param>
		/// <param name="mstpart">当前单页幻灯片的包体信息。</param>
		private static void LoadLayout(Package package, Application app, IPresentation pres, SlideMaster master, PK.SlideLayoutPart lopart, BuildingCallbackLoadArgs loadArgs)
		{
			var xsld = lopart.SlideLayout;
			var csld = xsld.CommonSlideData;
			var xtiming = GetTiming(xsld);

			var newsld = BaseSlideBuilder.BuildLayout(lopart) as SlideLayout;
			var shps = newsld.Shapes as Shapes;

			var uridic = new Dictionary<string, Uri>();

			// 自定义 Xml 与自定义 Tag 被设计在这个节点中。
			var xcustomlist = csld.GetFirstChild<P.CustomerDataList>();

			var colorArgs = new ColorLoadArgs(lopart, pres.Theme as Theme, pres.MediaManager as MediaManager);
			var timelineArgs = new TimeLineLoadArgs(app, shps, colorArgs, loadArgs.BundleHandler);

			if (lopart.Parts != null)
				foreach (var idpair in lopart.Parts)
					uridic.Add(idpair.RelationshipId, idpair.OpenXmlPart.Uri);

			shps.Application = app;
			shps.Parent = newsld;

			var treeNodes = csld.ShapeTree.Skip(2);
			// 为每一页读入图形信息。
			if (treeNodes != null)
				foreach (var shpe in treeNodes)
				{
					LoadShape(package, uridic, app, shps, shpe, colorArgs, loadArgs.ShapeHandler);
				}

			// 导入时间轴。
			LoadTimeLine(newsld, xtiming, timelineArgs);

			// 为幻灯片载入自定义信息。
			LoadCustomInfo(app, newsld, package, xcustomlist, uridic, lopart);

			master.Layouts.Add(newsld);

			newsld.Application = app;
			newsld.Parent = pres.Slides;

		}


		/// <summary>
		/// 导入幻灯片。
		/// </summary>
		/// <param name="package">幻灯片的包体。</param>
		/// <param name="app">顶层对象。</param>
		/// <param name="pres">演示文稿对象。</param>
		/// <param name="slides">幻灯片集合对象。</param>
		/// <param name="sldpart">当前单页幻灯片的包体信息。</param>
		private static void LoadSlide(Package package, Application app, IPresentation pres, Slides slides, PK.SlidePart sldpart, BuildingCallbackLoadArgs loadArgs)
		{
			var xsld = sldpart.Slide;
			var csld = xsld.CommonSlideData;
			var xtiming = GetTiming(xsld);

			var newsld = BaseSlideBuilder.BuildSlide(sldpart) as Slide;
			var shps = newsld.Shapes as Shapes;

			var uridic = new Dictionary<string, Uri>();

			// 自定义 Xml 与自定义 Tag 被设计在这个节点中。
			var xcustomlist = csld.GetFirstChild<P.CustomerDataList>();

			var colorArgs = new ColorLoadArgs(sldpart, pres.Theme as Theme, pres.MediaManager as MediaManager);
			var timelineArgs = new TimeLineLoadArgs(app, shps, colorArgs, loadArgs.BundleHandler);

			if (sldpart.Parts != null)
				foreach (var idpair in sldpart.Parts)
					uridic.Add(idpair.RelationshipId, idpair.OpenXmlPart.Uri);

			shps.Application = app;
			shps.Parent = newsld;

			// 为每一页读入图形信息。
			var treeNodes = csld.ShapeTree.Skip(2);
			if (treeNodes != null)
				foreach (var shpe in treeNodes)
				{
					LoadShape(package, uridic, app, shps, shpe, colorArgs, loadArgs.ShapeHandler);
				}

			// 导入时间轴。
			LoadTimeLine(newsld, xtiming, timelineArgs);

			// 为幻灯片载入自定义信息。
			LoadCustomInfo(app, newsld, package, xcustomlist, uridic, sldpart);

			slides.Add(newsld);

			newsld.Application = app;
			newsld.Parent = pres.Slides;

		}

		/// <summary>
		/// 导入图形。
		/// </summary>
		private static void LoadShape
			(Package package, Dictionary<string, Uri> uridic, IApplication app, Shapes shps, OpenXmlElement shpe, ColorLoadArgs loadArgs, RaiseLoadingShapeEventHandler handler)
		{
			var newshp = ShapeBuilder.Build(shps, shpe, loadArgs);
			if (newshp == null) return;
			if (newshp is GroupShape)
			{
				(newshp as GroupShape).Application = app;
				(newshp as GroupShape).Parent = shps;
			}
			else if (newshp is Shape)
			{
				(newshp as Shape).Application = app;
				(newshp as Shape).Parent = shps;
			}

			shps.Add(newshp);

			var xshpcustomlist =
				shpe.FirstChild.ChildElements[2].GetFirstChild<P.CustomerDataList>();
			LoadCustomInfo(app, newshp, package, xshpcustomlist, uridic, loadArgs.SlidePart);

			if (newshp.Type != ShapeType.Common) return;

			(newshp.TextBody as TextBody).Application = newshp.Application;
			(newshp.TextBody as TextBody).Parent = newshp;

			if (shpe.GetFirstChild<P.TextBody>() != null && shpe.GetFirstChild<P.TextBody>().Elements<D.Paragraph>() != null)
				foreach (var para in shpe.GetFirstChild<P.TextBody>().Elements<D.Paragraph>())
				{
					LoadTextRange(newshp, para, loadArgs);
				}
			handler?.Invoke(newshp.Name);
		}


		// 导入字体相关设置。
		private static void LoadTextRange(IShape shp, D.Paragraph para, ColorLoadArgs loadArgs)
		{
			var newpara = TextParagraphBuilder.Build(para) as TextParagraph;

			if (para.Elements<D.Run>() != null)
				foreach (var run in para.Elements<D.Run>())
				{
					var newblock = TextBlockBuilder.Build(run, loadArgs) as TextBlock;

					newpara.Add(newblock);

					newblock.Application = shp.Application;
					newblock.Parent = newpara;
				}

			(shp.TextBody as TextBody).Add(newpara);

			newpara.Application = shp.Application;
			newpara.Parent = shp.TextBody;
		}

		// 提取时间节点。
		private static P.Timing GetTiming(P.Slide slide)
		{
			var timing = (P.Timing)null;
			// Console.WriteLine(sld.Slide.OuterXml);
			var alterContent = slide.GetFirstChild<AlternateContent>();

			if (alterContent != null)
			{
				var choice = alterContent.GetFirstChild<AlternateContentChoice>();
				if (choice != null)
				{
					timing = choice.GetFirstChild<P.Timing>();
				}
			}
			else
			{
				timing = slide.GetFirstChild<P.Timing>();
			}
			return timing;
		}

		// 导入时间轴动画数据。
		private static void LoadTimeLine(Slide slide, P.Timing timing, TimeLineLoadArgs loadArgs)
		{
			var timeLine = TimeLineBuilder.Build(slide, timing, loadArgs);

			slide.TimeLine = timeLine;
		}

		// 提取时间节点。
		private static P.Timing GetTiming(P.SlideLayout slide)
		{
			var timing = (P.Timing)null;
			// Console.WriteLine(sld.Slide.OuterXml);
			var alterContent = slide.GetFirstChild<AlternateContent>();

			if (alterContent != null)
			{
				var choice = alterContent.GetFirstChild<AlternateContentChoice>();
				if (choice != null)
				{
					timing = choice.GetFirstChild<P.Timing>();
				}
			}
			else
			{
				timing = slide.GetFirstChild<P.Timing>();
			}
			return timing;
		}

		// 导入时间轴动画数据。
		private static void LoadTimeLine(SlideLayout slide, P.Timing timing, TimeLineLoadArgs loadArgs)
		{
			var timeLine = TimeLineBuilder.Build(slide, timing, loadArgs);

			slide.TimeLine = timeLine;
		}

		// 载入自定义信息。
		// 自定义信息包括自定义 Xml 与自定义 Tag 。
		private static void LoadCustomInfo
			(IApplication app, ICustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.SlidePart sldpart)
		{

			// 自定义列表。
			var customList = container.CustomXmlList as CustomXmlList;
			var tags = container.Tags; // 获得父对象的Tags容器并使用它，添加字段。

			customList.Application = app;
			customList.Parent = container;

			if (xlist == null) return;

			// 遍历Xml节点。
			foreach (var customXmlPart in xlist)
			{

				// 处理自定义Xml节点。
				if (customXmlPart is P.CustomerData)
				{
					var xmlpart = CustomXmlBuilder.Build(
						customXmlPart as P.CustomerData, package, uridic) as CustomXml;
					xmlpart.Application = app;
					xmlpart.Parent = customList;
					customList.Add(xmlpart);
				}

				// 处理自定义Tag节点。
				else if (customXmlPart is P.CustomerDataTags)
				{
					string rId = (customXmlPart as P.CustomerDataTags).Id;
					var t =
						from tagpair in sldpart.UserDefinedTagsParts
						where tagpair.Uri == uridic[rId]
						select tagpair;
					if (t.Count() != 0)
					{
						var taglist = t.First().TagList;
						if (taglist != null)
							foreach (P.Tag tag in taglist)
							{
								container.Tags.Add(tag.Name, tag.Val);
							}
					}
				}

			}
		}


		// 载入自定义信息。
		// 自定义信息包括自定义 Xml 与自定义 Tag 。
		private static void LoadCustomInfo
			(IApplication app, ICustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.SlideLayoutPart sldpart)
		{

			// 自定义列表。
			var customList = container.CustomXmlList as CustomXmlList;
			var tags = container.Tags; // 获得父对象的Tags容器并使用它，添加字段。

			customList.Application = app;
			customList.Parent = container;

			if (xlist == null) return;

			// 遍历Xml节点。
			foreach (var customXmlPart in xlist)
			{

				// 处理自定义Xml节点。
				if (customXmlPart is P.CustomerData)
				{
					var xmlpart = CustomXmlBuilder.Build(
						customXmlPart as P.CustomerData, package, uridic) as CustomXml;
					xmlpart.Application = app;
					xmlpart.Parent = customList;
					customList.Add(xmlpart);
				}

				// 处理自定义Tag节点。
				else if (customXmlPart is P.CustomerDataTags)
				{
					string rId = (customXmlPart as P.CustomerDataTags).Id;
					var t =
						from tagpair in sldpart.UserDefinedTagsParts
						where tagpair.Uri == uridic[rId]
						select tagpair;
					if (t.Count() != 0)
					{
						var taglist = t.First().TagList;
						if (taglist != null)
							foreach (P.Tag tag in taglist)
							{
								container.Tags.Add(tag.Name, tag.Val);
							}
					}
				}

			}
		}


		// 载入自定义信息。
		// 自定义信息包括自定义 Xml 与自定义 Tag 。
		private static void LoadCustomInfo
			(IApplication app, ICustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.OpenXmlPart sldpart)
		{
			if (sldpart is PK.SlidePart)
				LoadCustomInfo(app, container, package, xlist, uridic, sldpart as PK.SlidePart);

			else if (sldpart is PK.SlideLayoutPart)
				LoadCustomInfo(app, container, package, xlist, uridic, sldpart as PK.SlideLayoutPart);




		}

	}
}
