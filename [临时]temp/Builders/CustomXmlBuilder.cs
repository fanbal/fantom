﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Presentation;
using PK = System.IO.Packaging;
using Fantom.Models;
namespace Fantom.Builders
{

	/// <summary>
	/// 自定义Xml内容生成器。
	/// </summary>
	internal static class CustomXmlBuilder
	{

		/// <summary>
		/// 建立 <see cref="ICustomXml"/> 实例。
		/// </summary>
		/// <param name="element">openxml中的 <see cref="CustomerData"/> 对象，用于获得uid，在解包的时候顺利解析出xml对象。</param>
		/// <param name="pack">xml文件所在的压缩包，即PPTX文件包源。</param>
		/// <param name="dic">一个uid-uri关系的字典，在包体一开始就被建立。</param>
		/// <returns></returns>
		public static ICustomXml Build(CustomerData element, PK.Package pack, Dictionary<string, Uri> dic)
		{
			var newCustomXml = new CustomXml();
			var uri = dic[element.Id];

			int len = 500000; // 默认一个包体的大小不会超过该数值。
			var buffer = new byte[len]; // 实例化一个缓存区。

			int counter = 0; // 字节计数器。
			int temp; // 用于储存每次返回时的状态。

			using (var stream = pack.GetPart(uri).GetStream())
			{
				// 使用逐字节读取，
				while (counter < len)
				{
					temp = stream.ReadByte();
					if (temp == -1) break;
					else buffer[counter] = (byte)temp;

					counter++;
				}
			}

			if (counter != 0)
			{
				// 由于XDocument只支持读入stream，不支持读入buffer，所以这里实例化了一个MemoryStream实例。
				var memoryStream = new MemoryStream(buffer, 0, counter);
				newCustomXml.XDocument = XDocument.Load(memoryStream);
				memoryStream.Dispose();
			}
			else
			{
				// 为空自定义xml加入Root根节点。
				newCustomXml.XDocument = new XDocument(new XElement("Root"));
			}

			return newCustomXml;
		}

		// 建立空的元素。
		public static ICustomXml Build(PK.Package pack)
		{
			var xdoc = new XDocument();
			xdoc.AddFirst("Root");
			return new CustomXml() { XDocument = xdoc };
		}


		public static CustomerData Export(ICustomXml model)
		{
			throw new NotImplementedException();
		}


	}
}
