﻿using System;
using DocumentFormat.OpenXml.Packaging;
using Fantom.Models;
namespace Fantom.Builders
{
	internal static class BaseSlideBuilder 
	{
		// 逆向解析。
		public static int GetSlideId(string uristr)
		{
			int[] stk = new int[5];
			int top = 0;
			int i = uristr.Length - ".xml".Length - 1;
			while (i >= 0 && uristr[i] >= '0' && uristr[i] <= '9')
			{
				stk[top] = uristr[i] - '0';
				i--;
				top++;
			}

			int rst = 0;
			int temp = 1;
			for (int j = 0; j < top; j++)
			{
				rst += temp * stk[j];
				temp *= 10;
			}
			return rst;
		}

		/// <summary>
		/// 导入幻灯片。
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		public static ISlide BuildSlide(SlidePart element)
		{
			var uri = element.Uri.OriginalString;
			var sldid = GetSlideId(uri);

			return new Slide();
		}

		/// <summary>
		/// 导入幻灯片布局。
		/// </summary>
		public static ISlideLayout BuildLayout(SlideLayoutPart element)
		{
			var uri = element.Uri.OriginalString;
			var sldid = GetSlideId(uri);

			return new SlideLayout();
		}

		/// <summary>
		/// 新建幻灯片。
		/// </summary>
		/// <returns></returns>
		public static ISlide BuildSlide()
		{
			return new Slide();
		}

		public static SlidePart Export(ISlide model)
		{
			throw new NotImplementedException();
		}


	}
}
