﻿using System;
using Fantom.Drawing;
using Fantom.Helper;
using D = DocumentFormat.OpenXml.Drawing;
using Fantom.Models;
namespace Fantom.Builders
{
	internal static class ThemeBuilder 
	{
		#region methods

		

		private static SolidColor GetBaseColor(D.Color2Type element, ThemeColorType colorType)
		{
			SolidColor color = SolidColor.FromHex("000000") ;

			switch (ColorAnalyzeHelper.GetColorBrushType(element))
			{
				case ColorBrushType.System:
					color = new	SolidColor(element.SystemColor.LastColor.Value);
					// (baseColor as SystemColorBrush).Name = element.SystemColor.Val;
					break;
				case ColorBrushType.SRGB:
					color = new SolidColor(element.RgbColorModelHex.Val);
					break;
				case ColorBrushType.Preset:
					// TODO 预设颜色的导入。
					break;
				case ColorBrushType.HSL:
					// TODO HSL颜色的导入。
					break;
				default:
					return null;
			}


			return color;
		}

		private static void AddColor(Theme theme, ThemeColorType colorType, D.Color2Type color2Type)
		{
			theme._colors.Add(colorType, GetBaseColor(color2Type, colorType));
		}

		public static ITheme Build(D.Theme element)
		{
			var theme = new Theme();
			var scheme = element.ThemeElements.ColorScheme;
			AddColor(theme, ThemeColorType.Accent1, scheme.Accent1Color);
			AddColor(theme, ThemeColorType.Accent2, scheme.Accent2Color);
			AddColor(theme, ThemeColorType.Accent3, scheme.Accent3Color);
			AddColor(theme, ThemeColorType.Accent4, scheme.Accent4Color);
			AddColor(theme, ThemeColorType.Accent5, scheme.Accent5Color);
			AddColor(theme, ThemeColorType.Accent6, scheme.Accent6Color);
			AddColor(theme, ThemeColorType.Dark1, scheme.Dark1Color);
			AddColor(theme, ThemeColorType.Dark2, scheme.Dark2Color);
			AddColor(theme, ThemeColorType.Light1, scheme.Light1Color);
			AddColor(theme, ThemeColorType.Light2, scheme.Light2Color);
			AddColor(theme, ThemeColorType.Hyperlink, scheme.Hyperlink);
			AddColor(theme, ThemeColorType.FollowedHyperlink, scheme.FollowedHyperlinkColor);


			//theme.Colors.Add(ThemeColorType.Accent1, )

			//element.ThemeElements.ColorScheme.Name;
			return theme;
		}


		public static D.Theme Export(ITheme model)
		{
			throw new NotImplementedException();
		}

		#endregion

	}
}
