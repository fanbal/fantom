﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;
using D = DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;
using PK = DocumentFormat.OpenXml.Packaging;
using Fantom.Helper;
using Fantom.Drawing;
using Fantom.Models;
using System.Linq;
using Fantom.Primitive;
using DocumentFormat.OpenXml.Bibliography;

namespace Fantom.Builders
{
	/// <summary>
	/// 时间轴与动画导出模块。
	/// </summary>
	internal static class TimeLineBuilder
	{
		static TimeLineBuilder()
		{
		}

		#region old build methods


		/// <summary>
		/// 建立时间轴对象。
		/// </summary>
		[Obsolete("参数列表过长", true)]
		public static ITimeLine Build(IApplication app, IFantomObject parent, P.Timing timing, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			var timeLine = new TimeLine();
			if (timing == null) return timeLine;
			var xTimeLine = timing.TimeNodeList;
			var par = xTimeLine.GetFirstChild<P.ParallelTimeNode>();
			var ctn = par.GetFirstChild<P.CommonTimeNode>();
			var rootNodeList = ctn.GetFirstChild<P.ChildTimeNodeList>();

			// 载入所有序列。
			LoadSequences(timeLine, app, rootNodeList, shapes, theme, mediaManager);

			timeLine.Application = app;
			timeLine.Parent = parent;

			return timeLine;
		}

		// 导入并分配当前幻灯片的序列。
		[Obsolete("参数列表过长", true)]
		private static void LoadSequences(TimeLine timeLine, IApplication app, P.ChildTimeNodeList nodeList, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			foreach (var xseq in nodeList)
			{
				var seq = new AnimationSequence();
				LoadSequence(seq, app, timeLine, xseq as P.SequenceTimeNode, shapes, theme, mediaManager);
			}
		}

		// 载入单条序列。
		[Obsolete("参数列表过长", true)]
		private static void LoadSequence(AnimationSequence seq, IApplication app, TimeLine timeLine, P.SequenceTimeNode xseq, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			var ctn = xseq.GetFirstChild<P.CommonTimeNode>();

			if (ctn.NodeType.Value != P.TimeNodeValues.MainSequence)
				timeLine.InteractiveSequences.Add(seq);
			else
				timeLine.MainSequence = seq;

			LoadFragments(seq, app, ctn.GetFirstChild<P.ChildTimeNodeList>(), shapes, theme, mediaManager);

			seq.Application = app;
			seq.Parent = timeLine;

		}

		// 导入效果集合。
		[Obsolete("参数列表过长", true)]
		private static void LoadFragments(AnimationSequence seq, IApplication app, P.ChildTimeNodeList nodeList, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			foreach (P.ParallelTimeNode par in nodeList.ChildElements)
			{
				var fra = new AnimationFragment();
				LoadFragment(fra, app, seq, par, shapes, theme, mediaManager);
			}
		}

		// 导入效果。
		[Obsolete("参数列表过长", true)]
		private static void LoadFragment(AnimationFragment fra, IApplication app, AnimationSequence parent, P.ParallelTimeNode par, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			var ctn = par.GetFirstChild<P.CommonTimeNode>();

			LoadBundles(fra, app, ctn.GetFirstChild<P.ChildTimeNodeList>(), shapes, theme, mediaManager);

			fra.Application = app;
			fra.Parent = parent;
			parent.Add(fra);
		}

		// 导入动画束集合。
		[Obsolete("参数列表过长", true)]
		private static void LoadBundles(AnimationFragment fra, IApplication app, P.ChildTimeNodeList nodeList, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			foreach (P.ParallelTimeNode par in nodeList.ChildElements)
			{
				var bun = new AnimationBundle();
				LoadBundle(bun, app, fra, par, shapes, theme, mediaManager);
			}
		}

		// 导入动画束。
		[Obsolete("参数列表过长", true)]
		private static void LoadBundle(AnimationBundle bun, IApplication app, AnimationFragment parent, P.ParallelTimeNode par, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			var ctn = par.GetFirstChild<P.CommonTimeNode>();

			LoadEffects(bun, app, ctn.GetFirstChild<P.ChildTimeNodeList>(), shapes, theme, mediaManager);

			bun.Application = app;
			bun.Parent = parent;
			parent.Add(bun);
		}


		// 导入效果集合。
		[Obsolete("参数列表过长", true)]
		private static void LoadEffects(AnimationBundle bun, IApplication app, P.ChildTimeNodeList nodeList, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			foreach (P.ParallelTimeNode par in nodeList.ChildElements)
			{
				var eff = new AnimationEffect();
				LoadEffect(eff, app, bun, par, shapes, theme, mediaManager);
			}
		}

		// 导入效果。
		[Obsolete("参数列表过长", true)]
		private static void LoadEffect(AnimationEffect eff, IApplication app, AnimationBundle parent, P.ParallelTimeNode par, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			var ctn = par.GetFirstChild<P.CommonTimeNode>();

			LoadBehaviors(eff, app, ctn.GetFirstChild<P.ChildTimeNodeList>(), shapes, theme, mediaManager);

			eff.Application = app;
			eff.Parent = parent;
			parent.Add(eff);
		}

		// 导入行为集合。
		[Obsolete("参数列表过长", true)]
		private static void LoadBehaviors(AnimationEffect eff, IApplication app, P.ChildTimeNodeList nodeList, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{

			foreach (var anim in nodeList)
			{
				LoadBehavior(app, eff, anim, shapes, theme, mediaManager);
			}

		}

		// 导入行为。
		[Obsolete("参数列表过长", true)]
		private static void LoadBehavior(IApplication app, AnimationEffect parent, OpenXmlElement anim, Shapes shapes, Theme theme, IMediaManager mediaManager)
		{
			var cbhv = anim.GetFirstChild<P.CommonBehavior>();

			var bhv = Animation.BaseBehavior.LoadBehaviorFromKeyWord(anim, null);
			var id = int.Parse(cbhv.TargetElement.ShapeTarget.ShapeId);
			bhv.Shape = shapes.ShapeIndexDic[id];
			bhv.Application = app;
			bhv.Parent = parent;
			parent.Add(bhv);
		}

		#endregion old build methods

		#region new build methods


		/// <summary>
		/// 建立时间轴对象。
		/// </summary>
		public static ITimeLine Build(IFantomObject parent, P.Timing timing, TimeLineLoadArgs loadArgs)
		{
			var timeLine = new TimeLine();

			if (timing == null) return timeLine;

			var xTimeLine = timing.TimeNodeList;
			var par = xTimeLine.GetFirstChild<P.ParallelTimeNode>();
			var ctn = par.GetFirstChild<P.CommonTimeNode>();
			var rootNodeList = ctn.GetFirstChild<P.ChildTimeNodeList>();

			// 载入所有序列。
			if (ctn.ChildTimeNodeList != null)
				foreach (var xseq in ctn.ChildTimeNodeList)
				{
					LoadSequence(timeLine, xseq as P.SequenceTimeNode, loadArgs);
				}

			timeLine.Application = loadArgs.Application;
			timeLine.Parent = parent;

			return timeLine;
		}


		// 载入单条序列。
		private static void LoadSequence(TimeLine timeLine, P.SequenceTimeNode xseq, TimeLineLoadArgs loadArgs)
		{
			var seq = new AnimationSequence();
			var ctn = xseq.GetFirstChild<P.CommonTimeNode>();

			if (ctn.NodeType.Value != P.TimeNodeValues.MainSequence)
				timeLine.InteractiveSequences.Add(seq);
			else
				timeLine.MainSequence = seq;

			foreach (P.ParallelTimeNode spar in ctn.ChildTimeNodeList)
			{
				LoadFragment(seq, spar, loadArgs);
			}

			seq.Application = loadArgs.Application;
			seq.Parent = timeLine;

		}

		// 导入效果。
		private static void LoadFragment(AnimationSequence seq, P.ParallelTimeNode par, TimeLineLoadArgs loadArgs)
		{
			var fra = new AnimationFragment();
			var ctn = par.GetFirstChild<P.CommonTimeNode>();

			foreach (P.ParallelTimeNode spar in ctn.ChildTimeNodeList)
			{
				LoadBundle(fra, spar, loadArgs);
			}

			fra.Application = loadArgs.Application;
			fra.Parent = seq;
			seq.Add(fra);
		}

		// 导入动画束。
		private static void LoadBundle(AnimationFragment fra, P.ParallelTimeNode par, TimeLineLoadArgs loadArgs)
		{
			var bun = new AnimationBundle();

			var ctn = par.GetFirstChild<P.CommonTimeNode>();

			foreach (P.ParallelTimeNode spar in ctn.ChildTimeNodeList)
			{
				LoadEffect(bun, spar, loadArgs);
			}

			bun.Application = loadArgs.Application;
			bun.Parent = fra;
			fra.Add(bun);
			loadArgs.Handler("add bundle");
		}


		// 导入效果。
		private static void LoadEffect(AnimationBundle bun, P.ParallelTimeNode par, TimeLineLoadArgs loadArgs)
		{
			var eff = new AnimationEffect();
			var ctn = par.GetFirstChild<P.CommonTimeNode>();

			foreach (var anim in ctn.ChildTimeNodeList)
			{
				LoadBehavior(eff, anim, loadArgs);
			}

			eff.Application = loadArgs.Application;
			eff.Parent = bun;
			bun.Add(eff);
		}


		// 导入行为。
		private static void LoadBehavior(AnimationEffect eff, OpenXmlElement anim, TimeLineLoadArgs loadArgs)
		{
			var cbhv = anim.GetFirstChild<P.CommonBehavior>();

			var bhv = Animation.BaseBehavior.LoadBehaviorFromKeyWord(anim, loadArgs.ColorLoadArgs);
			var id = int.Parse(cbhv.TargetElement.ShapeTarget.ShapeId);
			bhv.Shape = loadArgs.Shapes.ShapeIndexDic[id];
			bhv.Application = loadArgs.Application;
			bhv.Parent = eff;
			eff.Add(bhv);
		}

		#endregion new build methods

		public static OpenXmlElement Export(ITimeLine model)
		{
			throw new NotImplementedException();
		}


	}
}
