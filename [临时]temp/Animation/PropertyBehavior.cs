﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;
using Fantom.Primitive;

namespace Fantom.Animation
{
	public sealed class PropertyBehavior : BaseBehavior
	{
		/// <summary>
		/// 可执行动画的属性，一般为 PPT 的 X 轴坐标与 Y 轴坐标。
		/// </summary>
		public AnimatableProperty Property { get; set; }

		/// <summary>
		/// 其值以可执行动画的类型决定。
		/// </summary>
		public Emu From { get; set; }

		/// <summary>
		/// 其值以可执行动画的类型决定。
		/// </summary>
		public Emu To { get; set; }

		/// <summary>
		/// 关键帧序列。
		/// </summary>
		public List<BaseAnimationKeyFramePoint> KeyFramePoints { get; }
			= new List<BaseAnimationKeyFramePoint>();

		internal PropertyBehavior()
		{

		}

		#region static

		public override string ToString()
		{
			var fmt1 = "{0,17}, tim: {1,9}%, val: {2,-10}, fml: {3,-10}";
			var fmt2 = "{0,17}, tim: {1,9}%, val: {2,-10}";
			var sb = new StringBuilder(base.ToString());
			sb.AppendLine();

			foreach (var point in KeyFramePoints)
			{
				if (point is IntAnimationKeyFramePoint)
				{
					var p = point as IntAnimationKeyFramePoint;
					sb.AppendLine(string.Format(fmt1, "int",
					Math.Round(p.Time.ToPercent() * 100, 3), p.Value, p.Formula));
				}
				else if (point is FloatAnimationKeyFramePoint)
				{
					var p = point as FloatAnimationKeyFramePoint;
					sb.AppendLine(string.Format(fmt1, "float",
					Math.Round(p.Time.ToPercent() * 100, 3), p.Value, p.Formula));
				}
				else if (point is BoolAnimationKeyFramePoint)
				{
					var p = point as BoolAnimationKeyFramePoint;
					sb.AppendLine(string.Format(fmt2, "bool",
					Math.Round(p.Time.ToPercent() * 100, 3), p.Value));
				}
				else if (point is StringAnimationKeyFramePoint)
				{
					var p = point as StringAnimationKeyFramePoint;
					sb.AppendLine(string.Format(fmt1, "string",
					Math.Round(p.Time.ToPercent() * 100, 3), p.Value, p.Formula));
				}
				else if (point is ColorAnimationKeyFramePoint)
				{
					var p = point as ColorAnimationKeyFramePoint;
					sb.AppendLine(string.Format(fmt2, "color",
					Math.Round(p.Time.ToPercent() * 100, 3), p.Value));
				}

			}

			return sb.ToString();
		}

		/// <summary>
		/// 从指定动画描述中导入具体数据。
		/// </summary>
		[Obsolete("已过时", true)]
		internal static PropertyBehavior LoadBehaviorFromElement(OpenXmlElement element, ITheme theme)
		{
			var anim = element as DocumentFormat.OpenXml.Presentation.Animate;
			var xbhv = anim.CommonBehavior;

			var xavlst = anim.TimeAnimateValueList;

			var bhv = new PropertyBehavior();
			SetBehaviorFromElement(bhv, element);


			if (xbhv.To != null)
				bhv.To = Emu.Parse(xbhv.To);

			if (xavlst != null)
				foreach (DocumentFormat.OpenXml.Presentation.TimeAnimateValue keyPoint in xavlst)
				{
					bhv.KeyFramePoints.Add(BaseAnimationKeyFramePoint.CreateFromElement(keyPoint, null));
				}

			bhv.Property = GetAnimatableProperty(element);
			return bhv;
		}

		/// <summary>
		/// 从指定动画描述中导入具体数据。
		/// </summary>
		internal static PropertyBehavior LoadBehaviorFromElement(OpenXmlElement element, ColorLoadArgs loadArgs)
		{
			var anim = element as DocumentFormat.OpenXml.Presentation.Animate;
			var xbhv = anim.CommonBehavior;

			var xavlst = anim.TimeAnimateValueList;

			var bhv = new PropertyBehavior();
			SetBehaviorFromElement(bhv, element);


			if (xbhv.To != null)
				bhv.To = Emu.Parse(xbhv.To);

			if (xavlst != null)
				foreach (DocumentFormat.OpenXml.Presentation.TimeAnimateValue keyPoint in xavlst)
				{
					bhv.KeyFramePoints.Add(BaseAnimationKeyFramePoint.CreateFromElement(keyPoint, loadArgs));
				}

			bhv.Property = GetAnimatableProperty(element);
			return bhv;
		}


		#endregion




	}
}
