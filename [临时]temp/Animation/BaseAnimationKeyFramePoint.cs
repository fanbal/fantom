﻿using System;
using System.Collections.Generic;
using System.Text;
using P = DocumentFormat.OpenXml.Presentation;
using Fantom.Primitive;

namespace Fantom.Animation
{

	/// <summary>
	/// 动画帧点。
	/// </summary>
	public abstract class BaseAnimationKeyFramePoint
	{
		/// <summary>
		/// 当前关键帧值的时间（Emu 表示 比例）。
		/// </summary>
		public Emu Time { get; set; }

		/// <summary>
		/// 从节点构造动画点。
		/// </summary>
		/// <param name=""></param>
		/// <returns></returns>
		internal static BaseAnimationKeyFramePoint CreateFromElement(P.TimeAnimateValue element, ColorLoadArgs loadArgs)
		{
			BaseAnimationKeyFramePoint p = null;
			switch (element.VariantValue.FirstChild.LocalName)
			{
				case "clrVal":
					p = ColorAnimationKeyFramePoint.CreateFromElement(element, loadArgs);
					break;
				case "intVal":
					p = IntAnimationKeyFramePoint.CreateFromElement(element);
					break;
				case "fltVal":
					p = FloatAnimationKeyFramePoint.CreateFromElement(element);
					break;
				case "strVal":
					p = StringAnimationKeyFramePoint.CreateFromElement(element);
					break;
				case "boolVal":
					p = BoolAnimationKeyFramePoint.CreateFromElement(element);
					break;
				default:
					throw new Exception("未知的动画帧点。");
			}
			p.Time = Emu.Parse(element.Time);

			return p;

		}

	}

}
