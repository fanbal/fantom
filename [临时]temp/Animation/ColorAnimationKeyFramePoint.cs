﻿using System;
using System.Collections.Generic;
using System.Text;
using Fantom.Drawing;
using Fantom.Primitive;
using P = DocumentFormat.OpenXml.Presentation;

namespace Fantom.Animation
{
	/// <summary>
	/// 以颜色为主题的节点。
	/// </summary>
	public sealed class ColorAnimationKeyFramePoint : BaseAnimationKeyFramePoint
	{
		#region porperties

		/// <summary>
		/// 自变量。两帧点之间的差值变化为随时间线性变化。
		/// </summary>
		public BaseColor Value { get; set; }

		#endregion


		#region ctors
		internal ColorAnimationKeyFramePoint() { }
		#endregion

		#region static

		/// <summary>
		/// 从节点构造动画点。
		/// </summary>
		/// <param name=""></param>
		/// <returns></returns>
		internal static new ColorAnimationKeyFramePoint CreateFromElement(P.TimeAnimateValue element, ColorLoadArgs loadArgs)
		{
			var p = new ColorAnimationKeyFramePoint();
			var varValue = element.VariantValue;
			p.Value = ColorLoader.GetColorFromNodeWithColor(varValue.ColorValue, loadArgs);

			return p;

		}


		#endregion

	}



}
