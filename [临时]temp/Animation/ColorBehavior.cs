﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;
using Fantom.Drawing;
using Fantom.Primitive;

namespace Fantom.Animation
{
	/// <summary>
	/// 具有路径相关的信息的动画行为。
	/// </summary>
	public sealed class ColorBehavior : BaseBehavior
	{
		#region properties

		/// <summary>
		/// 初始纯色颜色。
		/// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
		/// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
		/// </summary>
		public BaseColor From { get; set; } = null;

		/// <summary>
		/// 终点纯色颜色。
		/// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
		/// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
		/// </summary>
		public BaseColor To { get; set; } = null;

		/// <summary>
		/// 相对变化颜色。
		/// </summary>
		public BaseColor By { get; set; }


		#endregion

		#region ctors
		internal ColorBehavior() { }
		#endregion


		#region static
		[Obsolete("已过时", true)]
		internal static ColorBehavior LoadBehaviorFromElement(OpenXmlElement element, ITheme theme)
		{
			var anim = element as DocumentFormat.OpenXml.Presentation.AnimateColor;

			var bhv = new ColorBehavior();
			SetBehaviorFromElement(bhv, element);

			if (anim.ToColor != null)
				bhv.To = ColorLoader.GetCustomColor(theme, anim.ToColor.FirstChild);

			if (anim.FromColor != null)
				bhv.From = ColorLoader.GetCustomColor(theme, anim.FromColor.FirstChild);

			if (anim.ByColor != null)
				bhv.By = ColorLoader.GetCustomColor(theme, anim.ByColor.FirstChild);

			return bhv;
		}

		internal static ColorBehavior LoadBehaviorFromElement(OpenXmlElement element, ColorLoadArgs loadArgs)
		{
			var anim = element as DocumentFormat.OpenXml.Presentation.AnimateColor;

			var bhv = new ColorBehavior();
			SetBehaviorFromElement(bhv, element);

			if (anim.ToColor != null)
				bhv.To = ColorLoader.GetCustomColor(anim.ToColor.FirstChild, loadArgs);

			if (anim.FromColor != null)
				bhv.From = ColorLoader.GetCustomColor(anim.FromColor.FirstChild, loadArgs);

			if (anim.ByColor != null)
				bhv.By = ColorLoader.GetCustomColor(anim.ByColor.FirstChild, loadArgs);

			return bhv;
		}

		#endregion
	}
}
