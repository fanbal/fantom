﻿using System;
using System.Collections.Generic;
using System.Text;
using Fantom.Animation;
using P = DocumentFormat.OpenXml.Presentation;

namespace Fantom.Animation
{
	/// <summary>
	/// 文本变化相关的动画断点。
	/// </summary>
	public sealed class StringAnimationKeyFramePoint : BaseAnimationKeyFramePoint
	{
		#region porperties

		/// <summary>
		/// 当前帧的文本值。
		/// </summary>
		public string Value { get; set; } = "0";


		/// <summary>
		/// 表达式。其自变量为 <see cref="Value"/> PPT的动画解析函数。
		/// </summary>
		public string Formula { get; set; }

		#endregion

		#region ctors
		internal StringAnimationKeyFramePoint() { }


		#endregion

		#region static


		/// <summary>
		/// 从节点构造动画点。
		/// </summary>
		/// <param name=""></param>
		/// <returns></returns>
		internal static StringAnimationKeyFramePoint CreateFromElement(P.TimeAnimateValue element)
		{

			var p = new StringAnimationKeyFramePoint();
			var varValue = element.VariantValue;
			p.Value = varValue.StringVariantValue.Val;
			p.Formula = element.Fomula;
			return p;

		}

		#endregion
	}
}