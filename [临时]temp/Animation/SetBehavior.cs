﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;


namespace Fantom.Animation
{
	public sealed class SetBehavior : BaseBehavior
	{
		/// <summary>
		/// 可执行动画的属性，一般为 PPT 的 X 轴坐标与 Y 轴坐标。
		/// </summary>
		public AnimatableProperty Property { get; set; }

		/// <summary>
		/// 其值以可执行动画的类型决定。
		/// </summary>
		public Emu To { get; set; }


		internal SetBehavior()
		{

		}


		/// <summary>
		/// 从指定动画描述中导入具体数据。
		/// </summary>
		/// <param name="anim"></param>
		/// <returns></returns>
		internal static SetBehavior LoadBehaviorFromElement(OpenXmlElement element)
		{
			var xbhv = element.GetFirstChild<DocumentFormat.OpenXml.Presentation.CommonBehavior>();
			var bhv = new SetBehavior();
			SetBehaviorFromElement(bhv, element);

			if (xbhv.To != null)
				bhv.To = Emu.Parse(xbhv.To);

			bhv.Property = GetAnimatableProperty(element);
			return bhv;
		}

	}

}
