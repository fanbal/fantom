﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;
using P = DocumentFormat.OpenXml.Presentation;

namespace Fantom.Animation
{
	/// <summary>
	/// 具有滤镜相关的信息的动画行为。
	/// </summary>
	public sealed class FilterBehavior : BaseBehavior
	{
		#region properties

		/// <summary>
		/// 滤镜效果。在 PPT 中，诸如淡入淡出，擦除、百叶窗、棋盘等出现消失动画的核心为滤镜效果。
		/// <para>我们认为这些滤镜效果的动画表现力并不够理想，如果您希望开始 PPT 动画的设计，请尽可能避免这类效果的运用。</para>
		/// 您完全可以用其他方式提升动画的质感。
		/// </summary>
		public FilterType FilterType { get; set; }


		/// <summary>
		/// 滤镜子效果。此处用于飞入飞出等滤镜动画的具体定义，您可以在这里设置飞入飞出的具体方向。
		/// <para>我们认为这些滤镜效果的动画表现力并不够理想，如果您希望开始 PPT 动画的设计，请尽可能避免这类效果的运用。</para>
		/// 您完全可以用其他方式提升动画的质感。
		/// </summary>
		public FilterType FilterSubType { get; set; }


		/// <summary>
		/// 是否为逆动画。
		/// 在 PPT 中，动画的淡入与淡出并不是两个动画滤镜，默认地只有淡入动画，淡出滤镜是淡入的逆动画，由此控制。
		/// </summary>
		public bool Reveal { get; set; }

		#endregion

		#region ctors
		internal FilterBehavior() { }
		#endregion

		#region methods
		public override string ToString()
		{
			var sb = new StringBuilder(base.ToString());
			sb.AppendLine();
			sb.AppendLine(string.Format("{0, 17}.", FilterType));
			return sb.ToString(); ;
		}

		#endregion

		#region static
		private static FilterType GetFilterType(P.AnimateEffect anim)
		{
			switch (anim.Filter.Value)
			{
				case "fade":
					return FilterType.Fade;
				default:
					break;
			}

			return FilterType.None;
		}

		internal static FilterBehavior LoadBehaviorFromElement(OpenXmlElement element)
		{
			var anim = element as P.AnimateEffect;
			var bhv = new FilterBehavior();
			SetBehaviorFromElement(bhv, element);

			if (anim.Transition != null)
				bhv.Reveal = anim.Transition.Value == P.AnimateEffectTransitionValues.Out;

			if (anim.Filter != null)
				bhv.FilterType = GetFilterType(anim);

			return bhv;
		}

		#endregion


	}

	// TODO 待加入滤镜效果枚举。
	public enum FilterType
	{
		None,

		/// <summary>
		/// 渐变淡入。
		/// </summary>
		Fade,
	}

	// TODO 待加入滤镜子效果枚举。
	public enum FilterSubType
	{
		None,
	}

}
