﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;

namespace Fantom.Animation
{
	/// <summary>
	/// 具有路径相关的信息的动画行为。
	/// </summary>
	public sealed class MotionBehavior : BaseBehavior
	{
		#region properties

		/// <summary>
		/// 移动起点，参考单位为 <see cref="Emu.Degree1"/>。
		/// 如果您希望获得其值，您需要调用 <see cref="Emu.ToAngle"/> 。
		/// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
		/// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
		/// </summary>
		public Emu? From { get; set; } = null;

		/// <summary>
		/// 移动终点，参考单位为 <see cref="Emu.Degree1"/>。
		/// 如果您希望获得其值，您需要调用 <see cref="Emu.ToAngle"/> 。
		/// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
		/// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
		/// </summary>
		public Emu? To { get; set; } = null;

		/// <summary>
		/// 相对移动，参考单位为 <see cref="Emu.Degree1"/>。
		/// 如果您希望获得其值，您需要调用 <see cref="Emu.ToAngle"/> 。
		/// </summary>
		public Emu By { get; set; } = Emu.Degree360;

		/// <summary>
		/// 路径的描述信息，其格式标准为VML。其描述具体语法如下。
		/// <para>重置点位置：M x y </para>
		/// <para>贝塞尔曲线：C x1 y1 x2 y2 x y</para>
		/// <para>直线：L x y </para>
		/// <para>折返：Z </para>
		/// </summary>
		public string Path { get; set; } = "";

		#endregion

		#region ctors
		internal MotionBehavior() { }
		#endregion

		#region methods

		public override string ToString()
		{
			var sb = new StringBuilder(base.ToString());
			sb.AppendLine();
			sb.AppendLine(string.Format("{0}", Path));
			return sb.ToString(); ;
		}

		#endregion

		#region static
		internal static MotionBehavior LoadBehaviorFromElement(OpenXmlElement element)
		{
			var anim = element as DocumentFormat.OpenXml.Presentation.AnimateMotion;
			var xbhv = anim.CommonBehavior;
			var bhv = new MotionBehavior();
			SetBehaviorFromElement(bhv, element);

			if (xbhv.To != null)
				bhv.To = Emu.Parse(xbhv.To);

			if (xbhv.From != null)
				bhv.From = Emu.Parse(xbhv.From);

			if (xbhv.By != null)
				bhv.By = Emu.Parse(xbhv.By);

			if (anim.Path != null)
				bhv.Path = anim.Path;

			return bhv;
		}

		#endregion

	}
}
