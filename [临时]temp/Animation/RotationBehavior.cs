﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;

namespace Fantom.Animation
{
	/// <summary>
	/// 具有角度相关的信息的动画行为。
	/// </summary>
	public sealed class RotationBehavior : BaseBehavior
	{
		#region properties

		/// <summary>
		/// 旋转起点角度，参考单位为 <see cref="Emu.Degree1"/>。
		/// 如果您希望获得其值，您需要调用 <see cref="Emu.ToAngle"/> 。
		/// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
		/// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
		/// </summary>
		public Emu? From { get; set; } = null;

		/// <summary>
		/// 旋转终点，参考单位为 <see cref="Emu.Degree1"/>。
		/// 如果您希望获得其值，您需要调用 <see cref="Emu.ToAngle"/> 。
		/// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
		/// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
		/// </summary>
		public Emu? To { get; set; } = null;

		/// <summary>
		/// 旋转相对角度，参考单位为 <see cref="Emu.Degree1"/>。
		/// 如果您希望获得其值，您需要调用 <see cref="Emu.ToAngle"/> 。
		/// </summary>
		public Emu By { get; set; } = Emu.Degree360;

		#endregion

		#region ctors

		// 不可以通过构造函数构造该对象。
		internal RotationBehavior() { }
		#endregion


		#region static

		/// <summary>
		/// 从指定动画描述中导入具体数据。
		/// </summary>
		/// <param name="anim"></param>
		/// <returns></returns>
		internal static RotationBehavior LoadBehaviorFromElement(OpenXmlElement element)
		{
			var xbhv = element.GetFirstChild<DocumentFormat.OpenXml.Presentation.CommonBehavior>();
			var bhv = new RotationBehavior();
			SetBehaviorFromElement(bhv, element);

			if (xbhv.To != null)
				bhv.To = Emu.Parse(xbhv.To);

			if (xbhv.From != null)
				bhv.From = Emu.Parse(xbhv.From);

			if (xbhv.By != null)
				bhv.By = Emu.Parse(xbhv.By);

			return bhv;
		}

		#endregion
	}
}
