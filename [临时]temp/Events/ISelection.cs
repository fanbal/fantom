﻿using System;
using System.Collections.Generic;

namespace Fantom
{
	/// <summary>
	/// 演示文稿事件委托。
	/// </summary>
	public delegate void SelectionEventHandler(ISelection sender, SelectionEventArgs args);

	/// <summary>
	/// 容器选择信息改变时传递出的参数列表。
	/// </summary>
	public class SelectionEventArgs : EventArgs
	{

		/// <summary>
		/// 在选中集合中消失的成员。
		/// </summary>
		public IEnumerable<ISelectable> LostRange { get; private set; }

		/// <summary>
		/// 在选中集合中新加的成员。
		/// </summary>
		public IEnumerable<ISelectable> AdditionRange { get; private set; }

		/// <summary>
		/// 构建容器选择信息改变时传递出的参数列表。
		/// </summary>
		/// <param name="range">当前选择集合。</param>
		/// <param name="lostRange">当前失去焦点的对象集合。</param>
		/// <param name="addRange">当前新增的对象的集合。</param>
		public SelectionEventArgs( IEnumerable<ISelectable> lostRange, IEnumerable<ISelectable> addRange)
		{
			LostRange = lostRange;
			AdditionRange = addRange;
		}
	}

}
