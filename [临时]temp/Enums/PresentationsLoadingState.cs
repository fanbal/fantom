﻿namespace Fantom
{
	/// <summary>
	/// 演示文稿加载时具体的状态。
	/// </summary>
	public enum PresentationsLoadingState
	{

		/// <summary>
		/// 包体已经导入完成，并且完成了计算统计。
		/// </summary>
		PackageLoadCompleted,

		/// <summary>
		/// 主题导入成功。
		/// </summary>
		ThemeLoadCompleted,

		/// <summary>
		/// 幻灯片重排已完成。
		/// </summary>
		SlidesReorderCompleted,

		/// <summary>
		/// 指定幻灯片已成功录入。
		/// </summary>
		SlideLoadCompleted,

		/// <summary>
		/// 单个图形导入完成。
		/// </summary>
		ShapeLoadCompleted,

		/// <summary>
		/// 单个动画束导入完成。
		/// </summary>
		BundleLoadCompleted,

		/// <summary>
		/// 演示文稿导入完成。
		/// </summary>
		PresentationLoadCompleted,

	}
}
