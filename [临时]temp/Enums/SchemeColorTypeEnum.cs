﻿namespace Fantom
{
	/// <summary>
	/// 主题枚举类型。
	/// </summary>
	public enum ThemeColorType
	{
		/// <summary>
		/// 未知颜色。
		/// </summary>
		Unknown = -1,

		/// <summary>
		/// 背景颜色1，标签为"bg1". 
		/// </summary>
		Background1 = 0,

		/// <summary>
		/// 文本颜色1，标签为"tx1". 
		/// </summary>
		Text1 = 1,

		/// <summary>
		/// 背景颜色2，标签为"bg2". 
		/// </summary>

		Background2 = 2,

		/// <summary>
		/// 文本颜色2，标签为"tx2". 
		/// </summary>
		Text2 = 3,

		/// <summary>
		/// 强调颜色1，标签为"accent1". 
		/// </summary>
		Accent1 = 4,

		/// <summary>
		/// 强调颜色2，标签为"accent2". 
		/// </summary>
		Accent2 = 5,

		/// <summary>
		/// 强调颜色3，标签为"accent3"
		/// </summary>
		Accent3 = 6,

		/// <summary>
		/// 强调颜色4，标签为"accent4"
		/// </summary>
		Accent4 = 7,

		/// <summary>
		/// 强调颜色5，标签为"accent5"
		/// </summary>
		Accent5 = 8,

		/// <summary>
		/// 强调颜色6，标签为"accent6"
		/// </summary>
		Accent6 = 9,

		/// <summary>
		/// 链接前颜色，标签为"hlink"
		/// </summary>
		Hyperlink = 10,

		/// <summary>
		/// 链接后颜色，标签为"folHlink"
		/// </summary>
		FollowedHyperlink = 11,

		/// <summary>
		/// 样式颜色，标签为"phClr"
		/// </summary>
		PhColor = 12,

		/// <summary>
		/// 夜间颜色1，标签为"dk1"
		/// </summary>
		Dark1 = 13,

		/// <summary>
		/// 日间颜色1，标签为"lt1"
		/// </summary>
		Light1 = 14,

		/// <summary>
		/// 夜间颜色2，标签为"dk2"
		/// </summary>
		Dark2 = 15,

		/// <summary>
		/// 日间颜色2，标签为"lt2"
		/// </summary>
		Light2 = 16
	}


}
