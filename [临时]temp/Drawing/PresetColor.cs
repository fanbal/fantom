﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Drawing
{
	/// <summary>
	/// 预设颜色。
	/// </summary>
	public sealed class PresetColor : BaseColor
	{
		#region prop

		public string Name { get; private set; }

		Emu _alpha = 0;

		/// <summary>
		/// 透明度，节点为a:alpha
		/// </summary>
		public Emu Alpha
		{
			get => _alpha;set => _alpha = value;
		}



		#endregion



		#region static

		private static Dictionary<string, PresetColor> _colorDic = new Dictionary<string, PresetColor>();

		static PresetColor()
		{
			AddColor("black", "000000");
			AddColor("white", "FFFFFF");
		}

		private static void AddColor(string name, byte red, byte green, byte blue)
		{
			_colorDic.Add(name, new PresetColor() { Name = name, _red = red, _green = green, _blue = blue });
		}

		private static void AddColor(string name, string hex)
		{

			var rgb = ColorConverter.HexToRGB(hex);

			_colorDic.Add(name, new PresetColor() { Name = name, _red = rgb[0], _green = rgb[1], _blue = rgb[2] });
		}


		/// <summary>
		/// 从相应名称中获得返回值。
		/// </summary>
		public static PresetColor FromColorName(string name)
		{
			if (_colorDic.ContainsKey(name))
				return _colorDic[name];
			else
				return _colorDic["black"];
		}
		#endregion
	}
}
