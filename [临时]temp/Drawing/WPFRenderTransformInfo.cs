﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Drawing
{
	/// <summary>
	/// WPF Visual坐标锚点信息，用于实时编辑。
	/// </summary>
	public struct WPFRenderTransformInfo
	{
		/// <summary>
		/// 角度。
		/// </summary>
		public double Rotation;

		/// <summary>
		/// 左坐标。
		/// </summary>
		public double Left;

		/// <summary>
		/// 顶坐标。
		/// </summary>
		public double Top;

		/// <summary>
		/// 宽度。
		/// </summary>
		public double Width;

		/// <summary>
		/// 宽度。
		/// </summary>
		public double Height;

		/// <summary>
		/// 锚点宽度。
		/// </summary>
		public double AnchorWidth;

		/// <summary>
		/// 锚点高度。
		/// </summary>
		public double AnchorHeight;

		// TODO 反向方向的配置。

		/*
		/// <summary>
		/// 是否水平反向。
		/// </summary>
		public bool HorizontalFlap;

		/// <summary>
		/// 是否垂直反向。
		/// </summary>
		public bool VerticalFlap;
		*/
	}
}
