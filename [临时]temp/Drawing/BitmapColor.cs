﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Drawing
{
	public class BitmapColor:BaseColor
	{
		/// <summary>
		/// 媒体源。
		/// </summary>
		public IMedia Media { get; set; }


		public BitmapColor(IMedia media)
		{
			Media = media;
		}
	}
}
