﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fantom.Drawing
{
	/// <summary>
	/// 矩形的尺寸。
	/// </summary>
	public struct RectScale
	{
		/// <summary>
		/// 矩形的横向拉伸比例。
		/// </summary>
		public readonly double HorizontalRatio;

		/// <summary>
		/// 矩形的纵向拉伸。
		/// </summary>
		public readonly double VerticalRatio;

		public RectScale(double horizontalRatio, double verticalRatio)
		{
			HorizontalRatio = horizontalRatio;
			VerticalRatio = verticalRatio;
		}

		public override string ToString()
		{
			return string.Format("HorzRatio: {0}, VertRatio: {1}",
				Math.Round(HorizontalRatio, 4),
				Math.Round(VerticalRatio, 4));
		}

		public static RectScale Default = new RectScale(1, 1);
	}
}
