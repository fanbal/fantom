﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using D = DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;
using PK = DocumentFormat.OpenXml.Packaging;
using Fantom.Helper;
using Fantom.Drawing;
using Fantom.Primitive;

namespace Fantom.Drawing
{
	/// <summary>
	/// 颜色导入相关方法集合。
	/// </summary>
	internal static class ColorLoader
	{
		#region old methods


		[Obsolete("参数列表冗长", true)]
		public static BaseColor GetDefaultColor
			(PK.SlidePart sldpart, ITheme theme, OpenXmlElement color, IMediaManager mediaManager)
		{
			if (color is D.SchemeColor)
			{
				var attr = (color as D.SchemeColor).Val.ToString();
				var type = ThemeColorTypeHelper.GetColorType(attr);
				return theme.Colors[type];
			}
			else if (color is D.RgbColorModelHex)
			{
				return new SolidColor((color as D.RgbColorModelHex).Val.Value);
			}
			else if (color is D.Blip)
			{
				var uris = from rp in sldpart.Parts where (color as D.Blip).Embed == rp.RelationshipId select rp.OpenXmlPart.Uri;
				var uri = uris.First();

				return new BitmapColor(mediaManager.CreateMedia(uri));
			}
			else
				return null;
		}

		[Obsolete("参数列表冗长", true)]
		public static BaseColor GetDefaultFill
			(PK.SlidePart sldpart, ITheme theme, OpenXmlElement style, IMediaManager mediaManager)
		{
			if (style == null) return null;

			var mark = "fillRef";
			var rst = (from fillnode in style
					   where fillnode.LocalName.EndsWith(mark)
					   select fillnode);

			var fill = rst.FirstOrDefault();
			if (fill == null) return null;

			var color = fill.FirstChild;
			return GetCustomColor(sldpart, theme, color, mediaManager);
		}

		[Obsolete("参数列表冗长", true)]
		public static BaseColor GetDefaultLine
			(PK.SlidePart sldpart, ITheme theme, OpenXmlElement style, IMediaManager mediaManager)
		{
			if (style == null) return null;

			var mark = "lnRef";
			var rst = (from node in style
					   where node.LocalName.EndsWith(mark)
					   select node);

			var line = rst.FirstOrDefault();
			if (line == null) return null;

			var color = line.FirstChild;
			return GetCustomColor(sldpart, theme, color, mediaManager);

		}

		[Obsolete("参数列表冗长", true)]
		public static BaseColor GetCustomColor(ITheme theme, OpenXmlElement color)
		{
			if (color is D.RgbColorModelHex)
			{
				return new SolidColor((color as D.RgbColorModelHex).Val.Value);
			}
			else if (color is D.SchemeColor)
			{
				return GetSchemeColorFromDXMLNode(theme, color as D.SchemeColor);
			}
			else if (color is P.HslColor)
			{
				return GetHSLColorFromDXMLNode(color as P.HslColor);
			}
			throw new Exception("不明颜色节点");

		}

		/// <summary>
		/// 导入颜色。
		/// </summary>
		[Obsolete("参数列表冗长", true)]
		public static BaseColor GetCustomColor
			(PK.SlidePart sldpart, ITheme theme, OpenXmlElement color, IMediaManager mediaManager)
		{
			if (color is D.RgbColorModelHex)
			{
				return SolidColor.FromHex((color as D.RgbColorModelHex).Val.Value);
			}
			else if (color is D.SchemeColor)
			{
				return GetSchemeColorFromDXMLNode(theme, color as D.SchemeColor);
			}
			else if (color is D.Blip)
			{
				var uris = from rp in sldpart.Parts where (color as D.Blip).Embed == rp.RelationshipId select rp.OpenXmlPart.Uri;
				var uri = uris.First();

				return new BitmapColor(mediaManager.CreateMedia(uri));
			}
			else if (color is P.HslColor)
			{
				return GetHSLColorFromDXMLNode(color as P.HslColor);
			}
			throw new Exception("不明颜色节点");

		}

		/// <summary>
		/// 导入颜色。
		/// </summary>
		[Obsolete("参数列表冗长", true)]
		public static BaseColor GetCustomFill
			(PK.SlidePart sldpart, ITheme theme, OpenXmlElement pr, IMediaManager mediaManager)
		{
			var mark = "Fill";
			var rst = (from fillnode in pr
					   where fillnode.LocalName.EndsWith(mark)
					   select fillnode);

			var fill = rst.FirstOrDefault();
			if (fill == null) return null;

			var color = fill.FirstChild;

			return GetCustomColor(sldpart, theme, color, mediaManager);

		}

		[Obsolete("参数列表冗长", true)]
		public static BaseColor GetCustomLine
			(PK.SlidePart sldpart, ITheme theme, OpenXmlElement pr, IMediaManager mediaManager)
		{
			var mark = "ln";
			var rst = (from fillnode in pr
					   where fillnode.LocalName.EndsWith(mark)
					   select fillnode);

			var line = rst.FirstOrDefault();
			if (line == null) return null;

			return GetCustomFill(sldpart, theme, line, mediaManager);

		}


		// 从 D.SchemeColor 对象中提取指定细节颜色。
		[Obsolete("参数列表冗长", true)]
		public static SchemeColor GetSchemeColorFromDXMLNode(ITheme theme, D.SchemeColor dcolor)
		{
			var attr = dcolor.Val.ToString();
			var type = ThemeColorTypeHelper.GetColorType(attr);

			var xmod = dcolor.GetFirstChild<D.LuminanceModulation>();
			var xoff = dcolor.GetFirstChild<D.LuminanceOffset>();
			var xalp = dcolor.GetFirstChild<D.Alpha>();
			var xshd = dcolor.GetFirstChild<D.Shade>();

			var off = xoff != null ? xoff.Val.Value : Emu.Num0;
			var mod = xmod != null ? xmod.Val.Value : Emu.Num100;
			var alp = xalp != null ? xalp.Val.Value : Emu.Num0;
			var shd = xshd != null ? xshd.Val.Value : Emu.Num0;

			if (xoff != null || xmod != null)
				return SchemeColor.FromLum(theme.Colors[type], alp, mod, off, type);
			else if (xshd != null)
				return SchemeColor.FromShade(theme.Colors[type], alp, shd, type);

			return SchemeColor.FromLum(theme.Colors[type], alp, mod, off, type);
		}

		#endregion old methods

		#region new methods

		/// <summary>
		/// 获取来自母版的默认填充。
		/// </summary>
		public static BaseColor GetDefaultFill(OpenXmlElement style, ColorLoadArgs loadArgs)
		{
			if (style == null) return null;

			var fill = style.GetFirstChild<D.FillReference>();
			if (fill == null) return null;

			return GetColorFromNodeWithColor(fill, loadArgs);
		}

		/// <summary>
		/// 获取来自母版的默认线框填充。
		/// </summary>
		public static BaseColor GetDefaultLine
			(OpenXmlElement style, ColorLoadArgs loadArgs)
		{
			if (style == null) return null;

			var line = style.GetFirstChild<D.LineReference>();

			if (line == null) return null;

			return GetColorFromNodeWithColor(line, loadArgs);

		}

		/// <summary>
		/// 从节点中查找并提取颜色信息。
		/// </summary>
		public static BaseColor GetColorFromNodeWithColor(this OpenXmlElement element, ColorLoadArgs loadArgs)
		{
			foreach (var child in element.ChildElements)
			{
				if (child is D.RgbColorModelHex ||
					child is D.RgbColorModelPercentage ||
					child is D.SystemColor ||
					child is D.HslColor ||
					child is D.SchemeColor ||
					child is D.PresetColor)
					return GetCustomColor(child, loadArgs);
			}
			return null;
		}

		/// <summary>
		/// 导入颜色。
		/// </summary>
		public static BaseColor GetCustomColor(OpenXmlElement color, ColorLoadArgs loadArgs)
		{
			if (color is D.RgbColorModelHex)
			{
				return SolidColor.FromHex((color as D.RgbColorModelHex).Val.Value);
			}
			else if (color is D.SchemeColor)
			{
				return GetSchemeColorFromDXMLNode(color as D.SchemeColor, loadArgs);
			}
			else if (color is D.Blip)
			{
				return GetBitmapFromDXMLNode(color as D.Blip, loadArgs);
			}
			else if (color is P.HslColor)
			{
				return GetHSLColorFromDXMLNode(color as P.HslColor);
			}
			else if (color is D.PresetColor)
			{
				return GetPresetColorFromDXMLNode(color as D.PresetColor);
			}
			throw new Exception("不明颜色节点");

		}

		/// <summary>
		/// 获得自定义填充。
		/// </summary>
		public static BaseColor GetCustomFill(OpenXmlElement pr, ColorLoadArgs loadArgs)
		{
			var fill = pr.GetFirstChild<D.Fill>();
			if (fill == null) return null;

			return GetColorFromNodeWithColor(fill, loadArgs);

		}


		/// <summary>
		/// 获得自定义线框填充。
		/// </summary>
		public static BaseColor GetCustomLine(OpenXmlElement pr, ColorLoadArgs loadArgs)
		{
			var line = pr.GetFirstChild<D.Outline>();
			if (line == null) return null;

			return GetColorFromNodeWithColor(line, loadArgs);


		}

		/// <summary>
		/// 从节点中提取的图片相关数据。
		/// </summary>
		public static BitmapColor GetBitmapFromDXMLNode(D.Blip color, ColorLoadArgs loadArgs)
		{
			var uris =
				from rp in loadArgs.SlidePart.Parts
				where color.Embed == rp.RelationshipId
				select rp.OpenXmlPart.Uri;

			var uri = uris.First();

			return new BitmapColor(loadArgs.MediaManager.CreateMedia(uri));
		}


		/// <summary>
		/// 从节点中提取与之匹配的主题颜色。
		/// </summary>
		public static SchemeColor GetSchemeColorFromDXMLNode(D.SchemeColor color, ColorLoadArgs loadArgs)
		{
			var attr = color.Val.ToString();
			var type = ThemeColorTypeHelper.GetColorType(attr);

			var xmod = color.GetFirstChild<D.LuminanceModulation>();
			var xoff = color.GetFirstChild<D.LuminanceOffset>();
			var xalp = color.GetFirstChild<D.Alpha>();
			var xshd = color.GetFirstChild<D.Shade>();

			var off = xoff != null ? xoff.Val.Value : Emu.Num0;
			var mod = xmod != null ? xmod.Val.Value : Emu.Num100;
			var alp = xalp != null ? xalp.Val.Value : Emu.Num0;
			var shd = xshd != null ? xshd.Val.Value : Emu.Num0;

			if (xoff != null || xmod != null)
				return SchemeColor.FromLum(loadArgs.Theme.Colors[type], alp, mod, off, type);

			else if (xshd != null)
				return SchemeColor.FromShade(loadArgs.Theme.Colors[type], alp, shd, type);

			return SchemeColor.FromLum(loadArgs.Theme.Colors[type], alp, mod, off, type);
		}


		/// <summary>
		/// 从节点数据中提取正确的 HSL 变化值。
		/// </summary>
		/// <param name="color"></param>
		/// <returns></returns>
		public static SolidColor GetHSLColorFromDXMLNode(P.HslColor color)
		{
			var hue = 255d * color.Hue.Value / Emu.TwoPi;
			var sat = 255d * color.Saturation.Value / Emu.TwoPi;
			var lum = 255d * color.Lightness.Value / Emu.TwoPi;
			return SolidColor.FromHSL(hue, sat, lum);
		}

		/// <summary>
		/// 从节点数据中提取预设颜色。
		/// </summary>
		public static PresetColor GetPresetColorFromDXMLNode(D.PresetColor color)
		{
			var c = PresetColor.FromColorName(color.Val);
			c.Alpha = GetValue<D.Alpha>(color, Emu.Num0);
			return c;
		}


		private static Emu GetValue<T>(OpenXmlElement color, Emu def) where T : OpenXmlElement
		{
			var node = color.GetFirstChild<T>();
			if (node == null)
				return def;
			else
				return Emu.Parse(node.GetAttribute("val", "").Value);
		}

		private static byte GetValue<T>(OpenXmlElement color, byte def) where T : OpenXmlElement
		{
			var node = color.GetFirstChild<T>();
			if (node == null)
				return def;
			else
				return byte.Parse(node.GetAttribute("val", "").Value);
		}

		#endregion new methods

	}
}
