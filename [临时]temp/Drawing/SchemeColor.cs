﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml.Presentation;

namespace Fantom.Drawing
{
	//<a:solidFill>
	//	<a:schemeClr val = "accent1" >
	//		< a:lumMod val = "50000" />
	//		< a:alpha val = "50000" />
	//	</ a:schemeClr>
	//</a:solidFill>

	/// <summary>
	/// 纯色块的一阶引用。。
	/// </summary>
	public class SchemeColor : BaseColor
	{
		#region properties

		private BaseColor _origin = null; // 源颜色。

		public override byte Red => _red;
		public override byte Blue => _blue;
		public override byte Green => _green;


		/// <summary>
		/// 透明度。
		/// </summary>
		public Emu Alpha => _alpha;
		private Emu _alpha = Emu.Num100;

		/// <summary>
		/// 亮度调节值。
		/// </summary>
		public Emu LumMod => _lumMod;
		private Emu _lumMod = Emu.Num100;

		/// <summary>
		/// 阴影，通常用于描述默认线条的颜色，作用如LumMod，两者不兼容。
		/// </summary>
		public Emu Shade => _shade;
		private Emu _shade = Emu.Num0;

		private bool _isDarken = true; // 是否为暗色调。

		/// <summary>
		/// 主题颜色类型。
		/// </summary>
		public ThemeColorType ThemeColorType { get; private set; }

		#endregion

		#region ctors

		private SchemeColor() { }

		/// <summary>
		/// 构造函数。
		/// </summary>
		/// <param name="origin"></param>
		private SchemeColor(BaseColor origin, Emu alpha, Emu lumMod, Emu lumOff, ThemeColorType themeColorType)
		{
			if (origin == null)
				throw new ArgumentNullException("SchemeColor中参数不允许为空值");
			_origin = origin;

			var hsl = ColorConverter.RGBToHSL(_origin.Red, _origin.Green, _origin.Blue);


			_alpha = alpha;
			_lumMod = lumMod;


			// 暗色调。
			if (lumOff == Emu.Num0)
			{
				_isDarken = true;
				hsl[2] *= (1d * lumMod.Value / Emu.Num100.Value);
			}
			else
			{
				_isDarken = false;
				hsl[2] = hsl[2] + (1 - hsl[2]) * 1d * lumOff.Value / Emu.Num100.Value;
			}


			var realRgb = ColorConverter.HSLToRGB2(hsl[0], hsl[1], hsl[2]);
			_red = realRgb[0];
			_green = realRgb[1];
			_blue = realRgb[2];

			ThemeColorType = themeColorType;
		}

		private SchemeColor(BaseColor origin, Emu alpha, Emu lumMod, ThemeColorType themeColorType) :
			this(origin, alpha, lumMod, Emu.Num0, themeColorType)
		{ }


		/// <summary>
		/// 拷贝构造函数。
		/// </summary>
		public SchemeColor(SchemeColor color, ThemeColorType themeColorType)
		{
			if (color == null )
				throw new ArgumentNullException("SchemeColor中参数不允许为空值");
			ThemeColorType = themeColorType;
			_origin = color._origin;
		}

		#endregion


		#region static

		public static SchemeColor FromLum(BaseColor origin, Emu alpha, Emu lumMod, Emu lumOff, ThemeColorType themeColorType)
		{
			var color = new SchemeColor();

			if (origin == null)
				throw new ArgumentNullException("SchemeColor中参数不允许为空值");
			color._origin = origin;

			var hsl = ColorConverter.RGBToHSL(origin.Red, origin.Green, origin.Blue);


			color._alpha = alpha;
			color._lumMod = lumMod;


			// 暗色调。
			if (lumOff == Emu.Num0)
			{
				color._isDarken = true;
				hsl[2] *= (1d * lumMod.Value / Emu.Num100.Value);
			}
			else
			{
				color._isDarken = false;
				hsl[2] = hsl[2] + (1 - hsl[2]) * 1d * lumOff.Value / Emu.Num100.Value;
			}


			var realRgb = ColorConverter.HSLToRGB2(hsl[0], hsl[1], hsl[2]);
			color._red = realRgb[0];
			color._green = realRgb[1];
			color._blue = realRgb[2];

			color.ThemeColorType = themeColorType;

			return color;
		}

		public static SchemeColor FromShade(BaseColor origin, Emu alpha, Emu shade, ThemeColorType themeColorType)
		{
			var color = new SchemeColor();

			if (origin == null )
				throw new ArgumentNullException("SchemeColor中参数不允许为空值");
			color._origin = origin;

			var hsl = ColorConverter.RGBToHSL(origin.Red, origin.Green, origin.Blue);


			color._alpha = alpha;
			color._shade = shade;
			color._isDarken = true;

			hsl[2] *= (1d * shade.Value / Emu.Num100.Value);


			var realRgb = ColorConverter.HSLToRGB2(hsl[0], hsl[1], hsl[2]);
			color._red = realRgb[0];
			color._green = realRgb[1];
			color._blue = realRgb[2];

			color.ThemeColorType = themeColorType;

			return color;
		}


		#endregion

	}
}
