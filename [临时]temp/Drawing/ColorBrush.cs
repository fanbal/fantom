﻿namespace Fantom.Drawing
{




	/*
					<a:solidFill>
						<a:schemeClr val="tx1"/>
					</a:solidFill>
	
					<a:solidFill>
						<a:srgbClr val="66CCFF"/>
					</a:solidFill>

					<a:gradFill flip="none"
								rotWithShape="1">
						<a:gsLst>
							<a:gs pos="0">
								<a:schemeClr val="accent6">
									<a:tint val="66000"/>
									<a:satMod val="160000"/>
								</a:schemeClr>
							</a:gs>
							<a:gs pos="50000">
								<a:schemeClr val="accent6">
									<a:tint val="44500"/>
									<a:satMod val="160000"/>
								</a:schemeClr>
							</a:gs>
							<a:gs pos="100000">
								<a:schemeClr val="accent6">
									<a:tint val="23500"/>
									<a:satMod val="160000"/>
								</a:schemeClr>
							</a:gs>
						</a:gsLst>
						<a:lin ang="13500000"
							   scaled="1"/>
						<a:tileRect/>
					</a:gradFill>

					<a:solidFill>
						<a:schemeClr val="accent1">
							<a:lumMod val="50000"/>
							<a:alpha val="50000"/>
						</a:schemeClr>
					</a:solidFill>

					<a:solidFill>
						<a:srgbClr val="00B050">
							<a:alpha val="50000"/>
						</a:srgbClr>
					</a:solidFill>

					<a:blipFill dpi="0"
								rotWithShape="1">
						<a:blip r:embed="rId13">
							<a:alphaModFix amt="97000"/>
						</a:blip>
						<a:srcRect/>
						<a:tile tx="0"
								ty="0"
								sx="100000"
								sy="100000"
								flip="none"
								algn="tl"/>
					</a:blipFill>

					<a:pattFill prst="pct5">
						<a:fgClr>
							<a:schemeClr val="accent1"/>
						</a:fgClr>
						<a:bgClr>
							<a:schemeClr val="bg1"/>
						</a:bgClr>
					</a:pattFill>



	 */

	/// <summary>
	/// 填充格式，此处用于呈现填充的内容。
	/// </summary>
	public abstract class BaseFill
	{
	}


	/// <summary>
	/// 纯色填充。
	/// </summary>
	public sealed class SolidFill : BaseFill
	{
		#region properties

		/// <summary>
		/// 颜色。
		/// </summary>
		public SolidColor Color { get; set; } = null;

		/// <summary>
		/// 透明度，以 <see cref="Emu.Num0"/>  ~ <see cref="Emu.Num100"/> 为范围。
		/// 可以通过 小数百分比 * <see cref="Emu.Num100"/> 来赋值。
		/// 如 Alpha = 0.5 * <see cref="Emu.Num100"/> 。
		/// </summary>
		public Emu Alpha { get; set; } = Emu.Num100;

		/// <summary>
		/// 亮度，以 <see cref="Emu.Num0"/>  ~ <see cref="Emu.Num100"/> 为范围。
		/// 可以通过 小数百分比 * <see cref="Emu.Num100"/> 来赋值。
		/// 如 Alpha = 0.5 * <see cref="Emu.Num100"/> 。
		/// </summary>
		public Emu LumMod { get; set; } = Emu.Num100;

		#endregion


		#region ctors

		public SolidFill(SolidColor color, Emu alpha)
		{
			Color = color;
			Alpha = alpha;
		}

		#endregion

	}

	/// <summary>
	/// 空填充。
	/// </summary>
	public sealed class NoFill : BaseFill
	{

	}


	/// <summary>
	/// 颜色的基类,对应节点字段为clr。
	/// </summary>
	public abstract class BaseColorBrush : EchoBaseObject
	{
		#region properties

		//tint

		private Emu _tint = 0L;

		/// <summary>
		/// 色调，节点为a:tint
		/// </summary>
		public Emu Tint
		{
			get => _tint;
			set { _tint = value; PropertyChangedInvoke(); }
		}

		//shade

		Emu _shade = 0;

		/// <summary>
		/// 阴影，节点为a:shade
		/// </summary>
		public Emu Shade
		{
			get => _shade;
			set { _shade = value; PropertyChangedInvoke(); }
		}


		//comp

		private Emu _comp = 0;

		/// <summary>
		/// 复合，节点为a:comp
		/// </summary>
		public Emu Comp //TODO 写全称
		{
			get => _comp;
			set { _comp = value; PropertyChangedInvoke(); }
		}

		//inv

		Emu _inv = 0;

		/// <summary>
		/// 反转，节点为a:inv
		/// </summary>
		public Emu Inverse
		{
			get => _inv;
			set { _inv = value; PropertyChangedInvoke(); }
		}

		//gray

		private Emu _gray = 0;

		/// <summary>
		/// 灰度，节点为a:gray
		/// </summary>
		public Emu Gray
		{
			get => _gray;
			set { _gray = value; PropertyChangedInvoke(); }
		}

		//alpha

		Emu _alpha = 0;

		/// <summary>
		/// 透明度，节点为a:alpha
		/// </summary>
		public Emu Alpha
		{
			get => _alpha;
			set { _alpha = value; PropertyChangedInvoke(); }
		}

		//alphaOff

		Emu _alphaOff = 0;

		/// <summary>
		/// 透明度偏移，节点为a:alphaOff
		/// </summary>
		public Emu AlphaOff
		{
			get => _alphaOff;
			set { _alphaOff = value; PropertyChangedInvoke(); }
		}

		//hue

		Emu _hue = 0;

		/// <summary>
		/// 色彩，节点为a:hue
		/// </summary>
		public Emu Hue
		{
			get => _hue;
			set { _hue = value; PropertyChangedInvoke(); }
		}

		//hueOff

		Emu _hueOff = 0;

		/// <summary>
		/// 色彩偏移，节点为a:hueOff
		/// </summary>
		public Emu HueOff
		{
			get => _hueOff;
			set { _hueOff = value; PropertyChangedInvoke(); }
		}

		//hueMod

		Emu _hueMod = 0;

		/// <summary>
		/// 色彩修正，节点为a:hueMod
		/// </summary>
		public Emu HueMod
		{
			get => _hueMod;
			set { _hueMod = value; PropertyChangedInvoke(); }
		}

		//sat

		Emu _sat = 0;

		// TODO 不知大Sat是什么。
		/// <summary>
		/// ？？？，节点为a:sat
		/// </summary>
		public Emu Sat
		{
			get => _sat;
			set { _sat = value; PropertyChangedInvoke(); }
		}

		//satOff

		Emu _satOff = 0;

		// TODO 不知到Sat是什么。
		/// <summary>
		/// ？？？，节点为a:satOff
		/// </summary>
		public Emu SatOff
		{
			get => _satOff;
			set { _satOff = value; PropertyChangedInvoke(); }
		}

		//satMod

		Emu _satMod = 0;

		// TODO 不知到Sat是什么。
		/// <summary>
		/// ？？？，节点为a:satMod
		/// </summary>
		public Emu SatMod
		{
			get => _satMod;
			set { _satMod = value; PropertyChangedInvoke(); }
		}

		//lum

		Emu _lum = 0;

		/// <summary>
		/// 亮度，节点为a:lum
		/// </summary>
		public Emu Luminance
		{
			get => _lum;
			set { _lum = value; PropertyChangedInvoke(); }
		}

		//lumOff

		Emu _lumOff = 0;

		/// <summary>
		/// 亮度偏移，节点为a:lumOff
		/// </summary>
		public Emu LuminanceOff
		{
			get => _lumOff;
			set { _lumOff = value; PropertyChangedInvoke(); }
		}

		//red

		Emu _red = 0;

		/// <summary>
		/// 红色通道，节点为a:red
		/// </summary>
		public Emu Red
		{
			get => _red;
			set { _red = value; PropertyChangedInvoke(); }
		}

		//redOff

		Emu _redOff = 0;

		/// <summary>
		/// 红色偏移，节点为a:redOff
		/// </summary>
		public Emu RedOff
		{
			get => _redOff;
			set { _redOff = value; PropertyChangedInvoke(); }
		}

		//redMod

		Emu _redMod = 0;

		/// <summary>
		/// 红色修正，节点为a:redMod
		/// </summary>
		public Emu RedMod
		{
			get => _redMod;
			set { _redMod = value; PropertyChangedInvoke(); }
		}

		//green

		Emu _green = 0;

		/// <summary>
		/// 绿色通道，节点为a:green
		/// </summary>
		public Emu Green
		{
			get => _green;
			set { _green = value; PropertyChangedInvoke(); }
		}

		//greenOff

		Emu _greenOff = 0;

		/// <summary>
		/// 绿色偏移，节点为a:greenOff
		/// </summary>
		public Emu GreenOff
		{
			get => _greenOff;
			set { _greenOff = value; PropertyChangedInvoke(); }
		}


		//greenMod

		Emu _greenMod = 0;

		/// <summary>
		/// 绿色修正，节点为a:greenMod
		/// </summary>
		public Emu GreenMod
		{
			get => _greenMod;
			set { _greenMod = value; PropertyChangedInvoke(); }
		}


		//blue

		Emu _blue = 0;

		/// <summary>
		/// 蓝色通道，节点为a:blue
		/// </summary>
		public Emu Blue
		{
			get => _blue;
			set { _blue = value; PropertyChangedInvoke(); }
		}

		//blueOff

		Emu _blueOff = 0;

		/// <summary>
		/// 蓝色偏移，节点为a:blueOff
		/// </summary>
		public Emu BlueOff
		{
			get => _blueOff;
			set { _blueOff = value; PropertyChangedInvoke(); }
		}


		//blueMod

		Emu _blueMod = 0;

		/// <summary>
		/// 蓝色修正，节点为a:blueMod
		/// </summary>
		public Emu BlueMod
		{
			get => _blueMod;
			set { _blueMod = value; PropertyChangedInvoke(); }
		}

		//gamma

		Emu _gamma = 0;

		/// <summary>
		/// 伽马值，节点为a:gamma
		/// </summary>
		public Emu Gamma
		{
			get => _gamma;
			set { _gamma = value; PropertyChangedInvoke(); }
		}


		//invGamma

		Emu _invGamma = 0;

		/// <summary>
		/// 反伽马值，节点为a:invGamma
		/// </summary>
		public Emu InvGamma
		{
			get => _invGamma;
			set { _invGamma = value; PropertyChangedInvoke(); }
		}


		#endregion

		#region ctors
		// 禁止直接构造。
		protected BaseColorBrush() { }
		#endregion

		#region methods

		// 将一个颜色的全部信息拷贝至另一个对象上。
		protected static void CopyTo(BaseColorBrush brushfrom, BaseColorBrush brushto)
		{

			brushto._alpha = brushfrom.Alpha;
			brushto._alphaOff = brushfrom.AlphaOff;
			brushto._blue = brushfrom.Blue;
			brushto._blueOff = brushfrom.BlueOff;
			brushto._blueMod = brushfrom.BlueMod;
			brushto._comp = brushfrom.Comp;
			brushto._gamma = brushfrom.Gamma;
			brushto._gray = brushfrom.Gray;
			brushto._red = brushfrom.Red;
			brushto._redOff = brushfrom.RedOff;
			brushto._redMod = brushfrom.RedMod;
			brushto._green = brushfrom.Green;
			brushto._greenOff = brushfrom.GreenOff;
			brushto._greenMod = brushfrom.GreenMod;
			brushto._hue = brushfrom.Hue;
			brushto._hueOff = brushfrom.HueOff;
			brushto._hueMod = brushfrom.HueMod;
			brushto._inv = brushfrom.Inverse;
			brushto._invGamma = brushfrom.InvGamma;
			brushto._lum = brushfrom.Luminance;
			brushto._lumOff = brushfrom.LuminanceOff;
			brushto._sat = brushfrom.Sat;
			brushto._satOff = brushfrom.SatOff;
			brushto._satMod = brushfrom.SatMod;
			brushto._shade = brushfrom.Shade;
			brushto._tint = brushfrom.Tint;
		}


		public override string ToString()
		{
			return string.Format("RGB: ({0}, {1}, {2}), HSL: ({3}, {4}, {5})",
				this.Red, this.Green, this.Blue, this.Hue, this.Sat, this.Luminance);
		}
		#endregion

		#region  static
		public static void SetColorFromHexBinString(BaseColorBrush brush, string hex)
		{
			int[] hexArr = new int[6];
			for (int i = 0; i < 6; i++)
			{
				if (char.IsNumber(hex[i]))
					hexArr[i] = hex[i] - '0';
				else if (char.IsUpper(hex[i]))
					hexArr[i] = hex[i] - 'A' + 10;
				else
					hexArr[i] = hex[i] - 'a' + 10;
			}

			brush.Red = hexArr[0] * 16 + hexArr[1];
			brush.Green = hexArr[2] * 16 + hexArr[3];
			brush.Blue = hexArr[4] * 16 + hexArr[5];
		}
		#endregion
	}

	/// <summary>
	/// 默认色盘颜色。无法被轻易构造，只能被引用。
	/// </summary>
	public sealed class SchemeColorBrush : BaseColorBrush
	{
		#region properties
		private ThemeColorType _type =   ThemeColorType.Unknown;

		// 系统颜色名。
		public ThemeColorType Type { get => _type; set { _type = value; PropertyChangedInvoke(); } }

		#endregion

		#region ctors

		/// <summary>
		/// 建立一个色盘对象。
		/// </summary>
		/// <returns>色盘颜色对象。</returns>
		public static SchemeColorBrush Build()
		{
			return new SchemeColorBrush();
		}

		#endregion


		#region override
		public override string ToString()
		{
			return "Scheme: " + _type + ", " + base.ToString();
		}
		#endregion
	}


	/// <summary>
	/// 系统颜色，无法被轻易构造，只能被引用。
	/// </summary>
	public sealed class SystemColorBrush : BaseColorBrush
	{
		#region properties
		private string _name = "Unknown";

		// 系统颜色名。
		public string Name { get => _name; set { _name = value; PropertyChangedInvoke(); } }
		#endregion

		#region ctors

		/// <summary>
		/// 建立一个系统对象。
		/// </summary>
		/// <returns>系统颜色对象。</returns>
		public static SystemColorBrush Build()
		{
			return new SystemColorBrush();
		}

		#endregion

		#region override
		public override string ToString()
		{
			return "Name: " + Name + ", " + base.ToString();
		}
		#endregion

	}

	/// <summary>
	/// 纯色色刷，不能被引用，只能被构造。
	/// </summary>
	public sealed class SolidRGBColorBrush : BaseColorBrush
	{
		#region ctors

		/// <summary>
		/// 建立一个纯色颜色画刷对象。
		/// </summary>
		public SolidRGBColorBrush() : base()
		{

		}

		public SolidRGBColorBrush(BaseColorBrush brush) : base()
		{
			CopyTo(brush, this);
		}

		public SolidRGBColorBrush(string hex)
		{
			SetColorFromHexBinString(this, hex);
		}

		#endregion


		#region static
		public SolidRGBColorBrush FromHex(string hex)
		{
			return new SolidRGBColorBrush(hex);
		}
		#endregion

	}


}
