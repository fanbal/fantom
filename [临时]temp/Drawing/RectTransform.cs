﻿using System;
using System.ComponentModel;
using Fantom.Models;
namespace Fantom.Drawing
{

	/// <summary>
	/// 形变对象。
	/// </summary>
	public class RectTransform : INotifyPropertyChanged
	{
		#region events

		public event PropertyChangedEventHandler PropertyChanged;
		#endregion


		#region properties

		public IShape Shape { get; internal set; }

		private AnchorTransform _anchorTransform = AnchorTransform.Default;

		/// <summary>
		/// 锚点信息。
		/// </summary>
		public AnchorTransform AnchorTransform
		{
			get => _anchorTransform;
			set
			{
				_anchorTransform = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AnchorTransform"));
				(Shape as Shape).RaiseRenderUpdate();
			}
		}

		private RectSize _rectSize = RectSize.Default;

		/// <summary>
		/// 矩形的尺寸，包含Width与Height的定义。
		/// </summary>
		public RectSize RectSize
		{
			get => _rectSize;
			set
			{
				_rectSize = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RectSize"));
				(Shape as Shape).RaiseRenderUpdate();
			}
		}

		// TODO 待开发，矩形的比例修正，可用于视图缩放。

		/*
		private RectScale _rectScale = RectScale.Default;

		/// <summary>
		/// 矩形的比例修正，包含WidthScale与HeightScale的定义，。
		/// </summary>
		public RectScale RectScale
		{
			get => _rectScale;
			set
			{
				_rectScale = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RectScale"));
			}
		}
		*/

		private Emu _rotation = new Emu(0);

		/// <summary>
		/// 角度。
		/// </summary>
		public Emu Rotation
		{
			get => _rotation;
			set
			{
				var v = value.Value;
				if (v < 0)
				{
					var nv = (-v) % Emu.TwoPi;
					v = Emu.TwoPi - nv;
				}
				else if (v > Emu.TwoPi)
				{
					v = v % Emu.TwoPi;
				}

				_rotation = v;

				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Rotation"));
				(Shape as Shape).RaiseRenderUpdate();
			}
		}

		#endregion


		#region ctors
		public RectTransform(IShape shape)
		{
			Shape = shape;

		}


		#endregion

		#region methods


		// TODO 待完成PML相关设定的描述。
		/// <summary>
		/// PML规范的形变参数。
		/// </summary>
		public PMLRenderTransform GetPMLRenderTransform() { return new PMLRenderTransform(); }

		/// <summary>
		/// WPF系统的形变参数。
		/// </summary>
		public WPFRenderTransformInfo GetWPFRenderTransformInfo()
		{
			return new WPFRenderTransformInfo()
			{
				Rotation = Rotation.ToAngle() * Math.PI / 180,
				Left = AnchorTransform.Left.ToPixel().Value,
				Top = AnchorTransform.Top.ToPixel().Value,
				Width = RectSize.Width.ToPixel().Value,
				Height = RectSize.Height.ToPixel().Value,
				AnchorWidth = AnchorTransform.WidthRatio,
				AnchorHeight = AnchorTransform.HeightRatio,
				// TODO 反向方向的配置。
			};

		}

		/// <summary>
		/// 以画布像素尺寸缩放宽高（不是比例）。
		/// </summary>
		/// <param name="deltaw"></param>
		/// <param name="deltah"></param>
		public void IncreaseSize(double deltaw, double deltah)
		{
			var dw = new Pixel(deltaw).ToEmu();
			var dh = new Pixel(deltah).ToEmu();

			IncreaseSize(dw, dh);


		}

		/// <summary>
		/// 以Emu尺寸格式缩放宽高。
		/// </summary>
		/// <param name="deltaw"></param>
		/// <param name="deltah"></param>
		public void IncreaseSize(Emu deltaw, Emu deltah)
		{
			var dx = deltaw * AnchorTransform.WidthRatio;
			var dy = deltah * AnchorTransform.HeightRatio;
			var w = RectSize.Width + deltaw;
			var h = RectSize.Height + deltah;
			_rectSize = new RectSize(w, h);
			_anchorTransform = new AnchorTransform(
				AnchorTransform.WidthRatio,
				AnchorTransform.HeightRatio,
				_anchorTransform.Left - dx,
				_anchorTransform.Top - dy);

			(Shape as Shape).RaiseRenderUpdate();
		}

		/// <summary>
		/// 以画布像素尺寸移动。
		/// </summary>
		/// <param name="deltax"></param>
		/// <param name="deltay"></param>
		public void IncreaseOffset(double deltax, double deltay)
		{
			var dx = new Pixel(deltax).ToEmu();
			var dy = new Pixel(deltay).ToEmu();
			IncreaseOffset(dx, dy);
		}

		/// <summary>
		/// 以Emu尺寸格式移动。
		/// </summary>
		/// <param name="deltax"></param>
		/// <param name="deltay"></param>
		public void IncreaseOffset(Emu deltax, Emu deltay)
		{
			var x = _anchorTransform.Left + deltax;
			var y = _anchorTransform.Top + deltay;
			_anchorTransform = new AnchorTransform(
				_anchorTransform.WidthRatio,
				_anchorTransform.HeightRatio,
				x, y);

			(Shape as Shape).RaiseRenderUpdate();
		}

		#endregion


		#region events

		#endregion


	}
}
