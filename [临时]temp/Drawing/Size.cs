﻿namespace Fantom.Drawing
{
	/// <summary>
	/// 尺寸。
	/// </summary>
	public struct Size
	{
		private Emu _width;

		/// <summary>
		/// 宽度。
		/// </summary>
		public Emu Width
		{
			get { return _width; }
			set { _width = value; }
		}

		private Emu _height;

		/// <summary>
		/// 高度。
		/// </summary>
		public Emu Height
		{
			get { return _height; }
			set { _height = value; }
		}

		public Size(Emu width, Emu height)
		{
			_width = width;
			_height = height;
		}

		public override string ToString()
		{
			return string.Format("Width: {0}, Height: {1}", Width.ToPixel(), Height.ToPixel());
		}

		public static bool operator ==(Size s1, Size s2)
		{
			return s1.Width == s2.Width && s1.Height == s2.Height;
		}


		public static bool operator !=(Size s1, Size s2)
		{
			return s1.Width != s2.Width || s1.Height != s2.Height;
		}

		public override int GetHashCode()
		{
			int v1 = (int)this.Width.Value;
			int v2 = (int)this.Height.Value;
			return v1 % 1000 * 10 + v2 % 1000;
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			if (obj.GetType() == this.GetType())
			{
				if (obj.GetHashCode() == this.GetHashCode())
				{
					var s2 = (Size)obj;
					return (this.Width == s2.Width && this.Height == s2.Height);
				}
				else
					return false;
			}
			else
				return false;
		}

		public static Size Zero = new Size() { Width = 0, Height = 0 };
	}
}