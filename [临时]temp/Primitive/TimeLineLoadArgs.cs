﻿using System;
using System.Collections.Generic;
using System.Text;
using D = DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;
using PK = DocumentFormat.OpenXml.Packaging;
using Fantom.Models;

namespace Fantom.Primitive
{
	/// <summary>
	/// 建立时间轴时会传递的相关公共参数。
	/// </summary>
	class TimeLineLoadArgs
	{

		/// <summary>
		/// 调用事件的句柄。
		/// </summary>
		public Fantom.Builders.PresentationLoader.RaiseLoadingBundleEventHandler Handler;

		/// <summary>
		/// 顶层的应用对象。
		/// </summary>
		public readonly Application Application;

		/// <summary>
		/// 图形集合对象。
		/// </summary>
		public readonly Shapes Shapes;

		/// <summary>
		/// 颜色相关参数。
		/// </summary>
		public readonly ColorLoadArgs ColorLoadArgs;

		public TimeLineLoadArgs(Application application, Shapes shapes, ColorLoadArgs colorArgs, Fantom.Builders.PresentationLoader.RaiseLoadingBundleEventHandler handler)
		{
			Application = application;
			Shapes = shapes;
			ColorLoadArgs = colorArgs;
			Handler = handler;
		}

	}
}
