﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Fantom.IconSelector
{
	public class IconInfo
	{
		/// <summary>
		/// 图标的名称。
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// 图标的位置。
		/// </summary>
		public string Path { get; set; }


		public override string ToString()
		{
			return Name;
		}


	}
}
