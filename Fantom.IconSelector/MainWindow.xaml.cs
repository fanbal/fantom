﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.ComponentModel;

namespace Fantom.IconSelector
{
	public partial class MainWindow : Window, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private Dictionary<IconInfo, UIElement> _iconDic;
		private List<IconInfo> _iconList;
		private HashSet<IconInfo> _iconHashSet;
		public int CurrentId { get => _currentId; set { _currentId = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentId")); } }
		private int _currentId = 0;
		public MainWindow()
		{
			InitializeComponent();
			DataContext = this;
			var rootPath = GetIconResourceDir();
			_iconList = GetIconInfos(rootPath);
			_iconDic = new Dictionary<IconInfo, UIElement>();
			_iconHashSet = new HashSet<IconInfo>();
			PART_slider.Maximum = _iconList.Count - 1;
			PART_slider.Value = 0;

			LoadSvg(AddIcon(_iconList[CurrentId]));
			PART_list.ItemsSource = _iconHashSet;
		}

		private void Window_MoveWindow(object sender, MouseButtonEventArgs e)
		{
			if (e.ButtonState == MouseButtonState.Pressed)
				DragMove();
		}

		/// <summary>
		/// 添加图标。
		/// </summary>
		/// <param name="iconInfo"></param>
		private UIElement AddIcon(IconInfo iconInfo)
		{
			if (_iconDic.ContainsKey(iconInfo))
				return _iconDic[iconInfo];
			var text = GetXamlText(iconInfo.Path);
			var svg = (UIElement)XamlReader.Parse(text);
			_iconDic.Add(iconInfo, svg);
			return svg;
		}

		/// <summary>
		/// 获得 SVG 文本。
		/// </summary>
		private string GetXamlText(string path)
		{
			var bytes = System.IO.File.ReadAllBytes(path);
			if (bytes[0] == 0xEF && bytes[1] == 0xBB && bytes[2] == 0xBF)
				return Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
			else
				return Encoding.UTF8.GetString(bytes);
		}

		private List<IconInfo> GetIconInfos(string root)
		{
			var list = new List<IconInfo>();
			var dirs = Directory.GetDirectories(root);
			//for(int i = 0; i<10; i++)
			foreach (var dir in dirs)
			{
				//var dir = dirs[i];
				foreach (var file in Directory.GetFiles(dir))
				{
					if (file.EndsWith("xaml", StringComparison.InvariantCultureIgnoreCase))
					{
						var name = GetFileShortName(file);
						var icon = new IconInfo() { Name = name, Path = file };
						list.Add(icon);
					}
				}
			}
			return list;
		}

		/// <summary>
		/// 去除拓展名的短文件名。
		/// </summary>
		private string GetFileShortName(string file)
		{
			int i = file.Length - 1;
			int j = 0;
			while (file[i] != '\\' && file[i] != '/' && i >= 0)
			{
				if (file[i] == '.')
					j = i;
				i--;
			}
			return file.Substring(i + 1, j - i - 1);
		}

		// 获得图标的资源文件夹。
		private string GetIconResourceDir()
		{
			var current = Environment.CurrentDirectory;
			while (true)
			{
				if (!Directory.Exists(current))
					break;
				if (Directory.Exists(System.IO.Path.Combine(current, "IconResource")))
					return System.IO.Path.Combine(current, "IconResource");
				else
					current = GetParentFullPath(current);
			}
			return null;
		}

		/// <summary>
		/// 获得父级的目录完整路径。
		/// </summary>
		public static string GetParentFullPath(string path)
		{
			int i;
			for (i = path.Length - 1; i >= 0; i--)
			{
				if (path[i] == '\\' || path[i] == '/')
					break;
			}
			return path.Substring(0, i);
		}

		private void LoadSvg(UIElement svg)
		{
			if (svg is Viewbox)
			{
				var viewb = svg as Viewbox;
				viewb.Width = viewb.Height = 200;
				PART_border.Child = viewb;
			}
			else
				PART_border.Child = svg;

		}

		private void Button_NextSvg(object sender, RoutedEventArgs e)
		{
			CurrentId = (CurrentId + 1) % _iconList.Count;
			LoadSvg(AddIcon(_iconList[CurrentId]));
		}

		private void Button_PrevSvg(object sender, RoutedEventArgs e)
		{
			CurrentId = (CurrentId - 1 + _iconList.Count) % _iconList.Count;
			LoadSvg(AddIcon(_iconList[CurrentId]));
		}

		private void Button_SaveSvg(object sender, RoutedEventArgs e)
		{
			_iconHashSet.Add(_iconList[CurrentId]);
			PART_list.Items.Refresh();
		}

		private void Button_SaveLog(object sender, RoutedEventArgs e)
		{
			ExportLog();
		}

		/// <summary>
		/// 关闭程序。
		/// </summary>
		private void Button_CloseWindow(object sender, RoutedEventArgs e)
		{
			Application.Current.Shutdown();
		}


		/// <summary>
		/// 导出收藏夹内容。
		/// </summary>
		private void ExportLog()
		{

			var filestream = File.OpenWrite(System.IO.Path.Combine(Environment.CurrentDirectory, "iconsave.txt"));
			foreach (var iconInfo in _iconHashSet)
			{
				filestream.Write(Encoding.Default.GetBytes(iconInfo.Name));
				filestream.WriteByte((byte)'\r');
				filestream.WriteByte((byte)'\n');
			}
			filestream.Close();
			filestream.Dispose();
		}

		private void PART_backgroundSwitch_Checked(object sender, RoutedEventArgs e)
		{
			if (PART_backgroundSwitch.IsChecked == true)
				PART_border.Background = new SolidColorBrush(Colors.DarkGray);
			else
				PART_border.Background = new SolidColorBrush(Colors.White);
		}

		private void PART_slider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			LoadSvg(AddIcon(_iconList[CurrentId]));

		}

		private void PART_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			LoadSvg(AddIcon(_iconList[CurrentId]));
		}
	}
}
