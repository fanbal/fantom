#001 开始 Fantom 的调用

您可以很方便的开始 Fantom 的调用。

## 遍历输出PPT的全部图形名称。

您需要导入 FantomLib 程序集，并且使用`Fantom`命名空间，由于`Fantom`存在一些类名可能会与 WPF、UWP、MAUI 等框架中的一些对象同名，所以此处使用`ft = Fantom`缩略指代。

在该示例中，`path`字段表示文档的路径，您可以按您自己的方式修改编辑它。

```CSharp
using System;
using ft = Fantom;

class Program
{
	static void Main(string[] args)
	{
		// 文档路径任意。
		var path = @"D:\Code\Fantom\FantomUI\OpenXml测试.pptx";
		var app = ft.Application.Create(); // 创建fantom的顶层对象。
		var pres = app.Presentations.Load(path); // 载入演示文档。

		// 遍历所有幻灯片。
		foreach (var sld in pres.Slides)
		{
			// 遍历所有图形对象。
			foreach (var shp in sld.Shapes)
			{
				Console.WriteLine(shp.Name);
			}
		}

	}
}

```
