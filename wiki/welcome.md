# 欢迎使用Fantom SDK

Fantom 是一个基于 .NET5 面向 pptx 格式解析、支持 MVVM 通讯与绑定的解析库。
在 Fantom 中，所有的 API 均以接口呈现，我们尽量避免使用 `new` 关键字建立新实例，所有对象均可以从其父类中创建。

## 一、Fantom中PPT模型的基本结构
1. <b>顶层应用 Application</b>：Fantom的顶层对象为`Application`，继承自接口`IApplication`，在Fantom中存在其唯一静态实例，其为Fantom的唯一入口。

2. <b>演示文稿与其集合 Presentation/Presentations</b>：在`IApplication`内部维护着`Presentations`演示文稿集合实例对象，该对象实现了`IPresentations`接口，您可以通过在此处为集合元素添加成员实现 pptx 文档的加载。演示文稿具有`IPresentation`接口，能够用于描述其中的所有幻灯片、媒体素材、主题颜色等信息。

3. <b>幻灯片与其集合</b>：幻灯片始终是 PPT 编辑的焦点，在`IPresentation`接口中暴露了`Slides`属性用于描述文档中所有幻灯片，其中单个幻灯片具有`ISlide`接口。`ISlide`接口描述了具体单页幻灯片的信息，包括其编号，业内图形布局的描述，动画轴（暂未实装）。

4. <b>图形与其集合</b>：对幻灯片的编辑将落实到图形系统上，与PPT的COM结构一样，Fantom 同样落实到了图形的描述层。Fantom 在`ISlide`接口中开放了`Shapes`属性表示指定幻灯片的图形对象集合，其承载了当前幻灯片的所有图形对象，您可以通过访问该集合对象实现对图形元素的管理。



