﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace FantomUI
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public App()
		{
			Startup += Application_Startup;
		}

		private void Application_Startup(object sender, StartupEventArgs e)
		{
			
			// RenderOptions.SetBitmapScalingMode(imageObject, BitmapScalingMode.LowQuality);
		}
	}
}
