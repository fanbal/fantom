﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace FantomUI.Converters
{
	/// <summary>
	/// 背景色与选中转换器。
	/// </summary>
	class SelectedBackgroundConverter : IValueConverter
	{
		private static SolidColorBrush _normalColorBrush = new SolidColorBrush(Colors.Pink);
		private static SolidColorBrush _selectedColorBrush = new SolidColorBrush(Colors.OrangeRed);
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(targetType == typeof(Brush))
			{
				var selected = (bool)value;
				if (selected)
					return _selectedColorBrush;
				else
					return _normalColorBrush;
				
			}

			return _normalColorBrush;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
