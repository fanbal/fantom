﻿using System;
using FantomUI.Controls;
using System.Windows.Media;
using System.Windows;
using FantomUI.Util;

namespace FantomUI.Models
{
	/// <summary>
	/// 预绘制方法集合。
	/// </summary>
	public static class PresetRenderMethods
	{
		private static SolidColorBrush _defaultBrush = new SolidColorBrush(Colors.Black);

		public static PresentationVisual.DrawCustomVisualHandler GetDrawCustomVisualMethod(Fantom.ShapePresetType type)
		{
			switch (type)
			{
				case Fantom.ShapePresetType.Line:
					break;
				case Fantom.ShapePresetType.LineInverse:
					break;
				case Fantom.ShapePresetType.Triangle:
					return DrawDefault;
				case Fantom.ShapePresetType.RightTriangle:
					break;
				case Fantom.ShapePresetType.Rectangle:
					break;
				case Fantom.ShapePresetType.Diamond:
					break;
				case Fantom.ShapePresetType.Parallelogram:
					break;
				case Fantom.ShapePresetType.Trapezoid:
					break;
				case Fantom.ShapePresetType.NonIsoscelesTrapezoid:
					break;
				case Fantom.ShapePresetType.Pentagon:
					break;
				case Fantom.ShapePresetType.Hexagon:
					break;
				case Fantom.ShapePresetType.Heptagon:
					break;
				case Fantom.ShapePresetType.Octagon:
					break;
				case Fantom.ShapePresetType.Decagon:
					break;
				case Fantom.ShapePresetType.Dodecagon:
					break;
				case Fantom.ShapePresetType.Star4:
					break;
				case Fantom.ShapePresetType.Star5:
					break;
				case Fantom.ShapePresetType.Star6:
					break;
				case Fantom.ShapePresetType.Star7:
					break;
				case Fantom.ShapePresetType.Star8:
					break;
				case Fantom.ShapePresetType.Star10:
					break;
				case Fantom.ShapePresetType.Star12:
					break;
				case Fantom.ShapePresetType.Star16:
					break;
				case Fantom.ShapePresetType.Star24:
					break;
				case Fantom.ShapePresetType.Star32:
					break;
				case Fantom.ShapePresetType.RoundRectangle:
					return DrawRoundRectangle;
					break;
				case Fantom.ShapePresetType.Round1Rectangle:
					break;
				case Fantom.ShapePresetType.Round2SameRectangle:
					break;
				case Fantom.ShapePresetType.Round2DiagonalRectangle:
					break;
				case Fantom.ShapePresetType.SnipRoundRectangle:
					break;
				case Fantom.ShapePresetType.Snip1Rectangle:
					break;
				case Fantom.ShapePresetType.Snip2SameRectangle:
					break;
				case Fantom.ShapePresetType.Snip2DiagonalRectangle:
					break;
				case Fantom.ShapePresetType.Plaque:
					break;
				case Fantom.ShapePresetType.Ellipse:
					return DrawEllipse;
				case Fantom.ShapePresetType.Teardrop:
					break;
				case Fantom.ShapePresetType.HomePlate:
					break;
				case Fantom.ShapePresetType.Chevron:
					break;
				case Fantom.ShapePresetType.PieWedge:
					break;
				case Fantom.ShapePresetType.Pie:
					break;
				case Fantom.ShapePresetType.BlockArc:
					break;
				case Fantom.ShapePresetType.Donut:
					break;
				case Fantom.ShapePresetType.NoSmoking:
					break;
				case Fantom.ShapePresetType.RightArrow:
					break;
				case Fantom.ShapePresetType.LeftArrow:
					break;
				case Fantom.ShapePresetType.UpArrow:
					break;
				case Fantom.ShapePresetType.DownArrow:
					break;
				case Fantom.ShapePresetType.StripedRightArrow:
					break;
				case Fantom.ShapePresetType.NotchedRightArrow:
					break;
				case Fantom.ShapePresetType.BentUpArrow:
					break;
				case Fantom.ShapePresetType.LeftRightArrow:
					break;
				case Fantom.ShapePresetType.UpDownArrow:
					break;
				case Fantom.ShapePresetType.LeftUpArrow:
					break;
				case Fantom.ShapePresetType.LeftRightUpArrow:
					break;
				case Fantom.ShapePresetType.QuadArrow:
					break;
				case Fantom.ShapePresetType.LeftArrowCallout:
					break;
				case Fantom.ShapePresetType.RightArrowCallout:
					break;
				case Fantom.ShapePresetType.UpArrowCallout:
					break;
				case Fantom.ShapePresetType.DownArrowCallout:
					break;
				case Fantom.ShapePresetType.LeftRightArrowCallout:
					break;
				case Fantom.ShapePresetType.UpDownArrowCallout:
					break;
				case Fantom.ShapePresetType.QuadArrowCallout:
					break;
				case Fantom.ShapePresetType.BentArrow:
					break;
				case Fantom.ShapePresetType.UTurnArrow:
					break;
				case Fantom.ShapePresetType.CircularArrow:
					break;
				case Fantom.ShapePresetType.LeftCircularArrow:
					break;
				case Fantom.ShapePresetType.LeftRightCircularArrow:
					break;
				case Fantom.ShapePresetType.CurvedRightArrow:
					break;
				case Fantom.ShapePresetType.CurvedLeftArrow:
					break;
				case Fantom.ShapePresetType.CurvedUpArrow:
					break;
				case Fantom.ShapePresetType.CurvedDownArrow:
					break;
				case Fantom.ShapePresetType.SwooshArrow:
					break;
				case Fantom.ShapePresetType.Cube:
					break;
				case Fantom.ShapePresetType.Can:
					break;
				case Fantom.ShapePresetType.LightningBolt:
					break;
				case Fantom.ShapePresetType.Heart:
					break;
				case Fantom.ShapePresetType.Sun:
					break;
				case Fantom.ShapePresetType.Moon:
					break;
				case Fantom.ShapePresetType.SmileyFace:
					break;
				case Fantom.ShapePresetType.IrregularSeal1:
					break;
				case Fantom.ShapePresetType.IrregularSeal2:
					break;
				case Fantom.ShapePresetType.FoldedCorner:
					break;
				case Fantom.ShapePresetType.Bevel:
					break;
				case Fantom.ShapePresetType.Frame:
					break;
				case Fantom.ShapePresetType.HalfFrame:
					break;
				case Fantom.ShapePresetType.Corner:
					break;
				case Fantom.ShapePresetType.DiagonalStripe:
					break;
				case Fantom.ShapePresetType.Chord:
					break;
				case Fantom.ShapePresetType.Arc:
					break;
				case Fantom.ShapePresetType.LeftBracket:
					break;
				case Fantom.ShapePresetType.RightBracket:
					break;
				case Fantom.ShapePresetType.LeftBrace:
					break;
				case Fantom.ShapePresetType.RightBrace:
					break;
				case Fantom.ShapePresetType.BracketPair:
					break;
				case Fantom.ShapePresetType.BracePair:
					break;
				case Fantom.ShapePresetType.StraightConnector1:
					break;
				case Fantom.ShapePresetType.BentConnector2:
					break;
				case Fantom.ShapePresetType.BentConnector3:
					break;
				case Fantom.ShapePresetType.BentConnector4:
					break;
				case Fantom.ShapePresetType.BentConnector5:
					break;
				case Fantom.ShapePresetType.CurvedConnector2:
					break;
				case Fantom.ShapePresetType.CurvedConnector3:
					break;
				case Fantom.ShapePresetType.CurvedConnector4:
					break;
				case Fantom.ShapePresetType.CurvedConnector5:
					break;
				case Fantom.ShapePresetType.Callout1:
					break;
				case Fantom.ShapePresetType.Callout2:
					break;
				case Fantom.ShapePresetType.Callout3:
					break;
				case Fantom.ShapePresetType.AccentCallout1:
					break;
				case Fantom.ShapePresetType.AccentCallout2:
					break;
				case Fantom.ShapePresetType.AccentCallout3:
					break;
				case Fantom.ShapePresetType.BorderCallout1:
					break;
				case Fantom.ShapePresetType.BorderCallout2:
					break;
				case Fantom.ShapePresetType.BorderCallout3:
					break;
				case Fantom.ShapePresetType.AccentBorderCallout1:
					break;
				case Fantom.ShapePresetType.AccentBorderCallout2:
					break;
				case Fantom.ShapePresetType.AccentBorderCallout3:
					break;
				case Fantom.ShapePresetType.WedgeRectangleCallout:
					break;
				case Fantom.ShapePresetType.WedgeRoundRectangleCallout:
					break;
				case Fantom.ShapePresetType.WedgeEllipseCallout:
					break;
				case Fantom.ShapePresetType.CloudCallout:
					break;
				case Fantom.ShapePresetType.Cloud:
					break;
				case Fantom.ShapePresetType.Ribbon:
					break;
				case Fantom.ShapePresetType.Ribbon2:
					break;
				case Fantom.ShapePresetType.EllipseRibbon:
					break;
				case Fantom.ShapePresetType.EllipseRibbon2:
					break;
				case Fantom.ShapePresetType.LeftRightRibbon:
					break;
				case Fantom.ShapePresetType.VerticalScroll:
					break;
				case Fantom.ShapePresetType.HorizontalScroll:
					break;
				case Fantom.ShapePresetType.Wave:
					break;
				case Fantom.ShapePresetType.DoubleWave:
					break;
				case Fantom.ShapePresetType.Plus:
					break;
				case Fantom.ShapePresetType.FlowChartProcess:
					break;
				case Fantom.ShapePresetType.FlowChartDecision:
					break;
				case Fantom.ShapePresetType.FlowChartInputOutput:
					break;
				case Fantom.ShapePresetType.FlowChartPredefinedProcess:
					break;
				case Fantom.ShapePresetType.FlowChartInternalStorage:
					break;
				case Fantom.ShapePresetType.FlowChartDocument:
					break;
				case Fantom.ShapePresetType.FlowChartMultidocument:
					break;
				case Fantom.ShapePresetType.FlowChartTerminator:
					break;
				case Fantom.ShapePresetType.FlowChartPreparation:
					break;
				case Fantom.ShapePresetType.FlowChartManualInput:
					break;
				case Fantom.ShapePresetType.FlowChartManualOperation:
					break;
				case Fantom.ShapePresetType.FlowChartConnector:
					break;
				case Fantom.ShapePresetType.FlowChartPunchedCard:
					break;
				case Fantom.ShapePresetType.FlowChartPunchedTape:
					break;
				case Fantom.ShapePresetType.FlowChartSummingJunction:
					break;
				case Fantom.ShapePresetType.FlowChartOr:
					break;
				case Fantom.ShapePresetType.FlowChartCollate:
					break;
				case Fantom.ShapePresetType.FlowChartSort:
					break;
				case Fantom.ShapePresetType.FlowChartExtract:
					break;
				case Fantom.ShapePresetType.FlowChartMerge:
					break;
				case Fantom.ShapePresetType.FlowChartOfflineStorage:
					break;
				case Fantom.ShapePresetType.FlowChartOnlineStorage:
					break;
				case Fantom.ShapePresetType.FlowChartMagneticTape:
					break;
				case Fantom.ShapePresetType.FlowChartMagneticDisk:
					break;
				case Fantom.ShapePresetType.FlowChartMagneticDrum:
					break;
				case Fantom.ShapePresetType.FlowChartDisplay:
					break;
				case Fantom.ShapePresetType.FlowChartDelay:
					break;
				case Fantom.ShapePresetType.FlowChartAlternateProcess:
					break;
				case Fantom.ShapePresetType.FlowChartOffpageConnector:
					break;
				case Fantom.ShapePresetType.ActionButtonBlank:
					break;
				case Fantom.ShapePresetType.ActionButtonHome:
					break;
				case Fantom.ShapePresetType.ActionButtonHelp:
					break;
				case Fantom.ShapePresetType.ActionButtonInformation:
					break;
				case Fantom.ShapePresetType.ActionButtonForwardNext:
					break;
				case Fantom.ShapePresetType.ActionButtonBackPrevious:
					break;
				case Fantom.ShapePresetType.ActionButtonEnd:
					break;
				case Fantom.ShapePresetType.ActionButtonBeginning:
					break;
				case Fantom.ShapePresetType.ActionButtonReturn:
					break;
				case Fantom.ShapePresetType.ActionButtonDocument:
					break;
				case Fantom.ShapePresetType.ActionButtonSound:
					break;
				case Fantom.ShapePresetType.ActionButtonMovie:
					break;
				case Fantom.ShapePresetType.Gear6:
					break;
				case Fantom.ShapePresetType.Gear9:
					break;
				case Fantom.ShapePresetType.Funnel:
					break;
				case Fantom.ShapePresetType.MathPlus:
					break;
				case Fantom.ShapePresetType.MathMinus:
					break;
				case Fantom.ShapePresetType.MathMultiply:
					break;
				case Fantom.ShapePresetType.MathDivide:
					break;
				case Fantom.ShapePresetType.MathEqual:
					break;
				case Fantom.ShapePresetType.MathNotEqual:
					break;
				case Fantom.ShapePresetType.CornerTabs:
					break;
				case Fantom.ShapePresetType.SquareTabs:
					break;
				case Fantom.ShapePresetType.PlaqueTabs:
					break;
				case Fantom.ShapePresetType.ChartX:
					break;
				case Fantom.ShapePresetType.ChartStar:
					break;
				case Fantom.ShapePresetType.ChartPlus:
					break;
				default:
					break;
			}
			return DrawDefault;
		}

		/// <summary>
		/// 创建格式化的文本。
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static FormattedText CreateFormattedText(UShape shape)
		{
			return new FormattedText(
				shape.Source.ReadOnlyText,
				System.Globalization.CultureInfo.CurrentCulture,
				FlowDirection.LeftToRight,
				new Typeface(shape.Source.TextBody.FontFamily),
				shape.Source.TextBody.FontSize.ToFontSize(),
				shape.Source.TextBody.Fill.Color.GetBrush());
		}

		/// <summary>
		/// 绘制字体。
		/// </summary>
		/// <param name="render"></param>
		/// <param name="uShape"></param>
		public static void DrawText(this DrawingContext render, UShape uShape)
		{
			var size = uShape.Source.TextBody.FontSize.ToFontSize();
			var text = uShape.Source.ReadOnlyText;
			var width = uShape.FrameInfo.originLeft ;// ;
			var height = uShape.FrameInfo.originTop + uShape.FrameInfo.height / 2 - size;
			render.DrawText(CreateFormattedText(uShape), new Point(width, height));
		}


		public static void DrawDefault(DrawingContext render, UShape shape)
		{
			var frameInfo = shape.FrameInfo;

			// 获得传入的图形对象。
			if (shape != null)
			{
				// 图片绘制。
				if (shape.Source.Fill.Color is Fantom.Drawing.BitmapColor)
				{
					render.PushTransform(new RotateTransform(frameInfo.rotation * 180 / System.Math.PI, frameInfo.anchor.X, frameInfo.anchor.Y));
					render.DrawImage(shape.GetImageSource(), new Rect(frameInfo.originLeft, frameInfo.originTop, frameInfo.width, frameInfo.height));
				}
				// 纯色块绘制。
				else if (shape.Source.Fill.Color is Fantom.Drawing.BaseColor)
				{
					var brush = new SolidColorBrush(Color.FromRgb(
					(byte)shape.Source.Fill.Color.Red,
					(byte)shape.Source.Fill.Color.Green,
					(byte)shape.Source.Fill.Color.Blue));
					var pen = shape.Source.Line.Color != null ?
											new Pen(new SolidColorBrush(Color.FromRgb(
											shape.Source.Line.Color.Red,
											shape.Source.Line.Color.Green,
											shape.Source.Line.Color.Blue)), shape.Source.Line.Thickness.ToPixelValue())
					: null;
					render.PushTransform(new RotateTransform(frameInfo.rotation * 180 / System.Math.PI, frameInfo.anchor.X, frameInfo.anchor.Y));
					render.DrawRectangle(brush, pen, new Rect(frameInfo.originLeft, frameInfo.originTop, frameInfo.width, frameInfo.height));
				}
			}

			DrawText(render, shape);
		}


		public static void DrawRoundRectangle(DrawingContext render, UShape shape)
		{
			var frameInfo = shape.FrameInfo;

			// 获得传入的图形对象。
			if (shape != null)
			{
				// 图片绘制。
				if (shape.Source.Fill.Color is Fantom.Drawing.BitmapColor)
				{
					render.PushTransform(new RotateTransform(frameInfo.rotation * 180 / System.Math.PI, frameInfo.anchor.X, frameInfo.anchor.Y));
					render.DrawImage(shape.GetImageSource(), new Rect(frameInfo.originLeft, frameInfo.originTop, frameInfo.width, frameInfo.height));
				}
				// 纯色块绘制。
				else if (shape.Source.Fill.Color is Fantom.Drawing.BaseColor)
				{
					var rect = new Rect(frameInfo.originLeft, frameInfo.originTop, frameInfo.width, frameInfo.height);
					var radius = Math.Min(frameInfo.width, frameInfo.height) * frameInfo.extra[0];
					var brush = new SolidColorBrush(Color.FromRgb(
					(byte)shape.Source.Fill.Color.Red,
					(byte)shape.Source.Fill.Color.Green,
					(byte)shape.Source.Fill.Color.Blue));
					var pen = shape.Source.Line.Color != null ?
											new Pen(new SolidColorBrush(Color.FromRgb(
											shape.Source.Line.Color.Red,
											shape.Source.Line.Color.Green,
											shape.Source.Line.Color.Blue)), shape.Source.Line.Thickness.ToPixelValue())
					: null;
					render.PushTransform(new RotateTransform(frameInfo.rotation * 180 / System.Math.PI, frameInfo.anchor.X, frameInfo.anchor.Y));
					render.DrawRoundedRectangle(brush, pen, rect, radius, radius);
				}
			}

			DrawText(render, shape);
		}

		public static void DrawEllipse(DrawingContext render, UShape shape)
		{
			var frameInfo = shape.FrameInfo;

			// 获得传入的图形对象。
			if (shape != null)
			{
				// 图片绘制。
				if (shape.Source.Fill.Color is Fantom.Drawing.BitmapColor)
				{
					render.PushTransform(new RotateTransform(frameInfo.rotation * 180 / System.Math.PI, frameInfo.anchor.X, frameInfo.anchor.Y));
					render.DrawImage(shape.GetImageSource(), new Rect(frameInfo.originLeft, frameInfo.originTop, frameInfo.width, frameInfo.height));
				}
				// 纯色块绘制。
				else if (shape.Source.Fill.Color is Fantom.Drawing.BaseColor)
				{
					var center = new Point(frameInfo.originLeft + frameInfo.width / 2, frameInfo.originTop + frameInfo.height / 2);
					var radiusX = frameInfo.width / 2;
					var radiusY = frameInfo.height / 2;
					var brush = new SolidColorBrush(Color.FromRgb(
					(byte)shape.Source.Fill.Color.Red,
					(byte)shape.Source.Fill.Color.Green,
					(byte)shape.Source.Fill.Color.Blue));
					var pen = shape.Source.Line.Color != null ?
											new Pen(new SolidColorBrush(Color.FromRgb(
											shape.Source.Line.Color.Red,
											shape.Source.Line.Color.Green,
											shape.Source.Line.Color.Blue)), shape.Source.Line.Thickness.ToPixelValue())
					: null;
					render.PushTransform(new RotateTransform(frameInfo.rotation * 180 / System.Math.PI, frameInfo.anchor.X, frameInfo.anchor.Y));
					render.DrawEllipse(brush, pen, center, radiusX, radiusY);
				}
			}

			DrawText(render, shape);
		}

	}
}
