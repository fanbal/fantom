﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FantomUI.Controls
{

	/// <summary>
	/// 用于绘制缩放控点的选框的图层。
	/// </summary>
	public class AdjustmentFrameLayer : Panel
	{
		#region properties

		// 控点的可视化图层的位置。
		private List<Visual> _adjustmentVisuals = new List<Visual>();


		// 父级设计器对象。
		public Designer Designer = null;

		#endregion

		#region ctors

		static AdjustmentFrameLayer()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(AdjustmentFrameLayer),
				new FrameworkPropertyMetadata(typeof(AdjustmentFrameLayer)));
		}
		#endregion

		#region override

		protected override int VisualChildrenCount => _adjustmentVisuals.Count;

		protected override Visual GetVisualChild(int index)
		{
			return _adjustmentVisuals[index];
		}

		#endregion

		#region methods

		/// <summary>
		/// 向图形加入缩放选框。
		/// </summary>
		/// <param name="shape"></param>
		public AdjustmentFrameVisual Add(UShape uShape)
		{
			var adjustmentVisual =
				new AdjustmentFrameVisual(
					uShape,
					ScaleThumbVisual.ShapeTypeEnum.Circle,
					RotationThumbVisual.ShapeTypeEnum.Circle);
			// 注册记录。

			_adjustmentVisuals.Add(adjustmentVisual);
			AddVisualChild(adjustmentVisual);

			return adjustmentVisual;
		}

		/// <summary>
		/// 清除当前画布的全部控点。
		/// </summary>
		public void Clear()
		{
			foreach (var visual in _adjustmentVisuals)
			{
				this.RemoveVisualChild(visual);
			}

			_adjustmentVisuals.Clear();
			Children.Clear();
		}

		/// <summary>
		/// 移除指定图形的选框的显示。
		/// </summary>
		public void Remove(AdjustmentFrameVisual frameVisual)
		{
			RemoveVisualChild(frameVisual);
			_adjustmentVisuals.Remove(frameVisual);
		}

		#endregion

	}
}
