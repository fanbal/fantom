﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;


namespace FantomUI.Controls
{

	#region convertors

	// 获得左上角的横坐标。
	public class SelectionFrameLeftUpXConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var frame = (Rect)value;

			return frame.TopLeft.X;

		}

		object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	// 获得左上角的纵坐标。
	public class SelectionFrameLeftUpYConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var frame = (Rect)value;

			return frame.TopLeft.Y;

		}

		object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	public class SelectionFrameWidthConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{

			var frame = (Rect)value;

			return frame.Width;

		}

		object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	// 获得左上角的纵坐标。
	public class SelectionFrameHeightConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var frame = (Rect)value;

			return frame.Height;

		}

		object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}



	#endregion

	/// <summary>
	/// 选择框，用于框选PPT的元素。
	/// </summary>
	public class SelectionFrame : Control, INotifyPropertyChanged
	{

		#region dependency properties
		public event PropertyChangedEventHandler? PropertyChanged;
		// 横坐标位置。

		private Rect _rect;
		public Rect Rect
		{
			get => _rect;
			set
			{
				_rect = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Rect"));
			}
		}

		#endregion

		#region ctors
		public SelectionFrame(Point startPoint)
		{
			Rect = new Rect(startPoint, startPoint);
			DataContext = this;
		}

		static SelectionFrame()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(SelectionFrame),
				new FrameworkPropertyMetadata(typeof(SelectionFrame)));
		}
		#endregion

	}
}
