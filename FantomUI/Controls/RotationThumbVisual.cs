﻿using System.Windows.Media;
using FantomUI.Util;
using System.Windows;

namespace FantomUI.Controls
{
	/// <summary>
	/// 旋钮滑块。
	/// </summary>
	public class RotationThumbVisual : DrawingVisual
	{
		#region enum

		/// <summary>
		/// 控点的基本样式，应该没有特别的样式了吧？。
		/// </summary>
		public enum ShapeTypeEnum
		{
			/// <summary>
			/// 正方形。在Office2013中，微软采用了方形的选框。
			/// </summary>
			Square,

			/// <summary>
			/// 圆形。最常见的选框按钮样式。
			/// </summary>
			Circle,

			/// <summary>
			/// 绘制圆角方形
			/// </summary>
			RoundSquare

		}
		#endregion

		#region properties

		public UShape UShape { get; private set; } = null;

		/// <summary>
		/// 旋转控点的颜色。
		/// </summary>
		public static Brush Background { get; set; } = new SolidColorBrush(Colors.Orange);

		/// <summary>
		/// 旋转边的粗细与颜色。
		/// </summary>
		public static Pen Stroke { get; set; } =
			new Pen(new SolidColorBrush(Colors.Gray), 2d);

		/// <summary>
		/// 如果为圆形，该参数表示控点的尺寸。
		/// 如果为方形，该参数表示控点的边长。
		/// </summary>
		public static double Radius
		{
			get => _radius;
			set
			{
				// 写入保护，避免负数的写入。
				if (value < 0 || value < RoundCornerRadius)
					return;
				_radius = value;
			}
		}
		private static double _radius = 5d;

		/// <summary>
		/// 如果存在圆角，此处可访问圆角的尺寸。
		/// </summary>
		public static double RoundCornerRadius
		{
			get => _roundCornerRadius;
			set
			{
				// 写入保护，避免负数的写入。
				if (value < 0 || value > _radius)
					return;
				_roundCornerRadius = value;
			}
		}
		private static double _roundCornerRadius = 1d;


		/// <summary>
		/// 控点距选框的距离。
		/// </summary>
		public static double Distance { get; set; } = 30;

		#endregion

		#region ctors

		/// <summary>
		/// 实例化一个缩放按钮框图形。
		/// </summary>
		public RotationThumbVisual(UShape uShape, ShapeTypeEnum type)
		{
			UShape = uShape;
			var frameInfo = uShape.FrameInfo;
			var render = RenderOpen();

			switch (type)
			{
				case ShapeTypeEnum.Square:
					//render.DrawRectangle
					//	(Background, Stroke, frameInfo.rotationPos, Radius, frameInfo.rotation, 0d);
					render.DrawRectangle
						(Background, Stroke, new Point(frameInfo.anchor.X, frameInfo.originTop - Distance), Radius, frameInfo.rotation, 0d);

					break;
				case ShapeTypeEnum.Circle:
					// 绘制圆形……为什么一定要圆形呢？值得一提的是，2013是方的
					// render.DrawEllipse(Background, Stroke, frameInfo.rotationPos, Radius, Radius);
					render.DrawEllipse(Background, Stroke, new Point(frameInfo.anchor.X, frameInfo.originTop - Distance), Radius, Radius);
					break;
				case ShapeTypeEnum.RoundSquare:
					//render.DrawRectangle
					//	(Background, Stroke, frameInfo.rotationPos, Radius, frameInfo.rotation, RoundCornerRadius);
					render.DrawRectangle
						(Background, Stroke, new Point(frameInfo.anchor.X, frameInfo.originTop - Distance), Radius, frameInfo.rotation, RoundCornerRadius);

					break;
				default:
					break;
			}

			render.Close();
		}



		#endregion

	}
}
