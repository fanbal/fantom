﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace FantomUI.Controls
{

	// TODO 完成设计器画布。
	// TODO 完成设计器画布：元素在尺寸、旋转、位移、颜色、文本上的显示。
	// TODO 完成设计器画布：容器的高性能缩放。
	// TODO 完成设计器画布：命中事件的运用与对象的选定。

	/// <summary>
	/// 设计器画布，用于高性能地承载绘制的图形对象。
	/// </summary>
	public class DesignerCanvas : Panel
	{

		#region properties
		// 视图中的集合对象。
		private List<Visual> _visuals = new List<Visual>();

		/// <summary>
		/// 容器中的可视化对象。
		/// </summary>
		public IReadOnlyList<Visual> Visuals => _visuals;

		// 父级设计器对象。
		public Designer Designer = null;

		#endregion

		#region ctors

		public DesignerCanvas()
		{

		}

		static DesignerCanvas()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(DesignerCanvas),
				 new FrameworkPropertyMetadata(typeof(DesignerCanvas)));
		}

		#endregion

		#region methods

		/// <summary>
		/// 添加幻灯片显示对象。
		/// </summary>
		/// <returns></returns>
		internal PresentationVisual Add(UShape uShape)
		{
			var random = new Random();
			byte r = (byte)random.Next(256),
				g = (byte)random.Next(256),
				b = (byte)random.Next(256);
			var brush = new SolidColorBrush(Color.FromRgb(r, g, b));

			return Add(uShape, brush);
		}

		/// <summary>
		/// 添加幻灯片显示对象。
		/// </summary>
		/// <param name="brush"></param>
		/// <returns></returns>
		internal PresentationVisual Add(UShape uShape, Brush brush)
		{
			var visual =
				new PresentationVisual(uShape, brush, new Pen(brush, 3));

			_visuals.Add(visual);

			AddVisualChild(visual);
			return visual;
		}


		/// <summary>
		/// 移出指定元素。
		/// </summary>
		/// <param name="visual"></param>
		internal void Remove(Visual visual)
		{
			this.RemoveVisualChild(visual);
			this._visuals.Remove(visual);
		}

		/// <summary>
		/// 移出指定元素集合。
		/// </summary>
		/// <param name="visuals"></param>
		internal void Remove(IEnumerable<Visual> visuals)
		{
			foreach (var visual in visuals)
			{
				Remove(visual);
			}
		}


		/// <summary>
		/// 清除所有内容。
		/// </summary>
		internal void Clear()
		{
			foreach (var visual in _visuals)
			{
				RemoveVisualChild(visual);
			}
			_visuals.Clear();
		}

		#endregion

		#region override
		protected override int VisualChildrenCount => _visuals.Count;

		protected override Visual GetVisualChild(int index)
		{
			return _visuals[index];
		}
		#endregion

		#region events


		#endregion
	}
}
