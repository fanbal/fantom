﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using FantomUI.Util;
using System.Windows;
using System.Windows.Media;
using ft = Fantom;
using System.Windows.Media.Imaging;

namespace FantomUI
{
	/// <summary>
	/// 位于FantomUI中的演示文稿模型对象。
	/// </summary>
	public class UIPresentation
	{
		/// <summary>
		/// 源演示文稿。
		/// </summary>
		public ft.IPresentation Source { get; set; }

		/// <summary>
		/// 图片字典。
		/// </summary>
		public Dictionary<Uri, ImageSource> ImageDic { get; } = new Dictionary<Uri, ImageSource>();

		public UIPresentation(ft.IPresentation source)
		{
			Source = source;
			foreach (var uripair in source.MediaManager.LocalMediaDic)
			{
				var image = new BitmapImage();
				image.BeginInit();
				image.StreamSource= new MemoryStream(uripair.Value.Bytes);
				image.EndInit();
				ImageDic.Add(uripair.Key, image);
			}
		}

	}
}
