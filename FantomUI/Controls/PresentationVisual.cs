﻿using System.Windows;
using System.Windows.Media;
using FantomUI.Models;
using FantomUI.Util;

namespace FantomUI.Controls
{
	/// <summary>
	/// 演示文稿可视化对象。
	/// </summary>
	public class PresentationVisual : DrawingVisual
	{

		#region delegate
		public delegate void DrawCustomVisualHandler(DrawingContext render, UShape shape);

		#endregion
		
		
		#region properties

		/// <summary>
		/// 图形对象的具体内容。
		/// </summary>
		public UShape UShape { get; set; } = null;

		// 颜色与填充。
		private Brush _brush;
		private Pen _pen;

		public DrawCustomVisualHandler DrawCustomVisual = PresetRenderMethods.DrawDefault;

		#endregion

		#region static



		#endregion

		#region ctors

		public PresentationVisual(UShape uShape, Brush brush, Pen pen)
		{
			_brush = brush;
			_pen = pen;
			UShape = uShape;
			Update();

		}


		#endregion

		#region methods

		/// <summary>
		/// 刷新图形的样式。
		/// </summary>
		/// <param name="brush"></param>
		/// <param name="pen"></param>
		public void Update()
		{
			var render = RenderOpen();

			// 获得传入的图形对象。
			DrawCustomVisual(render, UShape);
			render.Close();

		}

		/// <summary>
		/// 清除内部全部内容。
		/// </summary>
		public void Clear()
		{
			var render = RenderOpen();
			render.Close();
		}

		#endregion



	}
}
