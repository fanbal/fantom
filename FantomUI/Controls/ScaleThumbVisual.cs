﻿using System.Windows;
using System.Windows.Media;
using FantomUI.Util;

namespace FantomUI.Controls
{

	/// <summary>
	/// 基本选框的样式配置，此处用于拉伸。
	/// </summary>
	public class ScaleThumbVisual : DrawingVisual
	{

		#region enum

		/// <summary>
		/// 控点的基本样式，应该没有特别的样式了吧？。
		/// </summary>
		public enum ShapeTypeEnum
		{
			/// <summary>
			/// 正方形。在Office2013中，微软采用了方形的选框。
			/// </summary>
			Square,

			/// <summary>
			/// 圆形。最常见的选框按钮样式。
			/// </summary>
			Circle,

			/// <summary>
			/// 绘制圆角方形
			/// </summary>
			RoundSquare

		}
		#endregion

		/// <summary>
		/// 控点方向。
		/// </summary>
		public enum ThumbType
		{
			OnCorner,
			OnEdge,
		}

		#region static

		/// <summary>
		/// 选框控点的颜色。
		/// </summary>
		public static Brush Background { get; set; } = new SolidColorBrush(Colors.White);

		/// <summary>
		/// 选框边的粗细与颜色。
		/// </summary>
		public static Pen Stroke { get; set; } =
			new Pen(new SolidColorBrush(Colors.Gray), 2d);


		/// <summary>
		/// 如果为圆形，该参数表示控点的尺寸。
		/// 如果为方形，该参数表示控点的边长。
		/// </summary>
		public static double Radius
		{
			get => _radius;
			set
			{
				// 写入保护，避免负数的写入。
				if (value < 0 || value < RoundCornerRadius)
					return;
				_radius = value;
			}
		}
		private static double _radius = 5d;

		/// <summary>
		/// 如果存在圆角，此处可访问圆角的尺寸。
		/// </summary>
		public static double RoundCornerRadius
		{
			get => _roundCornerRadius;
			set
			{
				// 写入保护，避免负数的写入。
				if (value < 0 || value > _radius)
					return;
				_roundCornerRadius = value;
			}
		}
		private static double _roundCornerRadius = 1d;


		/// <summary>
		/// 最小高度，当选框高度小于该值时，中间点将不予显示。
		/// </summary>
		public static double MinHeight { get; set; } = 50;

		/// <summary>
		/// 最小宽度，当选框宽度小于该值时，中间点将不予显示。
		/// </summary>
		public static double MinWidth { get; set; } = 50;


		#endregion

		#region properties

		/// <summary>
		/// 控点的父对象。
		/// </summary>
		public UShape UShape { get; private set; } = null;

		/// <summary>
		/// 控点方向。
		/// </summary>
		public ThumbType Direction { get; set; } = ThumbType.OnCorner;

		#endregion

		#region ctors

		/// <summary>
		/// 实例化一个缩放按钮框图形。
		/// </summary>
		/// <param name="rect">选框尺寸。</param>
		/// <param name="rotation">旋转角度。</param>
		/// <param name="type">选框周围控点的基本样式类型。</param>
		/// <param name="anchor">锚点所在比例。</param>
		public ScaleThumbVisual(UShape uShape, Point point, double rotation, ShapeTypeEnum type)
		{

			UShape = uShape;

			// 此处绘制选框的四个点。
			var render = RenderOpen();

			// 绘制框体的附近至少4个点。
			switch (type)
			{
				case ShapeTypeEnum.Square:
					render.DrawRectangle
								(Background, Stroke, point, Radius, rotation, 0f);
					break;
				case ShapeTypeEnum.Circle:
					render.DrawEllipse(Background, Stroke, point, Radius, Radius);
					break;
				case ShapeTypeEnum.RoundSquare:
					render.DrawRectangle
						(Background, Stroke, point, Radius, rotation, RoundCornerRadius);
					break;
				default:
					break;
			}

			render.Close();
		}

		#endregion

	}
}
