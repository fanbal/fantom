﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows;

namespace FantomUI.Controls
{

	/// <summary>
	/// 调节控点的对象绑组。
	/// </summary>
	public class AdjustmentFrameVisual : DrawingVisual
	{

		#region properties

		public UShape UShape { get; set; } = null;

		/// <summary>
		/// 选框线框。
		/// </summary>
		private BaseStrokeFrameVisual BaseStrokeFrameVisual { get; set; }

		/// <summary>
		/// 缩放手柄集合。
		/// </summary>
		private IEnumerable<ScaleThumbVisual> ScaleThumbVisuals { get; set; }

		/// <summary>
		/// 旋转手柄。
		/// </summary>
		private RotationThumbVisual RotationThumbVisual { get; set; }


		private ScaleThumbVisual.ShapeTypeEnum _scaleType;
		private RotationThumbVisual.ShapeTypeEnum _rotationType;

		#endregion

		#region ctors


		/// <summary>
		/// 构造调整框。
		/// </summary>
		/// <param name="frameInfo"></param>
		public AdjustmentFrameVisual(
			UShape uShape,
			ScaleThumbVisual.ShapeTypeEnum scaleType,
			RotationThumbVisual.ShapeTypeEnum rotationType)
		{
			_scaleType = scaleType;
			_rotationType = rotationType;
			UShape = uShape;
			Update();

		}


		#endregion


		#region events

		public void Update()
		{
			var frameInfo = UShape.FrameInfo;
			var list = new List<ScaleThumbVisual>();

			// 边缘框体的基线。
			BaseStrokeFrameVisual = new BaseStrokeFrameVisual(UShape);

			// 位于四角的控点。
			list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft, frameInfo.originTop), 0, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft, frameInfo.originTop + frameInfo.height), 0, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft + frameInfo.width, frameInfo.originTop), 0, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft + frameInfo.width, frameInfo.originTop + frameInfo.height), 0, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			// 当框体小于一定程度的时候不予显示边缘控点。
			if (frameInfo.width > ScaleThumbVisual.MinWidth)
			{
				list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft + frameInfo.width / 2, frameInfo.originTop), 0, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });

				list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft + frameInfo.width / 2, frameInfo.originTop + frameInfo.height), 0, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });
			}

			if (frameInfo.height > ScaleThumbVisual.MinHeight)
			{
				list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft + frameInfo.width, frameInfo.originTop + frameInfo.height / 2), 0, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });

				list.Add(new ScaleThumbVisual(UShape, new Point(frameInfo.originLeft, frameInfo.originTop + frameInfo.height / 2), 0, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });
			}


			// 获得列表，用于维护。
			ScaleThumbVisuals = list;

			RotationThumbVisual = new RotationThumbVisual(UShape, _rotationType);
			
			// Children.Clear();

			var render = RenderOpen();
			render.PushTransform(new RotateTransform(frameInfo.rotation *180 / Math.PI , frameInfo.anchor.X, frameInfo.anchor.Y));
			render.DrawDrawing(BaseStrokeFrameVisual.Drawing);
			foreach (var visual in ScaleThumbVisuals)
				render.DrawDrawing(visual.Drawing);
			render.DrawDrawing(RotationThumbVisual.Drawing);
			render.Close();

			//Children.Add(BaseStrokeFrameVisual);
			//foreach (var visual in ScaleThumbVisuals)
			//	Children.Add(visual);
			//Children.Add(RotationThumbVisual);


		}


		[Obsolete("过时的低效绘制调用", true)]
		public void Update(int unabled)
		{
			var frameInfo = UShape.FrameInfo;
			var list = new List<ScaleThumbVisual>();

			// 边缘框体的基线。
			BaseStrokeFrameVisual = new BaseStrokeFrameVisual(UShape);

			// 位于四角的控点。
			list.Add(new ScaleThumbVisual(UShape, frameInfo.point1, frameInfo.rotation, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			list.Add(new ScaleThumbVisual(UShape, frameInfo.point2, frameInfo.rotation, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			list.Add(new ScaleThumbVisual(UShape, frameInfo.point3, frameInfo.rotation, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			list.Add(new ScaleThumbVisual(UShape, frameInfo.point4, frameInfo.rotation, _scaleType)
			{ Direction = ScaleThumbVisual.ThumbType.OnCorner });

			// 当框体小于一定程度的时候不予显示边缘控点。
			if (frameInfo.width > ScaleThumbVisual.MinWidth)
			{
				list.Add(new ScaleThumbVisual(UShape, frameInfo.point12, frameInfo.rotation, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });

				list.Add(new ScaleThumbVisual(UShape, frameInfo.point34, frameInfo.rotation, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });
			}

			if (frameInfo.height > ScaleThumbVisual.MinHeight)
			{
				list.Add(new ScaleThumbVisual(UShape, frameInfo.point23, frameInfo.rotation, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });

				list.Add(new ScaleThumbVisual(UShape, frameInfo.point41, frameInfo.rotation, _scaleType)
				{ Direction = ScaleThumbVisual.ThumbType.OnEdge });
			}


			// 获得列表，用于维护。
			ScaleThumbVisuals = list;

			RotationThumbVisual = new RotationThumbVisual(UShape, _rotationType);


			Children.Clear();

			Children.Add(BaseStrokeFrameVisual);
			foreach (var visual in ScaleThumbVisuals)
				Children.Add(visual);
			Children.Add(RotationThumbVisual);
		}

		#endregion

	}
}
