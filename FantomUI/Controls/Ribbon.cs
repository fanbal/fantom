﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace FantomUI.Controls
{

	/// <summary>
	/// Ribbon控件，用于容纳标签。
	/// </summary>
	class Ribbon:ContentControl
	{


		#region ctors

		static Ribbon()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(Ribbon),
				new FrameworkPropertyMetadata(typeof(Ribbon)));
		}

		#endregion

	}
}
