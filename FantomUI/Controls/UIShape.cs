﻿using System;
using System.ComponentModel;
using System.Numerics;
using System.Windows.Media;
using Fantom;
using FantomUI.Models;
using FantomUI.Util;

namespace FantomUI.Controls
{

	/// <summary>
	/// 图形对象的一层封装，里面将会实现诸多控件特性。
	/// </summary>
	public class UShape
	{
		#region properties

		public UIPresentation UIPresentation { get; }

		// 图形信息。
		private Shape _shape;

		/// <summary>
		/// 源数据，即 <see cref="Shape"/> 数据。
		/// </summary>
		public Shape Source => _shape;

		// 调整框图层的位置。
		private AdjustmentFrameLayer _adjustmentFrameLayer;

		/// <summary>
		/// 调整框的可视化对象。。
		/// </summary>
		public AdjustmentFrameVisual AdjustmentFrameVisual { get; set; } = null;

		// 演示图层的画布。
		private DesignerCanvas _presentationCanvas;

		/// <summary>
		/// 图形本体绘制对象。
		/// </summary>
		public PresentationVisual PresentationVisual { get; set; } = null;

		// 选框信息。
		private FrameHelper.FrameInfo _frameInfo;

		/// <summary>
		/// 获得当前的选框信息。
		/// </summary>
		public FrameHelper.FrameInfo FrameInfo => _frameInfo;

		private FrameHelper.FrameInfo _prvFrameInfo;


		/// <summary>
		/// 是否正在编辑。
		/// </summary>
		public bool IsEditing
		{
			get => _isEditing;
			set
			{

				if (value != _isEditing)
				{
					if (value)
					{
						OnEditMode();
						HideAdjustmentFrame();
					}

					else
					{
						CloseEditMode();
						ShowAdjustmentFrame();
					}

				}

				_isEditing = value;
			}
		}

		private bool _isEditing = false;


		/// <summary>
		/// 是否正在编辑移动。
		/// </summary>
		public bool IsMoving
		{
			get => _isMoving;
			set
			{

				if (value != _isMoving)
				{
					if (value)
					{
						OnMoveMode();
						HideAdjustmentFrame();
					}

					else
					{
						CloseMoveMode();
						ShowAdjustmentFrame();
					}

				}

				_isMoving = value;
			}
		}
		private bool _isMoving = false;

		#endregion


		#region ctors

		public UShape(UIPresentation uipres, DesignerCanvas canvas, AdjustmentFrameLayer layer, Shape shape)
		{
			if (uipres == null || canvas == null || layer == null || shape == null)
				throw new ArgumentNullException("构造UShape时参数均不能为空。");

			UIPresentation = uipres;// 当前数据模型的总对象。
			_presentationCanvas = canvas;
			_adjustmentFrameLayer = layer;
			_shape = shape;

			_frameInfo = _shape.GetFrameInfo();

			// 绘制图形主体。
			PresentationVisual = _presentationCanvas.Add(this);

			PresentationVisual.DrawCustomVisual = PresetRenderMethods.GetDrawCustomVisualMethod(shape.PresetType);

			// 绘制选框，默认不绘制。
			// AdjustmentFrameVisual = _adjustmentFrameLayer.Add(ref _frameInfo);
			Update();

			_shape.PropertyChanged += _shape_PropertyChanged;

		}



		#endregion


		#region events

		// 事件解析器。
		private void _shape_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Visibility":
					if (_shape.Visibility) Show();
					else Hide();
					break;
				case "Transform":
					Update();
					break;
				default:
					break;
			}
		}

		#endregion


		#region methods

		public ImageSource GetImageSource()
		{
			return UIPresentation.ImageDic[(Source.Fill.Color as Fantom.Drawing.BitmapColor).Media.Uri];
		}

		/// <summary>
		/// 更新图形的渲染。
		/// </summary>
		public void Update()
		{
			_frameInfo = _shape.GetFrameInfo();

			PresentationVisual.Update();

			if (AdjustmentFrameVisual != null)
				AdjustmentFrameVisual.Update();
		}

		/// <summary>
		/// 显示调整框（即申请绘制调整框）。
		/// </summary>
		public void ShowAdjustmentFrame()
		{
			if (AdjustmentFrameVisual == null)
				AdjustmentFrameVisual = _adjustmentFrameLayer.Add(this);
		}

		/// <summary>
		/// 隐藏调整框（即从绘制图层中删除调整框）。
		/// </summary>
		public void HideAdjustmentFrame()
		{
			if (AdjustmentFrameVisual != null)
			{
				_adjustmentFrameLayer.Remove(AdjustmentFrameVisual);
				AdjustmentFrameVisual = null;
			}
		}

		/// <summary>
		/// 隐藏图形。
		/// </summary>
		public void Hide()
		{
			PresentationVisual.Clear();
			HideAdjustmentFrame();
		}

		/// <summary>
		/// 显示图形。
		/// </summary>
		public void Show()
		{
			PresentationVisual.Update();
		}


		/// <summary>
		/// 相对移动。
		/// </summary>
		public void RelativeMove(Vector2 vec)
		{
			if (IsMoving == false)
				_shape.Transform.IncreaseOffset(vec.X, vec.Y);
			else
			{
				/*
				_frameInfo.left = _prvFrameInfo.left.Add(vec);
				_frameInfo.right = _prvFrameInfo.right.Add(vec);
				_frameInfo.top = _prvFrameInfo.top.Add(vec);
				_frameInfo.buttom = _prvFrameInfo.buttom.Add(vec);
				*/
				_frameInfo.originLeft = _prvFrameInfo.originLeft + vec.X;
				_frameInfo.originTop = _prvFrameInfo.originTop + vec.Y;
				_frameInfo.anchor.X = _prvFrameInfo.anchor.X + vec.X;
				_frameInfo.anchor.Y = _prvFrameInfo.anchor.Y + vec.Y;

				//_frameInfo.point1 = _prvFrameInfo.point1.Add(vec);
				//_frameInfo.point2 = _prvFrameInfo.point2.Add(vec);
				//_frameInfo.point3 = _prvFrameInfo.point3.Add(vec);
				//_frameInfo.point4 = _prvFrameInfo.point4.Add(vec);
				/*
				_frameInfo.point12 = _prvFrameInfo.point12.Add(vec);
				_frameInfo.point23 = _prvFrameInfo.point23.Add(vec);
				_frameInfo.point34 = _prvFrameInfo.point34.Add(vec);
				_frameInfo.point41 = _prvFrameInfo.point41.Add(vec);
				*/
				PresentationVisual.Update();

			}
		}

		// 进入编辑状态。
		private void OnEditMode()
		{
			PresentationVisual.Opacity = 0.75;
			_prvFrameInfo = FrameInfo;
		}

		// 结束编辑状态。
		private void CloseEditMode()
		{
			PresentationVisual.Opacity = 1;
		}

		// 进入移动状态。
		private void OnMoveMode()
		{
			_prvFrameInfo = FrameInfo;
		}

		// 关闭移动状态。
		private void CloseMoveMode()
		{
			// Do Nothing
		}

		#endregion

	}


}
