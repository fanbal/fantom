﻿using System.Windows.Media;
using FantomUI.Util;
using System.Windows;

namespace FantomUI.Controls
{
	/// <summary>
	/// 底层线框视图，用于绘制线框。
	/// </summary>
	public class BaseStrokeFrameVisual : DrawingVisual
	{

		#region enum

		#endregion

		#region static

		public UShape UShape { get; set; } = null;

		/// <summary>
		/// 线框的相关配置。
		/// </summary>
		public static Pen Line { get; set; } =
			new Pen(new SolidColorBrush(Colors.Gray), 1d);


		/// <summary>
		/// 最小高度，当选框高度小于该值时，中间点将不予显示。
		/// </summary>
		public static double MinHeight { get; set; } = 50;

		/// <summary>
		/// 最小宽度，当选框宽度小于该值时，中间点将不予显示。
		/// </summary>
		public static double MinWidth { get; set; } = 50;

		#endregion

		#region properties

		#endregion

		#region ctors

		/// <summary>
		/// 实例化一个缩放按钮框图形。
		/// </summary>
		public BaseStrokeFrameVisual(UShape uShape)
		{
			UShape = uShape;
			var frameInfo = uShape.FrameInfo;

			// 此处绘制选框的四个点。
			var render = RenderOpen();

			// 绘制线框。
			//render.DrawLine(Line, frameInfo.point1, frameInfo.point2);
			//render.DrawLine(Line, frameInfo.point2, frameInfo.point3);
			//render.DrawLine(Line, frameInfo.point3, frameInfo.point4);
			//render.DrawLine(Line, frameInfo.point4, frameInfo.point1);

			render.DrawRectangle(null, Line, new Rect(frameInfo.originLeft, frameInfo.originTop, frameInfo.width, frameInfo.height));

			// 绘制线框。
			// render.DrawLine(Line, frameInfo.onEdge, frameInfo.rotationPos);

			render.DrawLine(Line, new Point(frameInfo.anchor.X, frameInfo.originTop), new Point(frameInfo.anchor.X, frameInfo.originTop-RotationThumbVisual.Distance));

			render.Close();
		}

		#endregion

	}
}
