﻿using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using ft = Fantom;
using Microsoft.Win32;

namespace FantomUI
{


	public partial class MainWindow : Window
	{
		private ft.IApplication _app;
		private UIPresentation _uipres;
		private ft.IPresentation _pres;
		private ft.ISlide _slide;
		private ft.Shapes _shapes;

		public MainWindow()
		{
			InitializeComponent();

			Loaded += MainWindow_Loaded;
			slideList.SelectionChanged += SlideList_SelectionChanged;

			_app = ft.Application.Create(); // 此处是Fantom的服务核心对象，必须要保证该实例存在。
		}


		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
#if DEBUG
			LoadPPTX(@"OpenXml测试.pptx");
#endif
		}



		// 选中其他页。
		private void SlideList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if ((sender as ListView).SelectedItem == null) return;
			_shapes = ((sender as ListView).SelectedItem as ft.ISlide).Shapes;
			_uipres = new UIPresentation(_pres);
			DataContext = _pres; // 绑定上下文数据。
			designer.Load(_uipres, _shapes);
			shapeList.ItemsSource = _shapes;
			slideList.ItemsSource = _pres.Slides;
		}


		// TODO 事件名需要有意义，之后会加入具体的事件名。
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "PowerPoint演示文档|*.pptx";
			openFileDialog.ShowDialog();
			string filePath = openFileDialog.FileName;
			if (filePath == "")
			{
				return; // MessageBox.Show("不能不选择文件的哟ヾ(•ω•`)o");
			}
			else
			{
				LoadPPTX(filePath);
			}
		}

		// TODO 事件名需要有意义，之后会加入具体的事件名。
		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			designer.SelectAll();
		}

		// TODO 事件名需要有意义，之后会加入具体的事件名。
		private void Button_Click_2(object sender, RoutedEventArgs e)
		{
			designer.UnSelectAll();
		}

		// TODO 事件名需要有意义，之后会加入具体的事件名。
		private void Button_Click_3(object sender, RoutedEventArgs e)
		{
			foreach (ft.Shape shape in _shapes.Selection)
			{
				shape.Transform.IncreaseOffset(100, 0);
			}

		}

		// 载入演示文稿。
		private void LoadPPTX(string filePath)
		{
			if (_pres != null) _pres.Close();
			_pres = _app.Presentations.Load(filePath);
			_slide = _pres.Slides[0];
			_shapes = _slide.Shapes;
			_uipres = new UIPresentation(_pres);
			DataContext = _pres; // 绑定上下文数据。
			designer.Load(_uipres, _shapes);
			shapeList.ItemsSource = _shapes;
			slideList.ItemsSource = _pres.Slides;

		}

		// 选择窗格选择图形。
		private void TextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			((sender as FrameworkElement).DataContext as ft.Shape).Select();
		}
	}
}
