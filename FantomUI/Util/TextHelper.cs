﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace FantomUI.Util
{
	public static class TextHelper
	{
		private static SolidColorBrush _defaultBrush = new SolidColorBrush(Colors.Black);

		/// <summary>
		/// 创建格式化的文本。
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static FormattedText CreateFormattedText(string text)
		{
			return new FormattedText(
				text,
				System.Globalization.CultureInfo.CurrentCulture,
				FlowDirection.LeftToRight,
				new Typeface("微软雅黑"), 18, _defaultBrush);
		}

		/// <summary>
		/// 绘制字体。
		/// </summary>
		/// <param name="render"></param>
		/// <param name="uShape"></param>
		public static void DrawText(this DrawingContext render, FantomUI.Controls.UShape uShape)
		{
			render.DrawText(
				CreateFormattedText(uShape.Source.ReadOnlyText),
				new Point(uShape.FrameInfo.originLeft, uShape.FrameInfo.originTop));
		}


	}
}
