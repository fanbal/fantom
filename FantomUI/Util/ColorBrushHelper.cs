﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace FantomUI.Util
{
	public static class ColorBrushHelper
	{
		public static SolidColorBrush GetBrush(this Fantom.Drawing.BaseColor color)
		{
			var color2 = Color.FromRgb(color.Red, color.Green, color.Blue);
			return new SolidColorBrush(color2);
		}
	}
}
