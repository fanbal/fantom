﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
	/// <summary>
	/// 元节点，用于描述表达式的元数据。
	/// </summary>
	public class MetaNode
	{
		/// <summary>
		/// 树的子节点。
		/// </summary>
		internal MetaNode[] MetaNodes = new MetaNode[6];

		/// <summary>
		/// 参数序列节点。
		/// </summary>
		internal int Top = 0;

		/// <summary>
		/// 父节点。
		/// </summary>
		internal MetaNode Parent;

		/// <summary>
		/// 该节点的值。
		/// </summary>
		public string Value;

		/// <summary>
		/// 该节点的主类型。
		/// </summary>
		public MetaNodeType Type ;

		/// <summary>
		/// 该节点的关键字信息。
		/// </summary>
		public MetaKeyWordType KeyWord = MetaKeyWordType.Unknown;

		/// <summary>
		/// 无参构造函数。
		/// </summary>
		public MetaNode()
		{

		}

		/// <summary>
		/// 拷贝构造函数。
		/// </summary>
		/// <param name="mt"></param>
		public MetaNode(MetaNode mt)
		{
			Top = mt.Top;
			Value = mt.Value;
			Type = mt.Type;
			KeyWord = mt.KeyWord;
		}
	}
}
