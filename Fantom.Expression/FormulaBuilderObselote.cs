﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
	/// <summary>
	/// 弃用的表达式生成器。
	/// </summary>
	internal class FormulaBuilderObselote
	{

		#region 内置的栈结构，用于性能测试，结果发现自己实现的并没有想象的那么高效
		// 参数栈（待优化为限长数组）。
		private static Stack<int> _paraStack;//  = new Stack<int>();
		private const int STACK_SIZE = 16;
		private static int[] _paraArr;//= new int[STACK_SIZE];
		private static int _stkTop = 0;

		// 压栈。
		private static void StackPush(int num)
		{
			_paraArr[_stkTop++] = num;
		}

		// 出栈。
		private static int StackPop()
		{
			return _paraArr[--_stkTop];
		}

		// 栈是否为空。
		private static bool StackIsEmpty()
		{
			return _stkTop == 0;
		}
		#endregion

		#region 检错与补全

		// 获得函数节点的参数数量。
		private static int GetParaCountOfFunctionNode(MetaNode funcNode)
		{
			return (int)funcNode.Type - (int)MetaNodeType.FunctionPara0;
		}

		// 设置函数节点的参数数量。
		private static void SetParaCountOfFunctionNode
			(MetaNode funcNode, FunctionNode function)
		{
			funcNode.Type = MetaNodeType.FunctionPara0 + (byte)function.ParameterCount;
		}

		/// <summary>
		/// 检查节点语法的合理性。
		/// </summary>
		private static bool CheckFirstMetaNodeSynax
			(MetaNode node, Dictionary<string, FunctionNode> functionDic, out int errPos)
		{
			// 如果首字符为操作符，则考虑单目与双目运算符的区别。为未定运算符确定符号。
			if (node.Type.HasFlag(MetaNodeType.Operator))
			{
				if (node.Type == MetaNodeType.Operator)
				{ // 将未定运算符表示为单目运算符。
					node.Type = MetaNodeType.OperatorSingle;
				}
				else
				{ // 如果已知为双目运算符，由于没有前操作数，该表达式非法。
					node.Type = MetaNodeType.Error; errPos = 0; return false;
				}
			}
			else if (node.KeyWord == MetaKeyWordType.EndOfSpace ||
				node.KeyWord == MetaKeyWordType.SplitOfSpace)
			{ // 如果表达式首字符为 ( 或 , 则肯定非法。
				node.Type = MetaNodeType.Error; errPos = 0; return false;
			}
			else if (node.Type.HasFlag(MetaNodeType.Function))
			{
				if (functionDic.ContainsKey(node.Value))
				{ // 为节点设置具体函数相关的参数信息。
					SetParaCountOfFunctionNode(node, functionDic[node.Value]);
				}
				else
				{ // 首元素如果为不认识的函数，则肯定非法。
					node.Type = MetaNodeType.Error; errPos = 0; return false;
				}
			}
			else if (node.Type == MetaNodeType.Empty)
			{
				// 解析开始。
				if (node.KeyWord == MetaKeyWordType.StartOfSpace)
				{
					StackPush(1);
				}

				// 解析开始。
				else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
				{
					node.Type = MetaNodeType.Error; errPos = 0; return false;
				}

				// 解析结束。
				else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
				{
					node.Type = MetaNodeType.Error; errPos = 0; return false;

				}
			}

			errPos = -1;
			return true;
		}


		/// <summary>
		/// 检查单个节点的语法。
		/// </summary>
		private static bool CheckExpressionMetaNodeSynax
			(List<MetaNode> nodes, ref int i, Dictionary<string, FunctionNode> functionDic, out int errPos)
		{
			var node = nodes[i];
			node = nodes[i];
			if (node.Type == MetaNodeType.Error)
			{
				errPos = i; return false;
			}

			if (i == 0)
			{
				if (node.Type == MetaNodeType.Operator)
				{
					node.Type = MetaNodeType.OperatorSingle;
				}
				else if (node.Type == MetaNodeType.OperatorBinary)
				{
					node.Type = MetaNodeType.Error; errPos = i; return false;
				}
				else if (node.KeyWord == MetaKeyWordType.EndOfSpace ||
					node.KeyWord == MetaKeyWordType.SplitOfSpace)
				{
					node.Type = MetaNodeType.Error; errPos = i; return false;
				}
				else if (node.Type.HasFlag(MetaNodeType.Function))
				{
					if (!functionDic.ContainsKey(node.Value))
					{
						node.Type = MetaNodeType.Error; errPos = i; return false;
					}
					else
					{
						SetParaCountOfFunctionNode(node, functionDic[node.Value]);
					}
				}
				else if (node.Type == MetaNodeType.Empty)
				{
					// 解析开始。
					if (node.KeyWord == MetaKeyWordType.StartOfSpace)
					{
						_paraStack.Push(1);
					}

					// 解析开始。
					else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
					{
						node.Type = MetaNodeType.Error; errPos = i; return false;
					}

					// 解析结束。
					else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
					{
						node.Type = MetaNodeType.Error; errPos = i; return false;

					}
				}
			}
			else
			{
				var last = nodes[i - 1];
				if (node.Type.HasFlag(MetaNodeType.Value))
				{
					if (last.Type.HasFlag(MetaNodeType.Value) ||
						last.KeyWord == MetaKeyWordType.EndOfSpace ||
						last.Type.HasFlag(MetaNodeType.Function))
					{
						node.Type = MetaNodeType.Error; errPos = i; return false;
					}
				}
				else if (node.Type.HasFlag(MetaNodeType.Operator))
				{
					// 待定操作符类型。
					if (node.Type == MetaNodeType.Operator)
					{
						if (last.Type.HasFlag(MetaNodeType.Operator) ||
							last.KeyWord == MetaKeyWordType.StartOfSpace ||
							last.KeyWord == MetaKeyWordType.SplitOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
						{
							node.Type = MetaNodeType.OperatorSingle;
						}

						else
						{
							node.Type = MetaNodeType.OperatorBinary;
						}
					}

					else if (node.Type == MetaNodeType.OperatorBinary)
					{
						if (last.Type.HasFlag(MetaNodeType.Operator) ||
							last.KeyWord == MetaKeyWordType.StartOfSpace ||
							last.KeyWord == MetaKeyWordType.SplitOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
						{
							node.Type = MetaNodeType.Error; errPos = i; return false;
						}
					}
					// 如果为单目运算符。
					else
					{
						if (last.Type.HasFlag(MetaNodeType.Value) ||
						last.KeyWord == MetaKeyWordType.EndOfSpace ||
						last.Type.HasFlag(MetaNodeType.Function))
						{
							node.Type = MetaNodeType.Error; errPos = i; return false;
						}
					}

				}
				else if (node.Type.HasFlag(MetaNodeType.Function))
				{
					if (last.Type.HasFlag(MetaNodeType.Value) ||
						last.KeyWord == MetaKeyWordType.EndOfSpace ||
						!functionDic.ContainsKey(node.Value))
					{
						node.Type = MetaNodeType.Error; errPos = i; return false;
					}
					else
					{
						SetParaCountOfFunctionNode(node, functionDic[node.Value]);
					}
				}
				else if (node.Type == MetaNodeType.Empty)
				{
					// 解析开始。
					if (node.KeyWord == MetaKeyWordType.StartOfSpace)
					{
						if (!last.Type.HasFlag(MetaNodeType.Function) &&
							!last.Type.HasFlag(MetaNodeType.Operator) &&
							!(last.KeyWord == MetaKeyWordType.StartOfSpace) &&
							!(last.KeyWord == MetaKeyWordType.SplitOfSpace))
						{
							node.Type = MetaNodeType.Error; errPos = i; return false;
						}
						else if (last.Type.HasFlag(MetaNodeType.Function))
						{
							int pc = GetParaCountOfFunctionNode(last);
							if (pc > 6)
							{
								last.Type = MetaNodeType.Error; errPos = i - 1; return false;
							}
							else if (pc >= 1)
							{
								_paraStack.Push(pc);
							}
							else if (pc == 0)
							{
								_paraStack.Push(1);
							}
							else
							{
								last.Type = MetaNodeType.Error; errPos = i - 1; return false;
							}

						}
						else
						{
							_paraStack.Push(1);
						}
					}

					// 解析开始。
					else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
					{
						if (last.Type.HasFlag(MetaNodeType.Function) ||
							last.Type.HasFlag(MetaNodeType.Operator) ||
							last.KeyWord == MetaKeyWordType.SplitOfSpace ||
							last.KeyWord == MetaKeyWordType.StartOfSpace ||
							_paraStack.Count == 0)
						{
							node.Type = MetaNodeType.Error; errPos = i; return false;
						}
						else
						{
							if (_paraStack.Count == 0)
							{
								node.Type = MetaNodeType.Error; errPos = i; return false;
							}
							else
							{
								int top = _paraStack.Pop();
								top--;
								_paraStack.Push(top);
							}

						}

					}

					// 解析结束。
					else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
					{
						if (last.Type.HasFlag(MetaNodeType.Function) ||
							last.Type.HasFlag(MetaNodeType.Operator) ||
							last.KeyWord == MetaKeyWordType.SplitOfSpace)
						{
							node.Type = MetaNodeType.Error; errPos = i; return false;
						}
						else
						{
							if (_paraStack.Count == 0)
							{
								node.Type = MetaNodeType.Error; errPos = i; return false;
							}
							else
							{
								int top = _paraStack.Pop();
								if (top != 1)
								{
									node.Type = MetaNodeType.Error; errPos = i; return false;
								}

							}


						}

					}
				}
			}

			errPos = -1;
			return true;
		}

		/// <summary>
		/// 检查表达式的语法。
		/// </summary>
		[Obsolete("代码结构更合理，但存在函数嵌套调用影响性能。")]
		public static void CheckExpressionMataGroupSynax1
			(List<MetaNode> nodes, Dictionary<string, FunctionNode> functionDic, out int errPos)
		{
			_stkTop = 0;

			int len = nodes.Count;
			errPos = -1;

			for (int i = 0; i < len; i++)
				if (!CheckExpressionMetaNodeSynax(nodes, ref i, functionDic, out errPos))
					return;
			if (!StackIsEmpty() ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Function) ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Operator))
				errPos = len - 1;
		}



		[Obsolete("性能意外的很强，采用了部分硬编码。")]
		public static void CheckExpressionMataGroupSynax2
			(List<MetaNode> nodes, Dictionary<string, FunctionNode> functionDic, out int errPos)
		{
			_paraStack.Clear();

			MetaNode node;
			int len = nodes.Count;
			errPos = -1;

			for (int i = 0; i < len; i++)
			{
				node = nodes[i];
				if (node.Type == MetaNodeType.Error)
				{
					errPos = i; return;
				}

				if (i == 0)
				{
					if (node.Type == MetaNodeType.Operator)
					{
						node.Type = MetaNodeType.OperatorSingle;
					}
					else if (node.Type == MetaNodeType.OperatorBinary)
					{
						node.Type = MetaNodeType.Error; errPos = i; return;
					}
					else if (node.KeyWord == MetaKeyWordType.EndOfSpace ||
						node.KeyWord == MetaKeyWordType.SplitOfSpace)
					{
						node.Type = MetaNodeType.Error; errPos = i; return;
					}
					else if (node.Type.HasFlag(MetaNodeType.Function))
					{
						if (!functionDic.ContainsKey(node.Value))
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}
						else
						{
							SetParaCountOfFunctionNode(node, functionDic[node.Value]);
						}
					}
					else if (node.Type == MetaNodeType.Empty)
					{
						// 解析开始。
						if (node.KeyWord == MetaKeyWordType.StartOfSpace)
						{
							_paraStack.Push(1);
						}

						// 解析开始。
						else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}

						// 解析结束。
						else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
						{
							node.Type = MetaNodeType.Error; errPos = i; return;

						}
					}
				}
				else
				{
					var last = nodes[i - 1];
					if (node.Type.HasFlag(MetaNodeType.Value))
					{
						if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
						{
							node.Type = MetaNodeType.Error; errPos = i;
							return;
						}
					}
					else if (node.Type.HasFlag(MetaNodeType.Operator))
					{
						// 待定操作符类型。
						if (node.Type == MetaNodeType.Operator)
						{
							if (last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.OperatorSingle;
							}

							else
							{
								node.Type = MetaNodeType.OperatorBinary;
							}
						}

						else if (node.Type == MetaNodeType.OperatorBinary)
						{
							if (last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.Error; errPos = i; return;
							}
						}
						// 如果为单目运算符。
						else
						{
							if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.Error; errPos = i; return;
							}
						}

					}
					else if (node.Type.HasFlag(MetaNodeType.Function))
					{
						if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							!functionDic.ContainsKey(node.Value))
						{
							node.Type = MetaNodeType.Error; errPos = i;
							return;
						}
						else
						{
							SetParaCountOfFunctionNode(node, functionDic[node.Value]);
						}
					}
					else if (node.Type == MetaNodeType.Empty)
					{
						// 解析开始。
						if (node.KeyWord == MetaKeyWordType.StartOfSpace)
						{
							if (!last.Type.HasFlag(MetaNodeType.Function) &&
								!last.Type.HasFlag(MetaNodeType.Operator) &&
								!(last.KeyWord == MetaKeyWordType.StartOfSpace) &&
								!(last.KeyWord == MetaKeyWordType.SplitOfSpace))
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else if (last.Type.HasFlag(MetaNodeType.Function))
							{
								int pc = GetParaCountOfFunctionNode(last);
								if (pc > 6)
								{
									last.Type = MetaNodeType.Error; errPos = i - 1;
									return;
								}
								else if (pc >= 1)
								{
									_paraStack.Push(pc);
								}
								else if (pc == 0)
								{
									_paraStack.Push(1);
								}
								else
								{
									last.Type = MetaNodeType.Error; errPos = i - 1;
									return;
								}

							}
							else
							{
								_paraStack.Push(1);
							}
						}

						// 解析开始。
						else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
						{
							if (last.Type.HasFlag(MetaNodeType.Function) ||
								last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								_paraStack.Count == 0)
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else
							{
								if (_paraStack.Count == 0)
								{
									node.Type = MetaNodeType.Error; errPos = i;
									return;
								}
								else
								{
									int top = _paraStack.Pop();
									top--;
									_paraStack.Push(top);
								}

							}

						}

						// 解析结束。
						else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
						{
							if (last.Type.HasFlag(MetaNodeType.Function) ||
								last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace)
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else
							{
								if (_paraStack.Count == 0)
								{
									node.Type = MetaNodeType.Error; errPos = i;
									return;
								}
								else
								{
									int top = _paraStack.Pop();
									if (top != 1)
									{
										node.Type = MetaNodeType.Error; errPos = i;
										return;
									}

								}


							}

						}
					}
				}
			}
			if (_paraStack.Count != 0 ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Function) ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Operator))
				errPos = len - 1;
		}



		/// <summary>
		/// 性能最理想的词法解析版本（但是并没有快多少）。
		/// </summary>
		public static void CheckExpressionMataGroupSynax3_0
			(List<MetaNode> nodes, Dictionary<string, FunctionNode> functionDic, out int errPos)
		{
			_stkTop = 0;

			MetaNode node, last;
			int len = nodes.Count;
			errPos = -1;

			for (int i = 0; i < len; i++)
			{
				node = nodes[i];
				if (node.Type == MetaNodeType.Error)
				{
					errPos = i; return;
				}

				if (i == 0)
				{
					if (node.Type.HasFlag(MetaNodeType.Value))
					{
						continue;
					}
					else if (node.Type == MetaNodeType.Operator)
					{
						node.Type = MetaNodeType.OperatorSingle;
					}
					else if (node.Type == MetaNodeType.OperatorBinary)
					{
						node.Type = MetaNodeType.Error; errPos = i; return;
					}
					else if (node.KeyWord == MetaKeyWordType.EndOfSpace ||
						node.KeyWord == MetaKeyWordType.SplitOfSpace)
					{
						node.Type = MetaNodeType.Error; errPos = i; return;
					}
					else if (node.Type.HasFlag(MetaNodeType.Function))
					{
						if (!functionDic.ContainsKey(node.Value))
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}
						else
						{
							node.Type = unchecked(MetaNodeType.FunctionPara0 + functionDic[node.Value].ParameterCount);
						}
					}
					else if (node.Type == MetaNodeType.Empty)
					{
						// 解析开始。
						if (node.KeyWord == MetaKeyWordType.StartOfSpace)
						{
							_paraStack.Push(1);
						}

						// 解析开始。
						else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}

						// 解析结束。
						else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}
					}
				}
				else
				{
					last = nodes[i - 1];

					if (node.Type.HasFlag(MetaNodeType.Value))
					{
						if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}
					}

					else if (node.Type.HasFlag(MetaNodeType.Operator))
					{
						// 待定操作符类型。
						if (node.Type == MetaNodeType.Operator)
						{
							if (last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.OperatorSingle;
							}

							else
							{
								node.Type = MetaNodeType.OperatorBinary;
							}
						}

						else if (node.Type == MetaNodeType.OperatorBinary)
						{
							if (last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.Error; errPos = i; return;
							}
						}
						// 如果为单目运算符。
						else
						{
							if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.Error; errPos = i; return;
							}
						}

					}

					else if (node.Type.HasFlag(MetaNodeType.Function))
					{
						if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							!functionDic.ContainsKey(node.Value))
						{
							node.Type = MetaNodeType.Error; errPos = i;
							return;
						}
						else
						{
							node.Type = unchecked(MetaNodeType.FunctionPara0 + functionDic[node.Value].ParameterCount);
						}
					}

					else if (node.Type == MetaNodeType.Empty)
					{
						// 解析开始。
						if (node.KeyWord == MetaKeyWordType.StartOfSpace)
						{
							if (!last.Type.HasFlag(MetaNodeType.Function) &&
								!last.Type.HasFlag(MetaNodeType.Operator) &&
								!(last.KeyWord == MetaKeyWordType.StartOfSpace) &&
								!(last.KeyWord == MetaKeyWordType.SplitOfSpace))
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else if (last.Type.HasFlag(MetaNodeType.Function))
							{

								int pc = unchecked(last.Type - MetaNodeType.FunctionPara0); ;
								if (pc > 6)
								{
									last.Type = MetaNodeType.Error; errPos = i - 1;
									return;
								}
								else if (pc >= 1)
								{
									_paraArr[_stkTop++] = pc;
								}
								else if (pc == 0)
								{
									_paraArr[_stkTop++] = 1;
								}
								else
								{
									last.Type = MetaNodeType.Error; errPos = i - 1; return;
								}

							}
							else
							{
								_paraArr[_stkTop++] = 1;
							}
						}

						// 解析开始。
						else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
						{
							if (last.Type.HasFlag(MetaNodeType.Function) ||
								last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								_stkTop == 0)
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else
							{
								if (_stkTop == 0)
								{
									node.Type = MetaNodeType.Error; errPos = i;
									return;
								}
								else
								{
									_paraArr[_stkTop - 1]--;
								}

							}

						}

						// 解析结束。
						else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
						{
							if (last.Type.HasFlag(MetaNodeType.Function) ||
								last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace)
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else
							{
								if (_stkTop == 0)
								{
									node.Type = MetaNodeType.Error; errPos = i;
									return;
								}
								else
								{
									int top = _paraArr[--_stkTop];
									if (top != 1)
									{
										node.Type = MetaNodeType.Error; errPos = i;
										return;
									}

								}


							}

						}
					}
				}
			}
			if (_stkTop != 0 ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Function) ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Operator))
				errPos = len - 1;
		}

		/// <summary>
		/// 性能最理想的词法解析版本（但是并没有快多少）。
		/// </summary>
		/// <param name="nodes"></param>
		/// <param name="functionDic"></param>
		/// <param name="errPos"></param>
		public static void CheckExpressionMataGroupSynax3_1
			(List<MetaNode> nodes, Dictionary<string, FunctionNode> functionDic, out int errPos)
		{
			_stkTop = 0;

			MetaNode node, last;
			int len = nodes.Count;
			errPos = -1;

			for (int i = 0; i < len; i++)
			{
				node = nodes[i];
				if (node.Type == MetaNodeType.Error)
				{
					errPos = i; return;
				}
				if (i != 0)
				{
					last = nodes[i - 1];

					if (node.Type.HasFlag(MetaNodeType.Value))
					{
						if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}
					}

					else if (node.Type.HasFlag(MetaNodeType.Operator))
					{
						// 待定操作符类型。
						if (node.Type == MetaNodeType.Operator)
						{
							if (last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.OperatorSingle;
							}

							else
							{
								node.Type = MetaNodeType.OperatorBinary;
							}
						}

						else if (node.Type == MetaNodeType.OperatorBinary)
						{
							if (last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.Error; errPos = i; return;
							}
						}
						// 如果为单目运算符。
						else
						{
							if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							last.Type.HasFlag(MetaNodeType.Function))
							{
								node.Type = MetaNodeType.Error; errPos = i; return;
							}
						}

					}

					else if (node.Type.HasFlag(MetaNodeType.Function))
					{
						if (last.Type.HasFlag(MetaNodeType.Value) ||
							last.KeyWord == MetaKeyWordType.EndOfSpace ||
							!functionDic.ContainsKey(node.Value))
						{
							node.Type = MetaNodeType.Error; errPos = i;
							return;
						}
						else
						{
							node.Type = unchecked(MetaNodeType.FunctionPara0 + functionDic[node.Value].ParameterCount);
						}
					}

					else if (node.Type == MetaNodeType.Empty)
					{
						// 解析开始。
						if (node.KeyWord == MetaKeyWordType.StartOfSpace)
						{
							if (!last.Type.HasFlag(MetaNodeType.Function) &&
								!last.Type.HasFlag(MetaNodeType.Operator) &&
								!(last.KeyWord == MetaKeyWordType.StartOfSpace) &&
								!(last.KeyWord == MetaKeyWordType.SplitOfSpace))
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else if (last.Type.HasFlag(MetaNodeType.Function))
							{

								int pc = unchecked(last.Type - MetaNodeType.FunctionPara0); ;
								if (pc > 6)
								{
									last.Type = MetaNodeType.Error; errPos = i - 1;
									return;
								}
								else if (pc >= 1)
								{
									_paraArr[_stkTop++] = pc;
								}
								else if (pc == 0)
								{
									_paraArr[_stkTop++] = 1;
								}
								else
								{
									last.Type = MetaNodeType.Error; errPos = i - 1; return;
								}

							}
							else
							{
								_paraArr[_stkTop++] = 1;
							}
						}

						// 解析开始。
						else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
						{
							if (last.Type.HasFlag(MetaNodeType.Function) ||
								last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace ||
								last.KeyWord == MetaKeyWordType.StartOfSpace ||
								_stkTop == 0)
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else
							{
								if (_stkTop == 0)
								{
									node.Type = MetaNodeType.Error; errPos = i;
									return;
								}
								else
								{
									_paraArr[_stkTop - 1]--;
								}

							}

						}

						// 解析结束。
						else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
						{
							if (last.Type.HasFlag(MetaNodeType.Function) ||
								last.Type.HasFlag(MetaNodeType.Operator) ||
								last.KeyWord == MetaKeyWordType.SplitOfSpace)
							{
								node.Type = MetaNodeType.Error; errPos = i;
								return;
							}
							else
							{
								if (_stkTop == 0)
								{
									node.Type = MetaNodeType.Error; errPos = i;
									return;
								}
								else
								{
									int top = _paraArr[--_stkTop];
									if (top != 1)
									{
										node.Type = MetaNodeType.Error; errPos = i;
										return;
									}

								}


							}

						}
					}
				}
				else
				{
					if (node.Type.HasFlag(MetaNodeType.Value))
					{
						continue;
					}
					else if (node.Type == MetaNodeType.Operator)
					{
						node.Type = MetaNodeType.OperatorSingle;
					}
					else if (node.Type == MetaNodeType.OperatorBinary)
					{
						node.Type = MetaNodeType.Error; errPos = i; return;
					}
					else if (node.KeyWord == MetaKeyWordType.EndOfSpace ||
						node.KeyWord == MetaKeyWordType.SplitOfSpace)
					{
						node.Type = MetaNodeType.Error; errPos = i; return;
					}
					else if (node.Type.HasFlag(MetaNodeType.Function))
					{
						if (!functionDic.ContainsKey(node.Value))
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}
						else
						{
							node.Type = unchecked(MetaNodeType.FunctionPara0 + functionDic[node.Value].ParameterCount);
						}
					}
					else if (node.Type == MetaNodeType.Empty)
					{
						// 解析开始。
						if (node.KeyWord == MetaKeyWordType.StartOfSpace)
						{
							_paraStack.Push(1);
						}

						// 解析开始。
						else if (node.KeyWord == MetaKeyWordType.SplitOfSpace)
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}

						// 解析结束。
						else if (node.KeyWord == MetaKeyWordType.EndOfSpace)
						{
							node.Type = MetaNodeType.Error; errPos = i; return;
						}
					}
				}

			}
			if (_stkTop != 0 ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Function) ||
				nodes[len - 1].Type.HasFlag(MetaNodeType.Operator))
				errPos = len - 1;
		}


		#endregion
	}
}
