﻿using DocumentFormat.OpenXml;
using P = DocumentFormat.OpenXml.Presentation;

namespace Fantom.Drawing
{
	//TODO 极为复杂的媒体标记类
	/// <summary>
	/// 表示a:nvPr节点,这是一个极为复杂的属性,它用于描述指定对象的媒体类型.
	/// 如果想要描述好,需要做充分的准备.
	/// </summary>
	public partial class VisualAdvancedMediaProperties : EchoBaseObject
	{
		private bool _isPhoto = false;

		public bool IsPhoto
		{
			get { return _isPhoto; }
			set { _isPhoto = value; PropertyChangedInvoke(); }
		}

		
		public VisualAdvancedMediaProperties()
		{

		}

	}
}
