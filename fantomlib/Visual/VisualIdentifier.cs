﻿using DocumentFormat.OpenXml;
using P = DocumentFormat.OpenXml.Presentation;

namespace Fantom.Drawing
{
	/// <summary>
	/// 描述图形的标识符对象，对应的属性为p:cnvPr节点，请注意，不是p:cNvSpPr也不是p:nvPr节点。
	/// 具体的创建工厂参考<see cref="IdentifierFactory"/>
	/// </summary>
	public class VisualIdentifier : BaseIdentifier
	{
		#region properties

		private string _name = "visual_object";

		/// <summary>
		/// 图形对象在选择窗格中的名称，该名称可以重复。
		/// </summary>
		public string Name
		{
			get => _name;
			set { _name = value; PropertyChangedInvoke(); }
		}


		private string _description = "";

		/// <summary>
		/// 对指定图形对象的描述。<see cref="Title"/>。
		/// </summary>
		public string Description
		{
			get { return _description; }
			set { _description = value; PropertyChangedInvoke(); }
		}


		private bool _hidden = false;

		/// <summary>
		/// 在对象窗格中是否显示，默认情况下均为显示。
		/// </summary>
		public bool Hidden
		{
			get { return _hidden; }
			set { _hidden = value; PropertyChangedInvoke(); }
		}


		private string _title;


		/// <summary>
		/// 指定对象的标题，需要与<see cref="Description"/>配合使用。
		/// </summary>
		public string Title
		{
			get { return _title; }
			set { _title = value; PropertyChangedInvoke(); }
		}

		#endregion

		#region ctors

		/// <summary>
		/// 经由通过工厂创建的对象
		/// </summary>
		public VisualIdentifier()
		{

		}

		public VisualIdentifier(uint id, string name)
		{
			_id = id;
			_name = name;
		}

		public VisualIdentifier(uint id) : this(id, "visual_object")
		{
		}

		#endregion


	}
}
