﻿using DocumentFormat.OpenXml;
using P = DocumentFormat.OpenXml.Presentation;
using Fantom.Drawing;
using Fantom.Factory;
namespace Fantom.Drawing
{

	/// <summary>
	/// 图形对象的抽象属性的基类
	/// </summary>
	/// <typeparam name="TNODE">输出的节点类型</typeparam>
	/// <typeparam name="TEXP">拓展节点的类型</typeparam>
	public abstract class BaseVisualAbstractProperty<TNODE, TEXP> : EchoBaseObject
		where TNODE : OpenXmlCompositeElement, new()
		where TEXP : IVisualExtend, new()
	{
		#region properties


		private VisualIdentifier _id;

		/// <summary>
		/// 标识符相关属性
		/// </summary>
		public VisualIdentifier Identifier
		{
			get { return _id; }
		}

		private TEXP _extend;


		/// <summary>
		/// 锁定相关属性
		/// </summary>
		public TEXP Extend
		{
			get { return _extend; }
		}

		private VisualAdvancedMediaProperties _advance;

		/// <summary>
		/// 高级功能
		/// </summary>
		public VisualAdvancedMediaProperties Advance
		{
			get { return _advance; }
		}

		#endregion

		public BaseVisualAbstractProperty(VisualIdentifierFactory factory)
		{
			_id = factory.Create();

		}


	}

	/// <summary>
	/// 对应p.nvSpPr节点
	/// </summary>
	public sealed class ShapeAbstractProperty : BaseVisualAbstractProperty<P.NonVisualShapeProperties, ShapeExtendProperties>
	{
		public ShapeAbstractProperty(VisualIdentifierFactory factory) : base(factory)
		{
		}
	}


	/// <summary>
	/// 对应p.nvGrpSpPr节点
	/// </summary>
	public sealed class GroupAbstractProperty : BaseVisualAbstractProperty<P.NonVisualGroupShapeProperties, GroupExtendProperties>
	{
		public GroupAbstractProperty(VisualIdentifierFactory factory) : base(factory)
		{
		}
	}


	/// <summary>
	/// 对应p.nvCxnSpPr节点
	/// </summary>
	public sealed class ConnectorAbstractProperty :
		BaseVisualAbstractProperty<P.NonVisualConnectionShapeProperties, ConnectorExtendProperties>
	{
		public ConnectorAbstractProperty(VisualIdentifierFactory factory) : base(factory)
		{
		}
	}

	/// <summary>
	/// 对应p.nvPicPr节点
	/// </summary>
	public sealed class PictureAbstractProperty :
		BaseVisualAbstractProperty<P.NonVisualPictureProperties, PictureExtendProperties>
	{
		public PictureAbstractProperty(VisualIdentifierFactory factory) : base(factory)
		{
		}
	}


}
