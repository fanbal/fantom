﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Builders
{

    internal class PresentationImportArgs
    {
        public class PackRIDToURLMapper:Dictionary<string, PK.OpenXmlPart>
        {
            public PackRIDToURLMapper(PK.OpenXmlPart part = null)
            {
                if (part is null) return;
                if (part.Parts is not null)
                {
                    foreach (var idpair in part.Parts)
                    {
                        Add(idpair.RelationshipId, idpair.OpenXmlPart);
                    }
                }
            }
        }

        internal class ShapesShapeIDMapper : Dictionary<uint, ShapeBase>
        {

        }

        public class URLCreatedPartMapper : Dictionary<Uri, EchoBaseObject>
        {

        }

        public PackRIDToURLMapper RID2URLMapper;

        public URLCreatedPartMapper URL2CreatedPartMapper;

        public ShapesShapeIDMapper ShapeMapper;

        public IOPK.Package Package;

        public Presentation Presentation;

        public Application Application;

        public PK.PresentationPart PresentationPart;

        public PK.OpenXmlPart ArgPart;

        public OX.OpenXmlElement ArgElement;

        public ColorLoadArgs ColorLoadArgs;

        public Fantom.Builders.PresentationLoader.RaiseLoadingBundleEventHandler LoadBundleHandler;

        public MediaManager MediaManager => Presentation.MediaManager;

        public PresentationImportArgs()
        {
            URL2CreatedPartMapper = new URLCreatedPartMapper();
            RID2URLMapper = new PackRIDToURLMapper();
        }


        public PresentationImportArgs(PresentationImportArgs presentationImportArgs)
        {
            URL2CreatedPartMapper = presentationImportArgs.URL2CreatedPartMapper;
            RID2URLMapper = presentationImportArgs.RID2URLMapper;
            Package = presentationImportArgs.Package;
            Presentation = presentationImportArgs.Presentation;
            Application = presentationImportArgs.Application;
            PresentationPart = presentationImportArgs.PresentationPart;
            ArgPart = presentationImportArgs.ArgPart;
            ArgElement = presentationImportArgs.ArgElement;
            ColorLoadArgs = presentationImportArgs.ColorLoadArgs;
            LoadBundleHandler = presentationImportArgs.LoadBundleHandler;
            ShapeMapper = presentationImportArgs.ShapeMapper;
        }

    }
}
