﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Builders
{
    /// <summary>
    /// 导出的参数配置。
    /// </summary>
    internal class PresentationExportArgs
    {
        public OpenXmlPartMapper Mapper { get; private set; }

        public Dictionary<EchoBaseObject,Uri> MediaMapper;

        public EchoBaseObject RelationshipMapperContainer;

        public PresentationExportArgs()
        {
            Mapper = new OpenXmlPartMapper();
            MediaMapper = new Dictionary<EchoBaseObject, Uri>();
        }

        public PresentationExportArgs(PresentationExportArgs exportArgs)
        {
            Mapper = exportArgs.Mapper;
            RelationshipMapperContainer = exportArgs.RelationshipMapperContainer;
            MediaMapper = exportArgs.MediaMapper;
        }



    }
}
