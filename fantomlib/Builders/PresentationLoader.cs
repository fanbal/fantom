﻿using System.IO;
using System.IO.Packaging;
using System.Linq;

namespace Fantom.Builders
{

    public class RaiseLoadingShapeArguments
    {
        public Shapes Shapes;
        public ShapeBase Shape;
        public int Index;
        public int Count;

    }

    public class RaiseLoadingSlideArguments
    {
        public IEnumerable<EchoBaseObject> Slides;
        public EchoBaseObject Slide;
        public int Index;
        public int Count;
    }

    public class RaiseLoadingBundleArguments
    {
        public AnimationBundle Bundle;
        public int Index;
        public int Count;
    }


    /// <summary>
    /// 用于演示文稿的整体导入。
    /// </summary>
    internal static class PresentationLoader
    {
        /// <summary>
        /// 导入图形时传递的消息句柄。
        /// </summary>
        /// <param name="info"></param>
        public delegate void RaiseLoadingShapeEventHandler(RaiseLoadingShapeArguments raiseLoadingShapeArguments);

        /// <summary>
        /// 导入动画束时传递的消息句柄。
        /// </summary>
        /// <param name="info"></param>
        public delegate void RaiseLoadingBundleEventHandler(RaiseLoadingBundleArguments arguments);

        /// <summary>
        /// 基本步骤数。基本步骤包括: 1. 包体导入 2. 页面排序 3. 主题导入 4. 解析完成。 
        /// </summary>
        private const int BASE_STEPS = 4;


        /// <summary>
        /// 触发响应事件。
        /// </summary>
        /// <param name="press">作为容器的演示文稿集合对象。</param>
        /// <param name="loadingState">导入状态。</param>
        /// <param name="steps">总步骤。</param>
        /// <param name="step">当前步骤。</param>
        private static void RaiseLoadingSlideEvent(Presentations press, PresentationLoadArguments presentationsLoadingArgs)
        {
            press.RaiseLoadingSlideEvent(presentationsLoadingArgs);
        }

        /// <summary>
        /// 触发响应事件。
        /// </summary>
        /// <param name="press">作为容器的演示文稿集合对象。</param>
        /// <param name="loadingState">导入状态。</param>
        /// <param name="steps">总步骤。</param>
        /// <param name="step">当前步骤。</param>
        /// <param name="info">传递的附加消息。</param>
        private static void RaiseLoadingSlideEvent(Presentations press, PresentationsLoadingState loadingState, int steps, int step, string info)
        {
            press.RaiseLoadingSlideEvent(loadingState, steps, step, info);
        }

        /// <summary>
        /// 导入并返回演示文稿。是导入模块的入口。
        /// </summary>
        /// <param name="press">演示文稿集合。需要存在此对象检查当前导入路径是否合法。</param>
        /// <param name="uristr">导入路径。</param>
        /// <param name="allowEdit">是否允许编辑。</param>
        /// <returns>成功导入后，将会在集合宿主中注册该对象，并返回该对象。如果路径不合法，则返回 <see cref="null"/> 。</returns>
        public static Presentation Load(Presentations press, string uristr, bool allowEdit)
        {
            uristr = uristr.Trim();
            if (File.Exists(uristr))
            {
                return LoadEntry(press, uristr, allowEdit);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 导入的核心模块。
        /// </summary>
        /// <param name="press">演示文稿集合。生成完成的演示文稿对象需要在演示文稿集合中进行注册。</param>
        /// <param name="uristr">合法的路径需要在演示文稿集合对象中进行注册。</param>
        /// <param name="allowEdit">是否允许编辑。</param>
        /// <returns>新建立的演示文稿对象。</returns>
        private static Presentation LoadEntry(Presentations press, string uristr, bool allowEdit)
        {
            var importArgs = new PresentationImportArgs();

            var app = press.Application;
            var stepCount = 0;
            var step = 0;

            var presdoc = PK.PresentationDocument.Open(uristr, false);
            // 截止目前为止，演示文稿包体的打开已完成。


            var package = presdoc.Package;

            var pres = PresentationBuilder.Build(presdoc, uristr, allowEdit);

            var sldps = presdoc.PresentationPart.SlideParts;

            importArgs.Application = app;
            importArgs.Presentation = pres;
            importArgs.Package = package;
            importArgs.PresentationPart = presdoc.PresentationPart;
            importArgs.ArgPart = presdoc.PresentationPart;

            press._filePathHashSet.Add(uristr); // 在集合的哈希表中注册该路径。

            stepCount = sldps.Count() + BASE_STEPS;

            // 截止目前为止，演示文稿的导入已经完成。
            RaiseLoadingSlideEvent(press, new PresentationLoadArguments(PresentationsLoadingState.PackageLoadCompleted, stepCount, ++step));

            // 导入主题完成。
            RaiseLoadingSlideEvent(press, new PresentationLoadArguments(PresentationsLoadingState.ThemeLoadCompleted, stepCount, ++step));

            // 此处用于导入备注母版。
            NoteMasterBuilder.LoadNoteMaster(new BuildingCallbackLoadArgs(), new PresentationImportArgs(importArgs) { ArgPart = presdoc.PresentationPart.NotesMasterPart });

            // 此处用于导入母版。
            SlideMasterBuilder.LoadSlideMasters(new BuildingCallbackLoadArgs(), new PresentationImportArgs(importArgs) { ArgPart = presdoc.PresentationPart });

            // TODO 幻灯片重排。
            var sldrange = from sld in sldps
                           orderby SlideBaseBuilder.GetSlideId(sld.Uri.OriginalString)
                           select sld;

            // 获得空的幻灯片集合。
            var slides = pres.Slides;

            // 幻灯片的重排已完成。
            RaiseLoadingSlideEvent(press, new PresentationLoadArguments(PresentationsLoadingState.SlidesReorderCompleted, stepCount, ++step));



            // 幻灯片处理。
            if (sldrange is not null)
            {
                var slideCount = sldrange.Count();
                var slideIndex = 1;
                foreach (var sldp in sldrange)
                {
                    var buildingArgs = new BuildingCallbackLoadArgs();
                    buildingArgs.ShapeHandler = (arg) =>
                    {
                        RaiseLoadingSlideEvent(press, new PresentationLoadArguments(PresentationsLoadingState.ShapeLoadCompleted, stepCount, step)
                        { RaiseLoadingShapeArguments = arg });
                    };
                    buildingArgs.BundleHandler = (arg) =>
                    {
                        RaiseLoadingSlideEvent(press, new PresentationLoadArguments(PresentationsLoadingState.BundleLoadCompleted, stepCount, step)
                        { RaiseLoadingBundleArguments = arg });
                    };


                    var slide = SlideBaseBuilder.LoadSlide(slides, buildingArgs, new PresentationImportArgs(importArgs) { ArgPart = sldp });

                    // 单页幻灯片导入完成。
                    RaiseLoadingSlideEvent(press, new PresentationLoadArguments(PresentationsLoadingState.SlideLoadCompleted, stepCount, ++step)
                    {
                        RaiseLoadingSlideArguments = new RaiseLoadingSlideArguments() { Slides = slides, Slide = slide, Count = slideCount, Index = slideIndex }
                    });
                    slideIndex++;
                }
            }

            presdoc.Close();
            presdoc.Dispose();

            pres.Parent = press; // 绑定父子集合关系。
            pres.Application = app;
            press.Add(pres);
            pres.ApplyApplicationAndParentForProperty();

            // 此时文档全部导入完成。
            RaiseLoadingSlideEvent(press, new PresentationLoadArguments(PresentationsLoadingState.PresentationLoadCompleted, stepCount, ++step));

            return pres;
        }




        #region Custom Info

        // 载入自定义信息。
        // 自定义信息包括自定义 Xml 与自定义 Tag 。
        public static void LoadSlideCustomInfo
            (Application app, CustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.SlidePart sldpart, PresentationImportArgs importArgs)
        {

            //// 自定义列表。
            //var customList = container.CustomXmlList;
            //var tags = container.Tags; // 获得父对象的Tags容器并使用它，添加字段。

            //customList.Application = app;
            //customList.Parent = container;

            //if (xlist is null)
            //{
            //    return;
            //}

            //// 遍历Xml节点。
            //foreach (var customXmlPart in xlist)
            //{

            //    // 处理自定义Xml节点。
            //    if (customXmlPart is P.CustomerData)
            //    {
            //        var xmlpart = CustomXmlBuilder.Build(
            //            customXmlPart as P.CustomerData, package, uridic);
            //        xmlpart.Application = app;
            //        xmlpart.Parent = customList;
            //        customList.Add(xmlpart);
            //    }

            //    // 处理自定义Tag节点。
            //    else if (customXmlPart is P.CustomerDataTags)
            //    {
            //        string rId = (customXmlPart as P.CustomerDataTags).Id;
            //        var t =
            //            from tagpair in sldpart.UserDefinedTagsParts
            //            where tagpair.Uri == uridic[rId]
            //            select tagpair;
            //        if (t.Count() != 0)
            //        {
            //            var taglist = t.First().TagList;
            //            if (taglist is not null)
            //            {
            //                foreach (P.Tag tag in taglist)
            //                {
            //                    container.Tags.Add(new TagKeyValuePair(tag.Name, tag.Val));
            //                }
            //            }
            //        }
            //    }

            //}
        }


        // 载入自定义信息。
        // 自定义信息包括自定义 Xml 与自定义 Tag 。
        public static void LoadSlideLayoutCustomInfo
            (Application app, CustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.SlideLayoutPart sldpart, PresentationImportArgs importArgs)
        {

            //// 自定义列表。
            //var customList = container.CustomXmlList;
            //var tags = container.Tags; // 获得父对象的Tags容器并使用它，添加字段。

            //customList.Application = app;
            //customList.Parent = container;

            //if (xlist is null)
            //{
            //    return;
            //}

            //// 遍历Xml节点。
            //foreach (var customXmlPart in xlist)
            //{

            //    // 处理自定义Xml节点。
            //    if (customXmlPart is P.CustomerData)
            //    {
            //        var xmlpart = CustomXmlBuilder.Build(
            //            customXmlPart as P.CustomerData, package, uridic);
            //        xmlpart.Application = app;
            //        xmlpart.Parent = customList;
            //        customList.Add(xmlpart);
            //    }

            //    // 处理自定义Tag节点。
            //    else if (customXmlPart is P.CustomerDataTags)
            //    {
            //        string rId = (customXmlPart as P.CustomerDataTags).Id;
            //        var t =
            //            from tagpair in sldpart.UserDefinedTagsParts
            //            where tagpair.Uri == uridic[rId]
            //            select tagpair;
            //        if (t.Count() != 0)
            //        {
            //            var taglist = t.First().TagList;
            //            if (taglist is not null)
            //            {
            //                foreach (P.Tag tag in taglist)
            //                {
            //                    container.Tags.Add(tag.Name, tag.Val);
            //                }
            //            }
            //        }
            //    }

            //}
        }


        // 载入自定义信息。
        // 自定义信息包括自定义 Xml 与自定义 Tag 。
        public static void LoadSlideMasterCustomInfo
            (Application app, CustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.SlideMasterPart sldpart, PresentationImportArgs importArgs)
        {

            //// 自定义列表。
            //var customList = container.CustomXmlList;
            //var tags = container.Tags; // 获得父对象的Tags容器并使用它，添加字段。

            //customList.Application = app;
            //customList.Parent = container;

            //if (xlist is null)
            //{
            //    return;
            //}

            //// 遍历Xml节点。
            //foreach (var customXmlPart in xlist)
            //{

            //    // 处理自定义Xml节点。
            //    if (customXmlPart is P.CustomerData)
            //    {
            //        var xmlpart = CustomXmlBuilder.Build(
            //            customXmlPart as P.CustomerData, package, uridic);
            //        xmlpart.Application = app;
            //        xmlpart.Parent = customList;
            //        customList.Add(xmlpart);
            //    }

            //    // 处理自定义Tag节点。
            //    else if (customXmlPart is P.CustomerDataTags)
            //    {
            //        string rId = (customXmlPart as P.CustomerDataTags).Id;
            //        var t =
            //            from tagpair in sldpart.UserDefinedTagsParts
            //            where tagpair.Uri == uridic[rId]
            //            select tagpair;
            //        if (t.Count() != 0)
            //        {
            //            var taglist = t.First().TagList;
            //            if (taglist is not null)
            //            {
            //                foreach (P.Tag tag in taglist)
            //                {
            //                    container.Tags.Add(tag.Name, tag.Val);
            //                }
            //            }
            //        }
            //    }

            //}
        }


        // 载入自定义信息。
        // 自定义信息包括自定义 Xml 与自定义 Tag 。
        public static void LoadCustomInfo
            (Application app, CustomContainer container, Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.OpenXmlPart sldpart, PresentationImportArgs importArgs)
        {
            if (sldpart is PK.SlidePart)
            {
                LoadSlideCustomInfo(app, container, package, xlist, uridic, sldpart as PK.SlidePart, importArgs);
            }
            else if (sldpart is PK.SlideLayoutPart)
            {
                LoadSlideLayoutCustomInfo(app, container, package, xlist, uridic, sldpart as PK.SlideLayoutPart, importArgs);
            }
        }
        #endregion



    }
}
