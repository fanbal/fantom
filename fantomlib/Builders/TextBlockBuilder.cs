﻿namespace Fantom.Builders
{
    /// <summary>
    /// 解析run。
    /// </summary>
    internal static class TextBlockBuilder
    {
       
        public static TextBlock Build(D.Run element, ColorLoadArgs loadArgs)
        {
            var tb = new TextBlock(element.Text.InnerText);

            var rpr = element.RunProperties;

            // tb.Fill.Fill = new SolidFill( ColorBuilder.GetCustomFill(rpr, loadArgs));
            

            SetFontSize(tb, rpr);

            SetFontFamily(tb, rpr);

            return tb;
        }

        public static FillFormatWrapper BuildTextBlockFillFormat(D.Run run, PresentationImportArgs importArgs)
        {
            var xRunProperties = run.RunProperties;
            if (xRunProperties is null) return null;

            var fillFormat = new FillFormatWrapper();
            var fill = new SolidFill(new SolidColor(255,0,0));
            fillFormat.Fill = fill;
            return fillFormat;
        }

        private static void SetFontSize(TextBlock tb, D.RunProperties rpr)
        {
            if (rpr.FontSize is not null)
            {
                tb.FontSize = rpr.FontSize.Value;
            }
        }

        // 设置并覆盖性地导入字体样式
        private static void SetFontFamily(TextBlock tb, D.RunProperties rpr)
        {
            SetFontFamily<D.LatinFont>(tb, rpr);
            SetFontFamily<D.EastAsianFont>(tb, rpr);
            //SetFontFamily<D.BulletFont>(tb, rpr);
            //SetFontFamily<D.SymbolFont>(tb, rpr);
            //SetFontFamily<D.ComplexScriptFont>(tb, rpr);

        }

        private static void SetFontFamily<T>(TextBlock tb, D.RunProperties rpr) where T : D.TextFontType
        {
            var font = rpr.GetFirstChild<T>();
            if (font is null)
            {
                return;
            }

            tb.FontFamily = font.Typeface;

        }

        public static TextBlock Build()
        {
            return new TextBlock("");
        }

        public static D.Run Export(TextBlock model)
        {
            throw new NotImplementedException();
        }


    }
}
