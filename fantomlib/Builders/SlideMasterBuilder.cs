﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Builders
{
    internal static class SlideMasterBuilder
    {
        #region read


        /// <summary>
        /// 导入母版。
        /// </summary>
        /// <param name="package">幻灯片的包体。</param>
        /// <param name="app">顶层对象。</param>
        /// <param name="pres">演示文稿对象。</param>
        public static void LoadSlideMasters(BuildingCallbackLoadArgs loadArgs, PresentationImportArgs importArgs)
        {
            Package package = importArgs.Package;
            Application app = importArgs.Application;
            Presentation pres = importArgs.Presentation;
            PK.PresentationPart prespart = importArgs.ArgPart as PK.PresentationPart;

            pres.SlideMasters = new SlideMasters();
            if (prespart.SlideMasterParts is not null)
            {
                foreach (var xMasterSlidePart in prespart.SlideMasterParts)
                {
                    var master = SlideMasterBuilder.LoadSlideMaster(
                        pres.SlideMasters, loadArgs,
                        new PresentationImportArgs(importArgs) { ArgPart = xMasterSlidePart });
                    if (xMasterSlidePart.SlideLayoutParts is not null)
                    {
                        foreach (var xSlideLayoutPart in xMasterSlidePart.SlideLayoutParts)
                        {
                            SlideLayoutBuilder.LoadLayout(master, loadArgs,
                                new PresentationImportArgs(importArgs) { ArgPart = xSlideLayoutPart });
                        }
                    }
                    //LoadMaster(package, app, pres, pres.SlideMasters as SlideMasters, );
                }
            }
        }


        public static SlideMaster LoadSlideMaster(SlideMasters masters, BuildingCallbackLoadArgs loadArgs, PresentationImportArgs importArgs)
        {

            var xMasterSlidePart = importArgs.ArgPart as PK.SlideMasterPart;
            var xMasterSlide = xMasterSlidePart.SlideMaster;
            var xCommonSlideData = xMasterSlide.CommonSlideData;
            var newMasterSlide = SlideMasterBuilder.Build(xMasterSlidePart);

            // 建立资源映射。
            importArgs = new PresentationImportArgs(importArgs)
            { RID2URLMapper = new PresentationImportArgs.PackRIDToURLMapper(xMasterSlidePart) };

            newMasterSlide.Theme = ThemeBuilder.BuildTheme(loadArgs,
                new PresentationImportArgs() { ArgPart = xMasterSlidePart.ThemePart });

            var newImportArgs = new PresentationImportArgs(importArgs)
            {
                ColorLoadArgs = new ColorLoadArgs(xMasterSlidePart, newMasterSlide.Theme, importArgs.Presentation.MediaManager),
                LoadBundleHandler = loadArgs.BundleHandler,
                ShapeMapper = new PresentationImportArgs.ShapesShapeIDMapper()
            };

            // 导入图形对象。
            ShapeBuilder.LoadShapes(newMasterSlide, loadArgs.ShapeHandler, new PresentationImportArgs(newImportArgs) { ArgElement = xCommonSlideData.ShapeTree });


            // 导入时间轴。
            TimeLineBuilder.LoadTimeLine(
                newMasterSlide,
                new PresentationImportArgs(newImportArgs) { ArgElement = TimeLineBuilder.GetTiming(xMasterSlide) });

            // 为幻灯片载入自定义信息。
            // PresentationLoader.LoadSlideCustomInfo(app, newSlide.CustomContainer, package, xcustomlist, uridic, xSlidePart, importArgs);

            masters.Add(newMasterSlide);
            importArgs.URL2CreatedPartMapper.TryAdd(xMasterSlidePart.Uri, newMasterSlide);


            return newMasterSlide;

        }




        internal static SlideMaster Build(PK.SlideMasterPart masterPart)
        {
            return new SlideMaster();
        }


        #endregion

        #region output

        public static PK.SlideMasterPart Export(SlideMaster model, PresentationExportArgs exportArgs)
        {
            if (exportArgs.Mapper.TryGetValue(model.GetParent<Presentation>(), out var part))
            {
                var masterPart = CreateSlideMasterPart(model, part as PK.PresentationPart, exportArgs);
                exportArgs.Mapper.TryAdd(model, masterPart);

                foreach (var layout in model.Items)
                {
                    SlideLayoutBuilder.Export(layout, exportArgs);
                }

                return masterPart;
            }
            else
            {
                throw new Exception($"在导出母版 {model.Name} 时，未找到对应父级演示文稿部件。");
            }
        }

        // 添加幻灯片母版的部分，演示文档的父级别连接。
        private static PK.SlideMasterPart CreateSlideMasterPart(SlideMaster sldMaster, PK.PresentationPart presentationPart, PresentationExportArgs exportArgs)
        {
            PK.SlideMasterPart slideMasterPart1 = presentationPart.AddNewPart<PK.SlideMasterPart>(sldMaster.GetRelationshipIDStringAsPropertyItem<Presentation>());
            P.SlideMaster slideMaster = new P.SlideMaster(
                new P.CommonSlideData(SlideBaseBuilder.CreateShapeTree<SlideMaster>(sldMaster.Shapes, exportArgs)),
                new P.ColorMap() { Background1 = D.ColorSchemeIndexValues.Light1, Text1 = D.ColorSchemeIndexValues.Dark1, Background2 = D.ColorSchemeIndexValues.Light2, Text2 = D.ColorSchemeIndexValues.Dark2, Accent1 = D.ColorSchemeIndexValues.Accent1, Accent2 = D.ColorSchemeIndexValues.Accent2, Accent3 = D.ColorSchemeIndexValues.Accent3, Accent4 = D.ColorSchemeIndexValues.Accent4, Accent5 = D.ColorSchemeIndexValues.Accent5, Accent6 = D.ColorSchemeIndexValues.Accent6, Hyperlink = D.ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = D.ColorSchemeIndexValues.FollowedHyperlink },

                CreateSlideLayoutIdList(sldMaster),

                new P.TextStyles(new P.TitleStyle(), new P.BodyStyle(), new P.OtherStyle()));

            slideMasterPart1.SlideMaster = slideMaster;

            return slideMasterPart1;
        }


        private static P.SlideLayoutIdList CreateSlideLayoutIdList(SlideMaster sldMaster)
        {
            var slideLayoutIdList = new P.SlideLayoutIdList();
            foreach (var layout in sldMaster.Items)
            {
                slideLayoutIdList.AppendExtend(new P.SlideLayoutId() { Id = layout._exportID, RelationshipId = sldMaster.GetPropertyRelationshipIDString(layout) });
            }
            return slideLayoutIdList;
        }

        #endregion




    }
}
