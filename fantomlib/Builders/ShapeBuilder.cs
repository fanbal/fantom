﻿using System.Linq;
namespace Fantom.Builders
{
    /// <summary>
    /// 图形的构建器。
    /// </summary>
    internal class ShapeBuilder
    {


        public static Shapes LoadShapes(EchoBaseObject slide, Fantom.Builders.PresentationLoader.RaiseLoadingShapeEventHandler? handler, PresentationImportArgs importArgs)
        {
            if (SlideBaseBuilder.IsSlide(slide) == false)
                throw new Exception("无法为非幻灯片对象添加图形。");

            var newShapes = new Shapes();

            var xTreeNodes = importArgs.ArgElement as P.ShapeTree;
            if (xTreeNodes is not null)
            {
                var xShapes = xTreeNodes.GetElements<P.Shape, P.Picture, P.GroupShape, P.ConnectionShape>();
                var count = xShapes.Count();
                var index = 1;
                foreach (var xshape in xShapes)
                {
                    ShapeBase newShape = null;
                    var newImportArgs = new PresentationImportArgs(importArgs) { ArgElement = xshape };
                    if (xshape is P.Shape) newShape = LoadShape(newShapes, newImportArgs);
                    else if (xshape is P.Picture) newShape = LoadPicture(newShapes, newImportArgs);
                    else if (xshape is P.GroupShape) newShape = LoadGroupShape(newShapes, newImportArgs);
                    else if (xshape is P.ConnectionShape) newShape = LoadConnection(newShapes, newImportArgs);
                    else
                    {
                        continue;
                    }
                    importArgs.ShapeMapper.Add(newShape._exportID, newShape);
                    handler?.Invoke(new RaiseLoadingShapeArguments() { Shapes = newShapes, Shape = newShape, Index = index, Count = count });
                    index++;


                }
            }

            if (slide is Slide)
                (slide as Slide).Shapes = newShapes;
            else if (slide is NoteMaster)
                (slide as NoteMaster).Shapes = newShapes;
            else if (slide is NoteSlide)
                (slide as NoteSlide).Shapes = newShapes;
            else if (slide is SlideMaster)
                (slide as SlideMaster).Shapes = newShapes;
            else if (slide is SlideLayout)
                (slide as SlideLayout).Shapes = newShapes;

            return newShapes;
        }



        public static Shape LoadShape(Shapes shapes, PresentationImportArgs importArgs)
        {
            var xShape = importArgs.ArgElement as P.Shape;
            var newShape = new Shape();

            LoadNonVisualShapeProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.NonVisualShapeProperties });

            LoadShapeProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.ShapeProperties });

            LoadShapeStyle(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.ShapeStyle });

            LoadShapeTextBody(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.TextBody });

            shapes.Add(newShape);

            return newShape;
        }

        public static Connection LoadConnection(Shapes shapes, PresentationImportArgs importArgs)
        {
            var xShape = importArgs.ArgElement as P.ConnectionShape;
            var newShape = new Connection();

            LoadNonVisualConnectionShapeProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.NonVisualConnectionShapeProperties });

            LoadShapeProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.ShapeProperties });

            LoadShapeStyle(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.ShapeStyle });

            shapes.Add(newShape);
            return newShape;
        }

        public static Group LoadGroupShape(Shapes shapes, PresentationImportArgs importArgs)
        {
            var xShape = importArgs.ArgElement as P.GroupShape;
            var newShape = new Group();

            newShape.Items = new Shapes();


            LoadNonVisualGroupShapeProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.NonVisualGroupShapeProperties });

            LoadGroupShapeProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.GroupShapeProperties });

            foreach (var xChild in xShape)
            {
                if (ShapeBase.IsShapeTypeX(xChild))
                {
                    var newImportArgs = new PresentationImportArgs(importArgs) { ArgElement = xChild };
                    if (xChild is P.Shape) LoadShape(newShape.Items, newImportArgs);
                    else if (xChild is P.ConnectionShape) LoadConnection(newShape.Items, newImportArgs);
                    else if (xChild is P.Picture) LoadPicture(newShape.Items, newImportArgs);
                    else if (xChild is P.GroupShape) LoadGroupShape(newShape.Items, newImportArgs);
                    else throw new Exception("未知图形。");
                }

            }

            shapes.Add(newShape);
            return newShape;
        }


        public static Picture LoadPicture(Shapes shapes, PresentationImportArgs importArgs)
        {
            var xShape = importArgs.ArgElement as P.Picture;
            var newShape = new Picture();

            LoadNonVisualPictureProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.NonVisualPictureProperties });

            LoadBilpFillProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.BlipFill });

            LoadShapeProperties(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.ShapeProperties });

            LoadShapeStyle(newShape, new PresentationImportArgs(importArgs) { ArgElement = xShape.ShapeStyle });

            shapes.Add(newShape);
            return newShape;
        }



        public static void LoadNonVisualShapeProperties(Shape shape, PresentationImportArgs importArgs)
        {
            var xElement = importArgs.ArgElement as P.NonVisualShapeProperties;
            LoadNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualDrawingProperties });
            LoadNonVisualShapeDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualShapeDrawingProperties });
            LoadApplicationNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.ApplicationNonVisualDrawingProperties });
        }

        public static P.NonVisualShapeProperties CreateNonVisualShapeProperties(Shape shape, PresentationExportArgs exportArgs)
        {
            return new P.NonVisualShapeProperties(
                CreateNonVisualDrawingProperties(shape, exportArgs),
                CreateNonVisualShapeDrawingProperties(shape, exportArgs),
                CreateApplicationNonVisualDrawingProperties(shape, exportArgs));
        }

        public static void LoadNonVisualConnectionShapeProperties(Connection shape, PresentationImportArgs importArgs)
        {
            var xElement = importArgs.ArgElement as P.NonVisualConnectionShapeProperties;
            LoadNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualDrawingProperties });
            LoadNonVisualConnectionShapeDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualConnectorShapeDrawingProperties });
            LoadApplicationNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.ApplicationNonVisualDrawingProperties });
        }

        public static P.NonVisualShapeProperties CreateNonVisualConnectionShapeProperties(Connection shape, PresentationExportArgs exportArgs)
        {
            return new P.NonVisualShapeProperties(
                CreateNonVisualDrawingProperties(shape, exportArgs),
                CreateNonVisualConnectionShapeDrawingProperties(shape, exportArgs),
                CreateApplicationNonVisualDrawingProperties(shape, exportArgs));
        }

        public static void LoadNonVisualPictureProperties(Picture shape, PresentationImportArgs importArgs)
        {
            var xElement = importArgs.ArgElement as P.NonVisualPictureProperties;
            LoadNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualDrawingProperties });
            LoadNonVisualPictureDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualPictureDrawingProperties });
            LoadApplicationNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.ApplicationNonVisualDrawingProperties });
        }

        public static P.NonVisualShapeProperties CreateNonVisualPictureProperties(Picture shape, PresentationExportArgs exportArgs)
        {
            return new P.NonVisualShapeProperties(
                CreateNonVisualDrawingProperties(shape, exportArgs),
                CreateNonVisualPictureDrawingProperties(shape, exportArgs),
                CreateApplicationNonVisualDrawingProperties(shape, exportArgs));
        }

        public static void LoadNonVisualGroupShapeProperties(Group shape, PresentationImportArgs importArgs)
        {
            var xElement = importArgs.ArgElement as P.NonVisualGroupShapeProperties;
            LoadNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualDrawingProperties });
            LoadNonVisualGroupShapeDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.NonVisualGroupShapeDrawingProperties });
            LoadApplicationNonVisualDrawingProperties(shape, new PresentationImportArgs(importArgs) { ArgElement = xElement.ApplicationNonVisualDrawingProperties });
        }

        public static P.NonVisualShapeProperties CreateNonVisualGroupShapeProperties(Group shape, PresentationExportArgs exportArgs)
        {
            return new P.NonVisualShapeProperties(
                CreateNonVisualDrawingProperties(shape, exportArgs),
                CreateNonVisualGroupShapeDrawingProperties(shape, exportArgs),
                CreateApplicationNonVisualDrawingProperties(shape, exportArgs));
        }

        public static void LoadNonVisualDrawingProperties(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var prop = importArgs.ArgElement as P.NonVisualDrawingProperties;
            shape.Name = prop.Name;
            shape._exportID = prop.Id;
            shape._importName = prop.Name?.Value;
            shape.BaseWrapper.HyperLinkClick = HyperLink.BuildHyperLink(new PresentationImportArgs(importArgs) { ArgElement = prop.HyperlinkOnClick });
            shape.BaseWrapper.HyperLinkHover = HyperLink.BuildHyperLink(new PresentationImportArgs(importArgs) { ArgElement = prop.HyperlinkOnHover });
            shape.BaseWrapper.Description = prop.Description?.Value;
            shape.BaseWrapper.Hidden = prop.Hidden?.Value;
        }

        public static P.NonVisualDrawingProperties CreateNonVisualDrawingProperties(ShapeBase shape, PresentationExportArgs exportArgs)
        {
            var xElement = new P.NonVisualDrawingProperties();
            xElement.Id = shape._exportID;
            xElement.Name = shape.Name;
            xElement.HyperlinkOnClick = HyperLink.CreateHyperLink<D.HyperlinkOnClick>(shape.BaseWrapper.HyperLinkClick, exportArgs);
            xElement.HyperlinkOnHover = HyperLink.CreateHyperLink<D.HyperlinkOnHover>(shape.BaseWrapper.HyperLinkHover, exportArgs);
            xElement.Description = shape.BaseWrapper.Description;
            xElement.Hidden = shape.BaseWrapper.Hidden;
            return xElement;
        }

        public static void LoadApplicationNonVisualDrawingProperties(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var prop = importArgs.ArgElement as P.ApplicationNonVisualDrawingProperties;
            shape.BaseWrapper.IsPhoto = prop.IsPhoto?.Value;
            shape.BaseWrapper.UserDrawn = prop.UserDrawn?.Value;
            shape.BaseWrapper.Media = MediaBase.LoadMediaEntry(shape, new PresentationImportArgs(importArgs) { ArgElement = prop.GetElement<D.AudioFromCD, D.WaveAudioFile, D.AudioFromFile, D.VideoFromFile, D.QuickTimeFromFile>() });
            shape.BaseWrapper.PlaceHolder = PlaceHolder.LoadPlaceHolder(shape, new PresentationImportArgs(importArgs) { ArgElement = prop.PlaceholderShape });
            // TODO 自定义数据列表
        }

        public static P.ApplicationNonVisualDrawingProperties CreateApplicationNonVisualDrawingProperties(ShapeBase shape, PresentationExportArgs exportArgs)
        {
            var xProp = new P.ApplicationNonVisualDrawingProperties();

            xProp.IsPhoto = shape.BaseWrapper.IsPhoto.ToOX();
            xProp.UserDrawn = shape.BaseWrapper.UserDrawn.ToOX();

            xProp.PlaceholderShape = PlaceHolder.CreatePlaceHolder(shape.BaseWrapper.PlaceHolder, exportArgs);

            if (shape.BaseWrapper.Media is not null) xProp.AppendExtend(MediaBase.CreateMediaEntry(shape.BaseWrapper.Media, exportArgs));

            return xProp;
        }

        public static void LoadNonVisualShapeDrawingProperties(Shape shape, PresentationImportArgs importArgs)
        {
            var prop = importArgs.ArgElement as P.NonVisualShapeDrawingProperties;
            shape.Locks = ShapeLocks.LoadShapeLock(shape, new PresentationImportArgs(importArgs) { ArgElement = prop.ShapeLocks });

            shape.Wrapper.IsTextBox = prop.TextBox?.Value;
        }

        public static P.NonVisualShapeDrawingProperties CreateNonVisualShapeDrawingProperties(Shape shape, PresentationExportArgs exportArgs)
        {
            var xElement = new P.NonVisualShapeDrawingProperties();
            xElement.ShapeLocks = ShapeLocks.CreateShapeLocks(shape, exportArgs);
            xElement.TextBox = shape.Wrapper.IsTextBox.ToOX();
            return xElement;
        }

        public static void LoadNonVisualConnectionShapeDrawingProperties(Connection shape, PresentationImportArgs importArgs)
        {
            var prop = importArgs.ArgElement as P.NonVisualConnectorShapeDrawingProperties;
            shape.Locks = ConnectionShapeLocks.LoadShapeLock(shape, new PresentationImportArgs(importArgs) { ArgElement = prop.ConnectionShapeLocks });
            shape.Wrapper.ConnectionStart = Connection.ConnectionData.BuildConnectionData(new PresentationImportArgs(importArgs) { ArgElement = prop.StartConnection });
            shape.Wrapper.ConnectionEnd = Connection.ConnectionData.BuildConnectionData(new PresentationImportArgs(importArgs) { ArgElement = prop.EndConnection });
        }

        public static P.NonVisualConnectorShapeDrawingProperties CreateNonVisualConnectionShapeDrawingProperties(Connection shape, PresentationExportArgs exportArgs)
        {
            var xElement = new P.NonVisualConnectorShapeDrawingProperties();
            xElement.ConnectionShapeLocks = ConnectionShapeLocks.CreateShapeLocks(shape, exportArgs);
            xElement.StartConnection = Connection.ConnectionData.CreateConnectionData<D.StartConnection>(shape.Wrapper.ConnectionStart, exportArgs);
            xElement.EndConnection = Connection.ConnectionData.CreateConnectionData<D.EndConnection>(shape.Wrapper.ConnectionEnd, exportArgs);
            return xElement;
        }

        public static void LoadNonVisualGroupShapeDrawingProperties(Group shape, PresentationImportArgs importArgs)
        {
            var prop = importArgs.ArgElement as P.NonVisualGroupShapeDrawingProperties;
            shape.Locks = GroupShapeLocks.LoadShapeLock(shape, new PresentationImportArgs(importArgs) { ArgElement = prop.GroupShapeLocks });

        }

        public static P.NonVisualGroupShapeDrawingProperties CreateNonVisualGroupShapeDrawingProperties(Group shape, PresentationExportArgs exportArgs)
        {
            var xElement = new P.NonVisualGroupShapeDrawingProperties();
            xElement.GroupShapeLocks = GroupShapeLocks.CreateShapeLocks(shape, exportArgs);
            return xElement;
        }

        public static void LoadNonVisualPictureDrawingProperties(Picture shape, PresentationImportArgs importArgs)
        {
            var prop = importArgs.ArgElement as P.NonVisualPictureDrawingProperties;
            shape.Locks = PictureLocks.LoadShapeLock(shape, new PresentationImportArgs(importArgs) { ArgElement = prop.PictureLocks });
        }

        public static P.NonVisualPictureDrawingProperties CreateNonVisualPictureDrawingProperties(Picture shape, PresentationExportArgs exportArgs)
        {
            var xElement = new P.NonVisualPictureDrawingProperties();
            xElement.PictureLocks = PictureLocks.CreateShapeLocks(shape, exportArgs);
            return xElement;
        }


        public static void LoadShapeProperties(Shape shape, PresentationImportArgs importArgs)
        {
            P.ShapeProperties xShapeProperties = importArgs.ArgElement as P.ShapeProperties;
            LoadTransform(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.Transform2D });
            shape.Wrapper.Geometry = GeometryBase.BuildGeometry(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.CustomGeometry, D.PresetGeometry>() });

            shape.BaseWrapper.Fill = FillFormatWrapper.BuildFillFormat(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement(FillBase._fillTypeArray) });
            shape.Wrapper.Line = LineFormat.BuildLineFormat(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement() });

            shape.BaseWrapper.EffectProperties = EffectProperties.BuildEffectProperties(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.EffectPropertiesType>() });
            Scene3D.LoadScene3D(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.Scene3DType>() });
            shape.Wrapper.Shape3D = Shape3D.BuildShape3D(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.Shape3DType>() });

        }

        public static P.ShapeProperties CreateShapeProperties(Shape shape, PresentationExportArgs exportArgs)
        {

            var xShapeProperties = new P.ShapeProperties();
            xShapeProperties.AppendExtend(CreateTransform(shape, exportArgs));
            xShapeProperties.AppendExtend(GeometryBase.CreateGeometry(shape.Wrapper.Geometry, exportArgs));
            xShapeProperties.AppendExtend(FillFormatWrapper.CreateFillFormat(shape.BaseWrapper.Fill, exportArgs));
            xShapeProperties.AppendExtend(LineFormat.CreateLineFormat(shape.Wrapper.Line, exportArgs));

            xShapeProperties.AppendExtend(EffectProperties.CreateEffectProperties(shape.BaseWrapper.EffectProperties, exportArgs));
            xShapeProperties.AppendExtend(Scene3D.CreateScene3D(shape.BaseWrapper.Scene3D, exportArgs));
            xShapeProperties.AppendExtend(Shape3D.CreateShape3D(shape.Wrapper.Shape3D, exportArgs));
            return xShapeProperties;
        }

        public static void LoadShapeProperties(Connection shape, PresentationImportArgs importArgs)
        {
            P.ShapeProperties xShapeProperties = importArgs.ArgElement as P.ShapeProperties;
            LoadTransform(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.Transform2D });
            shape.Wrapper.Geometry = GeometryBase.BuildGeometry(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.CustomGeometry, D.PresetGeometry>() });

            shape.BaseWrapper.Fill = FillFormatWrapper.BuildFillFormat(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement((FillBase._fillTypeArray)) });
            shape.Wrapper.Line = LineFormat.BuildLineFormat(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement() });

            shape.BaseWrapper.EffectProperties = EffectProperties.BuildEffectProperties(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.EffectPropertiesType>() });
            Scene3D.LoadScene3D(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.Scene3DType>() });
            shape.Wrapper.Shape3D = Shape3D.BuildShape3D(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.Shape3DType>() });

        }

        public static P.ShapeProperties CreateShapeProperties(Connection shape, PresentationExportArgs exportArgs)
        {
            var xShapeProperties = new P.ShapeProperties();
            xShapeProperties.AppendExtend(CreateTransform(shape, exportArgs));
            xShapeProperties.AppendExtend(GeometryBase.CreateGeometry(shape.Wrapper.Geometry, exportArgs));
            xShapeProperties.AppendExtend(FillFormatWrapper.CreateFillFormat(shape.BaseWrapper.Fill, exportArgs));
            xShapeProperties.AppendExtend(LineFormat.CreateLineFormat(shape.Wrapper.Line, exportArgs));

            xShapeProperties.AppendExtend(EffectProperties.CreateEffectProperties(shape.BaseWrapper.EffectProperties, exportArgs));
            xShapeProperties.AppendExtend(Scene3D.CreateScene3D(shape.BaseWrapper.Scene3D, exportArgs));
            xShapeProperties.AppendExtend(Shape3D.CreateShape3D(shape.Wrapper.Shape3D, exportArgs));
            return xShapeProperties;
        }

        public static void LoadShapeProperties(Picture shape, PresentationImportArgs importArgs)
        {
            P.ShapeProperties xShapeProperties = importArgs.ArgElement as P.ShapeProperties;
            LoadTransform(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.Transform2D });
            shape.Wrapper.Geometry = GeometryBase.BuildGeometry(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.CustomGeometry, D.PresetGeometry>() });

            shape.BaseWrapper.Fill = FillFormatWrapper.BuildFillFormat(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement((FillBase._fillTypeArray)) });
            shape.Wrapper.Line = LineFormat.BuildLineFormat(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement() });

            shape.BaseWrapper.EffectProperties = EffectProperties.BuildEffectProperties(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.EffectPropertiesType>() });
            Scene3D.LoadScene3D(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.Scene3DType>() });
            shape.Wrapper.Shape3D = Shape3D.BuildShape3D(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.Shape3DType>() });

        }

        public static P.ShapeProperties CreateShapeProperties(Picture shape, PresentationExportArgs exportArgs)
        {
            var xShapeProperties = new P.ShapeProperties();
            xShapeProperties.AppendExtend(CreateTransform(shape, exportArgs));
            xShapeProperties.AppendExtend(GeometryBase.CreateGeometry(shape.Wrapper.Geometry, exportArgs));
            xShapeProperties.AppendExtend(FillFormatWrapper.CreateFillFormat(shape.BaseWrapper.Fill, exportArgs));
            xShapeProperties.AppendExtend(LineFormat.CreateLineFormat(shape.Wrapper.Line, exportArgs));

            xShapeProperties.AppendExtend(EffectProperties.CreateEffectProperties(shape.BaseWrapper.EffectProperties, exportArgs));
            xShapeProperties.AppendExtend(Scene3D.CreateScene3D(shape.BaseWrapper.Scene3D, exportArgs));
            xShapeProperties.AppendExtend(Shape3D.CreateShape3D(shape.Wrapper.Shape3D, exportArgs));
            return xShapeProperties;
        }

        public static void LoadGroupShapeProperties(Group shape, PresentationImportArgs importArgs)
        {
            P.GroupShapeProperties xShapeProperties = importArgs.ArgElement as P.GroupShapeProperties;
            LoadTransform(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.TransformGroup });

            shape.BaseWrapper.Fill = FillFormatWrapper.BuildFillFormat(new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement((FillBase._fillTypeArray)) });
            Scene3D.LoadScene3D(shape, new PresentationImportArgs(importArgs) { ArgElement = xShapeProperties.GetElement<D.Scene3DType>() });

        }

        public static P.GroupShapeProperties CreateGroupShapeProperties(Group shape, PresentationExportArgs exportArgs)
        {
            var xGroupShapeProperties = new P.GroupShapeProperties();


            xGroupShapeProperties.AppendExtend(FillFormatWrapper.CreateFillFormat(shape.BaseWrapper.Fill, exportArgs));
            xGroupShapeProperties.AppendExtend(EffectProperties.CreateEffectProperties(shape.BaseWrapper.EffectProperties, exportArgs));
            xGroupShapeProperties.AppendExtend(Scene3D.CreateScene3D(shape.BaseWrapper.Scene3D, exportArgs));
            return xGroupShapeProperties;
        }

        public static void LoadTransform(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var xTransform = importArgs.ArgElement as D.Transform2D;
            if (xTransform is null) return;

            shape.BaseWrapper.Left = xTransform.Offset?.X?.Value;
            shape.BaseWrapper.Top = xTransform.Offset?.Y?.Value;
            shape.BaseWrapper.Width = xTransform.Extents?.Cx?.Value;
            shape.BaseWrapper.Height = xTransform.Extents?.Cy?.Value;
            shape.BaseWrapper.FlipHorizontal = xTransform.HorizontalFlip?.Value;
            shape.BaseWrapper.FlipVertical = xTransform.VerticalFlip?.Value;
            shape.BaseWrapper.Rotation = xTransform.Rotation?.Value;
        }

        public static D.Transform2D CreateTransform(ShapeBase shape, PresentationExportArgs exportArgs)
        {
            var xTransform = new D.Transform2D();

            if(shape.BaseWrapper.Left is not null && shape.BaseWrapper.Top is not null)
            {
                var xOffset = new D.Offset() { X = shape.BaseWrapper.Left.ToOX64(), Y = shape.BaseWrapper.Top.ToOX64() };
                xTransform.Offset = xOffset;
            }


            if(shape.BaseWrapper.Width is not null && shape.BaseWrapper.Height is not null)
            {
                var xExtents = new D.Extents() { Cx = shape.BaseWrapper.Width.ToOX64(), Cy = shape.BaseWrapper.Height.ToOX64() };
                xTransform.Extents = xExtents;
            }

          
            xTransform.Rotation = shape.BaseWrapper.Rotation.ToOX32();
            xTransform.VerticalFlip = shape.BaseWrapper.FlipVertical;
            xTransform.HorizontalFlip = shape.BaseWrapper.FlipHorizontal;
            return xTransform;
        }

        public static void LoadTransform(Group shape, PresentationImportArgs importArgs)
        {
            var xTransform = importArgs.ArgElement as D.TransformGroup;
            
            shape.BaseWrapper.Left = xTransform.Offset?.X?.Value;
            shape.BaseWrapper.Top = xTransform.Offset?.Y?.Value;
            shape.BaseWrapper.Width = xTransform.Extents?.Cx?.Value;
            shape.BaseWrapper.Height = xTransform.Extents?.Cy?.Value;
            if(xTransform.ChildOffset is not null)
            {
                shape.Wrapper.ChOffX = xTransform.ChildOffset.X?.Value;
                shape.Wrapper.ChOffY = xTransform.ChildOffset.Y?.Value;
                shape.Wrapper.ChExtX = xTransform.ChildExtents.Cx?.Value;
                shape.Wrapper.ChExtY = xTransform.ChildExtents.Cy?.Value;
            }
          
            shape.BaseWrapper.FlipHorizontal = xTransform.HorizontalFlip?.Value;
            shape.BaseWrapper.FlipVertical = xTransform.VerticalFlip?.Value;
            shape.BaseWrapper.Rotation = xTransform.Rotation?.Value;
        }

        public static D.TransformGroup CreateTransform(Group shape, PresentationExportArgs exportArgs)
        {
            var xTransform = new D.TransformGroup();
            var xOffset = new D.Offset() { X = shape.BaseWrapper.Left.ToOX64(), Y = shape.BaseWrapper.Top.ToOX64() };
            var xExtents = new D.Extents() { Cx = shape.BaseWrapper.Width.ToOX64(), Cy = shape.BaseWrapper.Height.ToOX64() };
            var xChildOffset = new D.ChildOffset() { X = shape.Wrapper.ChOffX.ToOX64(), Y = shape.Wrapper.ChOffY.ToOX64() };
            var xChildExtents = new D.ChildExtents() { Cx = shape.Wrapper.ChExtX.ToOX64(), Cy = shape.Wrapper.ChExtY.ToOX64() };
            xTransform.Offset = xOffset;
            xTransform.Extents = xExtents;
            xTransform.ChildOffset = xChildOffset;
            xTransform.ChildExtents = xChildExtents;
            xTransform.Rotation = shape.BaseWrapper.Rotation.ToOX32();
            xTransform.VerticalFlip = shape.BaseWrapper.FlipVertical.ToOX();
            xTransform.HorizontalFlip = shape.BaseWrapper.FlipHorizontal.ToOX();

            return xTransform;
        }

        public static void LoadBilpFillProperties(Picture shape, PresentationImportArgs importArgs)
        {
            var xBilpFill = importArgs.ArgElement as P.BlipFill;
            shape.Wrapper.PictureFill = PictureFill.BuildPictureFill(new PresentationImportArgs(importArgs) { ArgElement = xBilpFill });

        }

        public static P.BlipFill CreateBilpFillProperties(Picture shape, PresentationExportArgs exportArgs)
        {
            return PictureFill.CreateBlipFill(shape.Wrapper.PictureFill, exportArgs);
        }

        public static void LoadShapeStyle(Shape shape, PresentationImportArgs importArgs)
        {
            P.ShapeStyle xShapeStyle = importArgs.ArgElement as P.ShapeStyle;
            shape.Wrapper.Style = ShapeStyleWrapper.BuildShapeStyle(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle });
        }

        public static P.ShapeStyle CreateShapeStyle(Shape shape, PresentationExportArgs exportArgs)
        {
            return ShapeStyleWrapper.CreateShapeStyle(shape.Wrapper.Style, exportArgs);
        }

        public static void LoadShapeStyle(Connection shape, PresentationImportArgs importArgs)
        {
            P.ShapeStyle xShapeStyle = importArgs.ArgElement as P.ShapeStyle;
            shape.Wrapper.Style = ShapeStyleWrapper.BuildShapeStyle(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle });

        }

        public static P.ShapeStyle CreateShapeStyle(Connection shape, PresentationExportArgs exportArgs)
        {
            return ShapeStyleWrapper.CreateShapeStyle(shape.Wrapper.Style, exportArgs);
        }


        public static void LoadShapeStyle(Picture shape, PresentationImportArgs importArgs)
        {
            P.ShapeStyle xShapeStyle = importArgs.ArgElement as P.ShapeStyle;
            shape.Wrapper.Style = ShapeStyleWrapper.BuildShapeStyle(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle });

        }

        public static P.ShapeStyle CreateShapeStyle(Picture shape, PresentationExportArgs exportArgs)
        {
            return ShapeStyleWrapper.CreateShapeStyle(shape.Wrapper.Style, exportArgs);
        }

        public static void LoadShapeTextBody(Shape shape, PresentationImportArgs importArgs)
        {
            var xTextBody = importArgs.ArgElement as P.TextBody;
            if (xTextBody is null) return;
            TextBodyBuilder.LoadTextBodyNew(shape, new PresentationImportArgs(importArgs));

        }

        public static P.TextBody CreateShapeTextBody(Shape shape, PresentationExportArgs exportArgs)
        {
            if (shape.Wrapper.TextBody is null) return null;
            return TextBodyBuilder.CreateTextBody(shape.Wrapper.TextBody, exportArgs);
        }




        // 导出 xml。
        public static OX.OpenXmlElement Export(ShapeBase model, PresentationExportArgs exportArgs)
        {
            switch (model.Type)
            {
                case ShapeType.Shape:
                    return CreateCommonShape(model as Shape, exportArgs);

                case ShapeType.Group:
                    return CreateGroupShape(model as Group, exportArgs);

                case ShapeType.Connection:
                    return CreateConnectionShape(model as Connection, exportArgs);

                case ShapeType.Picture:
                    return CreatePicture(model as Picture, exportArgs);

                default:
                    throw new Exception("未知类型，无法解析该图形");

            }

        }




        private static P.Shape CreateCommonShape(Shape shape, PresentationExportArgs exportArgs)
        {
            var xShape = new P.Shape(
                CreateNonVisualShapeProperties(shape, exportArgs),/*new P.PlaceholderShape()*/
                CreateShapeProperties(shape, exportArgs));

            xShape.TextBody = CreateShapeTextBody(shape, exportArgs);
            xShape.ShapeStyle = CreateShapeStyle(shape, exportArgs);

            return xShape;
        }


        private static P.ConnectionShape CreateConnectionShape(Connection shape, PresentationExportArgs exportArgs)
        {

            var xConnectionShape = new P.ConnectionShape(
                CreateNonVisualConnectionShapeProperties(shape, exportArgs),
                CreateShapeProperties(shape, exportArgs));

            xConnectionShape.ShapeStyle = CreateShapeStyle(shape, exportArgs);



            return xConnectionShape;
        }

        private static P.GroupShape CreateGroupShape(Group shape, PresentationExportArgs exportArgs)
        {

            var xGroupShape = new P.GroupShape(
                CreateNonVisualGroupShapeProperties(shape, exportArgs),
                CreateGroupShapeProperties(shape, exportArgs));

            foreach (var childshape in shape.Items)
            {
                xGroupShape.AppendExtend(Export(childshape, exportArgs));
            }

            return xGroupShape;
        }


        private static P.Picture CreatePicture(Picture shape, PresentationExportArgs exportArgs)
        {

            var xPicture = new P.Picture(
                CreateNonVisualPictureProperties(shape, exportArgs),
                CreateBilpFillProperties(shape, exportArgs),
                CreateShapeProperties(shape, exportArgs));

            xPicture.ShapeStyle = CreateShapeStyle(shape, exportArgs);

            return xPicture;
        }


    }
}
