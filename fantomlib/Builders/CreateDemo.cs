﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Builders
{
    internal class CreateDemo
    {
        static void Main(string[] args)
        {
            string filepath = @"C:\Users\username\Documents\PresentationFromFilename.pptx";
            CreatePresentation(filepath);
        }

        public static void CreatePresentation(string filepath)
        {
            // Create a presentation at a specified file path. The presentation document type is pptx, by default.
            // 创建演示文稿文件路径，文件默认是 PPTX。
            PK.PresentationDocument presentationDoc = PK.PresentationDocument.Create(filepath, OX.PresentationDocumentType.Presentation);
            PK.PresentationPart presentationPart = presentationDoc.AddPresentationPart();
            presentationPart.Presentation = new P.Presentation();


            CreatePresentationParts(presentationPart);

            presentationDoc.Close();
        }

        private static void CreatePresentationParts(PK.PresentationPart presentationPart)
        {
            // 在主页的模板部分
            P.SlideMasterIdList slideMasterIdList1 = new P.SlideMasterIdList(new P.SlideMasterId() { Id = (OX.UInt32Value)2147483648U, RelationshipId = "rId1" });

            // 在主页的幻灯片部分，id 明确为 rid2
            P.SlideIdList slideIdList1 = new P.SlideIdList(new P.SlideId() { Id = (OX.UInt32Value)256U, RelationshipId = "rId2" });
            P.SlideSize slideSize1 = new P.SlideSize() { Cx = 9144000, Cy = 6858000, Type = P.SlideSizeValues.Screen4x3 };
            P.NotesSize notesSize1 = new P.NotesSize() { Cx = 6858000, Cy = 9144000 };
            P.DefaultTextStyle defaultTextStyle1 = new P.DefaultTextStyle();

           
            presentationPart.Presentation.AppendExtend(slideMasterIdList1, slideIdList1, slideSize1, notesSize1, defaultTextStyle1);

            // 幻灯片
            PK.SlidePart slidePart1;

            // 母版的子页
            PK.SlideLayoutPart slideLayoutPart1;

            // 母版
            PK.SlideMasterPart slideMasterPart1;

            // 主题
            PK.ThemePart themePart1;

            // 创建，并在演示文稿处添加记录。
            slidePart1 = CreateSlidePart(presentationPart);

            // 创建，并在幻灯片中添加引用
            slideLayoutPart1 = CreateSlideLayoutPart(slidePart1);

            // 创建，并让布局添加到列表中
            slideMasterPart1 = CreateSlideMasterPart(slideLayoutPart1);

            // 创建主题，并在母版中添加部分
            themePart1 = CreateTheme(slideMasterPart1);

            // 为布局添加引用
            slideMasterPart1.AddPart(slideLayoutPart1, "rId1");

            // 在整个文档中添加引用
            presentationPart.AddPart(slideMasterPart1, "rId1");

            // 为主题添加引用
            presentationPart.AddPart(themePart1, "rId5");
        }

        private static P.ShapeTree CreateShapeTree(params OX.OpenXmlElement[] openXmlElements)
        {
            var shapeTree = new P.ShapeTree(
                            new P.NonVisualGroupShapeProperties(
                                new P.NonVisualDrawingProperties() { Id = (OX.UInt32Value)1U, Name = "" },
                                new P.NonVisualGroupShapeDrawingProperties(),
                                new P.ApplicationNonVisualDrawingProperties()),
                            new P.GroupShapeProperties(new D.TransformGroup()));
            shapeTree.AppendExtend(openXmlElements);
            return shapeTree;
        }



        private static PK.SlidePart CreateSlidePart(PK.PresentationPart presentationPart)
        {
            // 第二个可引用对象的 id 叫做 rid2
            PK.SlidePart slidePart1 = presentationPart.AddNewPart<PK.SlidePart>("rId2");
            slidePart1.Slide = new P.Slide(
                    new P.CommonSlideData(CreateShapeTree()),
                    new P.ColorMapOverride(new D.MasterColorMapping()));
            return slidePart1;
        }

        // 先向演示文稿中，将母版底层的东西绑定上
        private static PK.SlideLayoutPart CreateSlideLayoutPart(PK.SlidePart slidePart1)
        {
            PK.SlideLayoutPart slideLayoutPart1 = slidePart1.AddNewPart<PK.SlideLayoutPart>("rId1");
            P.SlideLayout slideLayout = new P.SlideLayout(
            new P.CommonSlideData(new P.ShapeTree(
                new P.NonVisualGroupShapeProperties(
                new P.NonVisualDrawingProperties() { Id = (OX.UInt32Value)1U, Name = "" },
                new P.NonVisualGroupShapeDrawingProperties(),
                new P.ApplicationNonVisualDrawingProperties()),
                new P.GroupShapeProperties(new D.TransformGroup()),
                new P.Shape(
                new P.NonVisualShapeProperties(
                new P.NonVisualDrawingProperties() { Id = (OX.UInt32Value)2U, Name = "" },
                new P.NonVisualShapeDrawingProperties(new D.ShapeLocks() { NoGrouping = true }),
                new P.ApplicationNonVisualDrawingProperties(new P.PlaceholderShape())),
                new P.ShapeProperties(),
                new P.TextBody(
                new D.BodyProperties(),
                new D.ListStyle(),
                new D.Paragraph(new D.EndParagraphRunProperties()))))),
            new P.ColorMapOverride(new D.MasterColorMapping()));
            slideLayoutPart1.SlideLayout = slideLayout;
            return slideLayoutPart1;
        }

        // 添加幻灯片母版的部分，演示文档的父级别连接。
        private static PK.SlideMasterPart CreateSlideMasterPart(PK.SlideLayoutPart slideLayoutPart1)
        {
            PK.SlideMasterPart slideMasterPart1 = slideLayoutPart1.AddNewPart<PK.SlideMasterPart>("rId1");
            P.SlideMaster slideMaster = new P.SlideMaster(
            new P.CommonSlideData(new P.ShapeTree(
                new P.NonVisualGroupShapeProperties(
                new P.NonVisualDrawingProperties() { Id = (OX.UInt32Value)1U, Name = "" },
                new P.NonVisualGroupShapeDrawingProperties(),
                new P.ApplicationNonVisualDrawingProperties()),
                new P.GroupShapeProperties(new D.TransformGroup()),
                new P.Shape(
                new P.NonVisualShapeProperties(
                new P.NonVisualDrawingProperties() { Id = (OX.UInt32Value)2U, Name = "Title Placeholder 1" },
                new P.NonVisualShapeDrawingProperties(new D.ShapeLocks() { NoGrouping = true }),
                new P.ApplicationNonVisualDrawingProperties(new P.PlaceholderShape() { Type = P.PlaceholderValues.Title })),
                new P.ShapeProperties(),
                new P.TextBody(
                new D.BodyProperties(),
                new D.ListStyle(),
                new D.Paragraph())))),
            new P.ColorMap() { Background1 = D.ColorSchemeIndexValues.Light1, Text1 = D.ColorSchemeIndexValues.Dark1, Background2 = D.ColorSchemeIndexValues.Light2, Text2 = D.ColorSchemeIndexValues.Dark2, Accent1 = D.ColorSchemeIndexValues.Accent1, Accent2 = D.ColorSchemeIndexValues.Accent2, Accent3 = D.ColorSchemeIndexValues.Accent3, Accent4 = D.ColorSchemeIndexValues.Accent4, Accent5 = D.ColorSchemeIndexValues.Accent5, Accent6 = D.ColorSchemeIndexValues.Accent6, Hyperlink = D.ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = D.ColorSchemeIndexValues.FollowedHyperlink },
            new P.SlideLayoutIdList(new P.SlideLayoutId() { Id = (OX.UInt32Value)2147483649U, RelationshipId = "rId1" }),
            new P.TextStyles(new P.TitleStyle(), new P.BodyStyle(), new P.OtherStyle()));
            slideMasterPart1.SlideMaster = slideMaster;

            return slideMasterPart1;
        }


        //在母版页绑定主题。
        private static PK.ThemePart CreateTheme(PK.SlideMasterPart slideMasterPart1)
        {
            PK.ThemePart themePart1 = slideMasterPart1.AddNewPart<PK.ThemePart>("rId5");
            D.Theme theme1 = new D.Theme() { Name = "Office Theme" };

            D.ThemeElements themeElements1 = new D.ThemeElements(
            new D.ColorScheme(
                new D.Dark1Color(new D.SystemColor() { Val = D.SystemColorValues.WindowText, LastColor = "000000" }),
                new D.Light1Color(new D.SystemColor() { Val = D.SystemColorValues.Window, LastColor = "FFFFFF" }),
                new D.Dark2Color(new D.RgbColorModelHex() { Val = "1F497D" }),
                new D.Light2Color(new D.RgbColorModelHex() { Val = "EEECE1" }),
                new D.Accent1Color(new D.RgbColorModelHex() { Val = "4F81BD" }),
                new D.Accent2Color(new D.RgbColorModelHex() { Val = "C0504D" }),
                new D.Accent3Color(new D.RgbColorModelHex() { Val = "9BBB59" }),
                new D.Accent4Color(new D.RgbColorModelHex() { Val = "8064A2" }),
                new D.Accent5Color(new D.RgbColorModelHex() { Val = "4BACC6" }),
                new D.Accent6Color(new D.RgbColorModelHex() { Val = "F79646" }),
                new D.Hyperlink(new D.RgbColorModelHex() { Val = "0000FF" }),
                new D.FollowedHyperlinkColor(new D.RgbColorModelHex() { Val = "800080" }))
            { Name = "Office" },
                new D.FontScheme(
                new D.MajorFont(
                new D.LatinFont() { Typeface = "Calibri" },
                new D.EastAsianFont() { Typeface = "" },
                new D.ComplexScriptFont() { Typeface = "" }),
                new D.MinorFont(
                new D.LatinFont() { Typeface = "Calibri" },
                new D.EastAsianFont() { Typeface = "" },
                new D.ComplexScriptFont() { Typeface = "" }))
                { Name = "Office" },
                new D.FormatScheme(
                new D.FillStyleList(
                new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
                new D.GradientFill(
                new D.GradientStopList(
                new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 50000 },
                    new D.SaturationModulation() { Val = 300000 })
                { Val = D.SchemeColorValues.PhColor })
                { Position = 0 },
                new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 37000 },
                new D.SaturationModulation() { Val = 300000 })
                { Val = D.SchemeColorValues.PhColor })
                { Position = 35000 },
                new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 15000 },
                new D.SaturationModulation() { Val = 350000 })
                { Val = D.SchemeColorValues.PhColor })
                { Position = 100000 }
                ),
                new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
                new D.NoFill(),
                new D.PatternFill(),
                new D.GroupFill()),
                new D.LineStyleList(
                new D.Outline(
                new D.SolidFill(
                new D.SchemeColor(
                    new D.Shade() { Val = 95000 },
                    new D.SaturationModulation() { Val = 105000 })
                { Val = D.SchemeColorValues.PhColor }),
                new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
                {
                    Width = 9525,
                    CapType = D.LineCapValues.Flat,
                    CompoundLineType = D.CompoundLineValues.Single,
                    Alignment = D.PenAlignmentValues.Center
                },
                new D.Outline(
                new D.SolidFill(
                new D.SchemeColor(
                    new D.Shade() { Val = 95000 },
                    new D.SaturationModulation() { Val = 105000 })
                { Val = D.SchemeColorValues.PhColor }),
                new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
                {
                    Width = 9525,
                    CapType = D.LineCapValues.Flat,
                    CompoundLineType = D.CompoundLineValues.Single,
                    Alignment = D.PenAlignmentValues.Center
                },
                new D.Outline(
                new D.SolidFill(
                new D.SchemeColor(
                    new D.Shade() { Val = 95000 },
                    new D.SaturationModulation() { Val = 105000 })
                { Val = D.SchemeColorValues.PhColor }),
                new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
                {
                    Width = 9525,
                    CapType = D.LineCapValues.Flat,
                    CompoundLineType = D.CompoundLineValues.Single,
                    Alignment = D.PenAlignmentValues.Center
                }),
                new D.EffectStyleList(
                new D.EffectStyle(
                new D.EffectList(
                new D.OuterShadow(
                    new D.RgbColorModelHex(
                    new D.Alpha() { Val = 38000 })
                    { Val = "000000" })
                { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
                new D.EffectStyle(
                new D.EffectList(
                new D.OuterShadow(
                    new D.RgbColorModelHex(
                    new D.Alpha() { Val = 38000 })
                    { Val = "000000" })
                { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
                new D.EffectStyle(
                new D.EffectList(
                new D.OuterShadow(
                    new D.RgbColorModelHex(
                    new D.Alpha() { Val = 38000 })
                    { Val = "000000" })
                { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false }))),
                new D.BackgroundFillStyleList(
                new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
                new D.GradientFill(
                new D.GradientStopList(
                new D.GradientStop(
                    new D.SchemeColor(new D.Tint() { Val = 50000 },
                    new D.SaturationModulation() { Val = 300000 })
                    { Val = D.SchemeColorValues.PhColor })
                { Position = 0 },
                new D.GradientStop(
                    new D.SchemeColor(new D.Tint() { Val = 50000 },
                    new D.SaturationModulation() { Val = 300000 })
                    { Val = D.SchemeColorValues.PhColor })
                { Position = 0 },
                new D.GradientStop(
                    new D.SchemeColor(new D.Tint() { Val = 50000 },
                    new D.SaturationModulation() { Val = 300000 })
                    { Val = D.SchemeColorValues.PhColor })
                { Position = 0 }),
                new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
                new D.GradientFill(
                new D.GradientStopList(
                new D.GradientStop(
                    new D.SchemeColor(new D.Tint() { Val = 50000 },
                    new D.SaturationModulation() { Val = 300000 })
                    { Val = D.SchemeColorValues.PhColor })
                { Position = 0 },
                new D.GradientStop(
                    new D.SchemeColor(new D.Tint() { Val = 50000 },
                    new D.SaturationModulation() { Val = 300000 })
                    { Val = D.SchemeColorValues.PhColor })
                { Position = 0 }),
                new D.LinearGradientFill() { Angle = 16200000, Scaled = true })))
                { Name = "Office" });

            theme1.AppendExtend(themeElements1);
            theme1.AppendExtend(new D.ObjectDefaults());
            theme1.AppendExtend(new D.ExtraColorSchemeList());

            themePart1.Theme = theme1;
            return themePart1;

        }
    }
}
