﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Builders
{
    internal static class TextBodyBuilder
    {
        #region read
      
       
        public static TextBody LoadTextBodyNew(Shape shape, PresentationImportArgs importArgs)
        {
            var xtextbody = importArgs.ArgElement as P.TextBody;
            //ColorLoadArgs loadArgs = importArgs.ColorLoadArgs;

            var newTextBody = new TextBody();

            newTextBody._pTextBody = xtextbody.CloneNode(true) as P.TextBody;
       

            if (xtextbody is null)
            {
                shape.Wrapper.TextBody = newTextBody;
                return newTextBody;
            }
            var xbodyprop = xtextbody.BodyProperties;

            if (xbodyprop.Wrap is not null)
                newTextBody.TextWrap = xbodyprop.Wrap.Value == DocumentFormat.OpenXml.Drawing.TextWrappingValues.Square;

            // list Style 暂时没有。

            foreach (var xpara in xtextbody.Elements<D.Paragraph>())
            {
                var textpara = new TextParagraph();
                foreach (var xblock in xpara.GetElements<D.Run, D.Break, D.Field>())
                {
                    var block = new TextBlock();
                    if (xblock is D.Run)
                    {
                        var xrun = xblock as D.Run;
                        block.Type = TextBlock.TextBlockType.Run;
                        block.Text = xrun.Text.Text;
                        // TODO 语言相关。
                    }
                    else if (xblock is D.Break)
                    {
                        var xbreak = xblock as D.Break;
                        block.Type = TextBlock.TextBlockType.Break;

                    }
                    else if (xblock is D.Field)
                    {
                        var xfield = xblock as D.Field;
                        block.Type = TextBlock.TextBlockType.Feild;
                    }
                    textpara.Add(block);
                }
                newTextBody.Add(textpara);
            }



            shape.Wrapper.TextBody = newTextBody;
            return newTextBody;
        }



        public static P.TextBody CreateTextBody(TextBody textBody, PresentationExportArgs exportArgs)
        {
            var xtextBody = new P.TextBody();
            var xbodyProp = new D.BodyProperties();
            var listStyle = new D.ListStyle();

            if (textBody.TextWrap is not null)
                xbodyProp.Wrap = (D.TextWrappingValues)textBody.TextWrap.ToInt();

            //xtextBody.BodyProperties = textBody._pTextBody.BodyProperties.Clone() as D.BodyProperties;
            //xtextBody.ListStyle = textBody._pTextBody.ListStyle.Clone() as D.ListStyle;

            xtextBody.BodyProperties = xbodyProp;
            xtextBody.ListStyle = listStyle;

            foreach (var para in textBody)
            {
                var xpara = new D.Paragraph();
                var xparaProp = new D.ParagraphProperties();

                xpara.ParagraphProperties = xparaProp;

                foreach (var block in para)
                {


                    // TODO 语言的读取。


                    switch (block.Type)
                    {
                        case TextBlock.TextBlockType.Run:
                            {
                                var xrunProp = new D.RunProperties();
                                var xtext = new D.Text();
                                xtext.Text = block.Text;
                                var xrun = new D.Run();
                                xrun.RunProperties = xrunProp;
                                xrun.Text = xtext;
                                xpara.AppendExtend(xrun);
                            }

                            break;
                        case TextBlock.TextBlockType.Break:
                            {
                                var xrunProp = new D.RunProperties();
                                var xbreak = new D.Break();
                                xbreak.RunProperties = xrunProp;
                                xpara.AppendExtend(xbreak);
                            }

                            break;
                        case TextBlock.TextBlockType.Feild:
                            {

                            }
                            break;
                        default:
                            break;
                    }




                }
                xtextBody.AppendExtend(xpara);
            }

            return xtextBody;
        }
        #endregion

    }
}
