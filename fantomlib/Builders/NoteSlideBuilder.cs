﻿
namespace Fantom.Builders
{
    internal static class NoteSlideBuilder
    {
        #region read

        public static NoteSlide LoadNoteSlide(Slide slide, BuildingCallbackLoadArgs loadArgs, PresentationImportArgs importArgs)
        {
            var xNoteSlidePart = importArgs.ArgPart as PK.NotesSlidePart;
            if (xNoteSlidePart is null) return null;


            var newNoteSlide = NoteSlideBuilder.BuildNoteSlide(xNoteSlidePart);
            slide.NoteSlide = newNoteSlide;
            newNoteSlide.ParentNoteMaster = importArgs.Presentation.NoteMaster;

            importArgs.URL2CreatedPartMapper.TryAdd(xNoteSlidePart.Uri, newNoteSlide);

            var newImportArgs = new PresentationImportArgs(importArgs)
            {
                ColorLoadArgs = new ColorLoadArgs(xNoteSlidePart, importArgs.Presentation.NoteMaster.Theme, importArgs.Presentation.MediaManager),
                LoadBundleHandler = loadArgs.BundleHandler,
                ShapeMapper = new PresentationImportArgs.ShapesShapeIDMapper()
            };

            // 导入图形对象。
            ShapeBuilder.LoadShapes(newNoteSlide,
                loadArgs.ShapeHandler,
                new PresentationImportArgs(newImportArgs) { ArgElement = xNoteSlidePart.NotesSlide.CommonSlideData.ShapeTree, });

            return newNoteSlide;
        }


        public static NoteSlide BuildNoteSlide(PK.NotesSlidePart element)
        {
            return new NoteSlide();
        }

        #endregion

        #region output

        public static PK.NotesSlidePart Export(NoteSlide model, PresentationExportArgs exportArgs)
        {
            if (exportArgs.Mapper.TryGetValue(model.GetParent<Presentation>(), out var part))
            {
                var nodeSlidePart = CreateNoteSlidePart(model, part as PK.PresentationPart, exportArgs);
                exportArgs.Mapper.TryAdd(model, nodeSlidePart);
                return nodeSlidePart;
            }
            else
            {
                throw new Exception($"在导出幻灯片 {model.Index} 时，未找到对应父级演示文稿部件。");
            }
        }


        private static PK.NotesSlidePart CreateNoteSlidePart(NoteSlide noteSlide, PK.PresentationPart presentationPart, PresentationExportArgs exportArgs)
        {
            PK.NotesSlidePart noteSlidePart = presentationPart.AddNewPart<PK.NotesSlidePart>(noteSlide.GetRelationshipIDStringAsPropertyItem());
            noteSlidePart.NotesSlide = new P.NotesSlide(
                    new P.CommonSlideData(SlideBaseBuilder.CreateShapeTree<NoteSlide>(noteSlide.Shapes, exportArgs)),
                    new P.ColorMapOverride(new D.MasterColorMapping()));
            return noteSlidePart;
        }


        #endregion





    }
}
