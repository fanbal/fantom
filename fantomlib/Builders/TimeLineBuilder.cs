﻿using System.Runtime.Intrinsics.X86;

namespace Fantom.Builders
{
    /// <summary>
    /// 时间轴与动画导出模块。
    /// </summary>
    internal static class TimeLineBuilder
    {

        // 提取时间节点。
        public static P.Timing GetTiming(P.CommonSlideData slide)
        {
            var timing = (P.Timing)null;
            // Console.WriteLine(sld.Slide.OuterXml);
            var alterContent = slide.GetFirstChild<OX.AlternateContent>();

            if (alterContent is not null)
            {
                var choice = alterContent.GetFirstChild<OX.AlternateContentChoice>();
                if (choice is not null)
                {
                    timing = choice.GetFirstChild<P.Timing>();
                }
            }
            else
            {
                timing = slide.GetFirstChild<P.Timing>();
            }
            return timing;
        }

        // 导入时间轴动画数据。
        public static TimeLine LoadTimeLine(IHasTimeLine slide, PresentationImportArgs importArg)
        {
            P.Timing timing = importArg.ArgElement as P.Timing;
            var timeLine = TimeLineBuilder.Build(slide, timing, importArg);

            slide.TimeLine = timeLine;
            return timeLine;
        }

        // 提取时间节点。
        public static P.Timing GetTiming(OX.OpenXmlElement slide)
        {
            var timing = (P.Timing)null;
            // Console.WriteLine(sld.Slide.OuterXml);
            var alterContent = slide.GetFirstChild<OX.AlternateContent>();

            if (alterContent is not null)
            {
                var choice = alterContent.GetFirstChild<OX.AlternateContentChoice>();
                if (choice is not null)
                {
                    timing = choice.GetFirstChild<P.Timing>();
                }
            }
            else
            {
                timing = slide.GetFirstChild<P.Timing>();
            }
            return timing;
        }


        #region new build methods

        /// <summary>
        /// 建立时间轴对象。
        /// </summary>
        public static TimeLine Build(IHasTimeLine timeLineContainer, P.Timing timing, PresentationImportArgs importArg)
        {
            var timeLine = new TimeLine();

            if (timing is null)
            {
                return timeLine;
            }

            var xTimeLine = timing.TimeNodeList;
            var par = xTimeLine.GetFirstChild<P.ParallelTimeNode>();
            var ctn = par.GetFirstChild<P.CommonTimeNode>();

            TimeNodeBase.LoadTimeNodeBase(timeLine, new PresentationImportArgs(importArg) { ArgElement = ctn });

            // 载入所有序列。
            if (ctn.ChildTimeNodeList is not null)
            {
                foreach (var xseq in ctn.ChildTimeNodeList)
                {
                    AnimationSequence.LoadSequence(timeLine, xseq as P.SequenceTimeNode, importArg);
                }
            }

            return timeLine;
        }


       

        // 导入行为。
      
        #endregion new build methods

        internal static void CreateTimeNodeId(TimeNodeBase timeNodeBase)
        {
            var counter = 0U;
            CreateTimeNodeID(timeNodeBase, ref counter);
        }

        internal static void CreateTimeNodeID(TimeNodeBase timeNodeBase, ref uint counter )
        {
            counter++;
            timeNodeBase._exportID = counter;

            foreach (var child in timeNodeBase.GetChildren())
            {
                CreateTimeNodeID(child, ref counter);
            }
            
        }

    }
}
