﻿
namespace Fantom.Builders
{

    /// <summary>
    /// 自定义Xml内容生成器。
    /// </summary>
    internal static class CustomXmlBuilder
    {

        public static CustomXml Build(P.CustomerData element, IOPK.Package pack, Dictionary<string, Uri> dic)
        {
            var newCustomXml = new CustomXml();
            var uri = dic[element.Id];

            int len = 500000; // 默认一个包体的大小不会超过该数值。
            var buffer = new byte[len]; // 实例化一个缓存区。

            int counter = 0; // 字节计数器。
            int temp; // 用于储存每次返回时的状态。

            using (var stream = pack.GetPart(uri).GetStream())
            {
                // 使用逐字节读取，
                while (counter < len)
                {
                    temp = stream.ReadByte();
                    if (temp == -1)
                    {
                        break;
                    }
                    else
                    {
                        buffer[counter] = (byte)temp;
                    }

                    counter++;
                }
            }

            if (counter != 0)
            {
                // 由于XDocument只支持读入stream，不支持读入buffer，所以这里实例化了一个MemoryStream实例。
                var memoryStream = new IO.MemoryStream(buffer, 0, counter);
                newCustomXml.XDocument = XLINQ.XDocument.Load(memoryStream);
                memoryStream.Dispose();
            }
            else
            {
                // 为空自定义xml加入Root根节点。
                newCustomXml.XDocument = new XLINQ.XDocument(new XLINQ.XElement("Root"));
            }

            return newCustomXml;
        }

        // 建立空的元素。
        public static CustomXml Build(IOPK.Package pack)
        {
            var xdoc = new XLINQ.XDocument();
            xdoc.AddFirst("Root");
            return new CustomXml() { XDocument = xdoc };
        }


        //public static CustomData Export(CustomXml model)
        //{
        //	throw new NotImplementedException();
        //}


    }
}
