﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Builders
{
    /// <summary>
    /// OpenXml 部件映射器，用于在本地数据中暂存数据。
    /// </summary>
    internal class OpenXmlPartMapper : IReadOnlyDictionary<EchoBaseObject, PK.OpenXmlPart>
    {
        private Dictionary<EchoBaseObject, PK.OpenXmlPart> _map = new Dictionary<EchoBaseObject, DocumentFormat.OpenXml.Packaging.OpenXmlPart>();

        public IEnumerable<EchoBaseObject> Keys => ((IReadOnlyDictionary<EchoBaseObject, OpenXmlPart>)_map).Keys;

        public IEnumerable<OpenXmlPart> Values => ((IReadOnlyDictionary<EchoBaseObject, OpenXmlPart>)_map).Values;

        public int Count => ((IReadOnlyCollection<KeyValuePair<EchoBaseObject, OpenXmlPart>>)_map).Count;

        public OpenXmlPart this[EchoBaseObject key] => ((IReadOnlyDictionary<EchoBaseObject, OpenXmlPart>)_map)[key];

        public OpenXmlPartMapper()
        {
        }

        public bool TryAdd(EchoBaseObject obj, PK.OpenXmlPart part)
        {
            if (_map.ContainsKey(obj))  return false;
            _map.Add(obj, part);
            return true;
        }

        public bool ContainsKey(EchoBaseObject key)
        {
            return ((IReadOnlyDictionary<EchoBaseObject, OpenXmlPart>)_map).ContainsKey(key);
        }

        public bool TryGetValue(EchoBaseObject key, [MaybeNullWhen(false)] out OpenXmlPart value)
        {
            value = null;
            if (key is null)
                return false;
            return ((IReadOnlyDictionary<EchoBaseObject, OpenXmlPart>)_map).TryGetValue(key, out value);
        }

        public IEnumerator<KeyValuePair<EchoBaseObject, OpenXmlPart>> GetEnumerator()
        {
            return ((IEnumerable<KeyValuePair<EchoBaseObject, OpenXmlPart>>)_map).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_map).GetEnumerator();
        }
    }
}
