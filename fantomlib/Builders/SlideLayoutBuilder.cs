﻿
using DocumentFormat.OpenXml.Packaging;
using System.IO.Packaging;
using System.Linq;

namespace Fantom.Builders
{
    internal static class SlideLayoutBuilder
    {
        #region read


        /// <summary>
        /// 导入幻灯片。
        /// </summary>
        /// <param name="package">幻灯片的包体。</param>
        /// <param name="app">顶层对象。</param>
        /// <param name="pres">演示文稿对象。</param>
        /// <param name="master">演示文稿。</param>
        /// <param name="lopart">当前单页幻灯片的包体信息。</param>
        public static SlideLayout LoadLayout(SlideMaster master, BuildingCallbackLoadArgs loadArgs, PresentationImportArgs importArgs)
        {
            PK.SlideLayoutPart xLayoutPart = importArgs.ArgPart as PK.SlideLayoutPart;

            var xSlideLayout = xLayoutPart.SlideLayout;
            var xCommonSlideData = xSlideLayout.CommonSlideData;

            var newLayout = BuildLayout(xLayoutPart);
            var shapes = newLayout.Shapes;

            newLayout.Parent = master;

            importArgs = new PresentationImportArgs(importArgs)
            { RID2URLMapper = new PresentationImportArgs.PackRIDToURLMapper(xLayoutPart) };

            var newImportArgs = new PresentationImportArgs(importArgs)
            {
                ColorLoadArgs = new ColorLoadArgs(xLayoutPart, master.Theme, importArgs.Presentation.MediaManager),
                LoadBundleHandler = loadArgs.BundleHandler,
                ShapeMapper = new PresentationImportArgs.ShapesShapeIDMapper()
            };


            // 导入图形对象。
            ShapeBuilder.LoadShapes(newLayout,
                loadArgs.ShapeHandler,
                new PresentationImportArgs(newImportArgs) { ArgElement = xCommonSlideData.ShapeTree });


            // 导入时间轴。
            TimeLineBuilder.LoadTimeLine(
                newLayout,
                new PresentationImportArgs(newImportArgs) { ArgElement = TimeLineBuilder.GetTiming(xSlideLayout) });

            // 为幻灯片载入自定义信息。
            //PresentationLoader.LoadSlideLayoutCustomInfo(app, newLayout.CustomContainer, package, xcustomlist, uridic, xLayoutPart, importArgs);

            master.Add(newLayout);
            importArgs.URL2CreatedPartMapper.TryAdd(xLayoutPart.Uri, newLayout);
            return newLayout;
        }

        /// <summary>
        /// 导入幻灯片布局。
        /// </summary>
        public static SlideLayout BuildLayout(PK.SlideLayoutPart element)
        {
            var uri = element.Uri.OriginalString;
            var sldid = SlideBaseBuilder.GetSlideId(uri);

            return new SlideLayout() { };
        }



        #endregion

        #region output
        public static PK.SlideLayoutPart Export(SlideLayout model, PresentationExportArgs exportArgs)
        {
            if (exportArgs.Mapper.TryGetValue(model.GetParent<SlideMaster>(), out var part))
            {
                var slideLayoutPart = CreateSlideLayoutPart(model, part as PK.SlideMasterPart, exportArgs);
                exportArgs.Mapper.TryAdd(model, slideLayoutPart);
                return slideLayoutPart;
            }
            else
            {
                throw new Exception($"在导出布局页 {model.Index} 时，未找到对应父级母版部件。");
            }
        }

        /// <summary>
        /// 创建布局部分文件。
        /// </summary>
        /// <param name="sldLayout"></param>
        /// <param name="masterPart"></param>
        /// <returns></returns>
        private static PK.SlideLayoutPart CreateSlideLayoutPart(SlideLayout sldLayout, PK.SlideMasterPart masterPart, PresentationExportArgs exportArgs)
        {
            PK.SlideLayoutPart slideLayoutPart1 = masterPart.AddNewPart<PK.SlideLayoutPart>(sldLayout.GetRelationshipIDStringAsPropertyItem());
            P.SlideLayout slideLayout = new P.SlideLayout(
                    new P.CommonSlideData(SlideBaseBuilder.CreateShapeTree<SlideLayout>(sldLayout.Shapes, exportArgs)),
                    new P.ColorMapOverride(new D.MasterColorMapping()));
            new P.ColorMapOverride(new D.MasterColorMapping());
            slideLayoutPart1.SlideLayout = slideLayout;
            return slideLayoutPart1;
        }
        #endregion

    }
}
