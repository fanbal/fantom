﻿using DocumentFormat.OpenXml.Packaging;
using Fantom.Helper;
using System.Xml.Linq;

namespace Fantom.Builders
{
    internal static class ThemeBuilder
    {

        #region read

        /// <summary>
        /// 导入主题。
        /// </summary>
        /// <param name="themePart">主题部件。</param>
        /// <param name="loadArgs">构建的回调句柄参数。</param>
        /// <param name="importArgs">导入参数。</param>
        /// <returns></returns>
        public static Theme BuildTheme(BuildingCallbackLoadArgs loadArgs, PresentationImportArgs importArgs)
        {
            var xThemePart = importArgs.ArgPart as PK.ThemePart;
            
            if (xThemePart is null) return null;

            var xtheme = xThemePart.Theme;
            var theme = new Theme();

            importArgs.URL2CreatedPartMapper.TryAdd(xThemePart.Uri, theme);

            theme.ColorScheme = ColorScheme.LoadColorScheme(xtheme.ThemeElements.ColorScheme);
            theme.FontScheme = null; // TODO 字体的导入。
            var colorArgs = new ColorLoadArgs(null, theme, null);
            theme.FormatScheme = FormatScheme.BuildShapeStyle(new PresentationImportArgs(importArgs) { ArgElement = xtheme.ThemeElements.FormatScheme ,ColorLoadArgs = colorArgs});
            return theme;
        }

    


        #endregion



        #region write

        public static PK.ThemePart Export(Theme model, PresentationExportArgs exportArgs)
        {
            if (exportArgs.Mapper.TryGetValue(model.GetMasterParent(), out var part))
            {
                var themePart = CreateThemePart(model, part,exportArgs);
                exportArgs.Mapper.TryAdd(model, themePart);
                return themePart;
            }
            else
            {
                throw new Exception($"在导出主题 {model.Name} 时，未找到对应父级母版部件。");
            }
        }


        // 导出 id
        //private static PK.ThemePart CreateThemePart(Theme theme, PK.OpenXmlPart masterSlidePart)
        //{
        //    PK.ThemePart themePart1 = masterSlidePart.AddNewPart<PK.ThemePart>(theme.GetMasterParent().GetPropertyRelationshipIDString(theme));
        //    D.Theme theme1 = new D.Theme() { Name = theme.Name };
        //    D.ThemeElements themeElements1 = new D.ThemeElements(
        //    ColorScheme.CreateColorScheme(theme.ColorScheme),
        //        new D.FontScheme(
        //        new D.MajorFont(
        //        new D.LatinFont() { Typeface = "Calibri" },
        //        new D.EastAsianFont() { Typeface = "" },
        //        new D.ComplexScriptFont() { Typeface = "" }),
        //        new D.MinorFont(
        //        new D.LatinFont() { Typeface = "Calibri" },
        //        new D.EastAsianFont() { Typeface = "" },
        //        new D.ComplexScriptFont() { Typeface = "" }))
        //        { Name = "Office" },
        //        new D.FormatScheme(
        //        new D.FillStyleList(
        //        new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
        //        new D.GradientFill(
        //        new D.GradientStopList(
        //        new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 50000 },
        //            new D.SaturationModulation() { Val = 300000 })
        //        { Val = D.SchemeColorValues.PhColor })
        //        { Position = 0 },
        //        new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 37000 },
        //        new D.SaturationModulation() { Val = 300000 })
        //        { Val = D.SchemeColorValues.PhColor })
        //        { Position = 35000 },
        //        new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 15000 },
        //        new D.SaturationModulation() { Val = 350000 })
        //        { Val = D.SchemeColorValues.PhColor })
        //        { Position = 100000 }
        //        ),
        //        new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
        //        new D.NoFill(),
        //        new D.PatternFill(),
        //        new D.GroupFill()),
        //        new D.LineStyleList(
        //        new D.Outline(
        //        new D.SolidFill(
        //        new D.SchemeColor(
        //            new D.Shade() { Val = 95000 },
        //            new D.SaturationModulation() { Val = 105000 })
        //        { Val = D.SchemeColorValues.PhColor }),
        //        new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
        //        {
        //            Width = 9525,
        //            CapType = D.LineCapValues.Flat,
        //            CompoundLineType = D.CompoundLineValues.Single,
        //            Alignment = D.PenAlignmentValues.Center
        //        },
        //        new D.Outline(
        //        new D.SolidFill(
        //        new D.SchemeColor(
        //            new D.Shade() { Val = 95000 },
        //            new D.SaturationModulation() { Val = 105000 })
        //        { Val = D.SchemeColorValues.PhColor }),
        //        new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
        //        {
        //            Width = 9525,
        //            CapType = D.LineCapValues.Flat,
        //            CompoundLineType = D.CompoundLineValues.Single,
        //            Alignment = D.PenAlignmentValues.Center
        //        },
        //        new D.Outline(
        //        new D.SolidFill(
        //        new D.SchemeColor(
        //            new D.Shade() { Val = 95000 },
        //            new D.SaturationModulation() { Val = 105000 })
        //        { Val = D.SchemeColorValues.PhColor }),
        //        new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
        //        {
        //            Width = 9525,
        //            CapType = D.LineCapValues.Flat,
        //            CompoundLineType = D.CompoundLineValues.Single,
        //            Alignment = D.PenAlignmentValues.Center
        //        }),
        //        new D.EffectStyleList(
        //        new D.EffectStyle(
        //        new D.EffectList(
        //        new D.OuterShadow(
        //            new D.RgbColorModelHex(
        //            new D.Alpha() { Val = 38000 })
        //            { Val = "000000" })
        //        { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
        //        new D.EffectStyle(
        //        new D.EffectList(
        //        new D.OuterShadow(
        //            new D.RgbColorModelHex(
        //            new D.Alpha() { Val = 38000 })
        //            { Val = "000000" })
        //        { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
        //        new D.EffectStyle(
        //        new D.EffectList(
        //        new D.OuterShadow(
        //            new D.RgbColorModelHex(
        //            new D.Alpha() { Val = 38000 })
        //            { Val = "000000" })
        //        { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false }))),
        //        new D.BackgroundFillStyleList(
        //        new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
        //        new D.GradientFill(
        //        new D.GradientStopList(
        //        new D.GradientStop(
        //            new D.SchemeColor(new D.Tint() { Val = 50000 },
        //            new D.SaturationModulation() { Val = 300000 })
        //            { Val = D.SchemeColorValues.PhColor })
        //        { Position = 0 },
        //        new D.GradientStop(
        //            new D.SchemeColor(new D.Tint() { Val = 50000 },
        //            new D.SaturationModulation() { Val = 300000 })
        //            { Val = D.SchemeColorValues.PhColor })
        //        { Position = 0 },
        //        new D.GradientStop(
        //            new D.SchemeColor(new D.Tint() { Val = 50000 },
        //            new D.SaturationModulation() { Val = 300000 })
        //            { Val = D.SchemeColorValues.PhColor })
        //        { Position = 0 }),
        //        new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
        //        new D.GradientFill(
        //        new D.GradientStopList(
        //        new D.GradientStop(
        //            new D.SchemeColor(new D.Tint() { Val = 50000 },
        //            new D.SaturationModulation() { Val = 300000 })
        //            { Val = D.SchemeColorValues.PhColor })
        //        { Position = 0 },
        //        new D.GradientStop(
        //            new D.SchemeColor(new D.Tint() { Val = 50000 },
        //            new D.SaturationModulation() { Val = 300000 })
        //            { Val = D.SchemeColorValues.PhColor })
        //        { Position = 0 }),
        //        new D.LinearGradientFill() { Angle = 16200000, Scaled = true })))
        //        { Name = "Office" });

        //    theme1.AppendExtend(themeElements1);
        //    theme1.AppendExtend(new D.ObjectDefaults());
        //    theme1.AppendExtend(new D.ExtraColorSchemeList());

        //    themePart1.Theme = theme1;
        //    return themePart1;
        //}

        public static PK.ThemePart CreateThemePart(Theme theme, PK.OpenXmlPart masterSlidePart, PresentationExportArgs exportArgs)
        {
            PK.ThemePart xThemePart = masterSlidePart.AddNewPart<PK.ThemePart>(theme.GetMasterParent().GetPropertyRelationshipIDString(theme));
            D.Theme xTheme = new D.Theme() { Name = theme.Name };

            D.ThemeElements xThemeElements = new D.ThemeElements();

            xThemeElements.ColorScheme = ColorScheme.CreateColorScheme(theme.ColorScheme);
            //xThemeElements.FontScheme = FontScheme
            xThemeElements.FormatScheme = FormatScheme.CreateShapeStyle(theme.FormatScheme, exportArgs);
            xTheme.ThemeElements = xThemeElements;

            return xThemePart;
        }

        #endregion





    }
}
