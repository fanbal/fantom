﻿
namespace Fantom.Builders
{
    /// <summary>
    /// 演示文稿构建器。
    /// </summary>
    internal static class PresentationBuilder
    {

        #region read
        /// <summary>
        /// 用于构建演示文稿。
        /// </summary>
        public static Presentation Build
            (PK.PresentationDocument presdoc, string packagePath, bool allowEdit)
        {
            var package = presdoc.Package;
            var prespart = presdoc.PresentationPart.Presentation;
            var prespr = package.PackageProperties;
            var author = prespr.Creator; ;
            var name = IO.Path.GetFileNameWithoutExtension(packagePath);

            var sldsize = prespart.SlideSize;

            var pres = new Presentation()
            {
                Path = packagePath,
                Name = name,
                AllowedEdit = allowEdit,
                Author = author,
                SlideSize = new Size(sldsize.Cx.Value, sldsize.Cy.Value),
                MediaManager = new MediaManager(package),
                PackageProperties = PackageProperties.GetPackageProperties(package.PackageProperties)
            };

            return pres;
        }

        /// <summary>
        /// 用于构建演示文稿。
        /// </summary>
        public static Presentation Build(string packagePath, bool allowEdit)
        {
            var presdoc = PK.PresentationDocument.Open(packagePath, !allowEdit);
            return Build(presdoc, packagePath, allowEdit);

        }

        public static Presentation Build(string packagePath)
        {
            return Build(packagePath, false);
        }

        public static Presentation Build()
        {
            return new Presentation() { AllowedEdit = true };
        }
        #endregion



        public static void ExportEntry(Presentation model, string packPath)
        {
            var exportArgs = new PresentationExportArgs();
            if (packPath.Length == 0)
            {
                CreatePresentationPart(model, IO.Path.Combine(IO.Directory.GetCurrentDirectory(), $"{Guid.NewGuid()}.pptx"), exportArgs);
            }
            else
            {
                CreatePresentationPart(model, packPath, exportArgs);
            }
        }

        /// <summary>
        /// 另存为指定文件。
        /// </summary>
        private static void CreatePresentationPart(Presentation model, string packPath, PresentationExportArgs exportArgs)
        {
            // Create a presentation at a specified file path. The presentation document type is pptx, by default.
            // 创建演示文稿文件路径，文件默认是 PPTX。
            PK.PresentationDocument presentationDoc = PK.PresentationDocument.Create(packPath, OX.PresentationDocumentType.Presentation);
            PK.PresentationPart presentationPart = presentationDoc.AddPresentationPart();
            presentationPart.Presentation = new P.Presentation();

            exportArgs.Mapper.TryAdd(model, presentationPart);

            CreatePresentationParts(model, presentationPart, exportArgs);

            presentationDoc.Close();
        }



        private static void CreatePresentationParts(Presentation presentation, PK.PresentationPart presentationPart, PresentationExportArgs exportArgs)
        {
            presentation.ApplyPropertyRelationshipIDMap();
            var packInnerIdCounter = 0U; presentation.LoadPackIDMapper(presentation, ref packInnerIdCounter);

            // 在主页的模板部分
            P.SlideMasterIdList slideMasterIdList1 = CreateSlideMasterIdList(presentation);

            // 在主页的幻灯片部分
            P.SlideIdList slideIdList1 = CreateSlideIdList(presentation);

            // TODO 尺寸相关。
            P.SlideSize slideSize1 = new P.SlideSize() { Cx = presentation.SlideSize.Width.ToInt(), Cy = presentation.SlideSize.Height.ToInt(), Type = P.SlideSizeValues.Screen16x9 };
            P.NotesSize notesSize1 = new P.NotesSize() { Cx = 6858000, Cy = 9144000 };
            P.DefaultTextStyle defaultTextStyle1 = new P.DefaultTextStyle();

            presentationPart.Presentation.AppendExtend(slideMasterIdList1, slideIdList1, slideSize1, notesSize1, defaultTextStyle1);



            // 创建母版
            CreateMasterParts(presentation, exportArgs);

            // 创建主题，并在母版中添加部分
            CreateThemeParts(presentation, exportArgs);

            CreateSlideParts(presentation, exportArgs);

            // 再绑定
            CreateSlidePartsAddRelationship(presentation, exportArgs);
            CreateSlideMasterPartsAddRelationship(presentation, exportArgs);
            CreatePresentationThemePartRelationship(presentation, exportArgs);
        }


        private static P.SlideMasterIdList CreateSlideMasterIdList(Presentation presentation)
        {
            // 在主页的模板部分
            var slideMasterIdList = new P.SlideMasterIdList();
            // new P.SlideMasterId() { Id = (OX.UInt32Value)2147483648U, RelationshipId = "rId1" }

            foreach (var master in presentation.SlideMasters)
            {
                slideMasterIdList.AppendExtend(new P.SlideMasterId() { Id = master._exportID, RelationshipId = presentation.GetPropertyRelationshipIDString(master) });
            }

            return slideMasterIdList;
        }


        private static P.SlideIdList CreateSlideIdList(Presentation presentation)
        {
            var slideIdList = new P.SlideIdList();

            foreach (var slide in presentation.Slides)
            {
                slideIdList.AppendExtend(new P.SlideId() { Id = slide._exportID, RelationshipId = presentation.GetPropertyRelationshipIDString(slide) });
            }

            return slideIdList;
        }

        private static void CreateThemeParts(Presentation presentation, PresentationExportArgs exportArgs)
        {

            if (presentation.NoteMaster is not null && presentation.NoteMaster.Theme is not null)
                ThemeBuilder.Export(presentation.NoteMaster.Theme, exportArgs);

            foreach (var master in presentation.SlideMasters)
            {
                if (master.Theme is not null)
                    ThemeBuilder.Export(master.Theme, exportArgs);
            }

        }

        private static void CreateMasterParts(Presentation presentation, PresentationExportArgs exportArgs)
        {
            if (presentation.NoteMaster is not null)
                NoteMasterBuilder.Export(presentation.NoteMaster, new PresentationExportArgs(exportArgs) { RelationshipMapperContainer = presentation.NoteMaster });

            foreach (var slideMaster in presentation.SlideMasters)
            {
                SlideMasterBuilder.Export(slideMaster, new PresentationExportArgs(exportArgs) { RelationshipMapperContainer = slideMaster });
            }
        }

        private static void CreateSlideMasterPartsAddRelationship(Presentation presentation, PresentationExportArgs exportArgs)
        {
            foreach (var slideMaster in presentation.SlideMasters)
            {
                if (exportArgs.Mapper.TryGetValue(slideMaster, out var masterPart))
                {
                    if (exportArgs.Mapper.TryGetValue(slideMaster.Theme, out var themePart))
                    {
                        masterPart.AddPart(themePart as PK.ThemePart, slideMaster.GetPropertyRelationshipIDString(slideMaster.Theme));
                    }
                    else
                    {
                        if (slideMaster.Theme is null)
                            throw new Exception($"虽然解析出了母版 {slideMaster.Name}，但是对应主题为空。");
                        else
                            throw new Exception($"虽然解析出了母版 {slideMaster.Name}，但是无法找到主题 {slideMaster.Theme.Name} 所映射的部件。");
                    }
                }
                else
                {
                    throw new Exception($"无法找到母版 {slideMaster.Name} 所映射的部件。");
                }
            }
        }


        private static void CreateSlideParts(Presentation presentation, PresentationExportArgs exportArgs)
        {
            foreach (var slide in presentation.Slides)
            {
                SlideBaseBuilder.Export(slide, new PresentationExportArgs(exportArgs) { RelationshipMapperContainer = slide });
            }
        }

        private static void CreateNoteMasterPart(Presentation presentation, PresentationExportArgs exportArgs)
        {
            foreach (var slide in presentation.Slides)
            {
                SlideBaseBuilder.Export(slide, exportArgs);
            }
        }


        private static void CreateSlidePartsAddRelationship(Presentation presentation, PresentationExportArgs exportArgs)
        {
            foreach (var slide in presentation.Slides)
            {
                if (exportArgs.Mapper.TryGetValue(slide, out var slidePart))
                {
                    if (exportArgs.Mapper.TryGetValue(slide.Layout, out var layoutPart))
                    {
                        slidePart.AddPart(layoutPart as PK.SlideLayoutPart, slide.GetPropertyRelationshipIDString(slide.Layout));
                    }
                    else
                    {
                        if (slide.Layout is null)
                            throw new Exception($"虽然解析出了幻灯片 {slide.Name}，但是对应布局为空。");
                        else
                            throw new Exception($"虽然解析出了幻灯片 {slide.Name}，但是无法找到布局 {slide.Layout.Name} 所映射的部件。");
                    }
                }
                else
                {
                    throw new Exception($"无法找到幻灯片 {slide.Name} 所映射的部件。");
                }
            }
        }

        private static void CreatePresentationThemePartRelationship(Presentation presentation, PresentationExportArgs exportArgs)
        {
            if (exportArgs.Mapper.TryGetValue(presentation, out var presentationpart))
            {
                if (exportArgs.Mapper.TryGetValue(presentation.SlideMasters[0].Theme, out var themePart))
                    presentationpart.AddPart(themePart);
                else
                    throw new Exception($"虽然解析出了演示文文稿 {presentation.Name}，但是对应主题为空。");
            }
            else
            {
                throw new Exception($"无法找到演示文稿 {presentation.Name} 所映射的部件。");
            }

        }

    }
}
