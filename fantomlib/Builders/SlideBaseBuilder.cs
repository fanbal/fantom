﻿

using DocumentFormat.OpenXml.EMMA;
using DocumentFormat.OpenXml.Packaging;

namespace Fantom.Builders
{

    /// <summary>
    /// 对应的构建器还有：<see cref="NoteMasterBuilder"/>，<see cref="NoteMasterBuilder"/>，<see cref="SlideLayoutBuilder"/>，<see cref="NoteSlideBuilder"/>
    /// </summary>
    internal static class SlideBaseBuilder
    {
        #region read


        /// <summary>
        /// 导入幻灯片。
        /// </summary>
        /// <param name="package">幻灯片的包体。</param>
        /// <param name="app">顶层对象。</param>
        /// <param name="pres">演示文稿对象。</param>
        /// <param name="slides">幻灯片集合对象。</param>
        /// <param name="sldpart">当前单页幻灯片的包体信息。</param>
        public static Slide LoadSlide(Slides slides, BuildingCallbackLoadArgs loadArgs, PresentationImportArgs importArgs)
        {

            var xSlidePart = importArgs.ArgPart as PK.SlidePart;
            var xSlide = xSlidePart.Slide;
            var xCommonSlideData = xSlide.CommonSlideData;

            var newSlide = SlideBaseBuilder.BuildSlide(xSlidePart);

            // 建立资源映射。
            importArgs = new PresentationImportArgs(importArgs)
            {
                RID2URLMapper = new PresentationImportArgs.PackRIDToURLMapper(xSlidePart),
                ShapeMapper = new PresentationImportArgs.ShapesShapeIDMapper()
            };

            // 导入布局。
            newSlide.Layout = importArgs.URL2CreatedPartMapper[xSlidePart.SlideLayoutPart.Uri] as SlideLayout;

            NoteSlideBuilder.LoadNoteSlide(newSlide, loadArgs,
                new PresentationImportArgs(importArgs)
                {
                    ArgPart = xSlidePart.NotesSlidePart,
                });


            var newImportArgs = new PresentationImportArgs(importArgs)
            {
                ColorLoadArgs = new ColorLoadArgs(xSlidePart, newSlide.Layout.ParentMaster.Theme, importArgs.Presentation.MediaManager),
                LoadBundleHandler = loadArgs.BundleHandler,
                ShapeMapper = new PresentationImportArgs.ShapesShapeIDMapper()
            };

            // 导入图形对象。
            ShapeBuilder.LoadShapes(newSlide,
                loadArgs.ShapeHandler,
                new PresentationImportArgs(newImportArgs) { ArgElement = xCommonSlideData.ShapeTree, });


            // 导入时间轴。
            TimeLineBuilder.LoadTimeLine(
                newSlide,
                new PresentationImportArgs(newImportArgs) { ArgElement = TimeLineBuilder.GetTiming(xSlide) });

            // 为幻灯片载入自定义信息。
            // PresentationLoader.LoadSlideCustomInfo(app, newSlide.CustomContainer, package, xcustomlist, uridic, xSlidePart, importArgs);

            slides.Add(newSlide);
            importArgs.URL2CreatedPartMapper.TryAdd(xSlidePart.Uri, newSlide);


            return newSlide;
        }


        // 逆向解析，通过文件名获得幻灯片的编号。
        public static int GetSlideId(string uristr)
        {
            int[] stk = new int[5];
            int top = 0;
            int i = uristr.Length - ".xml".Length - 1;
            while (i >= 0 && uristr[i] >= '0' && uristr[i] <= '9')
            {
                stk[top] = uristr[i] - '0';
                i--;
                top++;
            }

            int rst = 0;
            int temp = 1;
            for (int j = 0; j < top; j++)
            {
                rst += temp * stk[j];
                temp *= 10;
            }
            return rst;
        }

        /// <summary>
        /// 导入幻灯片。
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static Slide BuildSlide(PK.SlidePart element)
        {
            var uri = element.Uri.OriginalString;
            var sldid = GetSlideId(uri);

            return new Slide();
        }

        /// <summary>
        /// 新建幻灯片。
        /// </summary>
        /// <returns></returns>
        public static Slide BuildSlide()
        {
            return new Slide();
        }


        public static bool IsSlide(EchoBaseObject obj)
        {
            return obj is Slide || obj is NoteMaster || obj is NoteSlide || obj is SlideMaster || obj is SlideLayout;
        }


        #endregion


        #region output
        public static PK.SlidePart Export(Slide model, PresentationExportArgs exportArgs)
        {
            if (exportArgs.Mapper.TryGetValue(model.GetParent<Presentation>(), out var part))
            {
                var slidePart = CreateSlidePart(model, part as PK.PresentationPart, exportArgs);
                return slidePart;
            }
            else
            {
                throw new Exception($"在导出幻灯片 {model.Index} 时，未找到对应父级演示文稿部件。");
            }
        }


        private static PK.SlidePart CreateSlidePart(Slide slide, PK.PresentationPart presentationPart, PresentationExportArgs exportArgs)
        {
            PK.SlidePart slidePart = presentationPart.AddNewPart<PK.SlidePart>(slide.GetRelationshipIDStringAsPropertyItem<Presentation>());
            exportArgs.Mapper.TryAdd(slide, slidePart);
            slidePart.Slide = new P.Slide(
                    new P.CommonSlideData(CreateShapeTree<Slide>(slide.Shapes, exportArgs)),
                    new P.ColorMapOverride(new D.MasterColorMapping()));
            return slidePart;
        }


        public static P.ShapeTree CreateShapeTree<T>(Shapes shapes, PresentationExportArgs exportArgs) where T : EchoBaseObject
        {
            var shapeTree = new P.ShapeTree(
                           new P.NonVisualGroupShapeProperties(
                               new P.NonVisualDrawingProperties() { Id = shapes._exportID, Name = "" },
                               new P.NonVisualGroupShapeDrawingProperties(),
                               new P.ApplicationNonVisualDrawingProperties()),
                           new P.GroupShapeProperties(new D.TransformGroup()));

            foreach (var sp in shapes)
            {
                shapeTree.AppendExtend(ShapeBuilder.Export(sp, exportArgs));
            }

            return shapeTree;
        }



        #endregion



    }
}
