﻿
using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Builders
{
    internal static class NoteMasterBuilder
    {
        // TODO
        public static NoteMaster LoadNoteMaster(BuildingCallbackLoadArgs loadArgs, PresentationImportArgs importArgs)
        {
            var pres = importArgs.Presentation;
            var prespart = importArgs.PresentationPart;
            var notesmasterpart = importArgs.ArgPart as PK.NotesMasterPart;
            if (notesmasterpart is null) return null;
            var newNoteMaster = NoteMasterBuilder.BuildNoteMaster(notesmasterpart);
            pres.NoteMaster = newNoteMaster;


            newNoteMaster.Theme = ThemeBuilder.BuildTheme(loadArgs, new PresentationImportArgs(importArgs) { ArgPart = prespart.NotesMasterPart.ThemePart });

            var newImportArgs = new PresentationImportArgs(importArgs)
            {
                ColorLoadArgs = new ColorLoadArgs(notesmasterpart, newNoteMaster.Theme, importArgs.Presentation.MediaManager),
                LoadBundleHandler = loadArgs.BundleHandler,
                ShapeMapper = new PresentationImportArgs.ShapesShapeIDMapper()
            };

            // 导入图形对象。
            ShapeBuilder.LoadShapes(newNoteMaster,
                loadArgs.ShapeHandler,
                new PresentationImportArgs(newImportArgs) { ArgElement = notesmasterpart.NotesMaster.CommonSlideData.ShapeTree, });

            return newNoteMaster;


        }


        public static NoteMaster BuildNoteMaster(PK.NotesMasterPart notesMasterPart)
        {
            return new NoteMaster();
        }


        public static PK.NotesMasterPart Export(NoteMaster model, PresentationExportArgs exportArgs)
        {
            if (model is null) return null;
            if (exportArgs.Mapper.TryGetValue(model.GetParent<Presentation>(), out var part))
            {
                var masterPart = CreateNoteMasterPart(model, part as PK.PresentationPart, exportArgs);
                exportArgs.Mapper.TryAdd(model, masterPart);

                return masterPart;
            }
            else
            {
                throw new Exception($"在导出母版 {model.Name} 时，未找到对应父级演示文稿部件。");
            }
        }

        // 添加幻灯片母版的部分，演示文档的父级别连接。
        private static PK.NotesMasterPart CreateNoteMasterPart(NoteMaster sldMaster, PK.PresentationPart presentationPart, PresentationExportArgs exportArgs)
        {

            PK.NotesMasterPart notesMasterPart = presentationPart.AddNewPart<PK.NotesMasterPart>(sldMaster.GetRelationshipIDStringAsPropertyItem());
            P.NotesMaster noteMaster = new P.NotesMaster(
                new P.CommonSlideData(SlideBaseBuilder.CreateShapeTree<NoteMaster>(sldMaster.Shapes, exportArgs),
                new P.ColorMap() { Background1 = D.ColorSchemeIndexValues.Light1, Text1 = D.ColorSchemeIndexValues.Dark1, Background2 = D.ColorSchemeIndexValues.Light2, Text2 = D.ColorSchemeIndexValues.Dark2, Accent1 = D.ColorSchemeIndexValues.Accent1, Accent2 = D.ColorSchemeIndexValues.Accent2, Accent3 = D.ColorSchemeIndexValues.Accent3, Accent4 = D.ColorSchemeIndexValues.Accent4, Accent5 = D.ColorSchemeIndexValues.Accent5, Accent6 = D.ColorSchemeIndexValues.Accent6, Hyperlink = D.ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = D.ColorSchemeIndexValues.FollowedHyperlink },
                new P.TextStyles(new P.TitleStyle(), new P.BodyStyle(), new P.OtherStyle())));

            notesMasterPart.NotesMaster = noteMaster;

            return notesMasterPart;
        }




    }
}
