﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Attribute
{
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    sealed class PackInnerRelationshipMemberAttribute : System.Attribute
    {

        /// <summary>
        /// 具有子成员。
        /// </summary>
        public bool HasItem { get; set; } = false;


        public bool HasRoot { get; set; } = false;

        public bool IsSpecial { get; set; } = false;

        public PackInnerRelationshipMemberAttribute()
        {
            
        }

     
    }    
}
