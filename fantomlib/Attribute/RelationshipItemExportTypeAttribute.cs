﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Attribute
{
    /// <summary>
    /// 导出内容类型。
    /// </summary>
    public enum RelationshipExportType
    {
        /// <summary>
        /// 导出数组的根
        /// </summary>
        ExportRoot,

        /// <summary>
        /// 导出内部成员，一般以 Presentation Slide 这样的外部连接。
        /// </summary>
        ExportItem,

        /// <summary>
        /// 导出两者。
        /// </summary>
        ExportBoth,
    }

    [System.AttributeUsage(AttributeTargets.Property)]
    public sealed class RelationshipItemExportTypeAttribute : System.Attribute
    {

        /// <summary>
        /// 导出关系 ID 的处理方式。
        /// </summary>
        public RelationshipExportType RelationshipExport { get; set; }

        public bool IsRef { get; set; } = false;

        public RelationshipItemExportTypeAttribute(RelationshipExportType exportType = RelationshipExportType.ExportRoot)
        {
            RelationshipExport = exportType;
        }

        public RelationshipItemExportTypeAttribute(RelationshipExportType exportType = RelationshipExportType.ExportRoot, bool fromParent = false)
        {
            RelationshipExport = exportType;
            IsRef = fromParent;
        }


    }
}
