﻿using System;
using System.Collections.Generic;
using System.Text;
using Fantom.Drawing;
using Fantom.Interface;
namespace Fantom.Factory
{
	/// <summary>
	/// 基础的标识符工厂，因为标识符有很多，比如图形，关系，所以需要一个基类来描述它们。
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class BaseIdentifierFactory<T> where T : class , IIdentifier, new()
	{
		protected uint _counter = 0;

		public BaseIdentifierFactory(uint initcounter)
		{
			_counter = initcounter;
		}

		public virtual T Create()
		{
			T oid = new T();
			oid.SetId(_counter++);
			return oid;
		}
	}

}
