﻿using Fantom.Drawing;
using Fantom.Interface;
namespace Fantom.Factory
{
	/// <summary>
	/// 视觉元素标识符工厂，该标记为p:cnvPr。
	/// </summary>
	public class VisualIdentifierFactory: BaseIdentifierFactory<VisualIdentifier>
	{
		public VisualIdentifierFactory(uint initcounter):base(initcounter)
		{

		}

		/// <summary>
		/// 从<see cref="VisualIdentifierFactory"/>创建并返回一个具有名称的<see cref="VisualIdentifier"/>对象。
		/// </summary>
		/// <param name="name">标识符的名称</param>
		/// <returns><see cref="VisualIdentifier"/>对象</returns>
		public VisualIdentifier Create(string name)
		{
			var visualId =
				new VisualIdentifier(_counter++, name);
			return visualId;
		}

		/// <summary>
		/// 从<see cref="VisualIdentifierFactory"/>创建并返回一个默认名称的<see cref="VisualIdentifier"/>对象。
		/// </summary>
		/// <param name="name">标识符的名称</param>
		/// <returns><see cref="VisualIdentifier"/>对象</returns>
		public override VisualIdentifier Create()
		{
			var visualId = 
				new VisualIdentifier(_counter++, "empty_object");
			return visualId;
		}

	}

	
	
}
