﻿using System.Reflection;
namespace Fantom.Base
{
    public abstract class EchoSelectableObject : EchoBaseObject, ISelectable
    {
        public virtual bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                RaiseEvent();
            }
        }
        protected bool _isSelected = false;

        public void Select(bool isReplace)
        {
            if (Parent is ISelection)
            {
                var selection = Parent as ISelection;
                if (isReplace)
                {
                    selection.UnSelectAll();
                    this.IsSelected = true;
                }
                else
                {
                    this.IsSelected = true;
                }

            }
            else
            {
                throw new Exception($"{GetType().Name} 不是 {Parent.GetType()} 的选择容器成员。");
            }

        }


      



    }
}
