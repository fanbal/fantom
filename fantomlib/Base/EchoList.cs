﻿using System.Collections;
using System.Collections.Specialized;
using System.Linq;
namespace Fantom.Base
{
    public class EchoList<T> : EchoBaseObject, IList<T>, INotifyCollectionChanged where T : EchoBaseObject
    {
        protected List<T> _list;
        public EchoList()
        {
            _list = new List<T>();
        }

        public T this[string name]
        {
            get
            {
                T targetItem = null;
                foreach (var item in _list)
                {
                    if (item.Name == name)
                    {
                        targetItem = item; break;
                    }
                }
                if (targetItem is null)
                {
                    throw new NullReferenceException($"无法找到名为 {name} 的成员。");
                }

                return targetItem;
            }
            set
            {
                T targetItem = null;
                foreach (var item in _list)
                {
                    if (item.Name == name)
                    {
                        targetItem = item; break;
                    }
                }
                if (targetItem is null)
                {
                    throw new NullReferenceException($"无法找到名为 {name} 的成员。");
                }

                targetItem = value;
                value.Name = name;
            }

        }


        public T this[int index] { get => _list[index]; set => _list[index] = value; }

        public IList<T> Items => _list;

        public int Count => _list.Count;

        public bool IsReadOnly => false;

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public void Add(T item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            CheckReentrancy();
            _list.Clear();
            base.RaiseEvent(CountString);
            base.RaiseEvent(IndexerName);
            OnCollectionReset();

        }

        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            CheckReentrancy();
            _list.Insert(index, item);

            base.RaiseEvent(CountString);
            base.RaiseEvent(IndexerName);
            OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index);

        }

        public bool Remove(T item)
        {

            CheckReentrancy();
            var index = _list.IndexOf(item);
            var result = _list.Remove(item);

            base.RaiseEvent(CountString);
            base.RaiseEvent(IndexerName);
            OnCollectionChanged(NotifyCollectionChangedAction.Remove, item, index);

            return result;
        }

        public void RemoveAt(int index)
        {
            CheckReentrancy();
            T removedItem = this[index];

            _list.RemoveAt(index);

            base.RaiseEvent(CountString);
            base.RaiseEvent(IndexerName);
            OnCollectionChanged(NotifyCollectionChangedAction.Remove, removedItem, index);
        }


        /// <summary>
        /// 排序。
        /// </summary>
        /// <typeparam name="TKey">用于排序的值。</typeparam>
        /// <param name="keySelector">where 子句。</param>
        /// <param name="comparer">比较器。</param>
        public void Sort<TKey>(Func<T, TKey> keySelector, IComparer<TKey>? comparer = null)
        {
            CheckReentrancy();
            if (comparer is null)
                _list = new List<T>(_list.OrderBy(keySelector));
            else
                _list = new List<T>(_list.OrderBy(keySelector, comparer));
            base.RaiseEvent(CountString);
            base.RaiseEvent(IndexerName);
            OnCollectionChanged(NotifyCollectionChangedAction.Reset, this, 0);
        }

        public bool Switch(T item1, T item2)
        {

            CheckReentrancy();
            var index1 = _list.IndexOf(item1);
            var index2 = _list.IndexOf(item2);

            if (index1 != -1 && index2 != -1)
            {
                _list[index1] = item2;
                _list[index2] = item1;
            }
            else
            {
                return false;
                //throw new Exception("对象中至少有一个不位于当前对象集合中，无法进行交换。");
            }


            base.RaiseEvent(CountString);
            base.RaiseEvent(IndexerName);
            OnCollectionChanged(NotifyCollectionChangedAction.Replace, item1, index1);
            OnCollectionChanged(NotifyCollectionChangedAction.Replace, item2, index2);

            return true;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        //------------------------------------------------------
        //
        //  Constructors
        //
        //------------------------------------------------------


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ObservableCollection class
        /// that contains elements copied from the specified list
        /// </summary>
        /// <param name="list">The list whose elements are copied to the new list.</param>
        /// <remarks>
        /// The elements are copied onto the ObservableCollection in the
        /// same order they are read by the enumerator of the list.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> list is a null reference </exception>
        public EchoList(List<T> list)

        {
            CopyFrom(list);
        }

        /// <summary>
        /// Initializes a new instance of the ObservableCollection class that contains
        /// elements copied from the specified collection and has sufficient capacity
        /// to accommodate the number of elements copied.
        /// </summary>
        /// <param name="collection">The collection whose elements are copied to the new list.</param>
        /// <remarks>
        /// The elements are copied onto the ObservableCollection in the
        /// same order they are read by the enumerator of the collection.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> collection is a null reference </exception>
        public EchoList(IEnumerable<T> collection)
        {
            if (collection is null)
            {
                throw new ArgumentNullException("collection");
            }

            CopyFrom(collection);
        }

        private void CopyFrom(IEnumerable<T> collection)
        {
            _list = new List<T>(collection);
        }

        #endregion Constructors


        //------------------------------------------------------
        //
        //  Public Methods
        //
        //------------------------------------------------------

        #region Public Methods

        

        /// <summary>
        /// Move item at oldIndex to newIndex.
        /// </summary>
        public void Move(int oldIndex, int newIndex)
        {
            MoveItem(oldIndex, newIndex);
        }

        #endregion Public Methods


        //------------------------------------------------------
        //
        //  Public Events
        //
        //------------------------------------------------------

        #region Public Events



        #endregion Public Events


        //------------------------------------------------------
        //
        //  Protected Methods
        //
        //------------------------------------------------------

        #region Protected Methods


        /// <summary>
        /// Called by base class Collection&lt;T&gt; when an item is set in list;
        /// raises a CollectionChanged event to any listeners.
        /// </summary>
        protected void SetItem(int index, T item)
        {
            CheckReentrancy();
            T originalItem = this[index];
            _list[index] = item; // base.SetItem(index, item);

            base.RaiseEvent(IndexerName);
            OnCollectionChanged(NotifyCollectionChangedAction.Replace, originalItem, item, index);
        }

        /// <summary>
        /// Called by base class ObservableCollection&lt;T&gt; when an item is to be moved within the list;
        /// raises a CollectionChanged event to any listeners.
        /// </summary>
        protected virtual void MoveItem(int oldIndex, int newIndex)
        {
            CheckReentrancy();

            T removedItem = this[oldIndex];


            _list.RemoveAt(oldIndex); // base.RemoveItem(oldIndex);
            _list.Insert(newIndex, removedItem); // base.InsertItem(newIndex, removedItem);

            base.RaiseEvent(IndexerName);
            OnCollectionChanged(NotifyCollectionChangedAction.Move, removedItem, newIndex, oldIndex);
        }



        /// <summary>
        /// Raise CollectionChanged event to any listeners.
        /// Properties/methods modifying this ObservableCollection will raise
        /// a collection changed event through this virtual method.
        /// </summary>
        /// <remarks>
        /// When overriding this method, either call its base implementation
        /// or call <see cref="BlockReentrancy"/> to guard against reentrant collection changes.
        /// </remarks>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged is not null)
            {
                using (BlockReentrancy())
                {
                    CollectionChanged(this, e);
                }
            }
        }

        /// <summary>
        /// Disallow reentrant attempts to change this collection. E.g. a event handler
        /// of the CollectionChanged event is not allowed to make changes to this collection.
        /// </summary>
        /// <remarks>
        /// typical usage is to wrap e.g. a OnCollectionChanged call with a using() scope:
        /// <code>
        ///         using (BlockReentrancy())
        ///         {
        ///             CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, item, index));
        ///         }
        /// </code>
        /// </remarks>
        protected IDisposable BlockReentrancy()
        {
            _monitor.Enter();
            return _monitor;
        }

        /// <summary> Check and assert for reentrant attempts to change this collection. </summary>
        /// <exception cref="InvalidOperationException"> raised when changing the collection
        /// while another collection change is still being notified to other listeners </exception>
        protected void CheckReentrancy()
        {
            if (_monitor.Busy)
            {
                // we can allow changes if there's only one listener - the problem
                // only arises if reentrant changes make the original event args
                // invalid for later listeners.  This keeps existing code working
                // (e.g. Selector.SelectedItems).
                if ((CollectionChanged is not null) && (CollectionChanged.GetInvocationList().Length > 1))
                {
                    throw new InvalidOperationException("Reentrancy Not Allow");
                }
            }
        }

        #endregion Protected Methods


        //------------------------------------------------------
        //
        //  Private Methods
        //
        //------------------------------------------------------


        /// <summary>
        /// Helper to raise CollectionChanged event to any listeners
        /// </summary>
        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index));
        }

        /// <summary>
        /// Helper to raise CollectionChanged event to any listeners
        /// </summary>
        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index, int oldIndex)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index, oldIndex));
        }

        /// <summary>
        /// Helper to raise CollectionChanged event to any listeners
        /// </summary>
        private void OnCollectionChanged(NotifyCollectionChangedAction action, object oldItem, object newItem, int index)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, newItem, oldItem, index));
        }

        /// <summary>
        /// Helper to raise CollectionChanged event with action == Reset to any listeners
        /// </summary>
        private void OnCollectionReset()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }



        #region Private Types

        // this class helps prevent reentrant calls
        private class SimpleMonitor : IDisposable
        {
            public void Enter()
            {
                ++_busyCount;
            }

            public void Dispose()
            {
                --_busyCount;
            }

            public bool Busy { get { return _busyCount > 0; } }

            private int _busyCount;
        }

        #endregion Private Types

        //------------------------------------------------------
        //
        //  Private Fields
        //
        //------------------------------------------------------

        #region Private Fields

        private const string CountString = "Count";

        // This must agree with Binding.IndexerName.  It is declared separately
        // here so as to avoid a dependency on PresentationFramework.dll.
        private const string IndexerName = "Item[]";

        private SimpleMonitor _monitor = new SimpleMonitor();

        #endregion Private Fields

    }
}
