﻿using System.Linq;

namespace Fantom.Base
{
    public abstract class EchoSelectionList<T> : EchoList<T>, ISelection where T : EchoSelectableObject
    {
        public IEnumerable<ISelectable> Selection => from it in Items
                                                     where it.IsSelected
                                                     select it;

        public event SelectionEventHandler OnSelectionChanged;

        #region selection implement


        public void Select(ISelectable obj)
        {
            Select(new[] { obj });
        }

        public void Select(IEnumerable<ISelectable> objRange)
        {
            foreach (var item in Selection)
            {
                item.IsSelected = false;
            }

            foreach (var item in objRange)
            {
                item.IsSelected = true;
            }

            OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
        }


        public void SelectAdditional(ISelectable obj)
        {
            Select(new[] { obj });
        }

        public void SelectAdditional(IEnumerable<ISelectable> objRange)
        {
            foreach (var item in objRange)
            {
                item.IsSelected = true;
            }
            OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, objRange));
        }

        public void UnSelect(ISelectable obj)
        {
            UnSelect(new[] { obj });
        }

        public void UnSelect(IEnumerable<ISelectable> objRange)
        {

            foreach (var item in objRange)
            {
                item.IsSelected = false;
            }

            OnSelectionChanged?.Invoke(this, new SelectionEventArgs(objRange, new ISelectable[] { }));
        }

        public void SelectAll()
        {
            foreach (var item in Selection)
            {
                item.IsSelected = true;
            }
            OnSelectionChanged?.Invoke(this, new SelectionEventArgs(new ISelectable[] { }, Items.Cast<ISelectable>()));
        }

        public void UnSelectAll()
        {

            foreach (var item in Selection)
            {
                item.IsSelected = false;
            }

            OnSelectionChanged?.Invoke(this, new SelectionEventArgs(Items.Cast<ISelectable>(), new ISelectable[] { }));
        }

        IEnumerator<ISelectable> IEnumerable<ISelectable>.GetEnumerator()
        {
            return this.GetEnumerator();
        }



       

        #endregion

    }
}
