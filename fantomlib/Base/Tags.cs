﻿namespace Fantom
{
    /// <summary>
    /// 标签系统。
    /// </summary>
    public class Tags : EchoList<TagKeyValuePair>
    {
        public void Add(string key, string value)
        {
            Add(new TagKeyValuePair(key, value));
        }
    }
}
