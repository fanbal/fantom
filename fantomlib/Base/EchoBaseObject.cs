﻿using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Collections;
using System.Diagnostics.Metrics;
using DocumentFormat.OpenXml.Bibliography;
using System;
using DocumentFormat.OpenXml.EMMA;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Fantom
{
    /// <summary>
    /// 抽象基类回声对象，用于实现INotifyPropertyChanged实现MVVM通讯和节点导出。
    /// </summary>
    public abstract class EchoBaseObject : INotifyPropertyChanged, IEcho
    {
        /// <summary>
        /// 导出时依附的 ID，用于标记其唯一性，与 rid 不一致。
        /// </summary>
        internal uint _exportID = 0;

        /// <summary>
        /// 原导入名。
        /// </summary>
        internal string _importName = string.Empty;

        /// <summary>
        /// 对象名称。
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaiseEvent();
            }
        }
        private string _name = string.Empty;

        /// <summary>
        /// 顶层应用节点。
        /// </summary>
        public Application Application { get => _application; internal set { _application = value; } }
        protected Application _application;

        /// <summary>
        /// 父节点。
        /// </summary>
        public EchoBaseObject Parent { get => _parent; internal set { _parent = value; } }
        protected EchoBaseObject _parent;

        protected EchoBaseObject()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected void RaiseEvent([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public virtual EchoBaseObject Clone(EchoBaseObject echoBaseObject)
        {
            throw new NotImplementedException("未实现节点的复制。");
        }


        public T GetParent<T>() where T : EchoBaseObject
        {
            return GetParent<T>(this);
        }

        /// <summary>
        /// 仅对该对象建立属性联系映射表。
        /// </summary>
        internal virtual void LoadPropertyRelationshipIDMap(EchoBaseObject mapperContainer, ref uint counter)
        {
            var type = this.GetType();

            foreach (var prop in type.GetProperties())
            {
                if (IsEchoBaseObject(prop.PropertyType))
                {
                    var att = prop.GetCustomAttribute<RelationshipItemExportTypeAttribute>();
                    if (att is null) continue;
                    var member = prop.GetValue(this) as EchoBaseObject;

                    if (member is null) continue;

                    // 仅引用一层。
                    if (att.IsRef == true)
                    {
                        counter++;
                        mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                    }

                    else
                    {
                        if (att.RelationshipExport == RelationshipExportType.ExportItem)
                        {
                            foreach (EchoBaseObject item in member as IEnumerable)
                            {
                                counter++;
                                mapperContainer._propertyRelationshipIDMapper.Add(item, counter);
                                item.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
                            }
                        }
                        else if (att.RelationshipExport == RelationshipExportType.ExportBoth)
                        {
                            counter++;
                            mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                            member.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
                            foreach (EchoBaseObject item in member as IEnumerable)
                            {
                                counter++;
                                mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                                item.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
                            }
                        }
                        else // Root
                        {
                            counter++;
                            mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                            member.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
                        }
                    }



                }
            }



        }


        /// <summary>
        /// 属性关系映射器。
        /// </summary>
        internal Dictionary<EchoBaseObject, uint> _propertyRelationshipIDMapper = null;




        /// <summary>
        /// 获得属性关系ID。
        /// </summary>
        /// <param name="baseObject">属性</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        internal uint GetPropertyRelationshipID(EchoBaseObject baseObject)
        {
            if (_propertyRelationshipIDMapper is null)
                throw new Exception($"{baseObject.GetType().Name} 类型实例未构建映射器！");
            if (!_propertyRelationshipIDMapper.ContainsKey(baseObject))
                throw new Exception($"在关系集合中无法找到 {baseObject.GetType().Name} 类型实例。");
            return _propertyRelationshipIDMapper[baseObject];
        }


        internal string GetPropertyRelationshipIDString(EchoBaseObject baseObject)
        {
            return $"rId{GetPropertyRelationshipID(baseObject)}";
        }

        /// <summary>
        /// 从母对象中获得当前当前对象的 RID
        /// </summary>
        /// <returns></returns>
        internal uint GetRelationshipIDAsPropertyItem(EchoBaseObject mapperContainer = null)
        {
            if (mapperContainer is null)
                mapperContainer = Parent;

            if (mapperContainer is null)
                throw new Exception($"{this.GetType().Name} 实例内容为空。");

            return Parent.GetPropertyRelationshipID(this);
        }

        internal uint GetRelationshipIDAsPropertyItem<T>() where T : EchoBaseObject
        {
            return GetRelationshipIDAsPropertyItem(GetParent<T>());
        }

        /// <summary>
        /// 从母对象中获得当前当前对象的 RID
        /// </summary>
        /// <returns></returns>
        internal string GetRelationshipIDStringAsPropertyItem(EchoBaseObject mapperContainer = null)
        {
            if (mapperContainer is null)
                mapperContainer = Parent;

            if (mapperContainer is null)
                throw new Exception($"{this.GetType().Name} 实例内容为空。");

            return mapperContainer.GetPropertyRelationshipIDString(this);
        }

        internal string GetRelationshipIDStringAsPropertyItem<T>() where T : EchoBaseObject
        {
            return GetRelationshipIDStringAsPropertyItem(GetParent<T>());
        }


        /// <summary>
        /// 将应用和父级对象的信息告知子属性。
        /// </summary>
        internal void ApplyApplicationAndParentForProperty()
        {
            var type = GetType();
            var props = type.GetProperties();

            if (IsEchoList(this))
            {
                foreach (EchoBaseObject childObject in this as IEnumerable)
                {
                    childObject.Application = Application;
                    childObject.Parent = this;
                    childObject.ApplyApplicationAndParentForProperty();
                }
            }

            foreach (var prop in props)
            {
                var propType = prop.PropertyType;
                if (!IsEchoBaseObject(propType)) continue;

                if (prop.Name == "Application" || prop.Name == "Item" || prop.Name == "Parent") continue;

                if (IsFromParent(prop)) continue;

                var propValue = prop.GetValue(this);
                if (propValue is null) continue;

                //#if DEBUG
                //                Console.WriteLine(prop.Name);
                //#endif

                var echoObj = propValue as EchoBaseObject;
                echoObj.Parent = this;
                echoObj.Application = Application;

                if (IsEchoList(propValue))
                {
                    foreach (var childObj in propValue as IEnumerable<EchoBaseObject>)
                    {
                        childObj.Parent = echoObj;
                        childObj.Application = Application;
                        childObj.ApplyApplicationAndParentForProperty();
                    }
                }


                echoObj.ApplyApplicationAndParentForProperty();

            }

        }


        /// <summary>
        /// 包体内部关系映射器。
        /// </summary>
        internal Dictionary<EchoBaseObject, uint> _packIDMapper = null;


        /// <summary>
        /// 属性成员数据的批处理器。
        /// </summary>
        /// <param name="func"></param>
        internal virtual void PropertyMemberBatch(Action<PropertyInfo, EchoBaseObject> func)
        {
            var type = GetType();

            foreach (var prop in type.GetProperties())
            {
                if (IsEchoBaseObject(prop.PropertyType))
                {
                    var member = prop.GetValue(this) as EchoBaseObject;
                    if (member is not null)
                        func.Invoke(prop, member);
                }
            }
        }

        internal uint GetInPartID(EchoBaseObject baseObject)
        {
            return _packIDMapper[baseObject];
        }

        internal virtual void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            var type = GetType();

            foreach (var prop in type.GetProperties())
            {
                var att = prop.GetCustomAttribute<PackInnerRelationshipMemberAttribute>();
                if (att is null) continue;
                var innerValue = prop.GetValue(mapperContainer);
                if (innerValue is null) continue;

                if (att.IsSpecial)
                {
                    (innerValue as EchoBaseObject).LoadPackIDMapper(mapperContainer, ref counter);
                }
                else
                {
                    if (att.HasRoot)
                    {
                        counter++;
                        mapperContainer._packIDMapper.TryAdd(innerValue as EchoBaseObject, counter);
                    }


                    if (att.HasItem)
                    {
                        var coll = innerValue as IEnumerable;
                        foreach (EchoBaseObject subitem in coll)
                        {
                            counter++;
                            mapperContainer._packIDMapper.TryAdd(subitem, counter);
                            subitem.LoadPackIDMapper(mapperContainer, ref counter);
                        }
                    }
                }


            }
        }


        /// <summary>
        /// 检测对象是否为可导出的对象。
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static bool IsEchoBaseObject(object obj)
        {
            return obj is EchoBaseObject;
        }

        /// <summary>
        /// 检测对象是否为可导出列表对象。
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static bool IsEchoList(object obj)
        {
            return obj is EchoBaseObject && obj is IEnumerable;
        }


        internal static bool IsFromParent(PropertyInfo propInfo)
        {
            var att = propInfo.GetCustomAttribute<RelationshipItemExportTypeAttribute>();
            if (att is null) return false;
            else return att.IsRef;
        }





        /// <summary>
        /// 获得指定类型的父级对象。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static T GetParent<T>(EchoBaseObject obj) where T : EchoBaseObject
        {
            if (obj is null) return null;
            if (obj.Parent is null) return null;

            if (obj.Parent is T)
                return obj.Parent as T;
            else
                return GetParent<T>(obj.Parent);

        }

        internal static bool IsEchoBaseObject(Type type)
        {
            if (type is null)
            {
                return false;
            }
            if (type == typeof(EchoBaseObject))
            {
                return true;
            }
            else
            {
                return IsEchoBaseObject(type.BaseType);
            }

        }






    }
}
