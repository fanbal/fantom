﻿namespace Fantom
{

    /// <summary>
    /// <see cref="Emu"/> 格式的点结构。
    /// </summary>
    public struct EmuPoint
    {

        public Emu X;

        public Emu Y;

        public EmuPoint(Emu x, Emu y)
        {
            X = x; Y = y;
        }

        public static EmuPoint operator +(EmuPoint p1, EmuPoint p2)
        {
            return new EmuPoint(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static EmuPoint operator -(EmuPoint p1, EmuPoint p2)
        {
            return new EmuPoint(p1.X - p2.X, p1.Y - p2.Y);
        }


        /// <summary>
        /// 零点。
        /// </summary>
        public static EmuPoint Zero = new EmuPoint(0, 0);


    }
}
