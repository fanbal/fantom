﻿namespace Fantom.Effect
{
    public sealed class OuterShadow : Shadow
    {
        #region properties

        /// <summary>
        /// 阴影相较于图形的尺寸比例，PowerPoint 中允许范围在 0% ~ 200%，默认值为 100%。
        /// </summary>
        public Emu SizeX { get => _sizeX; set { _sizeX = value; RaiseEvent(); } }
        private Emu _sizeX = Emu.Percent100;

        /// <summary>
        /// 阴影相较于图形的尺寸比例，PowerPoint 中允许范围在 0% ~ 200%，默认值为 100%。
        /// </summary>
        public Emu SizeY { get => _sizeY; set { _sizeY = value; RaiseEvent(); } }
        private Emu _sizeY = Emu.Percent100;

        /// <summary>
        /// 阴影相较于图形的尺寸比例，PowerPoint 中允许范围在 0% ~ 200%，默认值为 100%。
        /// </summary>
        public Emu SkewX { get => _skewX; set { _skewX = value; RaiseEvent(); } }
        private Emu _skewX = Emu.Percent100;

        /// <summary>
        /// 阴影相较于图形的尺寸比例，PowerPoint 中允许范围在 0% ~ 200%，默认值为 100%。
        /// </summary>
        public Emu SkewY { get => _skewY; set { _skewY = value; RaiseEvent(); } }
        private Emu _skewY = Emu.Percent100;

        /// <summary>
        /// 阴影的偏移量。
        /// </summary>
        public EmuPoint Offset { get => _offset; set { _offset = value; RaiseEvent(); } }
        private EmuPoint _offset = EmuPoint.Zero;

        /// <summary>
        /// 对齐。
        /// </summary>
        public Alignment Alignment { get => _alignment; set { _alignment = value; RaiseEvent(); } }
        private Alignment _alignment = Alignment.Center;


        #endregion

        #region ctors

        internal OuterShadow()
        {

        }

        #endregion
    }
}
