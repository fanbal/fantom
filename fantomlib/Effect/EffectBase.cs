﻿
namespace Fantom.Effect
{
    public abstract class EffectBase
    {
        private static HashSet<Type> _pictureEffectHashSet = new HashSet<Type>()
        {
            typeof(D.AlphaBiLevel),
            typeof(D.AlphaCeiling),
            typeof(D.AlphaFloor),
            typeof(D.AlphaInverse),
            typeof(D.AlphaModulation),
            typeof(D.AlphaModulationFixed),
            typeof(D.AlphaReplace),
            typeof(D.BiLevel),
            typeof(D.Blur),
            typeof(D.ColorChange),
            typeof(D.ColorReplacement),
            typeof(D.Duotone),
            typeof(D.FillOverlay),
            typeof(D.Grayscale),
            typeof(D.Hsl),
            typeof(D.Luminance),
            typeof(D.Tint)
        };

        /// <summary>
        /// 判断该对象是否为图片效果对象。
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        internal static bool IsPictureEffectX(OX.OpenXmlElement element)
        {
            return _pictureEffectHashSet.Contains(element.GetType());
        }


        private static HashSet<Type> _effectHashSet = new HashSet<Type>()
        {
            typeof(D.AlphaBiLevel),
            typeof(D.AlphaCeiling),
            typeof(D.AlphaFloor),
            typeof(D.AlphaInverse),
            typeof(D.AlphaModulation),
            typeof(D.AlphaModulationFixed),
            typeof(D.AlphaReplace),
            typeof(D.BiLevel),
            typeof(D.Blur),
            typeof(D.ColorChange),
            typeof(D.ColorReplacement),
            typeof(D.Duotone),
            typeof(D.FillOverlay),
            typeof(D.Grayscale),
            typeof(D.Hsl),
            typeof(D.Luminance),
            typeof(D.Tint),



            typeof(D.AlphaOutset),
            typeof(D.EffectContainer),
            typeof(D.EffectReference),
            typeof(D.Blend),
            typeof(D.Fill),
            typeof(D.Glow),
            typeof(D.InnerShadow),
            typeof(D.OuterShadow),
            typeof(D.PresetShadow),
            typeof(D.Reflection),
            typeof(D.RelativeOffset),
            typeof(D.SoftEdge),
            typeof(D.TransformEffect),



        };

        // CT_AlphaOutsetEffect, CT_EffectContainer,CT_EffectReference, CT_BlendEffect,CT_FillEffect,  CT_GlowEffect, CT_InnerShadowEffect, CT_OuterShadowEffect,CT_PresetShadowEffect, CT_ReflectionEffect, CT_RelativeOffsetEffect,CT_SoftEdgesEffect, CT_TransformEffect 

        /// <summary>
        /// 判断该对象是否为图片效果对象。
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        internal static bool IsEffectX(OX.OpenXmlElement element)
        {
            return _effectHashSet.Contains(element.GetType());
        }

        public class AlphaBiLevelEffect : EffectBase
        {
            public Emu Threshold;

            internal static AlphaBiLevelEffect BuildEffect(D.AlphaBiLevel element, PresentationImportArgs importArgs)
            {

                return new AlphaBiLevelEffect() { Threshold = element.Threshold };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaBiLevel();
                xElement.Threshold = Threshold.ToInt();
                return xElement;
            }
        }

        public class AlphaCeilingEffect : EffectBase
        {
            internal static AlphaCeilingEffect BuildEffect(D.AlphaCeiling element, PresentationImportArgs importArgs)
            {
                return new AlphaCeilingEffect();
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaCeiling();
                return xElement;
            }
        }

        public class AlphaFloorEffect : EffectBase
        {
            internal static AlphaFloorEffect BuildEffect(D.AlphaFloor element, PresentationImportArgs importArgs)
            {
                return new AlphaFloorEffect();
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaFloor();
                return xElement;
            }
        }
        public class AlphaInverseEffect : EffectBase
        {
            public ColorBase Color;

            internal static AlphaInverseEffect BuildEffect(D.AlphaInverse element, PresentationImportArgs importArgs)
            {
                return new AlphaInverseEffect() { Color = ColorBuilder.GetColorFromNodeWithColor(element, importArgs.ColorLoadArgs) };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaInverse();
                xElement.AppendExtend(ColorBuilder.ColorToOpenXmlElement(Color));
                return xElement;
            }
        }
        public class AlphaModulateEffect : EffectBase
        {
            // TODO 特效列表。
            public EffectContainerEffect Container;

            internal static AlphaModulateEffect BuildEffect(D.AlphaModulation element, PresentationImportArgs importArgs)
            {

                return new AlphaModulateEffect() { Container = EffectContainerEffect.BuildEffect(element.GetFirstChild<D.EffectContainer>(), importArgs) };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaModulation();
                xElement.AppendExtend(Container.Export(exportArgs));
                return xElement;
            }
        }

        public class AlphaModulateFixedEffect : EffectBase
        {
            public Emu Amount;
            internal static AlphaModulateFixedEffect BuildEffect(D.AlphaModulationFixed element, PresentationImportArgs importArgs)
            {

                return new AlphaModulateFixedEffect() { Amount = element.Amount };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaModulationFixed();
                xElement.Amount = Amount.ToInt();
                return xElement;
            }
        }

        public class AlphaReplaceEffect : EffectBase
        {
            public Emu Alpha;
            internal static AlphaReplaceEffect BuildEffect(D.AlphaReplace element, PresentationImportArgs importArgs)
            {
                return new AlphaReplaceEffect() { Alpha = element.Alpha };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaReplace() { Alpha = Alpha.ToInt() };
                return xElement;
            }
        }
        public class BiLevelEffect : EffectBase
        {
            public Emu Threshold;
            internal static BiLevelEffect BuildEffect(D.BiLevel element, PresentationImportArgs importArgs)
            {
                return new BiLevelEffect() { Threshold = element.Threshold };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.BiLevel() { Threshold = Threshold.ToInt() };
                return xElement;
            }
        }
        public class BlurEffect : EffectBase
        {
            public Emu Radius;
            public bool Grow;
            internal static BlurEffect BuildEffect(D.Blur element, PresentationImportArgs importArgs)
            {
                return new BlurEffect() { Radius = element.Radius, Grow = element.Grow };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Blur() { Radius = Radius.Value, Grow = Grow };
                return xElement;
            }
        }
        public class ColorChangeEffect : EffectBase
        {
            public ColorBase ColorFrom;
            public ColorBase ColorTo;
            internal static ColorChangeEffect BuildEffect(D.ColorChange element, PresentationImportArgs importArgs)
            {
                return new ColorChangeEffect()
                {
                    ColorFrom = ColorBuilder.GetColorFromNodeWithColor(element.ColorFrom, importArgs.ColorLoadArgs),
                    ColorTo = ColorBuilder.GetColorFromNodeWithColor(element.ColorTo, importArgs.ColorLoadArgs)
                };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.ColorChange()
                {
                    ColorFrom = new D.ColorFrom(ColorBuilder.ColorToOpenXmlElement(ColorFrom)),
                    ColorTo = new D.ColorTo(ColorBuilder.ColorToOpenXmlElement(ColorTo))
                };
                return xElement;
            }
        }
        public class ColorReplaceEffect : EffectBase
        {
            public ColorBase Color;

            internal static ColorReplaceEffect BuildEffect(D.ColorReplacement element, PresentationImportArgs importArgs)
            {
                return new ColorReplaceEffect() { Color = ColorBuilder.GetColorFromNodeWithColor(element, importArgs.ColorLoadArgs) };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.ColorReplacement(ColorBuilder.ColorToOpenXmlElement(Color));
                return xElement;
            }
        }
        public class DuotoneEffect : EffectBase
        {

            public ColorBase Color1;
            public ColorBase Color2;
            internal static DuotoneEffect BuildEffect(D.Duotone element, PresentationImportArgs importArgs)
            {
                var colorNodes = new List<OX.OpenXmlElement>();
                foreach (var xChild in element)
                {
                    if (ColorBase.IsColorTypeX(xChild))
                        colorNodes.Add(xChild);
                }

                if (colorNodes.Count != 2) throw new Exception("DuotoneEffect 中找不到两个颜色。");


                return new DuotoneEffect()
                {
                    Color1 = ColorBuilder.OpenXmlElementToColor(colorNodes[0], importArgs.ColorLoadArgs),
                    Color2 = ColorBuilder.OpenXmlElementToColor(colorNodes[1], importArgs.ColorLoadArgs),
                };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                if (Color1 is null || Color2 is null) throw new Exception("DuotoneEffect 在导出时需要两个颜色，当前某一颜色为空！"); ;
                var xElement = new D.Duotone(ColorBuilder.ColorToOpenXmlElement(Color1), ColorBuilder.ColorToOpenXmlElement(Color2));

                return xElement;
            }
        }
        public class FillOverlayEffect : EffectBase
        {
            public FillBase Fill;
            public BlendMode Blend;
            internal static FillOverlayEffect BuildEffect(D.FillOverlay element, PresentationImportArgs importArgs)
            {
                return new FillOverlayEffect()
                {
                    Blend = (BlendMode)element.Blend.Value,
                    Fill = FillBase.BuildFillEntry(element, importArgs)
                };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.FillOverlay();
                return xElement;
            }
        }
        public class GrayscaleEffect : EffectBase
        {
            internal static GrayscaleEffect BuildEffect(D.Grayscale element, PresentationImportArgs importArgs)
            {
                return new GrayscaleEffect();
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Grayscale();
                return xElement;
            }
        }
        public class HSLEffect : EffectBase
        {
            public Emu Hue;
            public Emu Saturation;
            public Emu Luminance;
            internal static HSLEffect BuildEffect(D.Hsl element, PresentationImportArgs importArgs)
            {
                return new HSLEffect() { Hue = element.Hue, Saturation = element.Saturation, Luminance = element.Luminance };
            }

            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Hsl()
                {
                    Hue = Hue.ToInt(),
                    Saturation = Saturation.ToInt(),
                    Luminance = Luminance.ToInt()
                };
                return xElement;
            }
        }
        public class LuminanceEffect : EffectBase
        {
            public Emu? Bright;
            public Emu? Contrast;
            internal static LuminanceEffect BuildEffect(D.Luminance element, PresentationImportArgs importArgs)
            {
                var luminanceEffect = new LuminanceEffect();


                try
                {
                    var brightAtt = element.GetAttribute("bright", element.NamespaceUri);
                    luminanceEffect.Bright = Emu.Parse(brightAtt.Value);
                }
                catch { }

                try
                {
                    var contrastAtt = element.GetAttribute("contrast", element.NamespaceUri);
                    luminanceEffect.Contrast = Emu.Parse(contrastAtt.Value);
                }
                catch { }


                return luminanceEffect;
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Luminance();
                if (Bright is not null) xElement.SetAttribute(new OX.OpenXmlAttribute(xElement.NamespaceUri, "bright", Bright.ToString()));
                if (Contrast is not null) xElement.SetAttribute(new OX.OpenXmlAttribute(xElement.NamespaceUri, "contrast", Contrast.ToString()));
                return xElement;
            }
        }
        public class TintEffect : EffectBase
        {
            public Emu? Hue;
            public Emu? Amount;

            internal static TintEffect BuildEffect(D.Tint element, PresentationImportArgs importArgs)
            {
                var tintEffect = new TintEffect();

                try
                {
                    var hueAtt = element.GetAttribute("hue", element.NamespaceUri);
                    tintEffect.Hue = Emu.Parse(hueAtt.Value);
                }
                catch { }

                try
                {
                    var amtAtt = element.GetAttribute("amt", element.NamespaceUri);
                    tintEffect.Amount = Emu.Parse(amtAtt.Value);
                }
                catch { }

                return tintEffect;
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Tint();

                if (Hue is not null) xElement.SetAttribute(new OX.OpenXmlAttribute(xElement.NamespaceUri, "hue", Hue.ToString()));
                if (Amount is not null) xElement.SetAttribute(new OX.OpenXmlAttribute(xElement.NamespaceUri, "amt", Amount.ToString()));

                return xElement;
            }
        }

        public class AlphaOutsetEffect : EffectBase
        {
            public Emu? Radius;
            internal static AlphaOutsetEffect BuildEffect(D.AlphaOutset element, PresentationImportArgs importArgs)
            {
                var alphaOutsetEffect = new AlphaOutsetEffect();
                if (element.Radius is not null) alphaOutsetEffect.Radius = element.Radius;
                return alphaOutsetEffect;
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.AlphaOutset();
                if (Radius is not null) xElement.Radius = Radius.Value.Value;
                return xElement;
            }
        }
        public class EffectContainerEffect : EffectBase
        {
            public enum EffectContainerType
            {
                Sib, Tree
            }

            public List<EffectBase> Effects = new List<EffectBase>();
            public string? Name;
            public EffectContainerType Type = EffectContainerType.Sib;

            internal static EffectContainerEffect BuildEffect(D.EffectContainerType element, PresentationImportArgs importArgs)
            {
                var effectContainerEffect = new EffectContainerEffect();
                foreach (var xChild in element)
                {
                    if (EffectBase.IsEffectX(xChild))
                    {
                        effectContainerEffect.Effects.Add(EffectBase.BuildEffectEntry(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                    }
                }

                effectContainerEffect.Name = element.Name;
                effectContainerEffect.Type = (EffectContainerType)element.Type.Value;

                return effectContainerEffect;
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.EffectContainer();
                foreach (var child in Effects)
                {
                    xElement.AppendExtend(child.Export(exportArgs));
                }

                xElement.Name = Name;
                xElement.Type = (D.EffectContainerValues)Type;

                return xElement;
            }

            internal T Export<T>(PresentationExportArgs exportArgs) where T : D.EffectContainerType, new()
            {
                var xElement = new T();
                foreach (var child in Effects)
                {
                    xElement.AppendExtend(child.Export(exportArgs));
                }

                xElement.Name = Name;
                xElement.Type = (D.EffectContainerValues)Type;

                return xElement;
            }
        }
        public class EffectReferenceEffect : EffectBase
        {
            internal static EffectReferenceEffect BuildEffect(D.EffectReference element, PresentationImportArgs importArgs)
            {
                throw new Exception("暂时无法解析引用效果。");
                return new EffectReferenceEffect();
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                throw new Exception("暂时无法导出引用效果。");
                var xElement = new D.EffectReference();
                return xElement;
            }
        }
        public class BlendEffect : EffectBase
        {
            public EffectContainerEffect EffectContainer;
            public BlendMode Blend;

            internal static BlendEffect BuildEffect(D.Blend element, PresentationImportArgs importArgs)
            {
                return new BlendEffect()
                {
                    EffectContainer = EffectContainerEffect.BuildEffect(element.EffectContainer, importArgs),
                    Blend = (BlendMode)element.BlendMode.Value
                };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Blend(EffectContainer.Export(exportArgs)) { BlendMode = (D.BlendModeValues)Blend };

                return xElement;
            }
        }
        public class FillEffect : EffectBase
        {
            public FillBase Fill;
            internal static FillEffect BuildEffect(D.Fill element, PresentationImportArgs importArgs)
            {
                return new FillEffect() { Fill = FillBase.BuildFillEntry(element, importArgs) };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Fill(Fill.Export(exportArgs));
                return xElement;
            }
        }
        public class GlowEffect : EffectBase
        {
            public ColorBase Color;
            public Emu? Radius;
            internal static GlowEffect BuildEffect(D.Glow element, PresentationImportArgs importArgs)
            {
                return new GlowEffect() { Color = element.GetColorFromNodeWithColor(importArgs.ColorLoadArgs), Radius = element.Radius };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Glow(ColorBuilder.ColorToOpenXmlElement(Color));
                if (Radius is not null) xElement.Radius = Radius.Value.Value;
                return xElement;
            }
        }
        public class InnerShadowEffect : EffectBase
        {
            internal static InnerShadowEffect BuildEffect(D.InnerShadow element, PresentationImportArgs importArgs)
            {
                return new InnerShadowEffect();
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.InnerShadow();
                return xElement;
            }
        }
        public class OuterShadowEffect : EffectBase
        {
            public ColorBase Color;
            public Emu? BlurRadius;
            public Emu? Distance;
            public Emu? Direction;

            internal static OuterShadowEffect BuildEffect(D.OuterShadow element, PresentationImportArgs importArgs)
            {
                return new OuterShadowEffect()
                {
                    Color = element.GetColorFromNodeWithColor(importArgs.ColorLoadArgs),
                    BlurRadius = element.BlurRadius,
                    Direction = element.Direction,
                    Distance = element.Distance,
                };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.OuterShadow(ColorBuilder.ColorToOpenXmlElement(Color));
                if (BlurRadius is not null) xElement.BlurRadius = BlurRadius.Value.Value;
                if (Distance is not null) xElement.Distance = Distance.Value.Value;
                if (Direction is not null) xElement.Direction = Direction.Value.ToInt();

                return xElement;
            }
        }
        public class PresetShadowEffect : EffectBase
        {

            public ColorBase Color;
            public int PresetShadowID;
            public Emu? Distance;
            public Emu? Direction;


            internal static PresetShadowEffect BuildEffect(D.PresetShadow element, PresentationImportArgs importArgs)
            {
                return new PresetShadowEffect()
                {
                    Color = element.GetColorFromNodeWithColor(importArgs.ColorLoadArgs),
                    PresetShadowID = (int)element.Preset.Value,
                    Direction = element.Direction,
                    Distance = element.Distance,
                };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.PresetShadow(ColorBuilder.ColorToOpenXmlElement(Color));
                xElement.Preset = (D.PresetShadowValues)PresetShadowID;
                if (Distance is not null) xElement.Distance = Distance.Value.Value;
                if (Direction is not null) xElement.Direction = Direction.Value.ToInt();
                return xElement;
            }
        }
        public class ReflectionEffect : EffectBase
        {
            public Emu? BlurRadius;
            public Emu? StartAlpha;
            public Emu? StartPosition;
            public Emu? EndAlpha;
            public Emu? EndPosition;
            public Emu? Distance;
            public Emu? Direction;
            public Emu? FadeDirection;
            public Emu? HorizontalRatio;
            public Emu? VerticalRatio;
            public Emu? HorizontalSkew;
            public Emu? VerticalSkew;
            public RectAlignmentType? Alignment;
            public bool? RotateWithShape;

            internal static ReflectionEffect BuildEffect(D.Reflection element, PresentationImportArgs importArgs)
            {
                var alig = element.Alignment.Value;

                return new ReflectionEffect()
                {
                    BlurRadius = element.BlurRadius,
                    StartAlpha = element.StartOpacity,
                    StartPosition = element.StartPosition,
                    EndAlpha = element.EndAlpha,
                    EndPosition = element.EndPosition,
                    Distance = element.Distance,
                    Direction = element.Direction,
                    FadeDirection = element.FadeDirection,
                    HorizontalRatio = element.HorizontalRatio,
                    VerticalRatio = element.VerticalRatio,
                    HorizontalSkew = element.HorizontalSkew,
                    VerticalSkew = element.VerticalSkew,
                    Alignment = (RectAlignmentType)element.Alignment?.Value,
                    RotateWithShape = element.RotateWithShape
                };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.Reflection();

                if (BlurRadius is not null) xElement.BlurRadius = BlurRadius.Value.Value;
                if (StartAlpha is not null) xElement.StartOpacity = StartAlpha.Value.ToInt();
                if (StartPosition is not null) xElement.StartPosition = StartPosition.Value.ToInt();
                if (EndAlpha is not null) xElement.EndAlpha = EndAlpha.Value.ToInt();
                if (EndPosition is not null) xElement.EndPosition = EndPosition.Value.ToInt();
                if (Distance is not null) xElement.Distance = Distance.Value.ToInt();
                if (Direction is not null) xElement.Direction = Direction.Value.ToInt();
                if (FadeDirection is not null) xElement.FadeDirection = FadeDirection.Value.ToInt();
                if (HorizontalRatio is not null) xElement.HorizontalRatio = HorizontalRatio.Value.ToInt();
                if (VerticalRatio is not null) xElement.VerticalRatio = VerticalRatio.Value.ToInt();
                if (HorizontalSkew is not null) xElement.HorizontalSkew = HorizontalSkew.Value.ToInt();
                if (VerticalSkew is not null) xElement.VerticalSkew = VerticalSkew.Value.ToInt();
                if (Alignment is not null) xElement.Alignment = (D.RectangleAlignmentValues)Alignment;
                if (RotateWithShape is not null) xElement.RotateWithShape = RotateWithShape;

                return xElement;
            }
        }
        public class RelativeOffsetEffect : EffectBase
        {
            public Emu? OffsetX;
            public Emu? OffsetY;

            internal static RelativeOffsetEffect BuildEffect(D.RelativeOffset element, PresentationImportArgs importArgs)
            {
                return new RelativeOffsetEffect() { OffsetX = element.OffsetX, OffsetY = element.OffsetY };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.RelativeOffset();
                if (OffsetX is not null) xElement.OffsetX = OffsetX.Value.ToInt();
                if (OffsetY is not null) xElement.OffsetY = OffsetY.Value.ToInt();
                return xElement;
            }
        }
        public class SoftEdgeEffect : EffectBase
        {
            public Emu Radius;

            internal static SoftEdgeEffect BuildEffect(D.SoftEdge element, PresentationImportArgs importArgs)
            {
                return new SoftEdgeEffect() { Radius = element.Radius };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.SoftEdge() { Radius = Radius.ToInt() };
                return xElement;
            }
        }
        public class TransformEffectEffect : EffectBase
        {

            public Emu? HorizontalRatio;
            public Emu? VerticalRatio;
            public Emu? HorizontalSkew;
            public Emu? VerticalSkew;
            public Emu? HorizontalShift;
            public Emu? VerticalShift;

            internal static TransformEffectEffect BuildEffect(D.TransformEffect element, PresentationImportArgs importArgs)
            {
                return new TransformEffectEffect()
                {
                    HorizontalRatio = element.HorizontalRatio,
                    VerticalRatio = element.VerticalRatio,
                    HorizontalSkew = element.HorizontalSkew,
                    VerticalSkew = element.VerticalSkew,
                    HorizontalShift = element.HorizontalShift,
                    VerticalShift = element.VerticalShift,
                };
            }
            internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                var xElement = new D.TransformEffect();
                if (HorizontalRatio is not null) xElement.HorizontalRatio = HorizontalRatio.Value.ToInt();
                if (VerticalRatio is not null) xElement.VerticalRatio = VerticalRatio.Value.ToInt();
                if (HorizontalSkew is not null) xElement.HorizontalSkew = HorizontalSkew.Value.ToInt();
                if (VerticalSkew is not null) xElement.VerticalSkew = VerticalSkew.Value.ToInt();
                if (HorizontalShift is not null) xElement.HorizontalShift = HorizontalShift.Value.ToInt();
                if (VerticalShift is not null) xElement.VerticalShift = VerticalShift.Value.ToInt();
                return xElement;
            }
        }





        internal virtual OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
        {
            return null;
        }

        internal static EffectBase BuildEffectEntry(PresentationImportArgs importArgs)
        {
            var element = importArgs.ArgElement;

            if (element is D.AlphaBiLevel)
            {
                return AlphaBiLevelEffect.BuildEffect(element as D.AlphaBiLevel, importArgs);
            }
            else if (element is D.AlphaCeiling)
            {
                return AlphaCeilingEffect.BuildEffect(element as D.AlphaCeiling, importArgs);
            }
            else if (element is D.AlphaFloor)
            {
                return AlphaFloorEffect.BuildEffect(element as D.AlphaFloor, importArgs);
            }
            else if (element is D.AlphaInverse)
            {
                return AlphaInverseEffect.BuildEffect(element as D.AlphaInverse, importArgs);
            }
            else if (element is D.AlphaModulation)
            {
                return AlphaModulateEffect.BuildEffect(element as D.AlphaModulation, importArgs);
            }
            else if (element is D.AlphaModulationFixed)
            {
                return AlphaModulateFixedEffect.BuildEffect(element as D.AlphaModulationFixed, importArgs);
            }
            else if (element is D.AlphaReplace)
            {
                return AlphaReplaceEffect.BuildEffect(element as D.AlphaReplace, importArgs);
            }
            else if (element is D.BiLevel)
            {
                return BiLevelEffect.BuildEffect(element as D.BiLevel, importArgs);
            }
            else if (element is D.Blur)
            {
                return BlurEffect.BuildEffect(element as D.Blur, importArgs);
            }
            else if (element is D.ColorChange)
            {
                return ColorChangeEffect.BuildEffect(element as D.ColorChange, importArgs);
            }
            else if (element is D.ColorReplacement)
            {
                return ColorReplaceEffect.BuildEffect(element as D.ColorReplacement, importArgs);
            }
            else if (element is D.Duotone)
            {
                return DuotoneEffect.BuildEffect(element as D.Duotone, importArgs);
            }
            else if (element is D.FillOverlay)
            {
                return FillOverlayEffect.BuildEffect(element as D.FillOverlay, importArgs);
            }
            else if (element is D.Grayscale)
            {
                return GrayscaleEffect.BuildEffect(element as D.Grayscale, importArgs);
            }
            else if (element is D.Hsl)
            {
                return HSLEffect.BuildEffect(element as D.Hsl, importArgs);
            }
            else if (element is D.Luminance)
            {
                return LuminanceEffect.BuildEffect(element as D.Luminance, importArgs);
            }
            else if (element is D.Tint)
            {
                return TintEffect.BuildEffect(element as D.Tint, importArgs);
            }


            else if (element is D.AlphaOutset)
            {
                return AlphaOutsetEffect.BuildEffect(element as D.AlphaOutset, importArgs);
            }
            else if (element is D.EffectContainer)
            {
                return EffectContainerEffect.BuildEffect(element as D.EffectContainer, importArgs);
            }
            else if (element is D.EffectReference)
            {
                return EffectReferenceEffect.BuildEffect(element as D.EffectReference, importArgs);
            }
            else if (element is D.Blend)
            {
                return BlendEffect.BuildEffect(element as D.Blend, importArgs);
            }
            else if (element is D.Fill)
            {
                return FillEffect.BuildEffect(element as D.Fill, importArgs);
            }
            else if (element is D.Glow)
            {
                return GlowEffect.BuildEffect(element as D.Glow, importArgs);
            }
            else if (element is D.InnerShadow)
            {
                return InnerShadowEffect.BuildEffect(element as D.InnerShadow, importArgs);
            }
            else if (element is D.OuterShadow)
            {
                return OuterShadowEffect.BuildEffect(element as D.OuterShadow, importArgs);
            }
            else if (element is D.PresetShadow)
            {
                return PresetShadowEffect.BuildEffect(element as D.PresetShadow, importArgs);
            }
            else if (element is D.Reflection)
            {
                return ReflectionEffect.BuildEffect(element as D.Reflection, importArgs);
            }
            else if (element is D.RelativeOffset)
            {
                return RelativeOffsetEffect.BuildEffect(element as D.RelativeOffset, importArgs);
            }
            else if (element is D.SoftEdge)
            {
                return SoftEdgeEffect.BuildEffect(element as D.SoftEdge, importArgs);
            }
            else if (element is D.TransformEffect)
            {
                return TransformEffectEffect.BuildEffect(element as D.TransformEffect, importArgs);
            }




            throw new Exception("未知效果。");
        }

        internal static OX.OpenXmlElement CreateEffect(EffectBase pictureEffect, PresentationExportArgs exportArgs)
        {
            return pictureEffect.Export(exportArgs);
        }

    }
}
