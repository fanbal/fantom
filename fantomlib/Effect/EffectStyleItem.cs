﻿namespace Fantom.Effect
{

    public class EffectList : List<EffectBase>
    {
        private static HashSet<Type> _effectHashCode = new HashSet<Type>()
        {
            typeof(D.Blur), typeof(D.FillOverlay), typeof(D.Glow), typeof(D.InnerShadow), typeof(D.OuterShadow), typeof(D.PresetShadow), typeof(D.Reflection), typeof(D.SoftEdge)
        };

        internal static bool IsEffectTypeX(OX.OpenXmlElement element)
        {
            return _effectHashCode.Contains(element.GetType());
        }

        internal static EffectList BuildEffectList(PresentationImportArgs importArgs)
        {
            var xEffectList = importArgs.ArgElement as D.EffectList;
            if (xEffectList is null) return null;
            var effectList = new EffectList();
            foreach (var xChildEffect in xEffectList.GetElements(new List<Type>(_effectHashCode).ToArray()))
            {
                effectList.Add(EffectBase.BuildEffectEntry(new PresentationImportArgs(importArgs) { ArgElement = xChildEffect }));
            }
            return effectList;
        }

        internal static D.EffectList CreateEffectList(EffectList effectList, PresentationExportArgs exportArgs)
        {
            if (effectList is null) return null;
            var xEffectList = new D.EffectList();
            foreach (var effect in effectList)
            {
                xEffectList.AppendExtend(effect.Export(exportArgs));
            }
            return xEffectList;
        }

    }

    public class EffectProperties
    {
        public enum EffectPropertiesType
        {
            Effects, Dag
        }

        public EffectList Effects;
        public EffectBase.EffectContainerEffect Dag;
        public EffectPropertiesType Type;

        public EffectProperties()
        {
        }

        public EffectProperties(EffectProperties effectProperties)
        {

        }

        internal static EffectProperties BuildEffectProperties(PresentationImportArgs importArgs)
        {
            var xEffectProperties = importArgs.ArgElement as D.EffectPropertiesType;
            if (xEffectProperties is null) return null;
            var effectProperies = new EffectProperties();

            if (xEffectProperties.FirstChild is D.EffectList)
            {
                effectProperies.Type = EffectPropertiesType.Effects; 
                effectProperies.Effects = EffectList.BuildEffectList(new PresentationImportArgs(importArgs) { ArgElement = xEffectProperties.EffectList });
            }
            else if (xEffectProperties.FirstChild is D.EffectDag)
            {
                effectProperies.Type = EffectPropertiesType.Dag;
                effectProperies.Dag = EffectBase.EffectContainerEffect.BuildEffect(xEffectProperties.EffectDag, new PresentationImportArgs(importArgs) { ArgElement = xEffectProperties.EffectDag });
            }

            return effectProperies;
        }

        internal static D.EffectPropertiesType CreateEffectProperties(EffectProperties effectProperties, PresentationExportArgs exportArgs)
        {
            if (effectProperties is null) return null;
            var xEffectProperties = new D.EffectPropertiesType();
            if(effectProperties.Type == EffectPropertiesType.Effects)
                xEffectProperties.EffectList = EffectList.CreateEffectList(effectProperties.Effects, exportArgs);
            else if(effectProperties.Type == EffectPropertiesType.Dag)
                xEffectProperties.EffectDag = effectProperties.Dag.Export<D.EffectDag>(exportArgs);
            return xEffectProperties;
        }

    }

}
