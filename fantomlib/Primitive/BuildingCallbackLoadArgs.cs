﻿namespace Fantom.Primitive
{
    /// <summary>
    /// 传递生成的回调函数。
    /// </summary>
    internal class BuildingCallbackLoadArgs
    {
        /// <summary>
        /// 调用事件的句柄。
        /// </summary>
        public Fantom.Builders.PresentationLoader.RaiseLoadingBundleEventHandler BundleHandler;

        /// <summary>
        /// 调用事件的句柄。
        /// </summary>
        public Fantom.Builders.PresentationLoader.RaiseLoadingShapeEventHandler ShapeHandler;


    }
}
