namespace Fantom.Code.Automatic
{

    [System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    internal sealed class XmlTypeAttribute : System.Attribute
    {

        public XmlTypeAttribute(string ns, string name)
        {

        }


    }
    public enum ST_TransitionSideDirectionType
    {
        l,
        u,
        r,
        d,

    }

    public enum ST_TransitionCornerDirectionType
    {
        lu,
        ru,
        ld,
        rd,

    }

    public enum ST_TransitionInOutDirectionType
    {
        eout,
        ein,

    }

    [XmlType("", "CT_SideDirectionTransition")]
    public class CT_SideDirectionTransition : ITranslation
    {
        public ST_TransitionSideDirectionType dir { get; set; }

    }

    [XmlType("", "CT_CornerDirectionTransition")]
    public class CT_CornerDirectionTransition : ITranslation
    {
        public ST_TransitionCornerDirectionType dir { get; set; }

    }

    [XmlType("", "ST_TransitionEightDirectionType")]
    public enum ST_TransitionEightDirectionType
    {
        l,
        u,
        r,
        d,

        lu,
        ru,
        ld,
        rd,


    }

    [XmlType("", "CT_EightDirectionTransition")]
    public class CT_EightDirectionTransition : ITranslation
    {
        public ST_TransitionEightDirectionType dir { get; set; }

    }

    [XmlType("", "CT_OrientationTransition")]
    public class CT_OrientationTransition : ITranslation
    {
        public ST_Direction dir { get; set; }

    }

    [XmlType("", "CT_InOutTransition")]
    public class CT_InOutTransition : ITranslation
    {
        public ST_TransitionInOutDirectionType dir { get; set; }

    }

    [XmlType("", "CT_OptionalBlackTransition")]
    public class CT_OptionalBlackTransition : ITranslation
    {
        public bool thruBlk { get; set; }

    }

    [XmlType("", "CT_SplitTransition")]
    public class CT_SplitTransition : ITranslation
    {
        public ST_Direction orient { get; set; }
        public ST_TransitionInOutDirectionType dir { get; set; }

    }

    [XmlType("", "CT_WheelTransition")]
    public class CT_WheelTransition : ITranslation
    {
        public uint spokes { get; set; }

    }

    [XmlType("", "CT_TransitionStartSoundAction")]
    public class CT_TransitionStartSoundAction : ITransitionStartSoundAction
    {
        public List<Isnd1> Isnd1List { get; set; }
        public bool loop { get; set; }

    }

    [XmlType("", "CT_TransitionSoundAction")]
    public class CT_TransitionSoundAction : ITransitionSoundAction
    {
        public ITransitionStartSoundAction IstSnddS2 { get; set; }

    }

    public enum ST_TransitionSpeed
    {
        slow,
        med,
        fast,

    }

    [XmlType("", "CT_SlideTransition")]
    public class CT_SlideTransition : IcSldanmitL5, IcSldanmihftL6, IcSlddLanmihfSttL8
    {
        public List<ITransitionSoundAction> IsndActL3List { get; set; }
        public ST_TransitionSpeed spd { get; set; }
        public bool advClick { get; set; }
        public uint advTm { get; set; }

    }

    public enum ST_TLTimeIndefinite
    {
        indefinite,

    }

    [XmlType("", "ST_TLTime")]
    public enum ST_TLTime
    {

        indefinite


    }

    public enum ST_TLTimeNodeID
    {

    }

    [XmlType("", "CT_TLIterateIntervalTime")]
    public class CT_TLIterateIntervalTime : ItmAbstmPct2
    {
        public ST_TLTime val { get; set; }

    }

    [XmlType("", "CT_TLIterateIntervalPercentage")]
    public class CT_TLIterateIntervalPercentage : ItmAbstmPct2
    {
        public ST_PositivePercentage val { get; set; }

    }

    public enum ST_IterateType
    {
        el,
        wd,
        lt,

    }

    [XmlType("", "CT_TLIterateData")]
    public class CT_TLIterateData : ICodCdSerilbT6
    {
        public ItmAbstmPct2 ItmAbstmPct2 { get; set; }
        public ST_IterateType type { get; set; }
        public bool backwards { get; set; }

    }

    [XmlType("", "CT_TLSubShapeId")]
    public class CT_TLSubShapeId : IbgsubSpeCtxElap5, IdTdTspTgtkT4
    {
        public ST_ShapeID spid { get; set; }

    }

    [XmlType("", "CT_TLTextTargetElement")]
    public class CT_TLTextTargetElement : IbgsubSpeCtxElap5
    {
        public IarpRg2 IarpRg2 { get; set; }

    }

    public enum ST_TLChartSubelementType
    {
        gridLegend,
        series,
        category,
        ptInSeries,
        ptInCategory,

    }

    [XmlType("", "CT_TLOleChartTargetElement")]
    public class CT_TLOleChartTargetElement : IbgsubSpeCtxElap5
    {
        public ST_TLChartSubelementType type { get; set; }
        public uint lvl { get; set; }

    }

    [XmlType("", "CT_TLShapeTargetElement")]
    public class CT_TLShapeTargetElement : IdTdTspTgtkT4
    {
        public IbgsubSpeCtxElap5 IbgsubSpeCtxElap5 { get; set; }
        public ST_DrawingElementId spid { get; set; }

    }

    [XmlType("", "CT_TLTimeTargetElement")]
    public class CT_TLTimeTargetElement : ItgtEltnrtn3, IcTntgtEltr3, IcTntgtEl2
    {
        public IdTdTspTgtkT4 IdTdTspTgtkT4 { get; set; }

    }

    [XmlType("", "CT_TLTriggerTimeNodeID")]
    public class CT_TLTriggerTimeNodeID : ItgtEltnrtn3
    {
        public ST_TLTimeNodeID val { get; set; }

    }

    public enum ST_TLTriggerRuntimeNode
    {
        first,
        last,
        all,

    }

    [XmlType("", "CT_TLTriggerRuntimeNode")]
    public class CT_TLTriggerRuntimeNode : ItgtEltnrtn3
    {
        public ST_TLTriggerRuntimeNode val { get; set; }

    }

    public enum ST_TLTriggerEvent
    {
        onBegin,
        onEnd,
        begin,
        end,
        onClick,
        onDblClick,
        onMouseOver,
        onMouseOut,
        onNext,
        onPrev,
        onStopAudio,

    }

    [XmlType("", "CT_TLTimeCondition")]
    public class CT_TLTimeCondition : Icond1, ICodCdSerilbT6
    {
        public ItgtEltnrtn3 ItgtEltnrtn3 { get; set; }
        public ST_TLTriggerEvent evt { get; set; }
        public ST_TLTime delay { get; set; }

    }

    [XmlType("", "CT_TLTimeConditionList")]
    public class CT_TLTimeConditionList : ICodCdSerilbT6, IcTnevxt3
    {
        public List<Icond1> Icond1List { get; set; }


    }

    [XmlType("", "CT_TimeNodeList")]
    public class CT_TimeNodeList : ICodCdSerilbT6, ItnLst1, ItnLstdLtL3
    {
        public Iparseqexclanimimimimimimcmdsetaudiovideo13 Iparseqexclanimimimimimimcmdsetaudiovideo13 { get; set; }

    }

    public enum ST_TLTimeNodePresetClassType
    {
        entr,
        exit,
        emph,
        path,
        verb,
        mediacall,

    }

    public enum ST_TLTimeNodeRestartType
    {
        always,
        whenNotActive,
        never,

    }

    public enum ST_TLTimeNodeFillType
    {
        remove,
        freeze,
        hold,
        transition,

    }

    public enum ST_TLTimeNodeSyncType
    {
        canSlip,
        locked,

    }

    public enum ST_TLTimeNodeMasterRelation
    {
        sameClick,
        lastClick,
        nextClick,

    }

    public enum ST_TLTimeNodeType
    {
        clickEffect,
        withEffect,
        afterEffect,
        mainSeq,
        interactiveSeq,
        clickPar,
        withGroup,
        afterGroup,
        tmRoot,

    }

    [XmlType("", "CT_TLCommonTimeNodeData")]
    public class CT_TLCommonTimeNodeData : IcTn1, IcTnevxt3, IcTntgtEltr3, IcTntgtEl2
    {
        public List<ICodCdSerilbT6> ICodCdSerilbT6List { get; set; }
        public ST_TLTimeNodeID id { get; set; }
        public int presetID { get; set; }
        public ST_TLTimeNodePresetClassType presetClass { get; set; }
        public int presetSubtype { get; set; }
        public ST_TLTime dur { get; set; }
        public ST_TLTime repeatCount { get; set; }
        public ST_TLTime repeatDur { get; set; }
        public ST_Percentage spd { get; set; }
        public ST_PositiveFixedPercentage accel { get; set; }
        public ST_PositiveFixedPercentage decel { get; set; }
        public bool autoRev { get; set; }
        public ST_TLTimeNodeRestartType restart { get; set; }
        public ST_TLTimeNodeFillType fill { get; set; }
        public ST_TLTimeNodeSyncType syncBehavior { get; set; }
        public string tmFilter { get; set; }
        public string evtFilter { get; set; }
        public bool display { get; set; }
        public ST_TLTimeNodeMasterRelation masterRel { get; set; }
        public int bldLvl { get; set; }
        public uint grpId { get; set; }
        public bool afterEffect { get; set; }
        public ST_TLTimeNodeType nodeType { get; set; }
        public bool nodePh { get; set; }

    }

    [XmlType("", "CT_TLTimeNodeParallel")]
    public class CT_TLTimeNodeParallel : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcTn1> IcTn1List { get; set; }


    }

    public enum ST_TLNextActionType
    {
        none,
        seek,

    }

    public enum ST_TLPreviousActionType
    {
        none,
        skipTimed,

    }

    [XmlType("", "CT_TLTimeNodeSequence")]
    public class CT_TLTimeNodeSequence : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcTnevxt3> IcTnevxt3List { get; set; }
        public bool concurrent { get; set; }
        public ST_TLPreviousActionType prevAc { get; set; }
        public ST_TLNextActionType nextAc { get; set; }

    }

    [XmlType("", "CT_TLTimeNodeExclusive")]
    public class CT_TLTimeNodeExclusive : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcTn1> IcTn1List { get; set; }


    }

    [XmlType("", "CT_TLBehaviorAttributeNameList")]
    public class CT_TLBehaviorAttributeNameList : IcTntgtEltr3
    {
        public List<Itr1> Itr1List { get; set; }


    }

    public enum ST_TLBehaviorAdditiveType
    {
        ebase,
        sum,
        repl,
        mult,
        none,

    }

    public enum ST_TLBehaviorAccumulateType
    {
        none,
        always,

    }

    public enum ST_TLBehaviorTransformType
    {
        pt,
        img,

    }

    public enum ST_TLBehaviorOverrideType
    {
        normal,
        childStyle,

    }

    [XmlType("", "CT_TLCommonBehaviorData")]
    public class CT_TLCommonBehaviorData : IcBhvrvL2, IcBhvrbyfromto4, IcBhvrog2, IcBhvrbyfromtorCtr5, IcBhvr1, IcBhvrto2
    {
        public List<IcTntgtEltr3> IcTntgtEltr3List { get; set; }
        public ST_TLBehaviorAdditiveType additive { get; set; }
        public ST_TLBehaviorAccumulateType accumulate { get; set; }
        public ST_TLBehaviorTransformType xfrmType { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string by { get; set; }
        public string rctx { get; set; }
        public ST_TLBehaviorOverrideType override_ { get; set; }

    }

    [XmlType("", "CT_TLAnimVariantboolVal")]
    public class CT_TLAnimVariantboolVal : IoltVtVrVrV5
    {
        public bool val { get; set; }

    }

    [XmlType("", "CT_TLAnimVariantIntegerVal")]
    public class CT_TLAnimVariantIntegerVal : IoltVtVrVrV5
    {
        public int val { get; set; }

    }

    [XmlType("", "CT_TLAnimVariantFloatVal")]
    public class CT_TLAnimVariantFloatVal : IoltVtVrVrV5
    {
        public float val { get; set; }

    }

    [XmlType("", "CT_TLAnimVariantStringVal")]
    public class CT_TLAnimVariantStringVal : IoltVtVrVrV5
    {
        public string val { get; set; }

    }

    [XmlType("", "CT_TLAnimVariant")]
    public class CT_TLAnimVariant : Ival1, IcBhvrog2, IcBhvrto2
    {
        public IoltVtVrVrV5 IoltVtVrVrV5 { get; set; }

    }

    [XmlType("", "ST_TLTimeAnimateValueTime")]
    public enum ST_TLTimeAnimateValueTime
    {

        indefinite,


    }

    [XmlType("", "CT_TLTimeAnimateValue")]
    public class CT_TLTimeAnimateValue : Itav1
    {
        public List<Ival1> Ival1List { get; set; }
        public ST_TLTimeAnimateValueTime tm { get; set; }
        public string fmla { get; set; }

    }

    [XmlType("", "CT_TLTimeAnimateValueList")]
    public class CT_TLTimeAnimateValueList : IcBhvrvL2
    {
        public List<Itav1> Itav1List { get; set; }

    }

    public enum ST_TLAnimateBehaviorCalcMode
    {
        discrete,
        lin,
        fmla,

    }

    public enum ST_TLAnimateBehaviorValueType
    {
        str,
        num,
        clr,

    }

    [XmlType("", "CT_TLAnimateBehavior")]
    public class CT_TLAnimateBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvrvL2> IcBhvrvL2List { get; set; }
        public string by { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public ST_TLAnimateBehaviorCalcMode calcmode { get; set; }
        public ST_TLAnimateBehaviorValueType valueType { get; set; }

    }

    [XmlType("", "CT_TLByRgbColorTransform")]
    public class CT_TLByRgbColorTransform : Irgbhsl2
    {
        public ST_FixedPercentage r { get; set; }
        public ST_FixedPercentage g { get; set; }
        public ST_FixedPercentage b { get; set; }

    }

    [XmlType("", "CT_TLByHslColorTransform")]
    public class CT_TLByHslColorTransform : Irgbhsl2
    {
        public ST_Angle h { get; set; }
        public ST_FixedPercentage s { get; set; }
        public ST_FixedPercentage l { get; set; }

    }

    [XmlType("", "CT_TLByAnimateColorTransform")]
    public class CT_TLByAnimateColorTransform : IcBhvrbyfromto4
    {
        public Irgbhsl2 Irgbhsl2 { get; set; }

    }

    public enum ST_TLAnimateColorSpace
    {
        rgb,
        hsl,

    }

    public enum ST_TLAnimateColorDirection
    {
        cw,
        ccw,

    }

    [XmlType("", "CT_TLAnimateColorBehavior")]
    public class CT_TLAnimateColorBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvrbyfromto4> IcBhvrbyfromto4List { get; set; }
        public ST_TLAnimateColorSpace clrSpc { get; set; }
        public ST_TLAnimateColorDirection dir { get; set; }

    }

    public enum ST_TLAnimateEffectTransition
    {
        ein,
        eout,
        none,

    }

    [XmlType("", "CT_TLAnimateEffectBehavior")]
    public class CT_TLAnimateEffectBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvrog2> IcBhvrog2List { get; set; }
        public ST_TLAnimateEffectTransition transition { get; set; }
        public string filter { get; set; }
        public string prLst { get; set; }

    }

    public enum ST_TLAnimateMotionBehaviorOrigin
    {
        parent,
        layout,

    }

    public enum ST_TLAnimateMotionPathEditMode
    {
        relative,
        efixed,

    }

    [XmlType("", "CT_TLPoint")]
    public class CT_TLPoint : IcBhvrbyfromtorCtr5, IcBhvrbyfromto4
    {
        public ST_Percentage x { get; set; }
        public ST_Percentage y { get; set; }

    }

    [XmlType("", "CT_TLAnimateMotionBehavior")]
    public class CT_TLAnimateMotionBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvrbyfromtorCtr5> IcBhvrbyfromtorCtr5List { get; set; }
        public ST_TLAnimateMotionBehaviorOrigin origin { get; set; }
        public string path { get; set; }
        public ST_TLAnimateMotionPathEditMode pathEditMode { get; set; }
        public ST_Angle rAng { get; set; }
        public string ptsTypes { get; set; }

    }

    [XmlType("", "CT_TLAnimateRotationBehavior")]
    public class CT_TLAnimateRotationBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvr1> IcBhvr1List { get; set; }
        public ST_Angle by { get; set; }
        public ST_Angle from { get; set; }
        public ST_Angle to { get; set; }

    }

    [XmlType("", "CT_TLAnimateScaleBehavior")]
    public class CT_TLAnimateScaleBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvrbyfromto4> IcBhvrbyfromto4List { get; set; }
        public bool zoomContents { get; set; }

    }

    public enum ST_TLCommandType
    {
        evt,
        call,
        verb,

    }

    [XmlType("", "CT_TLCommandBehavior")]
    public class CT_TLCommandBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvr1> IcBhvr1List { get; set; }
        public ST_TLCommandType type { get; set; }
        public string cmd { get; set; }

    }

    [XmlType("", "CT_TLSetBehavior")]
    public class CT_TLSetBehavior : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<IcBhvrto2> IcBhvrto2List { get; set; }

    }

    [XmlType("", "CT_TLCommonMediaNodeData")]
    public class CT_TLCommonMediaNodeData : Ied1
    {
        public List<IcTntgtEl2> IcTntgtEl2List { get; set; }
        public ST_PositiveFixedPercentage vol { get; set; }
        public bool mute { get; set; }
        public uint numSld { get; set; }
        public bool showWhenStopped { get; set; }

    }

    [XmlType("", "CT_TLMediaNodeAudio")]
    public class CT_TLMediaNodeAudio : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<Ied1> Ied1List { get; set; }
        public bool isNarration { get; set; }

    }

    [XmlType("", "CT_TLMediaNodeVideo")]
    public class CT_TLMediaNodeVideo : Iparseqexclanimimimimimimcmdsetaudiovideo13
    {
        public List<Ied1> Ied1List { get; set; }
        public bool fullScrn { get; set; }

    }

    [XmlType("", "AG_TLBuild")]
    public class AG_TLBuild
    {

    }

    [XmlType("", "CT_TLTemplate")]
    public class CT_TLTemplate : Itmpl1
    {
        public List<ItnLst1> ItnLst1List { get; set; }
        public uint lvl { get; set; }

    }

    [XmlType("", "CT_TLTemplateList")]
    public class CT_TLTemplateList : Ipl1
    {
        public List<Itmpl1> Itmpl1List { get; set; }

    }

    public enum ST_TLParaBuildType
    {
        allAtOnce,
        p,
        cust,
        whole,

    }

    [XmlType("", "CT_TLBuildParagraph")]
    public class CT_TLBuildParagraph : IbldPdDdOdG4
    {
        public List<Ipl1> Ipl1List { get; set; }
        public AG_TLBuild AG_TLBuild { get; set; }
        public ST_TLParaBuildType build { get; set; }
        public uint bldLvl { get; set; }
        public bool animBg { get; set; }
        public bool autoUpdateAnimBg { get; set; }
        public bool rev { get; set; }
        public ST_TLTime advAuto { get; set; }

    }

    public enum ST_TLDiagramBuildType
    {
        whole,
        depthByNode,
        depthByBranch,
        breadthByNode,
        breadthByLvl,
        cw,
        cwIn,
        cwOut,
        ccw,
        ccwIn,
        ccwOut,
        inByRing,
        outByRing,
        up,
        down,
        allAtOnce,
        cust,

    }

    [XmlType("", "CT_TLBuildDiagram")]
    public class CT_TLBuildDiagram : IbldPdDdOdG4
    {
        public AG_TLBuild AG_TLBuild { get; set; }
        public ST_TLDiagramBuildType bld { get; set; }

    }

    public enum ST_TLOleChartBuildType
    {
        allAtOnce,
        series,
        category,
        seriesEl,
        categoryEl,

    }

    [XmlType("", "CT_TLOleBuildChart")]
    public class CT_TLOleBuildChart : IbldPdDdOdG4
    {
        public AG_TLBuild AG_TLBuild { get; set; }
        public ST_TLOleChartBuildType bld { get; set; }
        public bool animBg { get; set; }

    }

    [XmlType("", "CT_TLGraphicalObjectBuild")]
    public class CT_TLGraphicalObjectBuild : IbldPdDdOdG4
    {
        public IdAdS2 IdAdS2 { get; set; }
        public AG_TLBuild AG_TLBuild { get; set; }

    }

    [XmlType("", "CT_BuildList")]
    public class CT_BuildList : ItnLstdLtL3
    {
        public IbldPdDdOdG4 IbldPdDdOdG4 { get; set; }

    }

    [XmlType("", "CT_SlideTiming")]
    public class CT_SlideTiming : IcSldanmitL5, IcSldanmihftL6, IcSlddLanmihfSttL8
    {
        public List<ItnLstdLtL3> ItnLstdLtL3List { get; set; }

    }


    public class ST_Name
    {

    }

    public enum ST_Direction
    {
        horz,
        vert,

    }

    public enum ST_Index
    {

    }

    [XmlType("", "CT_IndexRange")]
    public class CT_IndexRange : IarpRg2
    {
        public ST_Index st { get; set; }
        public ST_Index end { get; set; }

    }

    [XmlType("", "CT_SlideRelationshipListEntry")]
    public class CT_SlideRelationshipListEntry : Isld1
    {


    }

    [XmlType("", "CT_SlideRelationshipList")]
    public class CT_SlideRelationshipList : IdLtL2
    {
        public List<Isld1> Isld1List { get; set; }

    }

    [XmlType("", "CT_CustomShowId")]
    public class CT_CustomShowId
    {
        public uint id { get; set; }

    }

    [XmlType("", "EG_SlideListChoice")]
    public class EG_SlideListChoice
    {


    }

    [XmlType("", "CT_CustomerData")]
    public class CT_CustomerData : Isttags2
    {


    }

    [XmlType("", "CT_TagsData")]
    public class CT_TagsData : Isttags2
    {


    }

    [XmlType("", "CT_CustomerDataList")]
    public class CT_CustomerDataList : IdMtenddIsldSztearbestotstnsfaditL15, IphsttL4, IbgTrstnttL5
    {
        public List<Isttags2> Isttags2List { get; set; }

    }

    [XmlType("", "CT_Extension")]
    public class CT_Extension
    {
        public List<I1> I1List { get; set; }
        public token uri { get; set; }

    }

    [XmlType("", "EG_ExtensionList")]
    public class EG_ExtensionList
    {
        public List<Object> List { get; set; }

    }

    [XmlType("", "CT_ExtensionList")]
    public class CT_ExtensionList
    {
        public List<I1> I1List { get; set; }

    }

    [XmlType("", "CT_ExtensionListModify")]
    public class CT_ExtensionListModify 
    {
        public List<I1> I1List { get; set; }
        public bool mod { get; set; }

    }

    [XmlType("", "CT_CommentAuthor")]
    public class CT_CommentAuthor : IAu1
    {
        public List<ItL1> ItL1List { get; set; }
        public uint id { get; set; }
        public ST_Name name { get; set; }
        public ST_Name initials { get; set; }
        public uint lastIdx { get; set; }
        public uint clrIdx { get; set; }

    }

    [XmlType("", "CT_CommentAuthorList")]
    public class CT_CommentAuthorList
    {
        public List<IAu1> IAu1List { get; set; }

    }

    [XmlType("", "CT_Comment")]
    public class CT_Comment : Icm1
    {
        public List<IpostexttL3> IpostexttL3List { get; set; }
        public uint authorId { get; set; }
        public dateTime dt { get; set; }
        public ST_Index idx { get; set; }

    }

    [XmlType("", "CT_CommentList")]
    public class CT_CommentList
    {
        public List<Icm1> Icm1List { get; set; }

    }

    [XmlType("", "AG_Ole")]
    public class AG_Ole
    {

    }

    public enum ST_OleObjectFollowColorScheme
    {
        none,
        full,
        textAndBackground,

    }

    [XmlType("", "CT_OleObjectEmbed")]
    public class CT_OleObjectEmbed : Iembedlink2
    {
        public List<ItL1> ItL1List { get; set; }
        public ST_OleObjectFollowColorScheme followColorScheme { get; set; }

    }

    [XmlType("", "CT_OleObjectLink")]
    public class CT_OleObjectLink : Iembedlink2
    {
        public List<ItL1> ItL1List { get; set; }
        public bool updateAutomatic { get; set; }

    }

    [XmlType("", "CT_OleObject")]
    public class CT_OleObject
    {
        public List<Ipic2> Ipic2List { get; set; }
        public AG_Ole AG_Ole { get; set; }
        public string progId { get; set; }

    }

    [XmlType("", "CT_Control")]
    public class CT_Control : Int1
    {
        public List<ItLpic2> ItLpic2List { get; set; }
        public AG_Ole AG_Ole { get; set; }

    }

    [XmlType("", "CT_ControlList")]
    public class CT_ControlList : IbgTrstnttL5
    {
        public List<Int1> Int1List { get; set; }

    }

    public enum ST_SlideId
    {

    }

    [XmlType("", "CT_SlideIdListEntry")]
    public class CT_SlideIdListEntry : IsldId1
    {
        public List<ItL1> ItL1List { get; set; }
        public ST_SlideId id { get; set; }


    }

    [XmlType("", "CT_SlideIdList")]
    public class CT_SlideIdList : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public List<IsldId1> IsldId1List { get; set; }

    }

    public enum ST_SlideMasterId
    {

    }

    [XmlType("", "CT_SlideMasterIdListEntry")]
    public class CT_SlideMasterIdListEntry : IdM1
    {
        public List<ItL1> ItL1List { get; set; }
        public ST_SlideMasterId id { get; set; }


    }

    [XmlType("", "CT_SlideMasterIdList")]
    public class CT_SlideMasterIdList : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public List<IdM1> IdM1List { get; set; }

    }

    [XmlType("", "CT_NotesMasterIdListEntry")]
    public class CT_NotesMasterIdListEntry : Ite1
    {
        public List<ItL1> ItL1List { get; set; }


    }

    [XmlType("", "CT_NotesMasterIdList")]
    public class CT_NotesMasterIdList : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public List<Ite1> Ite1List { get; set; }

    }

    [XmlType("", "CT_HandoutMasterIdListEntry")]
    public class CT_HandoutMasterIdListEntry : Ind1
    {
        public List<ItL1> ItL1List { get; set; }


    }

    [XmlType("", "CT_HandoutMasterIdList")]
    public class CT_HandoutMasterIdList : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public List<Ind1> Ind1List { get; set; }

    }

    [XmlType("", "CT_EmbeddedFontDataId")]
    public class CT_EmbeddedFontDataId : Ifontguboldalld5
    {


    }

    [XmlType("", "CT_EmbeddedFontListEntry")]
    public class CT_EmbeddedFontListEntry : Ibe1
    {
        public List<Ifontguboldalld5> Ifontguboldalld5List { get; set; }

    }

    [XmlType("", "CT_EmbeddedFontList")]
    public class CT_EmbeddedFontList : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public List<Ibe1> Ibe1List { get; set; }

    }

    [XmlType("", "CT_SmartTags")]
    public class CT_SmartTags : IdMtenddIsldSztearbestotstnsfaditL15
    {


    }

    [XmlType("", "CT_CustomShow")]
    public class CT_CustomShow : Ist1
    {
        public List<IdLtL2> IdLtL2List { get; set; }
        public ST_Name name { get; set; }
        public uint id { get; set; }

    }

    [XmlType("", "CT_CustomShowList")]
    public class CT_CustomShowList : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public List<Ist1> Ist1List { get; set; }

    }

    public enum ST_PhotoAlbumLayout
    {
        fitToSlide,
        _1pic,
        _2pic,
        _4pic,
        _1picTitle,
        _2picTitle,
        _4picTitle,

    }

    public enum ST_PhotoAlbumFrameShape
    {
        frameStyle1,
        frameStyle2,
        frameStyle3,
        frameStyle4,
        frameStyle5,
        frameStyle6,
        frameStyle7,

    }

    [XmlType("", "CT_PhotoAlbum")]
    public class CT_PhotoAlbum : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public List<ItL1> ItL1List { get; set; }
        public bool bw { get; set; }
        public bool showCaptions { get; set; }
        public ST_PhotoAlbumLayout layout { get; set; }
        public ST_PhotoAlbumFrameShape frame { get; set; }

    }

    public enum ST_SlideSizeCoordinate
    {

    }

    public enum ST_SlideSizeType
    {
        screen4x3,
        letter,
        A4,
        _35mm,
        overhead,
        banner,
        custom,
        ledger,
        A3,
        B4ISO,
        B5ISO,
        B4JIS,
        B5JIS,
        hagakiCard,
        screen16x9,
        screen16x10,

    }

    [XmlType("", "CT_SlideSize")]
    public class CT_SlideSize : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public ST_SlideSizeCoordinate cx { get; set; }
        public ST_SlideSizeCoordinate cy { get; set; }
        public ST_SlideSizeType type { get; set; }

    }

    [XmlType("", "CT_Kinsoku")]
    public class CT_Kinsoku : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public string lang { get; set; }
        public string invalStChars { get; set; }
        public string invalEndChars { get; set; }

    }

    public enum ST_BookmarkIdSeed
    {

    }

    [XmlType("", "CT_ModifyVerifier")]
    public class CT_ModifyVerifier : IdMtenddIsldSztearbestotstnsfaditL15
    {
        public string algorithmName { get; set; }
        public base64Binary hashValue { get; set; }
        public base64Binary saltValue { get; set; }
        public uint spinValue { get; set; }
        public ST_CryptProv cryptProviderType { get; set; }
        public ST_AlgClass cryptAlgorithmClass { get; set; }
        public ST_AlgType cryptAlgorithmType { get; set; }
        public uint cryptAlgorithmSid { get; set; }
        public uint spinCount { get; set; }
        public base64Binary saltData { get; set; }
        public base64Binary hashData { get; set; }
        public string cryptProvider { get; set; }
        public uint algIdExt { get; set; }
        public string algIdExtSource { get; set; }
        public uint cryptProviderTypeExt { get; set; }
        public string cryptProviderTypeExtSource { get; set; }

    }

    [XmlType("", "CT_Presentation")]
    public class CT_Presentation
    {
        public List<IdMtenddIsldSztearbestotstnsfaditL15> IdMtenddIsldSztearbestotstnsfaditL15List { get; set; }
        public ST_Percentage serverZoom { get; set; }
        public int firstSlideNum { get; set; }
        public bool showSpecialPlsOnTitleSld { get; set; }
        public bool rtl { get; set; }
        public bool removePersonalInfoOnSave { get; set; }
        public bool compatMode { get; set; }
        public bool strictFirstAndLastChars { get; set; }
        public bool embedTrueTypeFonts { get; set; }
        public bool saveSubsetFonts { get; set; }
        public bool autoCompressPictures { get; set; }
        public ST_BookmarkIdSeed bookmarkIdSeed { get; set; }
        public ST_ConformanceClass conformance { get; set; }

    }

    [XmlType("", "CT_HtmlPublishProperties")]
    public class CT_HtmlPublishProperties : ImlwebPrprnProwrMtL6
    {
        public List<ItL2> ItL2List { get; set; }
        public bool showSpeakerNotes { get; set; }
        public string target { get; set; }
        public string title { get; set; }


    }

    public enum ST_WebColorType
    {
        none,
        browser,
        presentationText,
        presentationAccent,
        whiteTextOnBlack,
        blackTextOnWhite,

    }

    public enum ST_WebScreenSize
    {
        _544x376,
        _640x480,
        _720x512,
        _800x600,
        _1024x768,
        _1152x882,
        _1152x900,
        _1280x1024,
        _1600x1200,
        _1800x1400,
        _1920x1200,

    }

    public enum ST_WebEncoding
    {

    }

    [XmlType("", "CT_WebProperties")]
    public class CT_WebProperties : ImlwebPrprnProwrMtL6
    {
        public List<ItL1> ItL1List { get; set; }
        public bool showAnimation { get; set; }
        public bool resizeGraphics { get; set; }
        public bool allowPng { get; set; }
        public bool relyOnVml { get; set; }
        public bool organizeInFolders { get; set; }
        public bool useLongFilenames { get; set; }
        public ST_WebScreenSize imgSz { get; set; }
        public ST_WebEncoding encoding { get; set; }
        public ST_WebColorType clr { get; set; }

    }

    public enum ST_PrintWhat
    {
        slides,
        handouts1,
        handouts2,
        handouts3,
        handouts4,
        handouts6,
        handouts9,
        notes,
        outline,

    }

    public enum ST_PrintColorMode
    {
        bw,
        gray,
        clr,

    }

    [XmlType("", "CT_PrintProperties")]
    public class CT_PrintProperties : ImlwebPrprnProwrMtL6
    {
        public List<ItL1> ItL1List { get; set; }
        public ST_PrintWhat prnWhat { get; set; }
        public ST_PrintColorMode clrMode { get; set; }
        public bool hiddenSlides { get; set; }
        public bool scaleToFitPaper { get; set; }
        public bool frameSlides { get; set; }

    }

    [XmlType("", "CT_ShowInfoBrowse")]
    public class CT_ShowInfoBrowse
    {
        public bool showScrollbar { get; set; }

    }

    [XmlType("", "CT_ShowInfoKiosk")]
    public class CT_ShowInfoKiosk
    {
        public uint restart { get; set; }

    }

    [XmlType("", "EG_ShowType")]
    public class EG_ShowType
    {


    }

    [XmlType("", "CT_ShowProperties")]
    public class CT_ShowProperties : ImlwebPrprnProwrMtL6
    {
        public List<InCtL4> InCtL4List { get; set; }
        public bool loop { get; set; }
        public bool showNarration { get; set; }
        public bool showAnimation { get; set; }
        public bool useTimings { get; set; }

    }

    [XmlType("", "CT_PresentationProperties")]
    public class CT_PresentationProperties
    {
        public List<ImlwebPrprnProwrMtL6> ImlwebPrprnProwrMtL6List { get; set; }

    }

    [XmlType("", "CT_HeaderFooter")]
    public class CT_HeaderFooter : IcSldanmihftL6, IcSlddLanmihfSttL8, IcSldhftL4, IcSldhftetL5
    {
        public List<ItL1> ItL1List { get; set; }
        public bool sldNum { get; set; }
        public bool hdr { get; set; }
        public bool ftr { get; set; }
        public bool dt { get; set; }

    }

    public enum ST_PlaceholderType
    {
        title,
        body,
        ctrTitle,
        subTitle,
        dt,
        sldNum,
        ftr,
        hdr,
        obj,
        chart,
        tbl,
        clipArt,
        dgm,
        media,
        sldImg,
        pic,

    }

    public enum ST_PlaceholderSize
    {
        full,
        half,
        quarter,

    }

    [XmlType("", "CT_Placeholder")]
    public class CT_Placeholder : IphsttL4
    {
        public List<ItL1> ItL1List { get; set; }
        public ST_PlaceholderType type { get; set; }
        public ST_Direction orient { get; set; }
        public ST_PlaceholderSize sz { get; set; }
        public uint idx { get; set; }
        public bool hasCustomPrompt { get; set; }

    }

    [XmlType("", "CT_ApplicationNonVisualDrawingProps")]
    public class CT_ApplicationNonVisualDrawingProps : IcNvPrvSnvPr3, IcNvPrvCnvPr3, IcNvPrvPnvPr3, IcNvPrvGnvPr3
    {
        public List<IphsttL4> IphsttL4List { get; set; }
        public bool isPhoto { get; set; }
        public bool userDrawn { get; set; }

    }

    [XmlType("", "CT_ShapeNonVisual")]
    public class CT_ShapeNonVisual : IShapeLevel1Member
    {
        public List<IcNvPrvSnvPr3> IcNvPrvSnvPr3List { get; set; }

    }

    [XmlType("", "CT_Shape")]
    public class CT_Shape : IShape
    {
        public List<IShapeLevel1Member> ISpspPrstyleBotL5List { get; set; }
        public bool useBgFill { get; set; }

    }

    [XmlType("", "CT_ConnectorNonVisual")]
    public class CT_ConnectorNonVisual : ICxspPrstyletL4
    {
        public List<IcNvPrvCnvPr3> IcNvPrvCnvPr3List { get; set; }

    }

    [XmlType("", "CT_Connector")]
    public class CT_Connector : IShape
    {
        public List<ICxspPrstyletL4> ICxspPrstyletL4List { get; set; }

    }

    [XmlType("", "CT_PictureNonVisual")]
    public class CT_PictureNonVisual : IPiipspPrstyletL5
    {
        public List<IcNvPrvPnvPr3> IcNvPrvPnvPr3List { get; set; }

    }

    [XmlType("", "CT_Picture")]
    public class CT_Picture : Ipic2, ItLpic2, IShape
    {
        public List<IPiipspPrstyletL5> IPiipspPrstyletL5List { get; set; }

    }

    [XmlType("", "CT_GraphicalObjectFrameNonVisual")]
    public class CT_GraphicalObjectFrameNonVisual : IGrxfrmkntL4
    {
        public List<IcNvPrvGnvPr3> IcNvPrvGnvPr3List { get; set; }

    }

    [XmlType("", "CT_GraphicalObjectFrame")]
    public class CT_GraphicalObjectFrame : IShape
    {
        public List<IGrxfrmkntL4> IGrxfrmkntL4List { get; set; }
        public ST_BlackWhiteMode bwMode { get; set; }

    }

    [XmlType("", "CT_GroupShapeNonVisual")]
    public class CT_GroupShapeNonVisual : IGrpStL4
    {
        public List<IcNvPrvGnvPr3> IcNvPrvGnvPr3List { get; set; }

    }

    [XmlType("", "CT_GroupShape")]
    public class CT_GroupShape : IShape, IbgTrstnttL5
    {
        public List<IGrpStL4> IGrpStL4List { get; set; }

    }

    [XmlType("", "CT_Rel")]
    public class CT_Rel : IShape
    {


    }

    [XmlType("", "EG_TopLevelSlide")]
    public class EG_TopLevelSlide
    {
        public List<object> List { get; set; }

    }

    [XmlType("", "EG_ChildSlide")]
    public class EG_ChildSlide
    {
        public List<object> List { get; set; }

    }

    [XmlType("", "AG_ChildSlide")]
    public class AG_ChildSlide
    {

    }

    [XmlType("", "CT_BackgroundProperties")]
    public class CT_BackgroundProperties
    {
        public List<ItL3> ItL3List { get; set; }
        public bool shadeToTitle { get; set; }

    }

    [XmlType("", "EG_Background")]
    public class EG_Background
    {


    }

    [XmlType("", "CT_Background")]
    public class CT_Background : IbgTrstnttL5
    {
        public List<I1> I1List { get; set; }
        public ST_BlackWhiteMode bwMode { get; set; }

    }

    [XmlType("", "CT_CommonSlideData")]
    public class CT_CommonSlideData : IcSldanmitL5, IcSldanmihftL6, IcSlddLanmihfSttL8, IcSldhftL4, IcSldhftetL5, IcSldtL3
    {
        public List<IbgTrstnttL5> IbgTrstnttL5List { get; set; }
        public string name { get; set; }

    }

    [XmlType("", "CT_Slide")]
    public class CT_Slide
    {
        public List<IcSldanmitL5> IcSldanmitL5List { get; set; }
        public AG_ChildSlide AG_ChildSlide { get; set; }
        public bool show { get; set; }

    }

    public enum ST_SlideLayoutType
    {
        title,
        tx,
        twoColTx,
        tbl,
        txAndChart,
        chartAndTx,
        dgm,
        chart,
        txAndClipArt,
        clipArtAndTx,
        titleOnly,
        blank,
        txAndObj,
        objAndTx,
        objOnly,
        obj,
        txAndMedia,
        mediaAndTx,
        objOverTx,
        txOverObj,
        txAndTwoObj,
        twoObjAndTx,
        twoObjOverTx,
        fourObj,
        vertTx,
        clipArtAndVertTx,
        vertTitleAndTx,
        vertTitleAndTxOverChart,
        twoObj,
        objAndTwoObj,
        twoObjAndObj,
        cust,
        secHead,
        twoTxTwoObj,
        objTx,
        picTx,

    }

    [XmlType("", "CT_SlideLayout")]
    public class CT_SlideLayout
    {
        public List<IcSldanmihftL6> IcSldanmihftL6List { get; set; }
        public AG_ChildSlide AG_ChildSlide { get; set; }
        public string matchingName { get; set; }
        public ST_SlideLayoutType type { get; set; }
        public bool preserve { get; set; }
        public bool userDrawn { get; set; }

    }

    [XmlType("", "CT_SlideMasterTextStyles")]
    public class CT_SlideMasterTextStyles : IcSlddLanmihfSttL8
    {
        public List<ItldyhetL4> ItldyhetL4List { get; set; }

    }

    public enum ST_SlideLayoutId
    {

    }

    [XmlType("", "CT_SlideLayoutIdListEntry")]
    public class CT_SlideLayoutIdListEntry : IdL1
    {
        public List<ItL1> ItL1List { get; set; }
        public ST_SlideLayoutId id { get; set; }


    }

    [XmlType("", "CT_SlideLayoutIdList")]
    public class CT_SlideLayoutIdList : IcSlddLanmihfSttL8
    {
        public List<IdL1> IdL1List { get; set; }

    }

    [XmlType("", "CT_SlideMaster")]
    public class CT_SlideMaster
    {
        public List<IcSlddLanmihfSttL8> IcSlddLanmihfSttL8List { get; set; }
        public bool preserve { get; set; }

    }

    [XmlType("", "CT_HandoutMaster")]
    public class CT_HandoutMaster
    {
        public List<IcSldhftL4> IcSldhftL4List { get; set; }

    }

    [XmlType("", "CT_NotesMaster")]
    public class CT_NotesMaster
    {
        public List<IcSldhftetL5> IcSldhftetL5List { get; set; }

    }

    [XmlType("", "CT_NotesSlide")]
    public class CT_NotesSlide
    {
        public List<IcSldtL3> IcSldtL3List { get; set; }
        public AG_ChildSlide AG_ChildSlide { get; set; }

    }

    [XmlType("", "CT_SlideSyncProperties")]
    public class CT_SlideSyncProperties
    {
        public List<ItL1> ItL1List { get; set; }
        public string serverSldId { get; set; }
        public dateTime serverSldModifiedTime { get; set; }
        public dateTime clientInsertedTime { get; set; }

    }

    [XmlType("", "CT_StringTag")]
    public class CT_StringTag : Itag1
    {
        public string name { get; set; }
        public string val { get; set; }

    }

    [XmlType("", "CT_TagList")]
    public class CT_TagList
    {
        public List<Itag1> Itag1List { get; set; }

    }

    public enum ST_SplitterBarState
    {
        minimized,
        restored,
        maximized,

    }

    public enum ST_ViewType
    {
        sldView,
        sldMasterView,
        notesView,
        handoutView,
        notesMasterView,
        outlineView,
        sldSorterView,
        sldThumbnailView,

    }

    [XmlType("", "CT_NormalViewPortion")]
    public class CT_NormalViewPortion : IststtL3
    {
        public ST_PositiveFixedPercentage sz { get; set; }
        public bool autoAdjust { get; set; }

    }

    [XmlType("", "CT_NormalViewProperties")]
    public class CT_NormalViewProperties : IProperty
    {
        public List<IststtL3> IststtL3List { get; set; }
        public bool showOutlineIcons { get; set; }
        public bool snapVertSplitter { get; set; }
        public ST_SplitterBarState vertBarState { get; set; }
        public ST_SplitterBarState horzBarState { get; set; }
        public bool preferSingleView { get; set; }

    }

    [XmlType("", "CT_CommonViewProperties")]
    public class CT_CommonViewProperties : IietL2, IiedLtL3, Iieid2
    {
        public List<I2D> Iscaleig2List { get; set; }
        public bool varScale { get; set; }

    }

    [XmlType("", "CT_NotesTextViewProperties")]
    public class CT_NotesTextViewProperties : IProperty
    {
        public List<IietL2> IietL2List { get; set; }

    }

    [XmlType("", "CT_OutlineViewSlideEntry")]
    public class CT_OutlineViewSlideEntry : Isld1
    {

        public bool collapse { get; set; }

    }

    [XmlType("", "CT_OutlineViewSlideList")]
    public class CT_OutlineViewSlideList : IiedLtL3
    {
        public List<Isld1> Isld1List { get; set; }

    }

    [XmlType("", "CT_OutlineViewProperties")]
    public class CT_OutlineViewProperties : IProperty
    {
        public List<IiedLtL3> IiedLtL3List { get; set; }

    }

    [XmlType("", "CT_SlideSorterViewProperties")]
    public class CT_SlideSorterViewProperties : IProperty
    {
        public List<IietL2> IietL2List { get; set; }
        public bool showFormatting { get; set; }

    }

    [XmlType("", "CT_Guide")]
    public class CT_Guide : Iguide1
    {
        public ST_Direction orient { get; set; }
        public ST_Coordinate32 pos { get; set; }

    }

    [XmlType("", "CT_GuideList")]
    public class CT_GuideList : Iieid2
    {
        public List<Iguide1> Iguide1List { get; set; }

    }

    [XmlType("", "CT_CommonSlideViewProperties")]
    public class CT_CommonSlideViewProperties : IldtL2
    {
        public List<Iieid2> Iieid2List { get; set; }
        public bool snapToGrid { get; set; }
        public bool snapToObjects { get; set; }
        public bool showGuides { get; set; }

    }

    [XmlType("", "CT_SlideViewProperties")]
    public class CT_SlideViewProperties : IProperty
    {
        public List<IldtL2> IldtL2List { get; set; }

    }

    [XmlType("", "CT_NotesViewProperties")]
    public class CT_NotesViewProperties : IProperty
    {
        public List<IldtL2> IldtL2List { get; set; }

    }

    [XmlType("", "CT_ViewProperties")]
    public class CT_ViewProperties
    {
        public List<IProperty> IrmidtltertteidtL8List { get; set; }
        public ST_ViewType lastView { get; set; }
        public bool showComments { get; set; }

    }

    [XmlType("", "a:CT_EmbeddedWAVAudioFile")]
    public class CT_EmbeddedWAVAudioFile : Isnd1, IdTdTspTgtkT4
    {

    }

    [XmlType("", "Isnd1")]
    public interface Isnd1
    {

    }

    [XmlType("", "IstSnddS2")]
    public interface ITransitionStartSoundAction
    {

    }

    [XmlType("", "Iinecrcsscombcovercutamfadewspluspullpushndndsplitriwedgewheelwipezoom21")]
    public interface ITranslation
    {

    }

    [XmlType("", "IsndActL3")]
    public interface ITransitionSoundAction
    {

    }

    [XmlType("", "ST_PositivePercentage")]
    public class ST_PositivePercentage
    {

    }

    [XmlType("", "ItmAbstmPct2")]
    public interface ItmAbstmPct2
    {

    }

    [XmlType("", "ST_ShapeID")]
    public class ST_ShapeID
    {

    }

    [XmlType("", "IarpRg2")]
    public interface IarpRg2
    {

    }

    [XmlType("", "a:CT_AnimationElementChoice")]
    public class CT_AnimationElementChoice : IbgsubSpeCtxElap5
    {

    }

    [XmlType("", "IbgsubSpeCtxElap5")]
    public interface IbgsubSpeCtxElap5
    {

    }

    [XmlType("", "ST_DrawingElementId")]
    public class ST_DrawingElementId
    {

    }

    [XmlType("", "IdTdTspTgtkT4")]
    public interface IdTdTspTgtkT4
    {

    }

    [XmlType("", "ItgtEltnrtn3")]
    public interface ItgtEltnrtn3
    {

    }

    [XmlType("", "Icond1")]
    public interface Icond1
    {

    }

    [XmlType("", "Iparseqexclanimimimimimimcmdsetaudiovideo13")]
    public interface Iparseqexclanimimimimimimcmdsetaudiovideo13
    {

    }

    [XmlType("", "ICodCdSerilbT6")]
    public interface ICodCdSerilbT6
    {

    }


    [XmlType("", "ST_Percentage")]
    public class ST_Percentage
    {

    }

    [XmlType("", "ST_PositiveFixedPercentage")]
    public class ST_PositiveFixedPercentage
    {

    }

    [XmlType("", "IcTn1")]
    public interface IcTn1
    {

    }

    [XmlType("", "IcTnevxt3")]
    public interface IcTnevxt3
    {

    }

    [XmlType("", "Itr1")]
    public interface Itr1
    {

    }

    [XmlType("", "IcTntgtEltr3")]
    public interface IcTntgtEltr3
    {

    }


    [XmlType("", "a:CT_Color")]
    public class CT_Color : IoltVtVrVrV5, IcBhvrbyfromto4, InCtL4
    {

    }

    [XmlType("", "IoltVtVrVrV5")]
    public interface IoltVtVrVrV5
    {

    }

    [XmlType("", "a:ST_PositiveFixedPercentage")]
    public class a : ST_PositiveFixedPercentage
    {

    }

    [XmlType("", "Ival1")]
    public interface Ival1
    {

    }

    [XmlType("", "Itav1")]
    public interface Itav1
    {

    }

    [XmlType("", "IcBhvrvL2")]
    public interface IcBhvrvL2
    {

    }

    [XmlType("", "ST_FixedPercentage")]
    public class ST_FixedPercentage
    {

    }

    [XmlType("", "ST_Angle")]
    public class ST_Angle
    {

    }

    [XmlType("", "Irgbhsl2")]
    public interface Irgbhsl2
    {

    }

    [XmlType("", "IcBhvrbyfromto4")]
    public interface IcBhvrbyfromto4
    {

    }

    [XmlType("", "IcBhvrog2")]
    public interface IcBhvrog2
    {

    }

    [XmlType("", "IcBhvrbyfromtorCtr5")]
    public interface IcBhvrbyfromtorCtr5
    {

    }

    [XmlType("", "IcBhvr1")]
    public interface IcBhvr1
    {

    }

    [XmlType("", "IcBhvrto2")]
    public interface IcBhvrto2
    {

    }

    [XmlType("", "IcTntgtEl2")]
    public interface IcTntgtEl2
    {

    }

    [XmlType("", "Ied1")]
    public interface Ied1
    {

    }

    [XmlType("", "ItnLst1")]
    public interface ItnLst1
    {

    }

    [XmlType("", "Itmpl1")]
    public interface Itmpl1
    {

    }

    [XmlType("", "Ipl1")]
    public interface Ipl1
    {

    }

    [XmlType("", "a:CT_AnimationGraphicalObjectBuildProperties")]
    public class CT_AnimationGraphicalObjectBuildProperties : IdAdS2
    {

    }

    [XmlType("", "IdAdS2")]
    public interface IdAdS2
    {

    }

    [XmlType("", "IbldPdDdOdG4")]
    public interface IbldPdDdOdG4
    {

    }

    [XmlType("", "ItnLstdLtL3")]
    public interface ItnLstdLtL3
    {

    }

    [XmlType("", "Isld1")]
    public interface Isld1
    {

    }

    [XmlType("", "Isttags2")]
    public interface Isttags2
    {

    }

    [XmlType("", "I1")]
    public interface I1
    {

    }

    [XmlType("", "token")]
    public class token
    {

    }

    [XmlType("", "ItL1")]
    public interface ItL1
    {

    }

    [XmlType("", "IAu1")]
    public interface IAu1
    {

    }

    [XmlType("", "a:CT_Point2D")]
    public class CT_Point2D : IpostexttL3, I2D
    {

    }

    [XmlType("", "IpostexttL3")]
    public interface IpostexttL3
    {

    }

    [XmlType("", "dateTime")]
    public class dateTime
    {

    }

    [XmlType("", "Icm1")]
    public interface Icm1
    {

    }

    [XmlType("", "Iembedlink2")]
    public interface Iembedlink2
    {

    }

    [XmlType("", "Ipic2")]
    public interface Ipic2
    {

    }

    [XmlType("", "ItLpic2")]
    public interface ItLpic2
    {

    }

    [XmlType("", "Int1")]
    public interface Int1
    {

    }

    [XmlType("", "IsldId1")]
    public interface IsldId1
    {

    }

    [XmlType("", "IdM1")]
    public interface IdM1
    {

    }

    [XmlType("", "Ite1")]
    public interface Ite1
    {

    }

    [XmlType("", "Ind1")]
    public interface Ind1
    {

    }

    [XmlType("", "a:CT_TextFont")]
    public class CT_TextFont : Ifontguboldalld5
    {

    }

    [XmlType("", "Ifontguboldalld5")]
    public interface Ifontguboldalld5
    {

    }

    [XmlType("", "Ibe1")]
    public interface Ibe1
    {

    }

    [XmlType("", "IdLtL2")]
    public interface IdLtL2
    {

    }

    [XmlType("", "Ist1")]
    public interface Ist1
    {

    }

    [XmlType("", "base64Binary")]
    public class base64Binary
    {

    }

    [XmlType("", "ST_CryptProv")]
    public class ST_CryptProv
    {

    }

    [XmlType("", "ST_AlgClass")]
    public class ST_AlgClass
    {

    }

    [XmlType("", "ST_AlgType")]
    public class ST_AlgType
    {

    }

    [XmlType("", "a:CT_PositiveSize2D")]
    public class CT_PositiveSize2D : IdMtenddIsldSztearbestotstnsfaditL15, IProperty
    {

    }

    [XmlType("", "a:CT_TextListStyle")]
    public class CT_TextListStyle : IdMtenddIsldSztearbestotstnsfaditL15, ItldyhetL4, IcSldhftetL5
    {

    }

    [XmlType("", "IdMtenddIsldSztearbestotstnsfaditL15")]
    public interface IdMtenddIsldSztearbestotstnsfaditL15
    {

    }

    [XmlType("", "ST_ConformanceClass")]
    public class ST_ConformanceClass
    {

    }

    [XmlType("", "ItL2")]
    public interface ItL2
    {

    }

    [XmlType("", "InCtL4")]
    public interface InCtL4
    {

    }

    [XmlType("", "a:CT_ColorMRU")]
    public class CT_ColorMRU : ImlwebPrprnProwrMtL6
    {

    }

    [XmlType("", "ImlwebPrprnProwrMtL6")]
    public interface ImlwebPrprnProwrMtL6
    {

    }

    [XmlType("", "IphsttL4")]
    public interface IphsttL4
    {

    }

    [XmlType("", "a:CT_NonVisualDrawingProps")]
    public class CT_NonVisualDrawingProps : IcNvPrvSnvPr3, IcNvPrvCnvPr3, IcNvPrvPnvPr3, IcNvPrvGnvPr3
    {

    }

    [XmlType("", "a:CT_NonVisualDrawingShapeProps")]
    public class CT_NonVisualDrawingShapeProps : IcNvPrvSnvPr3
    {

    }

    [XmlType("", "IcNvPrvSnvPr3")]
    public interface IcNvPrvSnvPr3
    {

    }

    [XmlType("", "a:CT_ShapeProperties")]
    public class CT_ShapeProperties : IShapeLevel1Member, ICxspPrstyletL4, IPiipspPrstyletL5
    {

    }

    [XmlType("", "a:CT_ShapeStyle")]
    public class CT_ShapeStyle : IShapeLevel1Member, ICxspPrstyletL4, IPiipspPrstyletL5
    {

    }

    [XmlType("", "a:CT_TextBody")]
    public class CT_TextBody : IShapeLevel1Member
    {

    }

    [XmlType("", "ISpspPrstyleBotL5")]
    public interface IShapeLevel1Member
    {

    }

    [XmlType("", "a:CT_NonVisualConnectorProperties")]
    public class CT_NonVisualConnectorProperties : IcNvPrvCnvPr3
    {

    }

    [XmlType("", "IcNvPrvCnvPr3")]
    public interface IcNvPrvCnvPr3
    {

    }

    [XmlType("", "ICxspPrstyletL4")]
    public interface ICxspPrstyletL4
    {

    }

    [XmlType("", "a:CT_NonVisualPictureProperties")]
    public class CT_NonVisualPictureProperties : IcNvPrvPnvPr3
    {

    }

    [XmlType("", "IcNvPrvPnvPr3")]
    public interface IcNvPrvPnvPr3
    {

    }

    [XmlType("", "a:CT_BlipFillProperties")]
    public class CT_BlipFillProperties : IPiipspPrstyletL5
    {

    }

    [XmlType("", "IPiipspPrstyletL5")]
    public interface IPiipspPrstyletL5
    {

    }

    [XmlType("", "a:CT_NonVisualGraphicFrameProperties")]
    public class CT_NonVisualGraphicFrameProperties : IcNvPrvGnvPr3
    {

    }

    [XmlType("", "IcNvPrvGnvPr3")]
    public interface IcNvPrvGnvPr3
    {

    }

    [XmlType("", "a:CT_Transform2D")]
    public class CT_Transform2D : IGrxfrmkntL4
    {

    }

    [XmlType("", "unknown")]
    public class unknown : IGrxfrmkntL4
    {

    }

    [XmlType("", "IGrxfrmkntL4")]
    public interface IGrxfrmkntL4
    {

    }

    [XmlType("", "ST_BlackWhiteMode")]
    public class ST_BlackWhiteMode
    {

    }

    [XmlType("", "a:CT_NonVisualGroupDrawingShapeProps")]
    public class CT_NonVisualGroupDrawingShapeProps : IcNvPrvGnvPr3
    {

    }

    [XmlType("", "a:CT_GroupShapeProperties")]
    public class CT_GroupShapeProperties : IGrpStL4
    {

    }

    [XmlType("", "IspgrpSpapcxnSppicnt6")]
    public interface IShape
    {

    }

    [XmlType("", "IGrpStL4")]
    public interface IGrpStL4
    {

    }

    [XmlType("", "ItL3")]
    public interface ItL3
    {

    }

    [XmlType("", "IbgTrstnttL5")]
    public interface IbgTrstnttL5
    {

    }

    [XmlType("", "IcSldanmitL5")]
    public interface IcSldanmitL5
    {

    }

    [XmlType("", "IcSldanmihftL6")]
    public interface IcSldanmihftL6
    {

    }

    [XmlType("", "ItldyhetL4")]
    public interface ItldyhetL4
    {

    }

    [XmlType("", "IdL1")]
    public interface IdL1
    {

    }

    [XmlType("", "IcSlddLanmihfSttL8")] // ����
    public interface IcSlddLanmihfSttL8
    {

    }

    [XmlType("", "IcSldhftL4")]
    public interface IcSldhftL4
    {

    }

    [XmlType("", "IcSldhftetL5")]
    public interface IcSldhftetL5
    {

    }

    [XmlType("", "IcSldtL3")]
    public interface IcSldtL3
    {

    }

    [XmlType("", "Itag1")]
    public interface Itag1
    {

    }

    [XmlType("", "IststtL3")]
    public interface IststtL3
    {

    }

    [XmlType("", "a:CT_Scale2D")]
    public class CT_Scale2D : I2D
    {

    }

    [XmlType("", "Iscaleig2")]
    public interface I2D
    {

    }

    [XmlType("", "IietL2")]
    public interface IietL2
    {

    }

    [XmlType("", "IiedLtL3")]
    public interface IiedLtL3
    {

    }

    [XmlType("", "ST_Coordinate32")]
    public class ST_Coordinate32
    {

    }

    [XmlType("", "Iguide1")]
    public interface Iguide1
    {

    }

    [XmlType("", "Iieid2")]
    public interface Iieid2
    {

    }

    [XmlType("", "IldtL2")]
    public interface IldtL2
    {

    }

    [XmlType("", "IrmidtltertteidtL8")]
    public interface IProperty
    {

    }

}
