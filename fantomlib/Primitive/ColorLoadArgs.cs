﻿using DocumentFormat.OpenXml.Packaging;

namespace Fantom.Primitive
{
    /// <summary>
    /// 获取颜色时需要传递的消息参数。
    /// </summary>
    internal class ColorLoadArgs
    {
        /// <summary>
        /// 在解析图片时会使用到。
        /// </summary>
        public readonly OpenXmlPart SlidePart;

        /// <summary>
        /// 在获得主题颜色时会使用到。
        /// </summary>
        public readonly Theme Theme;

        /// <summary>
        /// 与解析图片联合使用。
        /// </summary>
        public readonly MediaManager MediaManager;

        ///// <summary>
        ///// 顶层的应用对象。
        ///// </summary>
        //public readonly Application Application;

        public ColorLoadArgs(OpenXmlPart sldpart, Theme theme, MediaManager mediaManager)
        {
            // Application = application,
            SlidePart = sldpart;
            Theme = theme;
            MediaManager = mediaManager;
        }

    }
}
