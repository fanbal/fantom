﻿using DocumentFormat.OpenXml;

namespace Fantom.Animation
{
    public class AnimationFragment : TimeNodeListBase<AnimationBundle>
    {

        internal override IEnumerable<TimeNodeBase> GetChildren()
        {
            return this;
        }

        // 导入效果。
        internal static void LoadFragment(AnimationSequence seq, P.ParallelTimeNode par, PresentationImportArgs importArg)
        {
            var fra = new AnimationFragment();
            var ctn = par.GetFirstChild<P.CommonTimeNode>();

            TimeNodeBase.LoadTimeNodeBase(fra, new PresentationImportArgs(importArg) { ArgElement = ctn });

            foreach (P.ParallelTimeNode spar in ctn.ChildTimeNodeList)
            {
                AnimationBundle.LoadBundle(fra, spar, importArg);
            }
            fra.Parent = seq;
            seq.Add(fra);
        }


        internal override OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xPara = new P.ParallelTimeNode();
            var xCommonTimeNode = new P.CommonTimeNode();
            var xChildList = new P.ChildTimeNodeList();

            foreach (var child in this)
            {
                xChildList.AppendExtend(child.CreateTimeNodeBase(exportArgs));
            }

            xCommonTimeNode.ChildTimeNodeList = xChildList;
            xPara.CommonTimeNode = xCommonTimeNode;
            return xPara;
        }

    }
}
