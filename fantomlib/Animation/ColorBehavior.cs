﻿using DocumentFormat.OpenXml;

namespace Fantom.Animation
{
    /// <summary>
    /// 具有路径相关的信息的动画行为。
    /// </summary>
    public sealed class ColorBehavior : BehaviorBase
    {
        #region properties

        public override BehaviorPropertyType PropertyType => BehaviorPropertyType.Color;

        /// <summary>
        /// 初始纯色颜色。
        /// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
        /// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
        /// </summary>
        public ColorBase From { get; set; } = null;

        /// <summary>
        /// 终点纯色颜色。
        /// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
        /// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
        /// </summary>
        public ColorBase To { get; set; } = null;

        /// <summary>
        /// 相对变化颜色。
        /// </summary>
        public ColorBase By { get; set; }


        #endregion

        #region ctors
        internal ColorBehavior() { }
        #endregion

        #region static
        

        internal static ColorBehavior LoadBehaviorFromElement(OpenXmlElement element,PresentationImportArgs importArgs)
        {
            var anim = element as DocumentFormat.OpenXml.Presentation.AnimateColor;

            var bhv = new ColorBehavior();
            SetBehaviorFromElement(bhv, element);

            if (anim.ToColor is not null)
            {
                bhv.To = ColorBuilder.OpenXmlElementToColor(anim.ToColor.FirstChild, importArgs.ColorLoadArgs.Theme);
            }

            if (anim.FromColor is not null)
            {
                bhv.From = ColorBuilder.OpenXmlElementToColor(anim.FromColor.FirstChild, importArgs.ColorLoadArgs.Theme);
            }

            if (anim.ByColor is not null)
            {
                bhv.By = ColorBuilder.OpenXmlElementToColor(anim.ByColor.FirstChild, importArgs.ColorLoadArgs.Theme);
            }

            return bhv;
        }

        #endregion

        internal override OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xAnim = new P.AnimateColor();
            var xBehavaior = new P.CommonBehavior();
            var xCommonTimeNode = new P.CommonTimeNode();
            var xChildList = new P.ChildTimeNodeList();


            xCommonTimeNode.ChildTimeNodeList = xChildList;
            xBehavaior.CommonTimeNode = xCommonTimeNode;
            xAnim.CommonBehavior = xBehavaior;
            return xAnim;
        }
    }
}
