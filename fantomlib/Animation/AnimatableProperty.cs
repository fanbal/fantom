﻿namespace Fantom.Animation
{
    /// <summary>
    /// 可执行动画的属性类。
    /// </summary>
    public enum AnimatableProperty
    {
        None,
        X,
        Y,
        Visibility,
        Color,
    }


}
