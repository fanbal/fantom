﻿
using Fantom.Helper;

namespace Fantom.Animation
{


    public class ComplexValue
    {
        public enum ComplexValueType
        {
            Boolean, String, Double, Int32, Int64, Single
        }

        public ComplexValueType Type;

        public string StringValue;
        public bool BooleanValue;
        public double DoubleValue;
        public float SingleValue;
        public int Int32Value;
        public long Int64Value;

        internal static ComplexValue BuildComplexValue(PresentationImportArgs importArgs)
        {
            var xTimeListAnimationVariantType = importArgs.ArgElement as P.TimeListAnimationVariantType;

            if (xTimeListAnimationVariantType.BooleanVariantValue is not null)
            {
                var xValue = xTimeListAnimationVariantType.BooleanVariantValue.Val;
                return new ComplexValue() { Type = ComplexValueType.Boolean, BooleanValue = (xValue as OX.BooleanValue).Value };
            }
            else if (xTimeListAnimationVariantType.FloatVariantValue is not null)
            {
                var xValue = xTimeListAnimationVariantType.FloatVariantValue.Val;
                return new ComplexValue() { Type = ComplexValueType.Single, SingleValue = (xValue as OX.SingleValue).Value };
            }

            else if (xTimeListAnimationVariantType.IntegerVariantValue is not null)
            {
                var xValue = xTimeListAnimationVariantType.IntegerVariantValue.Val;
                return new ComplexValue() { Type = ComplexValueType.Int32, Int32Value = (xValue as OX.Int32Value).Value };
            }
            else if (xTimeListAnimationVariantType.ColorValue is not null)
            {
                var xValue = xTimeListAnimationVariantType.ColorValue;
                throw new Exception();
                //return new ComplexValue() { Type = ComplexValueType.Int64, Int64Value = (xValue as OX.Int64Value).Value };
            }
            else if (xTimeListAnimationVariantType.StringVariantValue is not null)
            {
                var xValue = xTimeListAnimationVariantType.StringVariantValue.Val;
                return new ComplexValue() { Type = ComplexValueType.String, StringValue = (xValue as OX.StringValue).Value };
            }
            else
            {
                throw new Exception("无法解析。");
            }
        }


        internal static ComplexValue BuildComplexValue(OX.OpenXmlSimpleType xValue, PresentationImportArgs importArgs)
        {

            var value = new ComplexValue();
            if (xValue is OX.BooleanValue)
            {
                return new ComplexValue() { Type = ComplexValueType.Boolean, BooleanValue = (xValue as OX.BooleanValue).Value };
            }
            else if (xValue is OX.SingleValue)
            {
                return new ComplexValue() { Type = ComplexValueType.Single, SingleValue = (xValue as OX.SingleValue).Value };
            }
            else if (xValue is OX.DoubleValue)
            {
                return new ComplexValue() { Type = ComplexValueType.Double, DoubleValue = (xValue as OX.DoubleValue).Value };
            }
            else if (xValue is OX.Int32Value)
            {
                return new ComplexValue() { Type = ComplexValueType.Int32, Int32Value = (xValue as OX.Int32Value).Value };
            }
            else if (xValue is OX.Int64Value)
            {
                return new ComplexValue() { Type = ComplexValueType.Int64, Int64Value = (xValue as OX.Int64Value).Value };
            }
            else if (xValue is OX.StringValue)
            {
                return new ComplexValue() { Type = ComplexValueType.String, StringValue = (xValue as OX.StringValue).Value };
            }
            else
            {
                throw new Exception("无法解析。");
            }
        }


        internal static OX.OpenXmlSimpleType CreateComplexValue(ComplexValue complexValue)
        {
            switch (complexValue.Type)
            {
                case ComplexValueType.Boolean:
                    return new OX.BooleanValue(complexValue.BooleanValue);
                case ComplexValueType.String:
                    return new OX.StringValue(complexValue.StringValue);
                case ComplexValueType.Double:
                    return new OX.DoubleValue(complexValue.DoubleValue);
                case ComplexValueType.Int32:
                    return new OX.Int32Value(complexValue.Int32Value);
                case ComplexValueType.Int64:
                    return new OX.Int64Value(complexValue.Int64Value);
                case ComplexValueType.Single:
                    return new OX.SingleValue(complexValue.SingleValue);
                default:
                    return null;
            }
        }

    }

    public sealed class SetBehavior : BehaviorBase
    {

        public override BehaviorPropertyType PropertyType => BehaviorPropertyType.Set;

        /// <summary>
        /// 可执行动画的属性，一般为 PPT 的 X 轴坐标与 Y 轴坐标。
        /// </summary>
        public AnimatableProperty Property
        {
            get { return base.BehaviorWrapper.AttributeList[0]; }
            set
            {
                base.BehaviorWrapper.AttributeList[0] = value;
                RaiseEvent();
            }
        }

        /// <summary>
        /// 其值以可执行动画的类型决定。
        /// </summary>
        public Emu To { get; set; }

        public ComplexValue To2 { get; set; }

        internal SetBehavior()
        {

        }


        /// <summary>
        /// 从指定动画描述中导入具体数据。
        /// </summary>
        /// <param name="anim"></param>
        /// <returns></returns>
        internal static SetBehavior LoadBehaviorFromElement(OX.OpenXmlElement element, PresentationImportArgs importArgs)
        {
            var xset = element as P.SetBehavior;
            var xbhv = xset.CommonBehavior;
            var bhv = new SetBehavior();
            SetBehaviorFromElement(bhv, element);


            if (xset.ToVariantValue is not null)
            {
                bhv.To2 = ComplexValue.BuildComplexValue(new PresentationImportArgs(importArgs) { ArgElement = xset.ToVariantValue });
            }

            return bhv;
        }

        internal override OX.OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xAnim = new P.SetBehavior();
            var xBehavaior = CreateBehaviorEntry(this, exportArgs);
            xAnim.ToVariantValue = new DocumentFormat.OpenXml.Presentation.ToVariantValue();

            switch (To2.Type)
            {
                case ComplexValue.ComplexValueType.Boolean:
                    xAnim.ToVariantValue.BooleanVariantValue = new P.BooleanVariantValue() { Val = new OX.BooleanValue(To2.BooleanValue) };
                    break;
                case ComplexValue.ComplexValueType.String:
                    xAnim.ToVariantValue.StringVariantValue = new P.StringVariantValue() { Val = new OX.StringValue(To2.StringValue) };
                    break;
                case ComplexValue.ComplexValueType.Int32:
                    xAnim.ToVariantValue.IntegerVariantValue = new P.IntegerVariantValue() { Val = new OX.Int32Value(To2.Int32Value) };
                    break;
                case ComplexValue.ComplexValueType.Single:
                    xAnim.ToVariantValue.FloatVariantValue = new P.FloatVariantValue() { Val = new OX.SingleValue(To2.SingleValue) };
                    break;
                default:
                    break;
            }

            xAnim.CommonBehavior = xBehavaior;
            return xAnim;
        }

    }

}
