﻿
namespace Fantom.Animation
{
    public sealed class PropertyBehavior : BehaviorBase
    {

        public override BehaviorPropertyType PropertyType => BehaviorPropertyType.Property;

        /// <summary>
        /// 可执行动画的属性，一般为 PPT 的 X 轴坐标与 Y 轴坐标。
        /// </summary>
        public AnimatableProperty Property
        {
            get { return base.BehaviorWrapper.AttributeList[0]; }
            set
            {
                base.BehaviorWrapper.AttributeList[0] = value;
                RaiseEvent();
            }
        }

        /// <summary>
        /// 其值以可执行动画的类型决定。
        /// </summary>
        public Emu From { get; set; }

        /// <summary>
        /// 其值以可执行动画的类型决定。
        /// </summary>
        public Emu To { get; set; }

        public Emu By { get; set; }

        /// <summary>
        /// 关键帧序列。
        /// </summary>
        public List<BaseAnimationKeyFramePoint> KeyFramePoints { get; }
            = new List<BaseAnimationKeyFramePoint>();

        internal PropertyBehavior()
        {

        }

        #region static

        public override string ToString()
        {
            var fmt1 = "{0,17}, tim: {1,9}%, val: {2,-10}, fml: {3,-10}";
            var fmt2 = "{0,17}, tim: {1,9}%, val: {2,-10}";
            var sb = new StringBuilder(base.ToString());
            sb.AppendLine();

            foreach (var point in KeyFramePoints)
            {
                if (point is IntAnimationKeyFramePoint)
                {
                    var p = point as IntAnimationKeyFramePoint;
                    sb.AppendLine(string.Format(fmt1, "int",
                    Math.Round(p.Time.ToPercent() * 100, 3), p.Value, p.Formula));
                }
                else if (point is FloatAnimationKeyFramePoint)
                {
                    var p = point as FloatAnimationKeyFramePoint;
                    sb.AppendLine(string.Format(fmt1, "float",
                    Math.Round(p.Time.ToPercent() * 100, 3), p.Value, p.Formula));
                }
                else if (point is BoolAnimationKeyFramePoint)
                {
                    var p = point as BoolAnimationKeyFramePoint;
                    sb.AppendLine(string.Format(fmt2, "bool",
                    Math.Round(p.Time.ToPercent() * 100, 3), p.Value));
                }
                else if (point is StringAnimationKeyFramePoint)
                {
                    var p = point as StringAnimationKeyFramePoint;
                    sb.AppendLine(string.Format(fmt1, "string",
                    Math.Round(p.Time.ToPercent() * 100, 3), p.Value, p.Formula));
                }
                else if (point is ColorAnimationKeyFramePoint)
                {
                    var p = point as ColorAnimationKeyFramePoint;
                    sb.AppendLine(string.Format(fmt2, "color",
                    Math.Round(p.Time.ToPercent() * 100, 3), p.Value));
                }

            }

            return sb.ToString();
        }


        /// <summary>
        /// 从指定动画描述中导入具体数据。
        /// </summary>
        internal static PropertyBehavior LoadBehaviorFromElement(OX.OpenXmlElement element, PresentationImportArgs importArgs)
        {
            var anim = element as DocumentFormat.OpenXml.Presentation.Animate;
            var xbhv = anim.CommonBehavior;

            var xavlst = anim.TimeAnimateValueList;

            var bhv = new PropertyBehavior();
            SetBehaviorFromElement(bhv, element);


            if (xbhv.From is not null) bhv.From = Emu.Parse(xbhv.From);

            if (xbhv.To is not null) bhv.To = Emu.Parse(xbhv.To);

            if (xavlst is not null)
            {
                foreach (DocumentFormat.OpenXml.Presentation.TimeAnimateValue keyPoint in xavlst)
                {
                    bhv.KeyFramePoints.Add(BaseAnimationKeyFramePoint.BuildFromElement(keyPoint, importArgs.ColorLoadArgs.Theme));
                }
            }

            //bhv.Property = GetAnimatableProperty(element);
            return bhv;
        }


        #endregion


        internal override OX.OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xAnim = new P.Animate();
            var xBehavaior = CreateBehaviorEntry(this, exportArgs);
            xAnim.From = From.ToString();
            xAnim.To = To.ToString();
            xAnim.By = By.ToString();

            xAnim.CalculationMode = P.AnimateBehaviorCalculateModeValues.Formula;
            xAnim.ValueType = P.AnimateBehaviorValues.Number;

            if (KeyFramePoints is not null)
            {
                xAnim.TimeAnimateValueList = new P.TimeAnimateValueList();

                foreach (var keypoint in KeyFramePoints)
                {
                    xAnim.TimeAnimateValueList.AppendExtend(BaseAnimationKeyFramePoint.CreateTimeAnimateValueEntry(keypoint, exportArgs));
                }
            }


            xAnim.CommonBehavior = xBehavaior;
            return xAnim;
        }

    }
}
