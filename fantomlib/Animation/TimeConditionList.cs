﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Animation
{

    public class TimeCondition
    {
        public abstract class TimeConditionMemberBase
        {
            public enum TimeConditionMemberType
            {
                Unknown, TargetElementConditionMember, TriggerTimeNodeIDConditionMember, TriggerRuntimeNodeNodeMember
            }

            public virtual TimeConditionMemberType Type => TimeConditionMemberType.Unknown;

            internal static TimeConditionMemberBase BuildEntry(PresentationImportArgs importArgs)
            {
                var xTimeConditionMember = importArgs.ArgElement;
                if (xTimeConditionMember is null) return null;
                if (xTimeConditionMember is P.TargetElement)
                    return TargetElementConditionMember.Build(importArgs);
                else if (xTimeConditionMember is P.TimeNode)
                    return TriggerTimeNodeIDConditionMember.Build(importArgs);
                else if (xTimeConditionMember is P.RuntimeNodeTrigger)
                    return TriggerRuntimeNodeNodeMember.Build(importArgs);
                else
                    throw new Exception("未知TimeConditionMemberBase");
            }

            internal static OX.OpenXmlElement CreateEntry(TimeConditionMemberBase timeConditionMember, PresentationExportArgs exportArgs)
            {
                if (timeConditionMember is null) return null;
                switch (timeConditionMember.Type)
                {
                    case TimeConditionMemberType.Unknown:
                        return null;
                    case TimeConditionMemberType.TargetElementConditionMember:
                        return TargetElementConditionMember.Create(timeConditionMember as TargetElementConditionMember, exportArgs);
                    case TimeConditionMemberType.TriggerTimeNodeIDConditionMember:
                        return TriggerTimeNodeIDConditionMember.Create(timeConditionMember as TriggerTimeNodeIDConditionMember, exportArgs);
                    case TimeConditionMemberType.TriggerRuntimeNodeNodeMember:
                        return TriggerRuntimeNodeNodeMember.Create(timeConditionMember as TriggerRuntimeNodeNodeMember, exportArgs);
                    default:
                        return null;
                }
            }
        }

        public class TargetElementConditionMember : TimeConditionMemberBase
        {
            public override TimeConditionMemberType Type => TimeConditionMemberType.TargetElementConditionMember;

            public TimeTargetElementBase TargetElement;

            internal static TargetElementConditionMember Build(PresentationImportArgs importArgs)
            {
                var xTimeConditionMember = importArgs.ArgElement as P.TargetElement;
                if (xTimeConditionMember is null) return null;
                var timeConditionMember = new TargetElementConditionMember();
                timeConditionMember.TargetElement = TimeTargetElementBase.BuildEntry(new PresentationImportArgs(importArgs) { ArgElement = xTimeConditionMember.FirstChild });
                return timeConditionMember;
            }


            internal static P.TargetElement Create(TargetElementConditionMember timeConditionMember, PresentationExportArgs exportArgs)
            {
                if (timeConditionMember is null) return null;
                var xTimeConditionMember = new P.TargetElement();
                xTimeConditionMember.AppendExtend(TimeTargetElementBase.CreateEntry(timeConditionMember.TargetElement, exportArgs));
                return xTimeConditionMember;
            }
        }

        public class TriggerTimeNodeIDConditionMember : TimeConditionMemberBase
        {
            public override TimeConditionMemberType Type => TimeConditionMemberType.TriggerTimeNodeIDConditionMember;

            public uint? TimeNodeID;

            internal static TriggerTimeNodeIDConditionMember Build(PresentationImportArgs importArgs)
            {
                var xTimeConditionMember = importArgs.ArgElement as P.TimeNode;
                if (xTimeConditionMember is null) return null;
                var timeConditionMember = new TriggerTimeNodeIDConditionMember();
                if (xTimeConditionMember.Val is not null) timeConditionMember.TimeNodeID = xTimeConditionMember.Val;
                return timeConditionMember;
            }


            internal static P.TimeNode Create(TriggerTimeNodeIDConditionMember timeConditionMember, PresentationExportArgs exportArgs)
            {
                if (timeConditionMember is null) return null;
                var xTimeConditionMember = new P.TimeNode();
                if (timeConditionMember.TimeNodeID is not null) xTimeConditionMember.Val = new OX.UInt32Value(timeConditionMember.TimeNodeID);
                return xTimeConditionMember;
            }
        }

        public class TriggerRuntimeNodeNodeMember : TimeConditionMemberBase
        {
            public override TimeConditionMemberType Type => TimeConditionMemberType.TriggerRuntimeNodeNodeMember;

            public TriggerRuntimeNodeType? NodeType;

            internal static TriggerRuntimeNodeNodeMember Build(PresentationImportArgs importArgs)
            {
                var xTimeConditionMember = importArgs.ArgElement as P.RuntimeNodeTrigger;
                if (xTimeConditionMember is null) return null;
                var timeConditionMember = new TriggerRuntimeNodeNodeMember();

                if (xTimeConditionMember.Val is not null)
                    timeConditionMember.NodeType = (TriggerRuntimeNodeType)xTimeConditionMember.Val.Value;

                return timeConditionMember;
            }


            internal static P.RuntimeNodeTrigger Create(TriggerRuntimeNodeNodeMember timeConditionMember, PresentationExportArgs exportArgs)
            {
                if (timeConditionMember is null) return null;
                var xTimeConditionMember = new P.RuntimeNodeTrigger();
                if (timeConditionMember.NodeType is not null) xTimeConditionMember.Val = (P.TriggerRuntimeNodeValues)timeConditionMember.NodeType;
                return xTimeConditionMember;
            }
        }


        public TimeConditionMemberBase TimeConditionMember;
        public TriggerEventType? Event;
        public AnimationTime Delay;

        internal static TimeCondition BuildTimeCondition(PresentationImportArgs importArgs)
        {
            var xCondition = importArgs.ArgElement as P.TimeListConditionalType;
            if (xCondition is null) return null;
            var condition = new TimeCondition();

            condition.TimeConditionMember = TimeConditionMemberBase.BuildEntry(new PresentationImportArgs(importArgs) { ArgElement = xCondition.FirstChild });
            if (xCondition.Event is not null) condition.Event = (TriggerEventType)xCondition.Event.Value;
            if (xCondition.Delay is not null) condition.Delay = AnimationTime.Build(xCondition.Delay, Emu.Num0);

            return condition;
        }

        internal static T CreateTimeCondition<T>(TimeCondition condition, PresentationExportArgs exportArgs)
            where T : P.TimeListConditionalType,new()
        {
            if (condition is null) return null;
            var xCondition = new T();
            xCondition.AppendExtend(TimeConditionMemberBase.CreateEntry(condition.TimeConditionMember, exportArgs));
            if (condition.Event is not null) xCondition.Event = (P.TriggerEventValues)condition.Event;
            if (condition.Delay is not null) xCondition.Delay = AnimationTime.Create(condition.Delay);
            return xCondition;
        }
    }
    public class TimeConditionList : List<TimeCondition>
    {


        internal static TimeConditionList BuildTimeConditionList(PresentationImportArgs importArgs)
        {
            var xConditionList = importArgs.ArgElement as P.TimeListTimeConditionalListType;
            if (xConditionList is null) return null;
            var conditionList = new TimeConditionList();

            foreach (var xChild in xConditionList)
            {
                var condition = TimeCondition.BuildTimeCondition(new PresentationImportArgs(importArgs) { ArgElement = xChild });
                if(condition is not null) conditionList.Add(condition);
            }

            return conditionList;
        }

        internal static T CreateTimeConditionList<T>(TimeConditionList conditionList, PresentationExportArgs exportArgs)
        where T : P.TimeListTimeConditionalListType, new()
        {
            if (conditionList is null) return null;
            var xConditionList = new T();

            foreach (var child in conditionList)
            {
                xConditionList.AppendExtend(TimeCondition.CreateTimeCondition<P.Condition>(child,exportArgs));
            }

            return xConditionList;
        }
    }
}
