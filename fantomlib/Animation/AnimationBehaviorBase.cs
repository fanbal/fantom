﻿using System.Linq;
namespace Fantom.Animation
{
    /// <summary>
    /// 抽象类，描述的是基动画。一个基动画代表一个具体的行为动画。
    /// </summary>
    public abstract class BehaviorBase : TimeNodeBase
    {
        /// <summary>
        /// 行为效果类型。
        /// </summary>
        public enum BehaviorPropertyType
        {
            Unknown, Set, Property, Rotation, Motion, Color, Scale, Filter
        }

        /// <summary>
        /// 获得当前行为的最后的时间位置。
        /// </summary>
        /// <returns></returns>
        internal Emu GetLastTime()
        {
            var delay = Emu.Num0;
            if (BaseWrapper.StartConditionList is not null)
            {

                foreach (var condition in BaseWrapper.StartConditionList)
                {
                    if (condition.Delay is not null)
                    {
                        if (condition.Delay.IsForever) return Emu.MaxValue;
                        else if (condition.Delay.IsIndefinite) continue;

                        if (condition.Delay.Value > delay)
                            delay = condition.Delay.Value;

                    }

                }
            }

            var dur = Emu.Num0;
            if (BaseWrapper.Duration is not null)
                if (BaseWrapper.Duration.IsForever) return Emu.MaxValue;
                else if (BaseWrapper.Duration.IsIndefinite == false) dur = BaseWrapper.Duration.Value;

            return dur + delay;

        }

        public class BehaviorBaseWrapper
        {
            public string TimeFilter;


            /// <summary>
            /// 动画是否可叠加。
            /// 请和 <see cref="IsAccumulate"/> 联合使用。
            /// <para>
            /// 如果您希望您的动画允许矢量叠加，
            /// 您需要将 <see cref="IsAccumulate"/> 与 <see cref="IsAccumulate"/> 
            /// 同时设为 <see cref="true"/> 。
            /// </para>
            /// 在 Fantom 中，为了实现动画更自由的组合，该默认值为 <see cref="true"/> 。
            /// </summary>
            public bool? IsAccumulate { get; set; }

            /// <summary>
            /// 动画是否允许与其他运行中的动画合并。
            /// 请和 <see cref="IsAccumulate"/> 联合使用。
            /// <para>
            /// 如果您希望您的动画允许矢量叠加，
            /// 您需要将 <see cref="IsAccumulate"/> 与 <see cref="IsAccumulate"/> 
            /// 同时设为 <see cref="true"/> 。
            /// </para>
            /// 在 Fantom 中，为了实现动画更自由的组合，该默认值为 <see cref="true"/> 。
            /// </summary>
            public bool? AllowAdditive { get; set; }


            public List<AnimatableProperty> AttributeList { get; set; }

        }

        public BehaviorBaseWrapper BehaviorWrapper = new BehaviorBaseWrapper();


        [RelationshipItemExportType(RelationshipExportType.ExportRoot, IsRef = true)]
        public ShapeBase Shape { get; set; } = null;

        public virtual BehaviorPropertyType PropertyType => BehaviorPropertyType.Unknown;


        public double Delay
        {
            get
            {
                if (BaseWrapper.StartConditionList is null) return 0d;
                var temp = Emu.Num0;
                foreach (var condition in BaseWrapper.StartConditionList)
                {
                    if (condition.Delay is not null)
                    {
                        if (condition.Delay.IsForever) return double.MaxValue;
                        else if (condition.Delay.IsIndefinite) return 0d;

                        if (condition.Delay.Value > temp)
                            temp = condition.Delay.Value;

                    }

                }

                return temp.ToSeconds();
            }
            set
            {
                if (BaseWrapper.StartConditionList is null)
                {
                    BaseWrapper.StartConditionList = new TimeConditionList();
                    BaseWrapper.StartConditionList.Add(new TimeCondition() { Delay = new AnimationTime() { Value = Emu.FromSeconds(value) } });
                }
                else
                {
                    int counter = 0;
                    foreach (var condition in BaseWrapper.StartConditionList)
                    {
                        if (condition.Delay is not null)
                        {
                            if (counter == 0)
                            {
                                condition.Delay.IsForever = false;
                                condition.Delay.IsIndefinite = false;
                                condition.Delay.Value = Emu.FromSeconds(value);
                            }
                            else
                            {
                                condition.Delay.IsForever = false;
                                condition.Delay.IsIndefinite = false;
                                condition.Delay.Value = Emu.Seconds0;
                            }

                            counter++;
                        }

                    }

                    if (counter == 0) BaseWrapper.StartConditionList.Add(new TimeCondition() { Delay = new AnimationTime() { Value = Emu.FromSeconds(value) } });

                }



                RaiseEvent();
            }
        }

        public double Duration
        {
            get
            {
                if (BaseWrapper.Duration is null) return 0d;
                if (BaseWrapper.Duration.IsForever) return double.MaxValue;
                if (BaseWrapper.Duration.IsIndefinite) return 0;
                return BaseWrapper.Duration.Value.ToSeconds();
            }
            set
            {
                if (BaseWrapper.Duration is null) BaseWrapper.Duration = new AnimationTime() { Value = Emu.FromSeconds(value) };
                BaseWrapper.Duration.Value = Emu.FromSeconds(value);
                BaseWrapper.Duration.IsForever = false;
                BaseWrapper.Duration.IsIndefinite = false;
                RaiseEvent();
            }
        }




        public void Delete()
        {
            (Parent as AnimationEffect).Remove(this);
        }


        /// <summary>
        /// 由关键字生成指定行为对象。
        /// </summary>
        internal static BehaviorBase LoadBehaviorFromKeyWord(OX.OpenXmlElement anim, PresentationImportArgs importArgs)
        {
            if (anim is P.SetBehavior)
            {
                return SetBehavior.LoadBehaviorFromElement(anim, importArgs);
            }
            else if (anim is P.AnimateRotation)
            {
                return RotationBehavior.LoadBehaviorFromElement(anim);
            }
            else if (anim is P.Animate)
            {
                return PropertyBehavior.LoadBehaviorFromElement(anim, importArgs);
            }
            else if (anim is P.AnimateMotion)
            {
                return MotionBehavior.LoadBehaviorFromElement(anim);
            }
            else if (anim is P.AnimateEffect)
            {
                return FilterBehavior.LoadBehaviorFromElement(anim);
            }
            else if (anim is P.AnimateScale)
            {
                return ScaleBehavior.LoadBehaviorFromElement(anim);
            }
            else if (anim is P.AnimateColor)
            {
                return ColorBehavior.LoadBehaviorFromElement(anim, importArgs);
            }
            else
            {
                throw new Exception($"{anim.GetType().Name} 类型尚未解析。");
            }

        }

        /// <summary>
        /// 设置行为的基础属性。
        /// </summary>
        /// <param name="bhv">未初始化赋值的空行为实例。</param>
        /// <param name="element">不同行为节点。</param>
        public static void SetBehaviorFromElement(BehaviorBase bhv, OX.OpenXmlElement element)
        {
            var xbhv = element.GetFirstChild<P.CommonBehavior>();

            if (xbhv.Accumulate is not null)
            {
                bhv.BehaviorWrapper.IsAccumulate = xbhv.Accumulate.Value == P.BehaviorAccumulateValues.Always;
            }

            if (xbhv.Additive is not null)
            {
                bhv.BehaviorWrapper.AllowAdditive = xbhv.Additive.Value == P.BehaviorAdditiveValues.Sum;
            }



        }

        public override string ToString()
        {
            if (Shape is null)
            {
                return $"{GetType().Name}, delay: {Delay}, duration: {Duration}, 无图形绑定";
            }
            else
            {
                return $"{GetType().Name}, delay: {Delay}, duration: {Duration}, {Shape.Name}";
            }


        }

        [Obsolete("逻辑过时", true)]
        public static AnimatableProperty? GetAnimatableProperty(OX.OpenXmlElement element)
        {
            var xbhv = element.GetFirstChild<P.CommonBehavior>();
            if (xbhv is null) return null;
            if (xbhv.AttributeNameList is null) return null;
            var attname = xbhv.AttributeNameList.GetFirstChild<P.AttributeName>();
            switch (attname.InnerText)
            {
                case "ppt_x":
                    return AnimatableProperty.X;
                case "ppt_y":
                    return AnimatableProperty.Y;
                case "style.visibility":
                    return AnimatableProperty.Visibility;
                case "style.color":
                case "fillcolor":
                case "stroke.color":
                case "ppt_c":
                    return AnimatableProperty.Color;
                default:
                    return AnimatableProperty.None;
            }
        }


        public static AnimatableProperty? GetAnimatableProperty(string text)
        {
            switch (text)
            {
                case "ppt_x":
                    return AnimatableProperty.X;
                case "ppt_y":
                    return AnimatableProperty.Y;
                case "style.visibility":
                    return AnimatableProperty.Visibility;
                case "style.color":
                case "fillcolor":
                case "stroke.color":
                case "ppt_c":
                    return AnimatableProperty.Color;
                default:
                    return AnimatableProperty.None;
            }
        }

        public static string ExportAnimatableProperty(AnimatableProperty? animatableProperty)
        {
            if (animatableProperty is null) return null;
            switch (animatableProperty.Value)
            {
                case AnimatableProperty.None:
                    break;
                case AnimatableProperty.X:
                    return "ppt_x";
                case AnimatableProperty.Y:
                    return "ppt_y";
                case AnimatableProperty.Visibility:
                    return "style.visibility";
                case AnimatableProperty.Color:
                    return "ppt_c";
                default:
                    return "unknown";
            }
            return null;
        }



        internal static void LoadBehaviorEntry(AnimationEffect eff, OX.OpenXmlElement anim, PresentationImportArgs importArg)
        {
            var cbhv = anim.GetFirstChild<P.CommonBehavior>();

            var bhv = Animation.BehaviorBase.LoadBehaviorFromKeyWord(anim, importArg);
            LoadTimeNodeBase(bhv, new PresentationImportArgs(importArg) { ArgElement = cbhv.CommonTimeNode });

            var id = uint.Parse(cbhv.TargetElement.ShapeTarget.ShapeId);

            if (importArg.ShapeMapper.ContainsKey(id)) bhv.Shape = importArg.ShapeMapper[id];

            if (cbhv.AttributeNameList is not null)
            {
                bhv.BehaviorWrapper.AttributeList = new List<AnimatableProperty>();
                foreach (var item in cbhv.AttributeNameList.OfType<P.AttributeName>())
                {
                    var resultType = GetAnimatableProperty(item.InnerText);
                    if (resultType is not null) bhv.BehaviorWrapper.AttributeList.Add(resultType.Value);
                }
            }


            bhv.Parent = eff;
            eff.Add(bhv);
        }


        internal static P.CommonBehavior CreateBehaviorEntry(BehaviorBase behaviorBase, PresentationExportArgs exportArgs)
        {
            var xBehavior = new P.CommonBehavior();
            var xCommonTimeNode = CreateCommonTimeNode(behaviorBase);
            xBehavior.CommonTimeNode = xCommonTimeNode;
            xBehavior.TargetElement = new P.TargetElement();


            if (behaviorBase.Shape is not null)
            {
                var xTargetElement = new P.TargetElement();
                var xShapeTarget = new P.ShapeTarget();
                xShapeTarget.ShapeId = behaviorBase.Shape._exportID.ToString();
                xTargetElement.ShapeTarget = xShapeTarget;
                xBehavior.TargetElement = xTargetElement;
            }

            if (behaviorBase.BehaviorWrapper.AttributeList is not null)
            {
                xBehavior.AttributeNameList = new P.AttributeNameList();
                foreach (var item in behaviorBase.BehaviorWrapper.AttributeList)
                {
                    xBehavior.AttributeNameList.Append(new P.AttributeName() { InnerXml = ExportAnimatableProperty(item) });
                }
            }

            return xBehavior;

        }



    }
}
