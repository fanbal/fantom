﻿using DocumentFormat.OpenXml;
using System.Runtime.Intrinsics.X86;

namespace Fantom.Animation
{
    public class AnimationSequence : TimeNodeListBase<AnimationFragment>
    {

        internal override IEnumerable<TimeNodeBase> GetChildren()
        {
            return this;
        }


        // 载入单条序列。
        internal static void LoadSequence(TimeLine timeLine, P.SequenceTimeNode xseq, PresentationImportArgs importArg)
        {
            var seq = new AnimationSequence();
            var ctn = xseq.GetFirstChild<P.CommonTimeNode>();

            TimeNodeBase.LoadTimeNodeBase(seq, new PresentationImportArgs(importArg) { ArgElement = ctn });

            if (ctn.NodeType.Value != P.TimeNodeValues.MainSequence)
            {
                timeLine.InteractiveSequences.Add(seq);
            }
            else
            {
                timeLine.MainSequence = seq;
            }

            foreach (P.ParallelTimeNode spar in ctn.ChildTimeNodeList)
            {
                AnimationFragment.LoadFragment(seq, spar, importArg);
            }
            seq.Parent = timeLine;

        }


        internal override OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xSequence = new P.SequenceTimeNode();
            var xCommonTimeNode = new P.CommonTimeNode();
            var xChildList = new P.ChildTimeNodeList();

            foreach (var child in this)
            {
                xChildList.AppendExtend(child.CreateTimeNodeBase(exportArgs));
            }

            xCommonTimeNode.ChildTimeNodeList = xChildList;
            xSequence.CommonTimeNode = xCommonTimeNode;
            return xSequence;
        }

    }
}
