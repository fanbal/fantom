﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Animation
{
    public class TimeTargetElementBase
    {
        // TODO 完成对目标的绑定
        public enum TimeTargetElementType
        {
            Unknown, SlideTargetElement, SoundTargetElement, ShapeTargetElement, InkTargetElement
        }

        public virtual TimeTargetElementType Type => TimeTargetElementType.Unknown;


        public class SlideTargetElement : TimeTargetElementBase
        {
            public override TimeTargetElementType Type => TimeTargetElementType.SlideTargetElement;


            internal static SlideTargetElement Build(PresentationImportArgs importArgs)
            {
                var xTargetElement = importArgs.ArgElement as P.SlideTarget;
                if (xTargetElement is null) return null;
                var targetElement = new SlideTargetElement();
                return targetElement;
            }


            internal static P.SlideTarget Create(SlideTargetElement targetElement, PresentationExportArgs exportArgs)
            {
                if (targetElement is null) return null;
                var xTargetElement = new P.SlideTarget();
                return xTargetElement;
            }
        }

        public class SoundTargetElement : TimeTargetElementBase
        {
            public override TimeTargetElementType Type => TimeTargetElementType.SoundTargetElement;

            internal static SoundTargetElement Build(PresentationImportArgs importArgs)
            {
                var xTargetElement = importArgs.ArgElement as P.SoundTarget;
                if (xTargetElement is null) return null;
                var targetElement = new SoundTargetElement();
                return targetElement;
            }

            internal static P.SoundTarget Create(SoundTargetElement targetElement, PresentationExportArgs exportArgs)
            {
                if (targetElement is null) return null;
                var xTargetElement = new P.SoundTarget();
                return xTargetElement;
            }
        }

        public class ShapeTargetElement : TimeTargetElementBase
        {
            public override TimeTargetElementType Type => TimeTargetElementType.ShapeTargetElement;


            internal static ShapeTargetElement Build(PresentationImportArgs importArgs)
            {
                var xTargetElement = importArgs.ArgElement as P.ShapeTarget;
                if (xTargetElement is null) return null;
                var targetElement = new ShapeTargetElement();
                return targetElement;
            }

            internal static P.ShapeTarget Create(ShapeTargetElement targetElement, PresentationExportArgs exportArgs)
            {
                if (targetElement is null) return null;
                var xTargetElement = new P.ShapeTarget();
                return xTargetElement;
            }
        }

        public class InkTargetElement : TimeTargetElementBase
        {
            public override TimeTargetElementType Type => TimeTargetElementType.InkTargetElement;


            internal static InkTargetElement Build(PresentationImportArgs importArgs)
            {
                var xTargetElement = importArgs.ArgElement as P.InkTarget;
                if (xTargetElement is null) return null;
                var targetElement = new InkTargetElement();
                return targetElement;
            }

            internal static P.InkTarget Create(InkTargetElement targetElement, PresentationExportArgs exportArgs)
            {
                if (targetElement is null) return null;
                var xTargetElement = new P.InkTarget();
                return xTargetElement;
            }
        }



        internal static TimeTargetElementBase BuildEntry(PresentationImportArgs importArgs)
        {
            var xTimeTargetElement = importArgs.ArgElement;
            if (xTimeTargetElement is null) return null;
            if (xTimeTargetElement is P.SlideTarget)
                return SlideTargetElement.Build(importArgs);
            else if (xTimeTargetElement is P.SoundTarget)
                return SoundTargetElement.Build(importArgs);
            else if (xTimeTargetElement is P.ShapeTarget)
                return ShapeTargetElement.Build(importArgs);
            else if (xTimeTargetElement is P.InkTarget)
                return InkTargetElement.Build(importArgs);
            else
                throw new Exception("未知 TargetElement");
        }


        internal static OX.OpenXmlElement CreateEntry(TimeTargetElementBase timeTargetElement, PresentationExportArgs exportArgs)
        {
            switch (timeTargetElement.Type)
            {
                case TimeTargetElementType.Unknown:
                    return null;
                case TimeTargetElementType.SlideTargetElement:
                    return SlideTargetElement.Create(timeTargetElement as SlideTargetElement, exportArgs);
                case TimeTargetElementType.SoundTargetElement:
                    return SoundTargetElement.Create(timeTargetElement as SoundTargetElement, exportArgs);
                case TimeTargetElementType.ShapeTargetElement:
                    return ShapeTargetElement.Create(timeTargetElement as ShapeTargetElement, exportArgs);
                case TimeTargetElementType.InkTargetElement:
                    return InkTargetElement.Create(timeTargetElement as InkTargetElement, exportArgs);
                default:
                    return null;
            }
        }

    }
}
