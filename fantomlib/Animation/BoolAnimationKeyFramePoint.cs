﻿namespace Fantom.Animation
{
    /// <summary>
    /// 以颜色为主题的节点。
    /// </summary>
    public sealed class BoolAnimationKeyFramePoint : BaseAnimationKeyFramePoint
    {
        #region porperties

        /// <summary>
        /// 自变量。两帧点之间的差值变化为随时间线性变化。
        /// </summary>
        public bool Value { get; set; }

        #endregion

        #region ctors
        internal BoolAnimationKeyFramePoint() { }
        #endregion

        #region static

        /// <summary>
        /// 从节点构造动画点。
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        internal static BoolAnimationKeyFramePoint BuildFromElement(P.TimeAnimateValue element)
        {
            var p = new BoolAnimationKeyFramePoint();
            var varValue = element.VariantValue;
            p.Value = varValue.BooleanVariantValue.Val;

            return p;

        }

        internal override P.TimeAnimateValue Create(PresentationExportArgs exportArgs)
        {
            var xTimeAnimValue = new P.TimeAnimateValue();
            xTimeAnimValue.VariantValue = new P.VariantValue();
            xTimeAnimValue.Time = Time.ToString();
            xTimeAnimValue.VariantValue.BooleanVariantValue = new P.BooleanVariantValue() { InnerXml = Value.ToString() };
            return xTimeAnimValue;
        }



        #endregion

    }



}
