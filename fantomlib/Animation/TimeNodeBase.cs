﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Animation
{
    public class TimeNodeBase : EchoBaseObject
    {


        public class TimeNodeWrapper
        {

            public int? PresetID;
            public TimeNodePresetClassType? PresetClass;
            public int? PresetSubTypeID;

            public TimeNodeFillType? FillType;
            public TimeNodeType? TimeNodeType;

            public TimeConditionList StartConditionList;
            public TimeConditionList EndConditionList;
            public TimeCondition EndSyncCondition;

            public AnimationTime Duration { get; set; }

            public bool? AfterEffect { get; set; }
            public bool? AutoReverse { get; set; } = false;

            public Emu? Accelerate { get; set; } = Emu.Num0;

            public Emu? Decelerate { get; set; } = Emu.Num0;

            public Emu? BounceEnd { get; set; } = Emu.Num0;

            public AnimationCount RepeatCount { get; set; }
            public AnimationTime RepeatDuration { get; set; }

            public bool? IsRewindAtEnd { get; set; } = false;

        }

        public TimeNodeWrapper BaseWrapper = new TimeNodeWrapper();


        internal virtual IEnumerable<TimeNodeBase> GetChildren()
        {
            return new List<TimeNodeBase>();
        }

        internal static void LoadTimeNodeBase(TimeNodeBase timeNodeBase, PresentationImportArgs importArgs)
        {

            var xCommonTimeNode = importArgs.ArgElement as P.CommonTimeNode;

            if (xCommonTimeNode is null) return;

            if (xCommonTimeNode.Id is not null) timeNodeBase._exportID = xCommonTimeNode.Id;

            if (xCommonTimeNode.Fill is not null) timeNodeBase.BaseWrapper.FillType = (TimeNodeFillType)xCommonTimeNode.Fill.Value;

            if (xCommonTimeNode.PresetId is not null) timeNodeBase.BaseWrapper.PresetID = xCommonTimeNode.PresetId;
            if (xCommonTimeNode.PresetSubtype is not null) timeNodeBase.BaseWrapper.PresetSubTypeID = xCommonTimeNode.PresetSubtype;
            if (xCommonTimeNode.PresetClass is not null) timeNodeBase.BaseWrapper.PresetClass = (TimeNodePresetClassType)xCommonTimeNode.PresetClass.Value;

            if (xCommonTimeNode.NodeType is not null) timeNodeBase.BaseWrapper.TimeNodeType = (TimeNodeType)xCommonTimeNode.NodeType.Value;

            if (xCommonTimeNode.Restart is not null) timeNodeBase.BaseWrapper.IsRewindAtEnd = xCommonTimeNode.Restart.Value == P.TimeNodeRestartValues.Never;

            if (xCommonTimeNode.AfterEffect is not null) timeNodeBase.BaseWrapper.AfterEffect = xCommonTimeNode.AfterEffect;

            if (xCommonTimeNode.Acceleration is not null) timeNodeBase.BaseWrapper.Accelerate = xCommonTimeNode.Acceleration;

            if (xCommonTimeNode.Deceleration is not null) timeNodeBase.BaseWrapper.Decelerate = xCommonTimeNode.Deceleration;

            if (xCommonTimeNode.AutoReverse is not null) timeNodeBase.BaseWrapper.AutoReverse = xCommonTimeNode.AutoReverse;

            if (xCommonTimeNode.PresetBounceEnd is not null) timeNodeBase.BaseWrapper.BounceEnd = xCommonTimeNode.PresetBounceEnd;

            if (xCommonTimeNode.RepeatCount is not null) timeNodeBase.BaseWrapper.RepeatCount = AnimationCount.Build(xCommonTimeNode.RepeatCount, Emu.Double1);

            if (xCommonTimeNode.RepeatDuration is not null) timeNodeBase.BaseWrapper.RepeatDuration = AnimationTime.Build(xCommonTimeNode.RepeatDuration, Emu.Seconds1);

            timeNodeBase.BaseWrapper.StartConditionList = TimeConditionList.BuildTimeConditionList(new PresentationImportArgs(importArgs) { ArgElement = xCommonTimeNode.StartConditionList });

            timeNodeBase.BaseWrapper.EndConditionList = TimeConditionList.BuildTimeConditionList(new PresentationImportArgs(importArgs) { ArgElement = xCommonTimeNode.EndConditionList });
            timeNodeBase.BaseWrapper.EndSyncCondition = TimeCondition.BuildTimeCondition(new PresentationImportArgs(importArgs) { ArgElement = xCommonTimeNode.EndSync });

            if (xCommonTimeNode.Duration is not null) timeNodeBase.BaseWrapper.Duration = AnimationTime.Build(xCommonTimeNode.Duration, Emu.Seconds1 * 0.5);


            return;
        }

        internal virtual OX.OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            return null;
        } 


        internal static P.CommonTimeNode CreateCommonTimeNode(TimeNodeBase timeNode)
        {
            var xCommonTimeNode = new P.CommonTimeNode();
            var wrapper = timeNode.BaseWrapper;
            xCommonTimeNode.Acceleration = wrapper.Accelerate.ToOX32();
            xCommonTimeNode.AfterEffect = wrapper.AfterEffect;

            return xCommonTimeNode;
        }
    }
}
