﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Animation
{
    public class AnimationTime
    {
        public const string DURATION_FOREVER = "forever";

        public const string DURATION_INDEFINEITE = "indefinite";

        /// <summary>
        /// 时间是否为无限。
        /// </summary>
        public bool IsForever = false;

        /// <summary>
        /// 是否未定义。
        /// </summary>
        public bool IsIndefinite = false;

        /// <summary>
        /// 具体的时间值。
        /// </summary>
        public Emu Value;


        internal static AnimationTime Build(OX.StringValue xDuration, Emu defaultValue)
        {
            if (xDuration is null) return new AnimationTime() { IsForever = false, Value = defaultValue };
            if (xDuration.Value == DURATION_FOREVER) return new AnimationTime() { IsForever = true, Value = defaultValue };
            if(xDuration.Value == DURATION_INDEFINEITE) return new AnimationTime() { IsIndefinite = true, Value = defaultValue };
            return new AnimationTime() { IsForever = false, Value = Emu.Parse(xDuration.Value) };
        }


        internal static OX.StringValue Create(AnimationTime duration)
        {
            if (duration.IsForever) return new OX.StringValue(DURATION_FOREVER);
            if (duration.IsIndefinite) return new OX.StringValue(DURATION_INDEFINEITE);
            return new OX.StringValue(duration.Value.ToString());
        }


    }
}
