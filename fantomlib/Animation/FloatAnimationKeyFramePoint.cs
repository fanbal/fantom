﻿namespace Fantom.Animation
{
    public sealed class FloatAnimationKeyFramePoint : BaseAnimationKeyFramePoint
    {
        #region porperties

        /// <summary>
        /// 自变量。两帧点之间的差值变化为随时间线性变化。
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// 表达式。其自变量为 <see cref="Value"/> PPT的动画解析函数。
        /// </summary>
        public string Formula { get; set; }

        #endregion

        #region ctors
        internal FloatAnimationKeyFramePoint() { }
        #endregion

        #region static

        /// <summary>
        /// 从节点构造动画点。
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        internal static FloatAnimationKeyFramePoint BuildFromElement(P.TimeAnimateValue element)
        {
            var p = new FloatAnimationKeyFramePoint();
            var varValue = element.VariantValue;
            p.Value = varValue.FloatVariantValue.Val;
            p.Formula = element.Fomula;

            return p;

        }

        internal override P.TimeAnimateValue Create(PresentationExportArgs exportArgs)
        {
            var xTimeAnimValue = new P.TimeAnimateValue();
            xTimeAnimValue.VariantValue = new P.VariantValue();
            xTimeAnimValue.Time = Time.ToString();
            xTimeAnimValue.Fomula = Formula;
            xTimeAnimValue.VariantValue.FloatVariantValue = new P.FloatVariantValue() { InnerXml = Value.ToString() };
            return xTimeAnimValue;
        }


        #endregion

    }

}
