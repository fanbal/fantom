﻿namespace Fantom.Animation
{
    public sealed class IntAnimationKeyFramePoint : BaseAnimationKeyFramePoint
    {
        #region porperties

        /// <summary>
        /// 自变量。两帧点之间的差值变化为随时间线性变化。
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// 表达式。其自变量为 <see cref="Value"/> PPT的动画解析函数。
        /// </summary>
        public string Formula { get; set; }

        #endregion


        #region ctors
        internal IntAnimationKeyFramePoint() { }
        #endregion

        #region static

        /// <summary>
        /// 从节点构造动画点。
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        internal static IntAnimationKeyFramePoint BuildFromElement(P.TimeAnimateValue element)
        {
            var p = new IntAnimationKeyFramePoint();
            var varValue = element.VariantValue;
            p.Value = varValue.IntegerVariantValue.Val;
            p.Formula = element.Fomula;

            return p;

        }


        internal override P.TimeAnimateValue Create(PresentationExportArgs exportArgs)
        {
            var xTimeAnimValue = new P.TimeAnimateValue();
            xTimeAnimValue.VariantValue = new P.VariantValue();
            xTimeAnimValue.Time = Time.ToString();
            xTimeAnimValue.Fomula = Formula;
            xTimeAnimValue.VariantValue.IntegerVariantValue = new P.IntegerVariantValue() { InnerXml = Value.ToString()};
            return xTimeAnimValue;
        }

        #endregion

    }

}
