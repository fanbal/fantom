﻿
namespace Fantom.Animation
{

    /// <summary>
    /// 动画帧点，其子类包括：
    /// <see cref="IntAnimationKeyFramePoint"/>，
    /// <see cref="FloatAnimationKeyFramePoint"/>，
    /// <see cref="StringAnimationKeyFramePoint"/>，
    /// <see cref="BoolAnimationKeyFramePoint"/>，
    /// <see cref="ColorAnimationKeyFramePoint"/>。
    /// </summary>
    public abstract class BaseAnimationKeyFramePoint : EchoBaseObject
    {
        /// <summary>
        /// 当前关键帧值的时间（Emu 表示 比例）。
        /// </summary>
        public Emu Time { get; set; }
        



        /// <summary>
        /// 从节点构造动画点。
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        internal static BaseAnimationKeyFramePoint BuildFromElement(P.TimeAnimateValue element, Theme theme)
        {
            BaseAnimationKeyFramePoint p = null;
            switch (element.VariantValue.FirstChild.LocalName)
            {
                case "clrVal":
                    p = ColorAnimationKeyFramePoint.BuildFromElement(element, theme);
                    break;
                case "intVal":
                    p = IntAnimationKeyFramePoint.BuildFromElement(element);
                    break;
                case "fltVal":
                    p = FloatAnimationKeyFramePoint.BuildFromElement(element);
                    break;
                case "strVal":
                    p = StringAnimationKeyFramePoint.BuildFromElement(element);
                    break;
                case "boolVal":
                    p = BoolAnimationKeyFramePoint.BuildFromElement(element);
                    break;
                default:
                    throw new Exception("未知的动画帧点。");
            }
            p.Time = Emu.Parse(element.Time);

            return p;

        }



        internal virtual P.TimeAnimateValue Create(PresentationExportArgs exportArgs)
        {
            throw new NotImplementedException("基类不承担具体业务") ;
    
        }

        internal static P.TimeAnimateValue CreateTimeAnimateValueEntry(BaseAnimationKeyFramePoint keyFramePoint, PresentationExportArgs exportArgs)
        {
            return keyFramePoint.Create(exportArgs);
        }
    }

}
