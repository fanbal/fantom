﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Animation
{
    public class AnimationCount
    {
        public const string COUNT_ENDLESS = "indefinite";

        /// <summary>
        /// 时间是否为无限。
        /// </summary>
        public bool IsForever;

        /// <summary>
        /// 具体的时间值。
        /// </summary>
        public Emu Value;


        internal static AnimationCount Build(OX.StringValue xCount, Emu defaultValue)
        {
            if (xCount is null) return new AnimationCount() { IsForever = false, Value = defaultValue };
            if (xCount.Value == COUNT_ENDLESS) return new AnimationCount() { IsForever = true, Value = defaultValue };
            return new AnimationCount() { IsForever = false, Value = Emu.Parse(xCount.Value) };
        }


        internal static OX.StringValue Create(AnimationCount count)
        {
            if (count.IsForever) return new OX.StringValue(COUNT_ENDLESS);
            return new OX.StringValue(count.Value.ToString());
        }

    }
}
