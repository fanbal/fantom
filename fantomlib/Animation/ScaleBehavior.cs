﻿using DocumentFormat.OpenXml;

namespace Fantom.Animation
{
    public sealed class ScaleBehavior : BehaviorBase
    {
        #region properties

        public override BehaviorPropertyType PropertyType => BehaviorPropertyType.Scale;

        /// <summary>
        /// 移动起点。
        /// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
        /// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
        /// </summary>
        public Tuple<Emu, Emu> From { get; set; } = null;

        /// <summary>
        /// 移动终点。
        /// <para>当您设置了 <see cref="From"/> 与 <see cref="To"/> 后，
        /// 整个动画的描述即被固定，无法再与其他动画叠加。</para>
        /// </summary>
        public Tuple<Emu, Emu> To { get; set; } = null;

        /// <summary>
        /// 相对移动。
        /// </summary>
        public Tuple<Emu, Emu> By { get; set; } = null;

        #endregion


        #region ctors
        internal ScaleBehavior()
        {

        }
        #endregion

        #region static

        /// <summary>
        /// 从指定动画描述中导入具体数据。
        /// </summary>
        /// <param name="anim"></param>
        /// <returns></returns>
        internal static ScaleBehavior LoadBehaviorFromElement(OpenXmlElement element)
        {
            var anim = element as DocumentFormat.OpenXml.Presentation.AnimateScale;
            var xbhv = anim.CommonBehavior;

            var bhv = new ScaleBehavior();
            SetBehaviorFromElement(bhv, element);


            if (anim.FromPosition is not null)
            {
                bhv.From = new Tuple<Emu, Emu>(anim.FromPosition.X, anim.FromPosition.Y);
            }

            if (anim.ToPosition is not null)
            {
                bhv.To = new Tuple<Emu, Emu>(anim.ToPosition.X, anim.ToPosition.Y);
            }

            if (anim.ByPosition is not null)
            {
                bhv.By = new Tuple<Emu, Emu>(anim.ByPosition.X, anim.ByPosition.Y);
            }

            return bhv;
        }

        #endregion

        internal override OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xAnim = new P.AnimateScale();
            var xBehavaior = new P.CommonBehavior();
            var xCommonTimeNode = new P.CommonTimeNode();
            var xChildList = new P.ChildTimeNodeList();


            xCommonTimeNode.ChildTimeNodeList = xChildList;
            xBehavaior.CommonTimeNode = xCommonTimeNode;
            xAnim.CommonBehavior = xBehavaior;
            return xAnim;
        }

    }
}
