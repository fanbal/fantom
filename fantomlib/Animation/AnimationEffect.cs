﻿using DocumentFormat.OpenXml;

namespace Fantom.Animation
{
    public class AnimationEffect : TimeNodeListBase<BehaviorBase>
    {

        internal override IEnumerable<TimeNodeBase> GetChildren()
        {
            return this;
        }

        public double Delay
        {
            get
            {
                if (BaseWrapper.StartConditionList is null) return 0d;
                var temp = Emu.Num0;
                foreach (var condition in BaseWrapper.StartConditionList)
                {
                    if (condition.Delay is not null)
                    {
                        if (condition.Delay.IsForever) return double.MaxValue;
                        else if (condition.Delay.IsIndefinite) return 0d;

                        if (condition.Delay.Value > temp)
                            temp = condition.Delay.Value;

                    }

                }

                return temp.ToSeconds();
            }
            set
            {
                if (BaseWrapper.StartConditionList is null)
                {
                    BaseWrapper.StartConditionList = new TimeConditionList();
                    BaseWrapper.StartConditionList.Add(new TimeCondition() { Delay = new AnimationTime() { Value = Emu.FromSeconds(value) } });
                }
                else
                {
                    int counter = 0;
                    foreach (var condition in BaseWrapper.StartConditionList)
                    {
                        if (condition.Delay is not null)
                        {
                            if (counter == 0)
                            {
                                condition.Delay.IsForever = false;
                                condition.Delay.IsIndefinite = false;
                                condition.Delay.Value = Emu.FromSeconds(value);
                            }
                            else
                            {
                                condition.Delay.IsForever = false;
                                condition.Delay.IsIndefinite = false;
                                condition.Delay.Value = Emu.Seconds0;
                            }

                            counter++;
                        }

                    }

                    

                    if (counter == 0) BaseWrapper.StartConditionList.Add(new TimeCondition() { Delay = new AnimationTime() { Value = Emu.FromSeconds(value) } });

                }



                RaiseEvent();
            }
        }

        public double Duration
        {
            get
            {
                Emu max = Emu.Num0;    
                foreach (var behavior in this)
                {
                    var lastTime = behavior.GetLastTime();
                    max = max > lastTime ? max : lastTime;
                }
                return max.ToSeconds();
            }
            set
            {
                var ratio = value / Duration;
                foreach (var behavior in this)
                {
                    behavior.Delay *= ratio;
                    behavior.Duration *= ratio;
                }
                RaiseEvent();
            }
        }

        public override string ToString()
        {
            return $"{GetType().Name}, delay: {Delay}, duration: {Duration}";

        }


        // 导入效果。
        internal static void LoadEffect(AnimationBundle bun, P.ParallelTimeNode par, PresentationImportArgs importArg)
        {
            var eff = new AnimationEffect();
            var ctn = par.GetFirstChild<P.CommonTimeNode>();

            TimeNodeBase.LoadTimeNodeBase(eff, new PresentationImportArgs(importArg) { ArgElement = ctn });

            foreach (var anim in ctn.ChildTimeNodeList)
            {
                BehaviorBase.LoadBehaviorEntry(eff, anim, importArg);
            }

            eff.Parent = bun;
            bun.Add(eff);
        }


        internal override OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xPara = new P.ParallelTimeNode();
            var xCommonTimeNode = new P.CommonTimeNode();
            var xChildList = new P.ChildTimeNodeList();

            foreach (var child in this)
            {
                xChildList.AppendExtend(child.CreateTimeNodeBase(exportArgs));
            }

            xCommonTimeNode.ChildTimeNodeList = xChildList;
            xPara.CommonTimeNode = xCommonTimeNode;
            return xPara;
        }
    }
}
