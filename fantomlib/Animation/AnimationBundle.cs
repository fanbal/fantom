﻿using DocumentFormat.OpenXml;

namespace Fantom.Animation
{
    public class AnimationBundle : TimeNodeListBase<AnimationEffect>
    {

        internal override IEnumerable<TimeNodeBase> GetChildren()
        {
            return this;
        }

        // 导入动画束。
        internal static void LoadBundle(AnimationFragment fra, P.ParallelTimeNode par, PresentationImportArgs importArg)
        {
            var bun = new AnimationBundle();

            var ctn = par.GetFirstChild<P.CommonTimeNode>();

            TimeNodeBase.LoadTimeNodeBase(bun, new PresentationImportArgs(importArg) { ArgElement = ctn });

            foreach (P.ParallelTimeNode spar in ctn.ChildTimeNodeList)
            {
                AnimationEffect.LoadEffect(bun, spar, importArg);
            }



            bun.Parent = fra;
            fra.Add(bun);
            importArg.LoadBundleHandler?.Invoke(new RaiseLoadingBundleArguments() { Bundle = bun }); ;
        }


        internal override OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {
            var xPara = new P.ParallelTimeNode();
            var xCommonTimeNode = new P.CommonTimeNode();
            var xChildList = new P.ChildTimeNodeList();

            foreach (var child in this)
            {
                xChildList.AppendExtend(child.CreateTimeNodeBase(exportArgs));
            }

            xCommonTimeNode.ChildTimeNodeList = xChildList;
            xPara.CommonTimeNode = xCommonTimeNode;
            return xPara;
        }

    }
}
