﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Animation
{
    public enum TimeNodeRestartType
    {
        Always, WhenNotActive, Never
    }


    public enum TimeNodeFillType
    {
        Remove, Freeze, Hold, Transition
    }

    public enum TimeNodeSyncType
    {
        CanSlip, Locked
    }

    public enum TimeNodeMasterRelation
    {
        SameClick, LastClick, NextClick
    }

    public enum TimeNodeType
    {
        ClickEffect,
        WithEffect, 
        AfterEffect, 
        MainSeq, 
        InteractiveSeq,
        ClickPar,
        WithGroup,
        AfterGroup,
        TmRoot
    }


    public enum TimeNodePresetClassType
    {
        Enter, Exit, Emph, Path, Verb, Mediacall
    }

    public enum TriggerEventType
    {
        OnBegin, OnEnd, Begin, End, OnClick, OnDoubleClick,
        OnMouseOver, OnMouseOut, OnNext, OnPrev, OnStopAudio
    }

    public enum TriggerRuntimeNodeType
    {
        First, Last, All
    }




}
