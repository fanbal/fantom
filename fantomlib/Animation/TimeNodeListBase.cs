﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Animation
{
    public abstract class TimeNodeListBase<T> : TimeNodeBase, IList<T> where T : TimeNodeBase
    {
        public List<T> Items => _items;

        public int Count => ((ICollection<T>)_items).Count;

        public bool IsReadOnly => ((ICollection<T>)_items).IsReadOnly;

        public T this[int index] { get => ((IList<T>)_items)[index]; set => ((IList<T>)_items)[index] = value; }

        private List<T> _items = new List<T>();

        public int IndexOf(T item)
        {
            return ((IList<T>)_items).IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            ((IList<T>)_items).Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            ((IList<T>)_items).RemoveAt(index);
        }

        public void Add(T item)
        {
            ((ICollection<T>)_items).Add(item);
        }

        public void Clear()
        {
            ((ICollection<T>)_items).Clear();
        }

        public bool Contains(T item)
        {
            return ((ICollection<T>)_items).Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            ((ICollection<T>)_items).CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return ((ICollection<T>)_items).Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_items).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_items).GetEnumerator();
        }
    }
}
