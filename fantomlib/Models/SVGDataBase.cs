﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Models
{
    public class SVGNodeBase
    {

        private static HashSet<Type> _svgNodeHashSet = new HashSet<Type>()
        {
            typeof(D.CloseShapePath), typeof(D.MoveTo), typeof(D.LineTo), typeof(D.ArcTo), typeof(D.QuadraticBezierCurveTo), typeof(D.CubicBezierCurveTo)
        };

        internal static bool IsSVGNodeTypeX(OX.OpenXmlElement element)
        {
            return _svgNodeHashSet.Contains(element.GetType());
        }

        public class SVGCloseNode : SVGNodeBase
        {

            internal static SVGCloseNode BuildNode(PresentationImportArgs importArgs)
            {
                var xNode = importArgs.ArgElement as D.CloseShapePath;
                if (xNode is null) return null;
                var node = new SVGCloseNode();
                return node;
            }

            internal static D.CloseShapePath CreateNode(SVGCloseNode node, PresentationExportArgs exportArgs)
            {
                if (node is null) return null;
                var xNode = new D.CloseShapePath();
                return xNode;
            }

        }


        public class SVGMoveNode : SVGNodeBase
        {


            internal static SVGMoveNode BuildNode(PresentationImportArgs importArgs)
            {
                var xNode = importArgs.ArgElement as D.MoveTo;
                if (xNode is null) return null;
                var node = new SVGMoveNode();
                return node;
            }

            internal static D.MoveTo CreateNode(SVGMoveNode node, PresentationExportArgs exportArgs)
            {
                if (node is null) return null;
                var xNode = new D.MoveTo();
                return xNode;
            }

        }

        public class SVGLineNode : SVGNodeBase
        {
            internal static SVGLineNode BuildNode(PresentationImportArgs importArgs)
            {
                var xNode = importArgs.ArgElement as D.LineTo;
                if (xNode is null) return null;
                var node = new SVGLineNode();
                return node;
            }

            internal static D.LineTo CreateNode(SVGLineNode node, PresentationExportArgs exportArgs)
            {
                if (node is null) return null;
                var xNode = new D.LineTo();
                return xNode;
            }

        }

        public class SVGArcNode : SVGNodeBase
        {
            internal static SVGArcNode BuildNode(PresentationImportArgs importArgs)
            {
                var xNode = importArgs.ArgElement as D.ArcTo;
                if (xNode is null) return null;
                var node = new SVGArcNode();
                return node;
            }

            internal static D.ArcTo CreateNode(SVGArcNode node, PresentationExportArgs exportArgs)
            {
                if (node is null) return null;  
                var xNode = new D.ArcTo();
                return xNode;
            }

        }

        public class SVGQuadBezierNode : SVGNodeBase
        {
            internal static SVGQuadBezierNode BuildNode(PresentationImportArgs importArgs)
            {
                var xNode = importArgs.ArgElement as D.QuadraticBezierCurveTo;
                if (xNode is null) return null;
                var node = new SVGQuadBezierNode();
                return node;
            }

            internal static D.QuadraticBezierCurveTo CreateNode(SVGQuadBezierNode node, PresentationExportArgs exportArgs)
            {
                if (node is null) return null;
                var xNode = new D.QuadraticBezierCurveTo();
                return xNode;
            }

        }

        public class SVGCubicBezierNode : SVGNodeBase
        {
            internal static SVGCubicBezierNode BuildNode(PresentationImportArgs importArgs)
            {
                var xNode = importArgs.ArgElement as D.CubicBezierCurveTo;
                if (xNode is null) return null;
                var node = new SVGCubicBezierNode();
                return node;
            }

            internal static D.CubicBezierCurveTo CreateNode(SVGCubicBezierNode node, PresentationExportArgs exportArgs)
            {
                if (node is null) return null;
                var xNode = new D.CubicBezierCurveTo();
                return xNode;
            }

        }

        internal static SVGNodeBase BuildSVGNodeBaseEntry(PresentationImportArgs importArgs)
        {
            var xNode = importArgs.ArgElement;
            if (xNode is null) return null;
            var newImportArgs = new PresentationImportArgs(importArgs) { ArgElement = xNode };
            if (xNode is D.CloseShapePath) return SVGCloseNode.BuildNode(newImportArgs);
            else if (xNode is D.MoveTo) return SVGMoveNode.BuildNode(newImportArgs);
            else if (xNode is D.LineTo) return SVGLineNode.BuildNode(newImportArgs);
            else if (xNode is D.ArcTo) return SVGArcNode.BuildNode(newImportArgs);
            else if (xNode is D.QuadraticBezierCurveTo) return SVGQuadBezierNode.BuildNode(newImportArgs);
            else if (xNode is D.CubicBezierCurveTo) return SVGCubicBezierNode.BuildNode(newImportArgs);
            throw new Exception("SVG Node 未知类型读取。");
        }

        internal static OX.OpenXmlElement CreateSVGNodeBaseEntry(SVGNodeBase node, PresentationExportArgs exportArgs)
        {
            if (node is null) return null;
            if (node is SVGCloseNode) return SVGCloseNode.CreateNode(node as SVGCloseNode, exportArgs);
            else if (node is SVGMoveNode) return SVGMoveNode.CreateNode(node as SVGMoveNode, exportArgs);
            else if (node is SVGLineNode) return SVGLineNode.CreateNode(node as SVGLineNode, exportArgs);
            else if (node is SVGArcNode) return SVGArcNode.CreateNode(node as SVGArcNode, exportArgs);
            else if (node is SVGQuadBezierNode) return SVGQuadBezierNode.CreateNode(node as SVGQuadBezierNode, exportArgs);
            else if (node is SVGCubicBezierNode) return SVGCubicBezierNode.CreateNode(node as SVGCubicBezierNode, exportArgs);
            throw new Exception("SVG Node 未知类型导出。");
        }

    }
}
