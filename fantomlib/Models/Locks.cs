﻿using DocumentFormat.OpenXml.Wordprocessing;

namespace Fantom.Models
{
    public class Locks : EchoBaseObject
    {

        public bool NoGrouping { get; set; } = false;

        public bool NoSelection
        {
            get => _noSelect;
            set
            {
                _noSelect = value;
                RaiseEvent();
            }
        }
        private bool _noSelect = false;
        public bool NoRotation { get; set; } = false;
        public bool NoChangeAspect { get; set; } = false;
        public bool NoMove { get; set; } = false;
        public bool NoResize { get; set; } = false;
        public bool NoEditPoints { get; set; } = false;
        public bool NoAdjustHandles { get; set; } = false;
        public bool NoChangeArrowheads { get; set; } = false;
        public bool NoChangeShapeType { get; set; } = false;
        public bool NoTextEdit { get; set; } = false;
        public bool NoUnGrouping { get; set; } = false;
        public bool NoCorp { get; set; } = false;

        public void LockAll()
        {
            bool state = true;
            NoGrouping = state;
            NoSelection = state;
            NoRotation = state;
            NoChangeAspect = state;
            NoMove = state;
            NoResize = state;
            NoEditPoints = state;
            NoAdjustHandles = state;
            NoChangeArrowheads = state;
            NoChangeShapeType = state;
            NoTextEdit = state;
            NoUnGrouping = state;
            NoCorp = state;
        }

        public void UnlockAll()
        {
            bool state = false;
            NoGrouping = state;
            NoSelection = state;
            NoRotation = state;
            NoChangeAspect = state;
            NoMove = state;
            NoResize = state;
            NoEditPoints = state;
            NoAdjustHandles = state;
            NoChangeArrowheads = state;
            NoChangeShapeType = state;
            NoTextEdit = state;
            NoUnGrouping = state;
            NoCorp = state;
        }
    }

    public abstract class LocksBase : EchoBaseObject
    {
        public bool? NoGrouping { get; set; } = false;
       
        public bool? NoSelection { get; set; }

        public bool? NoRotation { get; set; } = false;
        public bool? NoChangeAspect { get; set; } = false;
        public bool? NoMove { get; set; } = false;
        public bool? NoResize { get; set; } = false;
      
        public LocksBase()
        {

        }

        public LocksBase(LocksBase locks)
        {
            NoGrouping = locks.NoGrouping;
            NoSelection = locks.NoSelection;
            NoRotation = locks.NoRotation;
            NoChangeAspect = locks.NoChangeAspect;
            NoMove = locks.NoMove;
            NoResize = locks.NoResize;
        }
    }

    public class ShapeLocks : LocksBase
    {
        public bool? NoTextEdit { get; set; } = false;
        public bool? NoEditPoints { get; set; } = false;
        public bool? NoAdjustHandles { get; set; } = false;
        public bool? NoChangeArrowheads { get; set; } = false;
        public bool? NoChangeShapeType { get; set; } = false;
        public ShapeLocks()
        {

        }

        public ShapeLocks(ShapeLocks locks) : base(locks)
        {
            NoTextEdit = locks.NoTextEdit;
            NoEditPoints = locks.NoEditPoints;
            NoAdjustHandles = locks.NoAdjustHandles;
            NoChangeArrowheads = locks.NoChangeArrowheads;
            NoChangeShapeType = locks.NoChangeShapeType;
        }

        internal static ShapeLocks LoadShapeLock(Shape shape, PresentationImportArgs importArgs)
        {
            var xElement = importArgs.ArgElement as D.ShapeLocks;
            if (xElement is null) return null;
            var locks = new ShapeLocks()
            {
                NoGrouping = xElement.NoGrouping?.Value,
                NoSelection = xElement.NoSelection?.Value,
                NoRotation = xElement.NoRotation?.Value,
                NoChangeAspect = xElement.NoChangeAspect?.Value,
                NoMove = xElement.NoMove?.Value,
                NoResize = xElement.NoResize?.Value,
                NoEditPoints = xElement.NoEditPoints?.Value,
                NoAdjustHandles = xElement.NoAdjustHandles?.Value,
                NoChangeArrowheads = xElement.NoChangeArrowheads?.Value,
                NoChangeShapeType = xElement.NoChangeShapeType?.Value,
                NoTextEdit = xElement.NoTextEdit?.Value,



            };
            shape.Locks = locks;
            return locks;
        }


        internal static D.ShapeLocks CreateShapeLocks(Shape shape, PresentationExportArgs exportArgs)
        {
            if (shape is null) return null;
            var locks = shape.ShapeLocks;
            if (locks is null) return null;
            return new D.ShapeLocks()
            {
                NoGrouping = locks.NoGrouping.ToOX(),
                NoSelection = locks.NoSelection.ToOX(),
                NoRotation = locks.NoRotation.ToOX(),
                NoChangeAspect = locks.NoChangeAspect.ToOX(),
                NoMove = locks.NoMove.ToOX(),
                NoResize = locks.NoResize.ToOX(),
                NoEditPoints = locks.NoEditPoints.ToOX(),
                NoAdjustHandles = locks.NoAdjustHandles.ToOX(),
                NoChangeArrowheads = locks.NoChangeArrowheads.ToOX(),
                NoChangeShapeType = locks.NoChangeShapeType.ToOX(),
                NoTextEdit = locks.NoTextEdit.ToOX(),


            };
        }

    }

    public class ConnectionShapeLocks : LocksBase
    {
        public bool? NoEditPoints { get; set; } = false;
        public bool? NoAdjustHandles { get; set; } = false;
        public bool? NoChangeArrowheads { get; set; } = false;
        public bool? NoChangeShapeType { get; set; } = false;
        public ConnectionShapeLocks()
        {

        }

        public ConnectionShapeLocks(ShapeLocks locks) : base(locks)
        {
            NoEditPoints = locks.NoEditPoints;
            NoAdjustHandles = locks.NoAdjustHandles;
            NoChangeArrowheads = locks.NoChangeArrowheads;
            NoChangeShapeType = locks.NoChangeShapeType;

        }

        internal static ConnectionShapeLocks LoadShapeLock(Connection shape, PresentationImportArgs importArgs)
        {
            var xElement = importArgs.ArgElement as D.ConnectionShapeLocks;
            if (xElement is null) return null;
            var locks = new ConnectionShapeLocks()
            {
                NoGrouping = xElement.NoGrouping?.Value,
                NoSelection = xElement.NoSelection?.Value,
                NoRotation = xElement.NoRotation?.Value,
                NoChangeAspect = xElement.NoChangeAspect?.Value,
                NoMove = xElement.NoMove?.Value,
                NoResize = xElement.NoResize?.Value,
                NoEditPoints = xElement.NoEditPoints?.Value,
                NoAdjustHandles = xElement.NoAdjustHandles?.Value,
                NoChangeArrowheads = xElement.NoChangeArrowheads?.Value,
                NoChangeShapeType = xElement.NoChangeShapeType?.Value,

            };
            shape.Locks = locks;
            return locks;
        }


        internal static D.ConnectionShapeLocks CreateShapeLocks(Connection shape, PresentationExportArgs exportArgs)
        {
            if (shape is null) return null;

            var locks = shape.ConnectionShapeLocks;
            if (locks is null) return null;
            return new D.ConnectionShapeLocks()
            {
                NoGrouping = locks.NoGrouping.ToOX(),
                NoSelection = locks.NoSelection.ToOX(),
                NoRotation = locks.NoRotation.ToOX(),
                NoChangeAspect = locks.NoChangeAspect.ToOX(),
                NoMove = locks.NoMove.ToOX(),
                NoResize = locks.NoResize.ToOX(),
                NoEditPoints = locks.NoEditPoints.ToOX(),
                NoAdjustHandles = locks.NoAdjustHandles.ToOX(),
                NoChangeArrowheads = locks.NoChangeArrowheads.ToOX(),
                NoChangeShapeType = locks.NoChangeShapeType.ToOX(),

            };
        }


    }

    public class GroupShapeLocks : LocksBase
    {
        public bool? NoUngrouping { get; set; } = false;
        public GroupShapeLocks()
        {

        }

        public GroupShapeLocks(GroupShapeLocks locks) : base(locks)
        {
            NoUngrouping = locks.NoUngrouping;
        }

        internal static GroupShapeLocks LoadShapeLock(Group shape, PresentationImportArgs importArgs)
        {
            var xElement = importArgs.ArgElement as D.GroupShapeLocks;

            if (xElement is null) return null;
            
            var locks = new GroupShapeLocks()
            {
                NoGrouping = xElement.NoGrouping?.Value,
                NoSelection = xElement.NoSelection?.Value,
                NoRotation = xElement.NoRotation?.Value,
                NoChangeAspect = xElement.NoChangeAspect?.Value,
                NoMove = xElement.NoMove?.Value,
                NoResize = xElement.NoResize?.Value,
                NoUngrouping = xElement.NoUngrouping?.Value,



            };
            shape.Locks = locks;
            return locks;
        }


        internal static D.GroupShapeLocks CreateShapeLocks(Group shape, PresentationExportArgs exportArgs)
        {
            if (shape is null) return null;
            var locks = shape.GroupShapeLocks;

            if (locks is null) return null;

            return new D.GroupShapeLocks()
            {
                NoGrouping = locks.NoGrouping.ToOX(),
                NoSelection = locks.NoSelection.ToOX(),
                NoRotation = locks.NoRotation.ToOX(),
                NoChangeAspect = locks.NoChangeAspect.ToOX(),
                NoMove = locks.NoMove.ToOX(),
                NoResize = locks.NoResize.ToOX(),
                NoUngrouping = locks.NoUngrouping.ToOX(),
                
            };
        }

    }

    public class PictureLocks : LocksBase
    {
        public bool? NoAdjustHandles { get; set; } = false;
        public bool? NoChangeArrowheads { get; set; } = false;
        public bool? NoChangeShapeType { get; set; } = false;
        public bool? NoEditPoints { get; set; } = false;
        public bool? NoCrop { get; set; } = false;

        public PictureLocks()
        {

        }

        public PictureLocks(PictureLocks locks) : base(locks)
        {
            NoCrop = locks.NoCrop;
            NoEditPoints = locks.NoEditPoints;
            NoAdjustHandles = locks.NoAdjustHandles;
            NoChangeArrowheads = locks.NoChangeArrowheads;
            NoChangeShapeType = locks.NoChangeShapeType;
        }

        internal static PictureLocks LoadShapeLock(Picture shape, PresentationImportArgs importArgs)
        {

            var xElement = importArgs.ArgElement as D.PictureLocks;

            if (xElement is null) return null;

            var locks = new PictureLocks()
            {
                NoGrouping = xElement.NoGrouping?.Value,
                NoSelection = xElement.NoSelection?.Value,
                NoRotation = xElement.NoRotation?.Value,
                NoChangeAspect = xElement.NoChangeAspect?.Value,
                NoMove = xElement.NoMove?.Value,
                NoResize = xElement.NoResize?.Value,
                NoEditPoints = xElement.NoEditPoints?.Value,
                NoAdjustHandles = xElement.NoAdjustHandles?.Value,
                NoChangeArrowheads = xElement.NoChangeArrowheads?.Value,
                NoChangeShapeType = xElement.NoChangeShapeType?.Value,
                NoCrop = xElement.NoCrop?.Value,
            };
            shape.Locks = locks;
            return locks;
        }


        internal static D.PictureLocks CreateShapeLocks(Picture shape, PresentationExportArgs exportArgs)
        {
            if (shape is null) return null;
            var locks = shape.PictureLocks;
            if (locks is null) return null;
            return new D.PictureLocks()
            {
                NoGrouping = locks.NoGrouping.ToOX(),
                NoSelection = locks.NoSelection.ToOX(),
                NoRotation = locks.NoRotation.ToOX(),
                NoChangeAspect = locks.NoChangeAspect.ToOX(),
                NoMove = locks.NoMove.ToOX(),
                NoResize = locks.NoResize.ToOX(),
                NoEditPoints = locks.NoEditPoints.ToOX(),
                NoAdjustHandles = locks.NoAdjustHandles.ToOX(),
                NoChangeArrowheads = locks.NoChangeArrowheads.ToOX(),
                NoChangeShapeType = locks.NoChangeShapeType.ToOX(),

                NoCrop = locks.NoCrop.ToOX(),
                

            };
        }

    }
}
