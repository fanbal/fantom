﻿
namespace Fantom.Models
{
    /// <summary>
    /// 时间轴对象。
    /// </summary>
    public class TimeLine : TimeNodeBase
    {
        public abstract class BuildListItemBase
        {

        }

        public class TimeLineBuildParagraph : BuildListItemBase
        {

        }

        public class TimeLineBuildDiagram : BuildListItemBase
        {

        }

        public class TimeLineOleBuildChart : BuildListItemBase
        {

        }

        public class TimeLineGraphicalObjectBuild : BuildListItemBase
        {

        }

        public class BuildList
        {



        }

        public AnimationSequence MainSequence { get; set; } = new AnimationSequence();

        public List<AnimationSequence> InteractiveSequences { get; set; } = new List<AnimationSequence>();


        internal override IEnumerable<TimeNodeBase> GetChildren()
        {
            var list = new List<TimeNodeBase>() { MainSequence };
            list.AddRange(InteractiveSequences);
            return list;
        }

        internal override OX.OpenXmlElement CreateTimeNodeBase(PresentationExportArgs exportArgs)
        {

            var xTimeLine = new P.Timing();
            var xTimeNodeList = new P.TimeNodeList();
            var xPara = new P.ParallelTimeNode();
            var xCommonTimeNode = new P.CommonTimeNode();
            var xChildList = new P.ChildTimeNodeList();

            if (MainSequence is not null) xChildList.AppendExtend(MainSequence.CreateTimeNodeBase(exportArgs));

            foreach (var sequence in InteractiveSequences)
            {
                xChildList.AppendExtend(sequence.CreateTimeNodeBase(exportArgs));
            }


            xCommonTimeNode.ChildTimeNodeList = xChildList;
            xPara.CommonTimeNode = xCommonTimeNode;
            xTimeNodeList.ParallelTimeNode = xPara;
            xTimeLine.TimeNodeList = xTimeNodeList;
            return xTimeLine;
        }

        internal static OX.OpenXmlElement CreateTimeLine(TimeLine timeLine, PresentationExportArgs exportArgs)
        {
            if (timeLine is null) return null;
            return timeLine.CreateTimeNodeBase(exportArgs);
        }
    }
}
