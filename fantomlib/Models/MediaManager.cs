﻿namespace Fantom.Models
{
    public class MediaManager
    {
        private Dictionary<Uri, MediaSource> _mediaDic = new Dictionary<Uri, MediaSource>();

        private System.IO.Packaging.Package _pack;

        public MediaManager(System.IO.Packaging.Package pack)
        {
            _pack = pack;
        }

        public MediaSource LoadMedia(Uri uri)
        {

            if (!_mediaDic.ContainsKey(uri))
            {
                var media = new MediaSource(uri, _pack);
                _mediaDic.Add(uri, media);

            }
            return _mediaDic[uri];

        }


        //internal void CreateMedias(PK.PresentationDocument document, PresentationExportArgs exportArgs)
        //{
        //    var counter = 0;

        //    foreach (var media in _mediaDic.Values)
        //    {
        //        counter++;
        //        if (media.Type == MediaSource.MediaSourceType.image_jpeg)
        //        {

        //            var uri = new Uri($"/ppt/media/image{counter}.jpg", UriKind.Relative);
        //            var contentType = media.TypeString;
        //            var jpgPart = document.Package.CreatePart(uri, contentType);
        //            var stream = jpgPart.GetStream();
        //            stream.Write(media.Bytes);
        //            stream.Close();
        //            stream.Dispose();

        //            exportArgs.MediaMapper.Add(media, uri);
        //        }

        //    }

        //}
    }
}
