﻿using System.Linq;
namespace Fantom.Models
{
    /// <summary>
    /// 演示文稿集合。
    /// </summary>
    public class Presentations : EchoSelectionList<Presentation>
    {
        #region events
        public event PresentationsLoadingDocumentEventHandler OnLoadingSlide;
        #endregion

        #region properties

        internal HashSet<string> _filePathHashSet = new HashSet<string>();


        private Presentation _currentPresentation = null;
        public Presentation CurrentPresentation
        {
            get
            {
                if (_currentPresentation is null)
                {
                    return this.Items.FirstOrDefault();
                }

                return _currentPresentation;
            }
            set { _currentPresentation = value; }
        }

        #endregion

        #region ctors

        internal Presentations(Application application)
        {
            _application = application;
            _parent = application;
        }

        #endregion

        #region methods

        public void Close(Presentation presentation)
        {
            if (presentation is not null)
            {
                this.Remove(presentation);
            }
        }

        public Presentation Load(string uristr)
        {
            return Load(uristr, false);
        }

        public Presentation Load(string uristr, bool allowEdit)
        {
            return PresentationLoader.Load(this, uristr, allowEdit);
        }

        // 载入自定义信息。
        // 自定义信息包括自定义Xml与自定义Tag。
        private void LoadCustomInfo
            (CustomContainer container, IOPK.Package package, P.CustomerDataList xlist, Dictionary<string, Uri> uridic, PK.SlidePart sldpart)
        {

            // 自定义列表。
            var customList = container.CustomXmlList;
            var tags = container.Tags; // 获得父对象的Tags容器并使用它，添加字段。

            customList.Application = Application;
            customList.Parent = container;

            if (xlist is null)
            {
                return;
            }

            // 遍历Xml节点。
            foreach (var customXmlPart in xlist)
            {

                // 处理自定义Xml节点。
                if (customXmlPart is P.CustomerData)
                {
                    var xmlpart = CustomXmlBuilder.Build(
                        customXmlPart as P.CustomerData, package, uridic);
                    xmlpart.Application = Application;
                    xmlpart.Parent = customList;
                    customList.Add(xmlpart);
                }

                // 处理自定义Tag节点。
                else if (customXmlPart is P.CustomerDataTags)
                {
                    string rId = (customXmlPart as P.CustomerDataTags).Id;
                    var t =
                        from tagpair in sldpart.UserDefinedTagsParts
                        where tagpair.Uri == uridic[rId]
                        select tagpair;
                    if (t.Count() != 0)
                    {
                        var taglist = t.First().TagList;
                        foreach (P.Tag tag in taglist)
                        {
                            container.Tags.Add(new TagKeyValuePair(tag.Name, tag.Val));
                        }
                    }
                }

            }
        }

        public Presentation New()
        {
            string guid = Guid.NewGuid().ToString();
            _filePathHashSet.Add(guid);
            return PresentationBuilder.Build();
        }

        // 触发事件。
        public void RaiseLoadingSlideEvent(PresentationLoadArguments presentationsLoadingArgs)
        {
            OnLoadingSlide?.Invoke(this, presentationsLoadingArgs);
        }

        public void RaiseLoadingSlideEvent(PresentationsLoadingState loadingState, int steps, int step, string info)
        {
            OnLoadingSlide?.Invoke(this, new PresentationLoadArguments(loadingState, steps, step, info));
        }

        #endregion
    }
}
