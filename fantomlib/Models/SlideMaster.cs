﻿using System.Reflection;

namespace Fantom.Models
{
    /// <summary>
    /// 演示文稿的母版对象。
    /// </summary>
    public class SlideMaster : EchoSelectionList<SlideLayout>, IHasTimeLine
    {

        #region properties

        [RelationshipItemExportType(RelationshipExportType.ExportRoot)]
        public Theme Theme { get; set; }

        [PackInnerRelationshipMember(HasItem = true, HasRoot = true, IsSpecial = true)]
        public Shapes Shapes { get; set; }

        [PackInnerRelationshipMember(HasItem = false, HasRoot = false)]
        public TimeLine TimeLine { get; set; } = null;


        [RelationshipItemExportType(RelationshipExportType.ExportRoot)]
        public CustomContainer CustomContainer { get; internal set; } = new CustomContainer();

        #endregion


        #region methods

        public override string ToString()
        {
            return $"SlideMasterId: {(Parent as SlideMasters).IndexOf(this)}" ;
        }

        #endregion


        internal override void LoadPropertyRelationshipIDMap(EchoBaseObject mapperContainer, ref uint counter)
        {

            foreach (var item in Items)
            {
                counter++;
                mapperContainer._propertyRelationshipIDMapper.Add(item, counter);
            }

            base.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
        }

        internal override void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            uint slideCounter = 0;
            _packIDMapper = new Dictionary<EchoBaseObject, uint>();
            base.LoadPackIDMapper(this, ref slideCounter);


            foreach (var member in Items)
            {
                var counter2 = 0U;
                member._packIDMapper = new Dictionary<EchoBaseObject, uint>();
                member.LoadPackIDMapper(member, ref counter2);
            }

        }

    }
}
