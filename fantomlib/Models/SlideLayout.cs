﻿using System.Reflection;

namespace Fantom.Models
{
    /// <summary>
    /// 幻灯片母版的子样式。
    /// </summary>
    public class SlideLayout : EchoSelectableObject, IHasTimeLine
    {
        #region properties

        public int Index => (Parent as SlideMaster).IndexOf(this);

        [RelationshipItemExportType(RelationshipExportType.ExportRoot, fromParent:true)]
        public SlideMaster ParentMaster => Parent as SlideMaster;

        [PackInnerRelationshipMember(HasItem = true, HasRoot = true, IsSpecial = true)]
        public Shapes Shapes { get; internal set; }

        [PackInnerRelationshipMember(HasItem = false, HasRoot = false)]
        public TimeLine TimeLine { get; set; } = null;

        public CustomContainer CustomContainer { get; private set; } = new CustomContainer();

        #endregion

        #region methods
        public override string ToString()
        {
            return string.Format("SlideId: {0}", Index);
        }


        internal override void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            base.LoadPackIDMapper(mapperContainer, ref counter);
        }

        #endregion




    }
}
