﻿namespace Fantom.Models
{
    /// <summary>
    /// 文本块。
    /// </summary>



    public class TextBlock : EchoBaseObject
    {
        public enum TextBlockType
        {
            Run, Break, Feild
        }

        public TextBlockType Type { get; set; }



        private string _text;
        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                RaiseEvent();
            }
        }

        public FillFormatWrapper Fill { get; set; } = new FillFormatWrapper();

        public Emu FontSize { get; set; } = Emu.FontSize1 * 18;
        public string FontFamily { get; set; } = "微软雅黑";

        public TextBlock(string text)
        {
            _text = text;
        }

        public TextBlock()
        {
        }
    }
}
