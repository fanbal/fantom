﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Models
{
    public class NoteSlide:EchoBaseObject
    {
        #region properties

        /// <summary>
        /// 父级的标签母版。
        /// </summary>
        [RelationshipItemExportType(RelationshipExportType.ExportRoot, fromParent: true)]
        public NoteMaster ParentNoteMaster { get; internal set; }

        /// <summary>
        /// 父级的幻灯片。
        /// </summary>
        [RelationshipItemExportType(RelationshipExportType.ExportRoot, fromParent: true)]
        public Slide ParentSlide => Parent as Slide;

        [PackInnerRelationshipMember(HasItem = true, HasRoot = true, IsSpecial = true)]
        public Shapes Shapes { get; internal set; }

        public int Index => (Parent as Slide).Index;

        [RelationshipItemExportType(RelationshipExportType.ExportRoot)]
        public CustomContainer CustomContainer { get; internal set; } = new CustomContainer();


        #endregion


     
    }
}
