﻿//using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml;
using Fantom.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Models
{
    public class Bevel
    {
        //
        // 摘要:
        //     Bevel Presets
        public enum BevelPresetType
        {
            //
            // 摘要:
            //     Relaxed Inset.
            //     When the item is serialized out as xml, its value is "relaxedInset".
            //[EnumString("relaxedInset")]
            RelaxedInset,
            //
            // 摘要:
            //     Circle.
            //     When the item is serialized out as xml, its value is "circle".
            //[EnumString("circle")]
            Circle,
            //
            // 摘要:
            //     Slope.
            //     When the item is serialized out as xml, its value is "slope".
            //[EnumString("slope")]
            Slope,
            //
            // 摘要:
            //     Cross.
            //     When the item is serialized out as xml, its value is "cross".
            //[EnumString("cross")]
            Cross,
            //
            // 摘要:
            //     Angle.
            //     When the item is serialized out as xml, its value is "angle".
            //[EnumString("angle")]
            Angle,
            //
            // 摘要:
            //     Soft Round.
            //     When the item is serialized out as xml, its value is "softRound".
            //[EnumString("softRound")]
            SoftRound,
            //
            // 摘要:
            //     Convex.
            //     When the item is serialized out as xml, its value is "convex".
            //[EnumString("convex")]
            Convex,
            //
            // 摘要:
            //     Cool Slant.
            //     When the item is serialized out as xml, its value is "coolSlant".
            //[EnumString("coolSlant")]
            CoolSlant,
            //
            // 摘要:
            //     Divot.
            //     When the item is serialized out as xml, its value is "divot".
            //[EnumString("divot")]
            Divot,
            //
            // 摘要:
            //     Riblet.
            //     When the item is serialized out as xml, its value is "riblet".
            //[EnumString("riblet")]
            Riblet,
            //
            // 摘要:
            //     Hard Edge.
            //     When the item is serialized out as xml, its value is "hardEdge".
            //[EnumString("hardEdge")]
            HardEdge,
            //
            // 摘要:
            //     Art Deco.
            //     When the item is serialized out as xml, its value is "artDeco".
            //[EnumString("artDeco")]
            ArtDeco
        }



        public Emu? Width;
        public Emu? Height;
        public BevelPresetType? Type;

        internal static Bevel BuildBevel(PresentationImportArgs importArgs)
        {
            var xBevel = importArgs.ArgElement as D.BevelType;
            if (xBevel is null) return null;
            var bevel = new Bevel();

            if (xBevel.Preset is not null) bevel.Type = (BevelPresetType)xBevel.Preset.Value;
            if (xBevel.Width is not null) bevel.Width = xBevel.Width;
            if (xBevel.Height is not null) bevel.Height = xBevel.Height;

            return bevel;
        }

        internal static T CreateBevel<T>(Bevel bevel, PresentationExportArgs exportArgs) where T : D.BevelType, new()
        {
            if (bevel is null) return null;
            var xBevel = new T();

            if (bevel.Type is not null) xBevel.Preset = (D.BevelPresetValues)bevel.Type;
            xBevel.Width = bevel.Width.ToOX64();
            xBevel.Height = bevel.Height.ToOX64();

            return xBevel;
        }
    }

    public class Shape3D
    {
        //
        // 摘要:
        //     Preset Material Type
        public enum PresetMaterialType
        {
            //
            // 摘要:
            //     Legacy Matte.
            //     When the item is serialized out as xml, its value is "legacyMatte".
            //[EnumString("legacyMatte")]
            LegacyMatte,
            //
            // 摘要:
            //     Legacy Plastic.
            //     When the item is serialized out as xml, its value is "legacyPlastic".
            //[EnumString("legacyPlastic")]
            LegacyPlastic,
            //
            // 摘要:
            //     Legacy Metal.
            //     When the item is serialized out as xml, its value is "legacyMetal".
            //[EnumString("legacyMetal")]
            LegacyMetal,
            //
            // 摘要:
            //     Legacy Wireframe.
            //     When the item is serialized out as xml, its value is "legacyWireframe".
            //[EnumString("legacyWireframe")]
            LegacyWireframe,
            //
            // 摘要:
            //     Matte.
            //     When the item is serialized out as xml, its value is "matte".
            //[EnumString("matte")]
            Matte,
            //
            // 摘要:
            //     Plastic.
            //     When the item is serialized out as xml, its value is "plastic".
            //[EnumString("plastic")]
            Plastic,
            //
            // 摘要:
            //     Metal.
            //     When the item is serialized out as xml, its value is "metal".
            //[EnumString("metal")]
            Metal,
            //
            // 摘要:
            //     Warm Matte.
            //     When the item is serialized out as xml, its value is "warmMatte".
            //[EnumString("warmMatte")]
            WarmMatte,
            //
            // 摘要:
            //     Translucent Powder.
            //     When the item is serialized out as xml, its value is "translucentPowder".
            //[EnumString("translucentPowder")]
            TranslucentPowder,
            //
            // 摘要:
            //     Powder.
            //     When the item is serialized out as xml, its value is "powder".
            //[EnumString("powder")]
            Powder,
            //
            // 摘要:
            //     Dark Edge.
            //     When the item is serialized out as xml, its value is "dkEdge".
            //[EnumString("dkEdge")]
            DarkEdge,
            //
            // 摘要:
            //     Soft Edge.
            //     When the item is serialized out as xml, its value is "softEdge".
            //[EnumString("softEdge")]
            SoftEdge,
            //
            // 摘要:
            //     Clear.
            //     When the item is serialized out as xml, its value is "clear".
            //[EnumString("clear")]
            Clear,
            //
            // 摘要:
            //     Flat.
            //     When the item is serialized out as xml, its value is "flat".
            //[EnumString("flat")]
            Flat,
            //
            // 摘要:
            //     Soft Metal.
            //     When the item is serialized out as xml, its value is "softmetal".
            //[EnumString("softmetal")]
            SoftMetal
        }

        public Bevel BevelTop;
        public Bevel BevelBottom;

        public ColorBase ExtursionColor;
        public ColorBase ContourColor;

        public Emu? Z;
        public Emu? ExtursionHeight;
        public Emu? ControurWidth;

        public PresetMaterialType? MaterialType;

        internal static Shape3D BuildShape3D(PresentationImportArgs importArgs)
        {
            var xShape3D = importArgs.ArgElement as D.Shape3DType;
            if (xShape3D is null) return null;
            var shape3D = new Shape3D();

            shape3D.BevelTop = Bevel.BuildBevel(new PresentationImportArgs(importArgs) { ArgElement = xShape3D.BevelTop });
            shape3D.BevelBottom = Bevel.BuildBevel(new PresentationImportArgs(importArgs) { ArgElement = xShape3D.BevelBottom });
            shape3D.ExtursionColor = xShape3D.ExtrusionColor.GetColorFromNodeWithColor(importArgs.ColorLoadArgs);
            shape3D.ContourColor = xShape3D.ContourColor.GetColorFromNodeWithColor(importArgs.ColorLoadArgs);

            if (xShape3D.Z is not null) shape3D.Z = xShape3D.Z;
            if (xShape3D.ExtrusionHeight is not null) shape3D.ExtursionHeight = xShape3D.ExtrusionHeight;
            if (xShape3D.ContourWidth is not null) shape3D.ControurWidth = xShape3D.ContourWidth;
            if (xShape3D.PresetMaterial is not null) shape3D.MaterialType = (PresetMaterialType)xShape3D.PresetMaterial.Value;

            return shape3D;
        }

        internal static D.Shape3DType CreateShape3D(Shape3D shape3D, PresentationExportArgs exportArgs)
        {
            if (shape3D is null) return null;
            var xShape3D = new D.Shape3DType();

            xShape3D.BevelTop = Bevel.CreateBevel<D.BevelTop>(shape3D.BevelTop, exportArgs);
            xShape3D.BevelBottom = Bevel.CreateBevel<D.BevelBottom>(shape3D.BevelBottom, exportArgs);
            xShape3D.ExtrusionColor = new D.ExtrusionColor(ColorBuilder.ColorToOpenXmlElement(shape3D.ExtursionColor));
            xShape3D.ContourColor = new D.ContourColor(ColorBuilder.ColorToOpenXmlElement(shape3D.ContourColor));

            xShape3D.Z = shape3D.Z.ToOX64();
            xShape3D.ExtrusionHeight = shape3D.ExtursionHeight.ToOX64();
            xShape3D.ContourWidth = shape3D.ControurWidth.ToOX64();
            if (shape3D.MaterialType is not null) xShape3D.PresetMaterial = (D.PresetMaterialTypeValues)shape3D.MaterialType;

            return xShape3D;
        }

    }
}
