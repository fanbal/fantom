﻿
namespace Fantom.Models
{



    // <xsd:element name = "camera" type="CT_Camera" minOccurs="1" maxOccurs="1"/>
    //<xsd:element name = "lightRig" type="CT_LightRig" minOccurs="1" maxOccurs="1"/>
    //<xsd:element name = "backdrop" type="CT_Backdrop" minOccurs="0" maxOccurs="1"/>
    public class Scene3D
    {


        public class Rotation
        {
            public Emu? Latitude;
            public Emu? Longitude;
            public Emu? Revolution;
            public Rotation()
            {

            }

            public Rotation(Rotation sphereCoords)
            {

            }


            internal static Rotation BuildRotation(PresentationImportArgs importArgs)
            {
                var xRotation = importArgs.ArgElement as D.Rotation;
                if (xRotation is null) return null;
                var rotation = new Rotation();
                if (xRotation.Latitude is not null) rotation.Latitude = xRotation.Latitude;
                if (xRotation.Longitude is not null) rotation.Longitude = xRotation.Longitude;
                if (xRotation.Revolution is not null) rotation.Revolution = xRotation.Revolution;
                return rotation;
            }

            internal static D.Rotation CreateRotation(Rotation rotation, PresentationExportArgs exportArgs)
            {
                if (rotation is null) return null;
                var xRotation = new D.Rotation();
                xRotation.Latitude = rotation.Latitude.ToOX32();
                xRotation.Longitude = rotation.Longitude.ToOX32();
                xRotation.Revolution = rotation.Revolution.ToOX32();
                return xRotation;
            }
        }

        public class Scene3DCamera
        {
            public enum PresetCameraType
            {
                //
                // 摘要:
                //     Legacy Oblique Top Left.
                //     When the item is serialized out as xml, its value is "legacyObliqueTopLeft".
                //[EnumString("legacyObliqueTopLeft")]
                LegacyObliqueTopLeft,
                //
                // 摘要:
                //     Legacy Oblique Top.
                //     When the item is serialized out as xml, its value is "legacyObliqueTop".
                //[EnumString("legacyObliqueTop")]
                LegacyObliqueTop,
                //
                // 摘要:
                //     Legacy Oblique Top Right.
                //     When the item is serialized out as xml, its value is "legacyObliqueTopRight".
                //[EnumString("legacyObliqueTopRight")]
                LegacyObliqueTopRight,
                //
                // 摘要:
                //     Legacy Oblique Left.
                //     When the item is serialized out as xml, its value is "legacyObliqueLeft".
                //[EnumString("legacyObliqueLeft")]
                LegacyObliqueLeft,
                //
                // 摘要:
                //     Legacy Oblique Front.
                //     When the item is serialized out as xml, its value is "legacyObliqueFront".
                //[EnumString("legacyObliqueFront")]
                LegacyObliqueFront,
                //
                // 摘要:
                //     Legacy Oblique Right.
                //     When the item is serialized out as xml, its value is "legacyObliqueRight".
                //[EnumString("legacyObliqueRight")]
                LegacyObliqueRight,
                //
                // 摘要:
                //     Legacy Oblique Bottom Left.
                //     When the item is serialized out as xml, its value is "legacyObliqueBottomLeft".
                //[EnumString("legacyObliqueBottomLeft")]
                LegacyObliqueBottomLeft,
                //
                // 摘要:
                //     Legacy Oblique Bottom.
                //     When the item is serialized out as xml, its value is "legacyObliqueBottom".
                //[EnumString("legacyObliqueBottom")]
                LegacyObliqueBottom,
                //
                // 摘要:
                //     Legacy Oblique Bottom Right.
                //     When the item is serialized out as xml, its value is "legacyObliqueBottomRight".
                //[EnumString("legacyObliqueBottomRight")]
                LegacyObliqueBottomRight,
                //
                // 摘要:
                //     Legacy Perspective Top Left.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveTopLeft".
                //[EnumString("legacyPerspectiveTopLeft")]
                LegacyPerspectiveTopLeft,
                //
                // 摘要:
                //     Legacy Perspective Top.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveTop".
                //[EnumString("legacyPerspectiveTop")]
                LegacyPerspectiveTop,
                //
                // 摘要:
                //     Legacy Perspective Top Right.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveTopRight".
                //[EnumString("legacyPerspectiveTopRight")]
                LegacyPerspectiveTopRight,
                //
                // 摘要:
                //     Legacy Perspective Left.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveLeft".
                //[EnumString("legacyPerspectiveLeft")]
                LegacyPerspectiveLeft,
                //
                // 摘要:
                //     Legacy Perspective Front.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveFront".
                //[EnumString("legacyPerspectiveFront")]
                LegacyPerspectiveFront,
                //
                // 摘要:
                //     Legacy Perspective Right.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveRight".
                //[EnumString("legacyPerspectiveRight")]
                LegacyPerspectiveRight,
                //
                // 摘要:
                //     Legacy Perspective Bottom Left.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveBottomLeft".
                //[EnumString("legacyPerspectiveBottomLeft")]
                LegacyPerspectiveBottomLeft,
                //
                // 摘要:
                //     Legacy Perspective Bottom.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveBottom".
                //[EnumString("legacyPerspectiveBottom")]
                LegacyPerspectiveBottom,
                //
                // 摘要:
                //     Legacy Perspective Bottom Right.
                //     When the item is serialized out as xml, its value is "legacyPerspectiveBottomRight".
                //[EnumString("legacyPerspectiveBottomRight")]
                LegacyPerspectiveBottomRight,
                //
                // 摘要:
                //     Orthographic Front.
                //     When the item is serialized out as xml, its value is "orthographicFront".
                //[EnumString("orthographicFront")]
                OrthographicFront,
                //
                // 摘要:
                //     Isometric Top Up.
                //     When the item is serialized out as xml, its value is "isometricTopUp".
                //[EnumString("isometricTopUp")]
                IsometricTopUp,
                //
                // 摘要:
                //     Isometric Top Down.
                //     When the item is serialized out as xml, its value is "isometricTopDown".
                //[EnumString("isometricTopDown")]
                IsometricTopDown,
                //
                // 摘要:
                //     Isometric Bottom Up.
                //     When the item is serialized out as xml, its value is "isometricBottomUp".
                //[EnumString("isometricBottomUp")]
                IsometricBottomUp,
                //
                // 摘要:
                //     Isometric Bottom Down.
                //     When the item is serialized out as xml, its value is "isometricBottomDown".
                //[EnumString("isometricBottomDown")]
                IsometricBottomDown,
                //
                // 摘要:
                //     Isometric Left Up.
                //     When the item is serialized out as xml, its value is "isometricLeftUp".
                //[EnumString("isometricLeftUp")]
                IsometricLeftUp,
                //
                // 摘要:
                //     Isometric Left Down.
                //     When the item is serialized out as xml, its value is "isometricLeftDown".
                //[EnumString("isometricLeftDown")]
                IsometricLeftDown,
                //
                // 摘要:
                //     Isometric Right Up.
                //     When the item is serialized out as xml, its value is "isometricRightUp".
                //[EnumString("isometricRightUp")]
                IsometricRightUp,
                //
                // 摘要:
                //     Isometric Right Down.
                //     When the item is serialized out as xml, its value is "isometricRightDown".
                //[EnumString("isometricRightDown")]
                IsometricRightDown,
                //
                // 摘要:
                //     Isometric Off Axis 1 Left.
                //     When the item is serialized out as xml, its value is "isometricOffAxis1Left".
                //[EnumString("isometricOffAxis1Left")]
                IsometricOffAxis1Left,
                //
                // 摘要:
                //     Isometric Off Axis 1 Right.
                //     When the item is serialized out as xml, its value is "isometricOffAxis1Right".
                //[EnumString("isometricOffAxis1Right")]
                IsometricOffAxis1Right,
                //
                // 摘要:
                //     Isometric Off Axis 1 Top.
                //     When the item is serialized out as xml, its value is "isometricOffAxis1Top".
                //[EnumString("isometricOffAxis1Top")]
                IsometricOffAxis1Top,
                //
                // 摘要:
                //     Isometric Off Axis 2 Left.
                //     When the item is serialized out as xml, its value is "isometricOffAxis2Left".
                //[EnumString("isometricOffAxis2Left")]
                IsometricOffAxis2Left,
                //
                // 摘要:
                //     Isometric Off Axis 2 Right.
                //     When the item is serialized out as xml, its value is "isometricOffAxis2Right".
                //[EnumString("isometricOffAxis2Right")]
                IsometricOffAxis2Right,
                //
                // 摘要:
                //     Isometric Off Axis 2 Top.
                //     When the item is serialized out as xml, its value is "isometricOffAxis2Top".
                //[EnumString("isometricOffAxis2Top")]
                IsometricOffAxis2Top,
                //
                // 摘要:
                //     Isometric Off Axis 3 Left.
                //     When the item is serialized out as xml, its value is "isometricOffAxis3Left".
                //[EnumString("isometricOffAxis3Left")]
                IsometricOffAxis3Left,
                //
                // 摘要:
                //     Isometric Off Axis 3 Right.
                //     When the item is serialized out as xml, its value is "isometricOffAxis3Right".
                //[EnumString("isometricOffAxis3Right")]
                IsometricOffAxis3Right,
                //
                // 摘要:
                //     Isometric Off Axis 3 Bottom.
                //     When the item is serialized out as xml, its value is "isometricOffAxis3Bottom".
                //[EnumString("isometricOffAxis3Bottom")]
                IsometricOffAxis3Bottom,
                //
                // 摘要:
                //     Isometric Off Axis 4 Left.
                //     When the item is serialized out as xml, its value is "isometricOffAxis4Left".
                //[EnumString("isometricOffAxis4Left")]
                IsometricOffAxis4Left,
                //
                // 摘要:
                //     Isometric Off Axis 4 Right.
                //     When the item is serialized out as xml, its value is "isometricOffAxis4Right".
                //[EnumString("isometricOffAxis4Right")]
                IsometricOffAxis4Right,
                //
                // 摘要:
                //     Isometric Off Axis 4 Bottom.
                //     When the item is serialized out as xml, its value is "isometricOffAxis4Bottom".
                //[EnumString("isometricOffAxis4Bottom")]
                IsometricOffAxis4Bottom,
                //
                // 摘要:
                //     Oblique Top Left.
                //     When the item is serialized out as xml, its value is "obliqueTopLeft".
                //[EnumString("obliqueTopLeft")]
                ObliqueTopLeft,
                //
                // 摘要:
                //     Oblique Top.
                //     When the item is serialized out as xml, its value is "obliqueTop".
                //[EnumString("obliqueTop")]
                ObliqueTop,
                //
                // 摘要:
                //     Oblique Top Right.
                //     When the item is serialized out as xml, its value is "obliqueTopRight".
                //[EnumString("obliqueTopRight")]
                ObliqueTopRight,
                //
                // 摘要:
                //     Oblique Left.
                //     When the item is serialized out as xml, its value is "obliqueLeft".
                //[EnumString("obliqueLeft")]
                ObliqueLeft,
                //
                // 摘要:
                //     Oblique Right.
                //     When the item is serialized out as xml, its value is "obliqueRight".
                //[EnumString("obliqueRight")]
                ObliqueRight,
                //
                // 摘要:
                //     Oblique Bottom Left.
                //     When the item is serialized out as xml, its value is "obliqueBottomLeft".
                //[EnumString("obliqueBottomLeft")]
                ObliqueBottomLeft,
                //
                // 摘要:
                //     Oblique Bottom.
                //     When the item is serialized out as xml, its value is "obliqueBottom".
                //[EnumString("obliqueBottom")]
                ObliqueBottom,
                //
                // 摘要:
                //     Oblique Bottom Right.
                //     When the item is serialized out as xml, its value is "obliqueBottomRight".
                //[EnumString("obliqueBottomRight")]
                ObliqueBottomRight,
                //
                // 摘要:
                //     Perspective Front.
                //     When the item is serialized out as xml, its value is "perspectiveFront".
                //[EnumString("perspectiveFront")]
                PerspectiveFront,
                //
                // 摘要:
                //     Perspective Left.
                //     When the item is serialized out as xml, its value is "perspectiveLeft".
                //[EnumString("perspectiveLeft")]
                PerspectiveLeft,
                //
                // 摘要:
                //     Perspective Right.
                //     When the item is serialized out as xml, its value is "perspectiveRight".
                //[EnumString("perspectiveRight")]
                PerspectiveRight,
                //
                // 摘要:
                //     Orthographic Above.
                //     When the item is serialized out as xml, its value is "perspectiveAbove".
                //[EnumString("perspectiveAbove")]
                PerspectiveAbove,
                //
                // 摘要:
                //     Perspective Below.
                //     When the item is serialized out as xml, its value is "perspectiveBelow".
                //[EnumString("perspectiveBelow")]
                PerspectiveBelow,
                //
                // 摘要:
                //     Perspective Above Left Facing.
                //     When the item is serialized out as xml, its value is "perspectiveAboveLeftFacing".
                //[EnumString("perspectiveAboveLeftFacing")]
                PerspectiveAboveLeftFacing,
                //
                // 摘要:
                //     Perspective Above Right Facing.
                //     When the item is serialized out as xml, its value is "perspectiveAboveRightFacing".
                //[EnumString("perspectiveAboveRightFacing")]
                PerspectiveAboveRightFacing,
                //
                // 摘要:
                //     Perspective Contrasting Left Facing.
                //     When the item is serialized out as xml, its value is "perspectiveContrastingLeftFacing".
                //[EnumString("perspectiveContrastingLeftFacing")]
                PerspectiveContrastingLeftFacing,
                //
                // 摘要:
                //     Perspective Contrasting Right Facing.
                //     When the item is serialized out as xml, its value is "perspectiveContrastingRightFacing".
                //[EnumString("perspectiveContrastingRightFacing")]
                PerspectiveContrastingRightFacing,
                //
                // 摘要:
                //     Perspective Heroic Left Facing.
                //     When the item is serialized out as xml, its value is "perspectiveHeroicLeftFacing".
                //[EnumString("perspectiveHeroicLeftFacing")]
                PerspectiveHeroicLeftFacing,
                //
                // 摘要:
                //     Perspective Heroic Right Facing.
                //     When the item is serialized out as xml, its value is "perspectiveHeroicRightFacing".
                //[EnumString("perspectiveHeroicRightFacing")]
                PerspectiveHeroicRightFacing,
                //
                // 摘要:
                //     Perspective Heroic Extreme Left Facing.
                //     When the item is serialized out as xml, its value is "perspectiveHeroicExtremeLeftFacing".
                //[EnumString("perspectiveHeroicExtremeLeftFacing")]
                PerspectiveHeroicExtremeLeftFacing,
                //
                // 摘要:
                //     Perspective Heroic Extreme Right Facing.
                //     When the item is serialized out as xml, its value is "perspectiveHeroicExtremeRightFacing".
                //[EnumString("perspectiveHeroicExtremeRightFacing")]
                PerspectiveHeroicExtremeRightFacing,
                //
                // 摘要:
                //     Perspective Relaxed.
                //     When the item is serialized out as xml, its value is "perspectiveRelaxed".
                //[EnumString("perspectiveRelaxed")]
                PerspectiveRelaxed,
                //
                // 摘要:
                //     Perspective Relaxed Moderately.
                //     When the item is serialized out as xml, its value is "perspectiveRelaxedModerately".
                //[EnumString("perspectiveRelaxedModerately")]
                PerspectiveRelaxedModerately
            }
            public Rotation Rotation;
            public PresetCameraType Type;
            public Emu? FieldOfView;
            public Emu? Zoom;

            public Scene3DCamera()
            {

            }

            public Scene3DCamera(Scene3DCamera camera)
            {

            }

            internal static Scene3DCamera BuildCamera(PresentationImportArgs importArgs)
            {
                var xCamera = importArgs.ArgElement as D.Camera;

                if (xCamera is null) return null;
                var camera = new Scene3DCamera();

                camera.Rotation = Rotation.BuildRotation(new PresentationImportArgs(importArgs) { ArgElement = xCamera.Rotation });
                if (xCamera.Preset is not null) camera.Type = (PresetCameraType)xCamera.Preset.Value;
                if (xCamera.FieldOfView is not null) camera.FieldOfView = xCamera.FieldOfView;
                if (xCamera.Zoom is not null) camera.Zoom = xCamera.Zoom;
                return camera;
            }


            internal static D.Camera CreateCamera(Scene3DCamera camera, PresentationExportArgs exportArgs)
            {
                if (camera is null) return null;
                var xCamera = new D.Camera();

                xCamera.Rotation = Rotation.CreateRotation(camera.Rotation, exportArgs);
                xCamera.Preset = (D.PresetCameraValues)camera.Type;
                xCamera.FieldOfView = camera.FieldOfView.ToOX32();
                xCamera.Zoom = camera.Zoom.ToOX32();
                return xCamera;
            }
        }

        public class Scene3DLightRig
        {
            //
            // 摘要:
            //     Light Rig Direction
            public enum LightRigDirection
            {
                //
                // 摘要:
                //     Top Left.
                //     When the item is serialized out as xml, its value is "tl".
                //[EnumString("tl")]
                TopLeft,
                //
                // 摘要:
                //     Top.
                //     When the item is serialized out as xml, its value is "t".
                //[EnumString("t")]
                Top,
                //
                // 摘要:
                //     Top Right.
                //     When the item is serialized out as xml, its value is "tr".
                //[EnumString("tr")]
                TopRight,
                //
                // 摘要:
                //     Left.
                //     When the item is serialized out as xml, its value is "l".
                //[EnumString("l")]
                Left,
                //
                // 摘要:
                //     Right.
                //     When the item is serialized out as xml, its value is "r".
                //[EnumString("r")]
                Right,
                //
                // 摘要:
                //     Bottom Left.
                //     When the item is serialized out as xml, its value is "bl".
                //[EnumString("bl")]
                BottomLeft,
                //
                // 摘要:
                //     Bottom.
                //     When the item is serialized out as xml, its value is "b".
                //[EnumString("b")]
                Bottom,
                //
                // 摘要:
                //     Bottom Right.
                //     When the item is serialized out as xml, its value is "br".
                //[EnumString("br")]
                BottomRight
            }

            //
            // 摘要:
            //     Light Rig Type
            public enum LightRigType
            {
                //
                // 摘要:
                //     Legacy Flat 1.
                //     When the item is serialized out as xml, its value is "legacyFlat1".
                //[EnumString("legacyFlat1")]
                LegacyFlat1,
                //
                // 摘要:
                //     Legacy Flat 2.
                //     When the item is serialized out as xml, its value is "legacyFlat2".
                //[EnumString("legacyFlat2")]
                LegacyFlat2,
                //
                // 摘要:
                //     Legacy Flat 3.
                //     When the item is serialized out as xml, its value is "legacyFlat3".
                //[EnumString("legacyFlat3")]
                LegacyFlat3,
                //
                // 摘要:
                //     Legacy Flat 4.
                //     When the item is serialized out as xml, its value is "legacyFlat4".
                //[EnumString("legacyFlat4")]
                LegacyFlat4,
                //
                // 摘要:
                //     Legacy Normal 1.
                //     When the item is serialized out as xml, its value is "legacyNormal1".
                //[EnumString("legacyNormal1")]
                LegacyNormal1,
                //
                // 摘要:
                //     Legacy Normal 2.
                //     When the item is serialized out as xml, its value is "legacyNormal2".
                //[EnumString("legacyNormal2")]
                LegacyNormal2,
                //
                // 摘要:
                //     Legacy Normal 3.
                //     When the item is serialized out as xml, its value is "legacyNormal3".
                //[EnumString("legacyNormal3")]
                LegacyNormal3,
                //
                // 摘要:
                //     Legacy Normal 4.
                //     When the item is serialized out as xml, its value is "legacyNormal4".
                //[EnumString("legacyNormal4")]
                LegacyNormal4,
                //
                // 摘要:
                //     Legacy Harsh 1.
                //     When the item is serialized out as xml, its value is "legacyHarsh1".
                //[EnumString("legacyHarsh1")]
                LegacyHarsh1,
                //
                // 摘要:
                //     Legacy Harsh 2.
                //     When the item is serialized out as xml, its value is "legacyHarsh2".
                //[EnumString("legacyHarsh2")]
                LegacyHarsh2,
                //
                // 摘要:
                //     Legacy Harsh 3.
                //     When the item is serialized out as xml, its value is "legacyHarsh3".
                //[EnumString("legacyHarsh3")]
                LegacyHarsh3,
                //
                // 摘要:
                //     Legacy Harsh 4.
                //     When the item is serialized out as xml, its value is "legacyHarsh4".
                //[EnumString("legacyHarsh4")]
                LegacyHarsh4,
                //
                // 摘要:
                //     Three Point.
                //     When the item is serialized out as xml, its value is "threePt".
                //[EnumString("threePt")]
                ThreePoints,
                //
                // 摘要:
                //     Light Rig Enum ( Balanced ).
                //     When the item is serialized out as xml, its value is "balanced".
                //[EnumString("balanced")]
                Balanced,
                //
                // 摘要:
                //     Soft.
                //     When the item is serialized out as xml, its value is "soft".
                //[EnumString("soft")]
                Soft,
                //
                // 摘要:
                //     Harsh.
                //     When the item is serialized out as xml, its value is "harsh".
                //[EnumString("harsh")]
                Harsh,
                //
                // 摘要:
                //     Flood.
                //     When the item is serialized out as xml, its value is "flood".
                //[EnumString("flood")]
                Flood,
                //
                // 摘要:
                //     Contrasting.
                //     When the item is serialized out as xml, its value is "contrasting".
                //[EnumString("contrasting")]
                Contrasting,
                //
                // 摘要:
                //     Morning.
                //     When the item is serialized out as xml, its value is "morning".
                //[EnumString("morning")]
                Morning,
                //
                // 摘要:
                //     Sunrise.
                //     When the item is serialized out as xml, its value is "sunrise".
                //[EnumString("sunrise")]
                Sunrise,
                //
                // 摘要:
                //     Sunset.
                //     When the item is serialized out as xml, its value is "sunset".
                //[EnumString("sunset")]
                Sunset,
                //
                // 摘要:
                //     Chilly.
                //     When the item is serialized out as xml, its value is "chilly".
                //[EnumString("chilly")]
                Chilly,
                //
                // 摘要:
                //     Freezing.
                //     When the item is serialized out as xml, its value is "freezing".
                //[EnumString("freezing")]
                Freezing,
                //
                // 摘要:
                //     Flat.
                //     When the item is serialized out as xml, its value is "flat".
                //[EnumString("flat")]
                Flat,
                //
                // 摘要:
                //     Two Point.
                //     When the item is serialized out as xml, its value is "twoPt".
                //[EnumString("twoPt")]
                TwoPoints,
                //
                // 摘要:
                //     Glow.
                //     When the item is serialized out as xml, its value is "glow".
                //[EnumString("glow")]
                Glow,
                //
                // 摘要:
                //     Bright Room.
                //     When the item is serialized out as xml, its value is "brightRoom".
                //[EnumString("brightRoom")]
                BrightRoom
            }

            public Rotation Rotation;

            public LightRigDirection Direction;
            public LightRigType Type;

            public Scene3DLightRig()
            {

            }

            public Scene3DLightRig(Scene3DLightRig lightRig)
            {

            }

            internal static Scene3DLightRig BuildLightRig(PresentationImportArgs importArgs)
            {
                var xLightRig = importArgs.ArgElement as D.LightRig;
                if (xLightRig is null) return null;
                var lightRig = new Scene3DLightRig();
                lightRig.Rotation = Rotation.BuildRotation(new PresentationImportArgs(importArgs) { ArgElement = xLightRig.Rotation });
                lightRig.Direction = (LightRigDirection)xLightRig.Direction.Value;
                lightRig.Type = (LightRigType)xLightRig.Rig.Value;
                return lightRig;
            }

            internal static D.LightRig CreateLightRig(Scene3DLightRig lightRig, PresentationExportArgs exportArgs)
            {
                if (lightRig is null) return null;
                var xLightRig = new D.LightRig();
                xLightRig.Rotation = Rotation.CreateRotation(lightRig.Rotation, exportArgs);
                xLightRig.Direction = (D.LightRigDirectionValues)lightRig.Direction;
                xLightRig.Rig = (D.LightRigValues)lightRig.Type;
                return xLightRig;
            }

        }

        public class Anchor
        {
            public Emu X, Y, Z;
            internal static Anchor Build(PresentationImportArgs importArgs)
            {
                var xAnchor = importArgs.ArgElement as D.Anchor;
                if (xAnchor is null) return null;
                var anchor = new Anchor();
                anchor.X = xAnchor.X;
                anchor.Y = xAnchor.Y;
                anchor.Z = xAnchor.Z;
                return anchor;
            }

            internal static D.Anchor Create(Anchor anchor, PresentationExportArgs exportArgs)
            {
                if (anchor is null) return null;
                var xAnchor = new D.Anchor();
                xAnchor.X = anchor.X.Value;
                xAnchor.Y = anchor.Y.Value;
                xAnchor.Z = anchor.Z.Value;
                return xAnchor;
            }
        }

        public class Vector3D
        {
            public Emu DX, DY, DZ;

            internal static Vector3D Build(PresentationImportArgs importArgs)
            {
                var xVector3D = importArgs.ArgElement as D.Vector3DType;
                if (xVector3D is null) return null;
                var vector3D = new Vector3D();
                vector3D.DX = xVector3D.Dx;
                vector3D.DY = xVector3D.Dy;
                vector3D.DZ = xVector3D.Dz;
                return vector3D;
            }

            internal static T Create<T>(Vector3D vector3D, PresentationExportArgs exportArgs) where T : D.Vector3DType, new()
            {
                if (vector3D is null) return null;
                var xVector3D = new T();
                xVector3D.Dx = vector3D.DX.Value;
                xVector3D.Dy = vector3D.DY.Value;
                xVector3D.Dz = vector3D.DZ.Value;
                return xVector3D;
            }
        }

        public class Scene3DBackdrop
        {
            public Anchor Anchor;
            public Vector3D Normal;
            public Vector3D UpVector;
            public Scene3DBackdrop()
            {

            }

            public Scene3DBackdrop(Scene3DBackdrop backdrop)
            {

            }

            internal static Scene3DBackdrop BuildBackdrop(PresentationImportArgs importArgs)
            {
                var xBackdrop = importArgs.ArgElement as D.Backdrop;
                if (xBackdrop is null) return null;
                var backdrop = new Scene3DBackdrop();
                backdrop.Anchor = Anchor.Build(new PresentationImportArgs(importArgs) { ArgElement = xBackdrop.Anchor });
                backdrop.Normal = Vector3D.Build(new PresentationImportArgs(importArgs) { ArgElement = xBackdrop.Normal });
                backdrop.UpVector = Vector3D.Build(new PresentationImportArgs(importArgs) { ArgElement = xBackdrop.UpVector });
                return backdrop;
            }


            internal static D.Backdrop CreateBackdrop(Scene3DBackdrop backdrop, PresentationExportArgs exportArgs)
            {
                if (backdrop is null) return null;
                var xBackdrop = new D.Backdrop();

                xBackdrop.Anchor = Anchor.Create(backdrop.Anchor, exportArgs);
                xBackdrop.Normal = Vector3D.Create<D.Normal>(backdrop.Normal, exportArgs);
                xBackdrop.UpVector = Vector3D.Create<D.UpVector>(backdrop.UpVector, exportArgs);
                return xBackdrop;
            }
        }


        public Scene3DCamera Camera;
        public Scene3DLightRig LightRig;
        public Scene3DBackdrop Backdrop;

        public Scene3D()
        {


        }

        public Scene3D(Scene3D scene3D)
        {

        }

        internal static Scene3D BuildScene3D(PresentationImportArgs importArgs)
        {

            var xScene3D = importArgs.ArgElement as D.Scene3DType;
            if (xScene3D is null) return null;
            var scene3d = new Scene3D();
            scene3d.Camera = Scene3DCamera.BuildCamera(new PresentationImportArgs(importArgs) { ArgElement = xScene3D.Camera });
            scene3d.LightRig = Scene3DLightRig.BuildLightRig(new PresentationImportArgs(importArgs) { ArgElement = xScene3D.LightRig });
            scene3d.Backdrop = Scene3DBackdrop.BuildBackdrop(new PresentationImportArgs(importArgs) { ArgElement = xScene3D.Backdrop });

            return scene3d;
        }

        internal static Scene3D LoadScene3D(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var scene3d = BuildScene3D(importArgs);
            shape.BaseWrapper.Scene3D = scene3d;
            return scene3d;
        }


        internal static D.Scene3DType CreateScene3D(Scene3D scene3D, PresentationExportArgs exportArgs)
        {
            if (scene3D is null) return null;
            var xScene3D = new D.Scene3DType();

            xScene3D.Camera = Scene3DCamera.CreateCamera(scene3D.Camera, exportArgs);
            xScene3D.LightRig = Scene3DLightRig.CreateLightRig(scene3D.LightRig, exportArgs);
            xScene3D.Backdrop = Scene3DBackdrop.CreateBackdrop(scene3D.Backdrop, exportArgs);

            return xScene3D;
        }

    }
}
