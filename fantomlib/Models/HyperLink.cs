﻿

namespace Fantom.Models
{
    public class HyperLink
    {
        public WaveAudioFileMedia Sound;
        public string? RID;
        public string? InvalidURL;
        public string? Action;
        public string? TargetFrame;
        public string? ToolTip;
        public bool? History;
        public bool? HighlightClick;
        public bool? EndSound;

        public HyperLink()
        {

        }

        public HyperLink(HyperLink hyperLink)
        {

        }

        
        internal static HyperLink BuildHyperLink(PresentationImportArgs importArgs)
        {
            var xHyperLink = importArgs.ArgElement as D.HyperlinkType;
            if (xHyperLink is null) return null;
            var hyperLink = new HyperLink();

            hyperLink.Sound = WaveAudioFileMedia.BuildMedia(new PresentationImportArgs(importArgs) { ArgElement = xHyperLink.GetElement<D.HyperlinkSound>()}) ;
            hyperLink.RID = xHyperLink.Id;
            hyperLink.InvalidURL = xHyperLink.InvalidUrl;
            hyperLink.Action = xHyperLink.Action;
            hyperLink.ToolTip = xHyperLink.Tooltip;
            hyperLink.History = xHyperLink.History;
            hyperLink.HighlightClick = xHyperLink.HighlightClick;
            hyperLink.EndSound = xHyperLink.EndSound;
            return hyperLink;
        }


        internal static T CreateHyperLink<T>(HyperLink hyperLink, PresentationExportArgs exportArgs)where T: D.HyperlinkType, new()
        {
            if (hyperLink is null) return null;
            var xHyperLink = new T();
            xHyperLink.HyperlinkSound = WaveAudioFileMedia.CreateMediaSound(hyperLink.Sound, exportArgs);
            xHyperLink.Id = xHyperLink.Id;
            xHyperLink.InvalidUrl = hyperLink.InvalidURL;
            xHyperLink.Action = hyperLink.Action;
            xHyperLink.Tooltip = hyperLink.ToolTip;
            xHyperLink.History = hyperLink.History;
            xHyperLink.HighlightClick = hyperLink.HighlightClick;
            xHyperLink.EndSound = hyperLink.EndSound;

            return xHyperLink;
        }

    }
}
