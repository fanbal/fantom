﻿namespace Fantom.Models
{
    public class CustomContainer : EchoBaseObject
    {
        public CustomXmlList CustomXmlList { get; set; } = new CustomXmlList();
        public Tags Tags { get; set; } = new Tags();
    }
}
