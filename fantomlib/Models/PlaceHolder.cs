﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Models
{
    public enum DirectionType
    {
        Horizontal, Vertical
    }


    public class PlaceHolder
    {
        public enum PlaceHolderSizeType
        {
            Full, Half, Quarter
        }


        public PlaceHolderType? Type { get; set; }
        public DirectionType? Orientation;
        public uint? Index { get; set; }
        public bool? HasCustomPrompt { get; set; }

        public PlaceHolderSizeType? SizeType { get; set; }


        internal static PlaceHolder LoadPlaceHolder(ShapeBase shape, PresentationImportArgs importArgs)
        {

            var xPlaceHolder = importArgs.ArgElement as P.PlaceholderShape;
            if (xPlaceHolder is null) return null;
            var placeHolder = new PlaceHolder();


            if (xPlaceHolder.Type is not null) placeHolder.Type = (PlaceHolderType)xPlaceHolder.Type?.Value;

            if (xPlaceHolder.Orientation is not null) placeHolder.Orientation = (DirectionType)xPlaceHolder.Orientation?.Value;

            placeHolder.Index = xPlaceHolder.Index?.Value;
            placeHolder.HasCustomPrompt = xPlaceHolder.HasCustomPrompt?.Value;
            shape.BaseWrapper.PlaceHolder = placeHolder;
            return placeHolder;
        }

        internal static P.PlaceholderShape CreatePlaceHolder(PlaceHolder placeHolder, PresentationExportArgs exportArgs)
        {
            if (placeHolder is null) return null;
            var xElement = new P.PlaceholderShape();
            if (placeHolder.Type is not null) xElement.Type = (P.PlaceholderValues)placeHolder.Type;
            if (placeHolder.Orientation is not null) xElement.Orientation = (P.DirectionValues)placeHolder.Orientation;

            xElement.Index = placeHolder.Index.ToOX();
            xElement.HasCustomPrompt = placeHolder.HasCustomPrompt.ToOX();

            return xElement;
        }

    }
}
