﻿namespace Fantom.Models
{
    /// <summary>
    /// 填充格式，包括图片，纯色等。
    /// </summary>
    public class FillFormatWrapper : EchoBaseObject
    {
        public FillBase Fill { get; set; }

        internal static FillFormatWrapper BuildFillFormat(PresentationImportArgs importArgs)
        {
            var xFill = importArgs.ArgElement;
            if (xFill is null) return null;
            var fill = new FillFormatWrapper();
            fill.Fill = FillBase.BuildFillEntry(new PresentationImportArgs(importArgs));
            return fill;
        }

        internal static OX.OpenXmlElement CreateFillFormat(FillFormatWrapper fill, PresentationExportArgs exportArgs)
        {
            //if (fill is null) return null;
            //var xFill = new D.FillProperties();

            //xFill.AppendExtend(FillBase.CreateFillEntry(fill.Fill, exportArgs));

            //return xFill;

            if (fill is null) return null;
            return FillBase.CreateFillEntry(fill.Fill, exportArgs);
        }

        public override string ToString()
        {
            if (Fill is null) return "FillFormat: Fill is null";
            return Fill.ToString();
        }

    }


    /// <summary>
    /// 图形的填充内容，是对 <see cref="FillFormatWrapper" 的封装。/>
    /// </summary>
    public class FillFormat : EchoBaseObject
    {
        internal ShapeBase ShapeBase;
        internal FillFormatWrapper FillFormatWrapper => ShapeBase.BaseWrapper.Fill;
        internal ShapeStyleWrapper ShapeStyle
        {
            get
            {
                if (ShapeBase is null) throw new Exception("未绑定图形。");
                switch (ShapeBase.Type)
                {

                    case ShapeType.Shape:
                        return (ShapeBase as Shape).Wrapper.Style;
                    case ShapeType.Connection:
                        return (ShapeBase as Connection).Wrapper.Style;
                    case ShapeType.Picture:
                        return (ShapeBase as Picture).Wrapper.Style;
                    default:
                        return null;
                }
            }

        }
        internal FillFormat() { }

        /// <summary>
        /// 颜色。
        /// </summary>
        public ColorBase Color
        {
            get
            {

                if (FillFormatWrapper is null)
                {
                    if (ShapeStyle is null) return Colors.White;
                    if (ShapeStyle.FillReference is null) return Colors.White;
                    return ShapeStyle.FillReference.Color;
                }
                else
                {
                    if (FillFormatWrapper.Fill is null) return Colors.White;
                    if (FillFormatWrapper.Fill is SolidFill) return (FillFormatWrapper.Fill as SolidFill).Color;
                    return Colors.White;
                }
            }
            set
            {
                ShapeBase.BaseWrapper.Fill = new FillFormatWrapper() { Fill = new SolidFill(value) };
                RaiseEvent();
            }
        }

    }
}
