﻿namespace Fantom
{
    /// <summary>
    /// 整个系统的顶层应用。
    /// </summary>
    public class Application : EchoBaseObject
    {

        public string Version => Properties.Resources.Version;

        public string Detail => Properties.Resources.Detail;

        public Presentations Presentations { get; private set; }

        internal Application()
        {
            Presentations = new Presentations(this);
        }

        /// <summary>
        /// 静态创建应用对象实例。
        /// </summary>
        /// <returns><see cref="Application"/> 应用 对象</returns>
        public static Application Create()
        {
            return new Application();
        }
    }
}
