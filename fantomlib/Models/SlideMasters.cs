﻿using System.Reflection;

namespace Fantom.Models
{
    /// <summary>
    /// 主题。
    /// </summary>
    public class SlideMasters : EchoList<SlideMaster>
    {

        internal override void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            foreach (var member in Items)
            {
                var counter2 = 1U;
                member._packIDMapper = new Dictionary<EchoBaseObject, uint>();
                member.LoadPackIDMapper(member, ref counter2);
            }

        }
    }
}
