﻿namespace Fantom.Models
{
    public class Theme : EchoBaseObject
    {
        #region properties


        public ColorScheme ColorScheme;
        public FontScheme FontScheme;
        public FormatScheme FormatScheme;

        #endregion

        #region methods

        public EchoBaseObject GetMasterParent()
        {
            var slideMaster = GetParent<SlideMaster>();
            if (slideMaster is not null) return slideMaster;
            var notesMaster = GetParent<NoteMaster>();
            if (notesMaster is not null) return notesMaster;
            throw new Exception("该主题对象未找到上级母版。");
        }

        #endregion

    }
}
