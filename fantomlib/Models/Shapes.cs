﻿using System.Linq;
namespace Fantom.Models
{
    public class Shapes : EchoSelectionList<ShapeBase>
    {

        internal void LoadPackIDMapper()
        {
            _packIDMapper = new Dictionary<EchoBaseObject, uint>();
            uint counter = 0;
            LoadPackIDMapper(this, ref counter);

            foreach (var child in this)
            {
                LoadPackIDMapperForShape(this, child, ref counter);
            }
        }


        internal override void LoadPropertyRelationshipIDMap(EchoBaseObject mapperContainer, ref uint counter)
        {
            foreach (var shape in this)
            {
                shape.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
            }

        }





        internal override void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            counter++;
            mapperContainer._packIDMapper.Add(mapperContainer, counter);
            mapperContainer._exportID = counter;



        }


        internal void LoadPackIDMapperForShape(EchoBaseObject mapperContainer, ShapeBase shape, ref uint counter)
        {
            counter++;
            switch (shape.Type)
            {
                case ShapeType.Connection:
                case ShapeType.Picture:
                case ShapeType.Shape:
                    {
                        mapperContainer._packIDMapper.Add(shape, counter);
                        shape._exportID = counter;
                    }
                    return;
                case ShapeType.Group:
                    {
                        mapperContainer._packIDMapper.Add(shape, counter);
                        shape._exportID = counter;
                        foreach (var childShape in shape as Group)
                        {
                            LoadPackIDMapperForShape(mapperContainer, childShape, ref counter);
                        }
                    }
                    return;

                default:
                    throw new Exception("未知类型，无法导出");
            }
        }


        internal void LoadPackIDMapperForShape(ShapeBase shape, EchoBaseObject mapperContainer, ref uint counter)
        {
            counter++;
            mapperContainer._packIDMapper.Add(shape, counter);
        }

        internal void LoadPackIDMapperForGroupShape(Group gshape, EchoBaseObject mapperContainer, ref uint counter)
        {
            counter++;
            mapperContainer._packIDMapper.Add(gshape, counter);
            foreach (var item in gshape.Items)
            {
                if (item is Group)
                {
                    LoadPackIDMapperForGroupShape(item as Group, mapperContainer, ref counter);
                }
                else
                {
                    LoadPackIDMapperForShape(item, mapperContainer, ref counter);
                }
            }
        }


    }
}
