﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Models
{
    public class NoteMaster:EchoList<NoteSlide>
    {
        #region properties

        [RelationshipItemExportType(RelationshipExportType.ExportRoot)]
        public Theme Theme { get; set; }

        [PackInnerRelationshipMember(HasItem = true, HasRoot = true,IsSpecial = true)]
        public Shapes Shapes { get; internal set; }


        public CustomContainer CustomContainer { get; internal set; } = new CustomContainer();



        #endregion

        #region method

        public override string ToString()
        {
            return "NoteMaster";
        }

        #endregion

    }
}
