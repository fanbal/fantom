﻿using DocumentFormat.OpenXml;
using System;
using System.Xml.Linq;

namespace Fantom.Models
{


    public abstract class TextAutofitBase
    {
        public class TextNoAutoFit : TextAutofitBase
        {

            internal static TextNoAutoFit Build(PresentationImportArgs importArgs)
            {
                var xfit = importArgs.ArgElement as D.NoAutoFit;
                var fit = new TextNoAutoFit();

                return fit;
            }
            internal override OpenXmlElement Create()
            {
                var xfit = new D.NoAutoFit();
                var fit = this;

                return xfit;
            }


        }

        public class TextNormalAutoFit : TextAutofitBase
        {
            public Emu? FontScale;
            public Emu? LineSpaceReduction;

            internal static TextNormalAutoFit Build(PresentationImportArgs importArgs)
            {
                var xfit = importArgs.ArgElement as D.NormalAutoFit;
                var fit = new TextNormalAutoFit();

                if (xfit.FontScale is not null) fit.FontScale = xfit.FontScale.Value;
                if (xfit.LineSpaceReduction is not null) fit.LineSpaceReduction = xfit.LineSpaceReduction.Value;

                return fit;
            }
            internal override OpenXmlElement Create()
            {
                var xfit = new D.NormalAutoFit();
                var fit = this;

                if (fit.FontScale is not null) xfit.FontScale = fit.FontScale.ToOX32();
                if (fit.LineSpaceReduction is not null) xfit.LineSpaceReduction = fit.LineSpaceReduction.ToOX32();

                return xfit;
            }
        }

        public class TextShapeAutoFit : TextAutofitBase
        {
            internal static TextShapeAutoFit Build(PresentationImportArgs importArgs)
            {
                var xfit = importArgs.ArgElement as D.ShapeAutoFit;
                var fit = new TextShapeAutoFit();

                return fit;
            }
            internal override OpenXmlElement Create()
            {
                var xfit = new D.ShapeAutoFit();
                var fit = this;

                return xfit;
            }
        }

        internal static TextAutofitBase BuildEntry(PresentationImportArgs importArgs)
        {
            var xfit = importArgs.ArgElement;
            if (xfit is D.NoAutoFit)
            {
                return TextNoAutoFit.Build(importArgs);
            }
            else if (xfit is D.NormalAutoFit)
            {
                return TextNormalAutoFit.Build(importArgs);
            }
            else if (xfit is D.ShapeAutoFit)
            {
                return TextShapeAutoFit.Build(importArgs);
            }
            return null;
        }

        internal virtual OX.OpenXmlElement Create()
        {
            return null;
        }

        internal static OX.OpenXmlElement CreateEntry(TextAutofitBase fitbase)
        {
            return fitbase.Create();
        }

    }



    public class TextBodyProperties
    {
        public TextAutofitBase AutoFit;

        internal static TextBodyProperties Build(PresentationImportArgs importArgs)
        {
            var xProperties = importArgs.ArgElement as D.BodyProperties;
            var properties = new TextBodyProperties();



            return properties;
        }

        internal static D.BodyProperties Create(TextBodyProperties textBodyProperties)
        {
            var xProperties = new D.BodyProperties();


            return xProperties;
        }
    }

    public class TextBody : EchoList<TextParagraph>
    {
        internal P.TextBody _pTextBody;

        public bool? TextWrap { get; set; }


        // 默认填充。
        private static FillFormatWrapper _whiteFill = new FillFormatWrapper() { Fill = new SolidFill(new SolidColor(255, 255, 255) { Alpha = Emu.Percent100 }) };
        private static FillFormatWrapper _blackFill = new FillFormatWrapper() { Fill = new SolidFill(new SolidColor(0, 0, 0) { Alpha = Emu.Percent100 }) };



        public string ReadOnlyText
        {
            get
            {
                var temp = string.Empty;
                foreach (var item in this.Items)
                {
                    temp += item.ReadOnlyText + Environment.NewLine;
                }
                return temp;
            }
        }

        public FillFormatWrapper Fill
        {
            get
            {
                foreach (var para in Items)
                {
                    foreach (var textblock in para)
                    {
                        if (textblock.Fill.Fill is not null)
                        {
                            return textblock.Fill;
                        }
                    }
                }

                if ((Parent as Shape).Wrapper.IsTextBox.IsTrue())
                {
                    return _blackFill;
                }
                else
                {
                    return _whiteFill;
                }
            }
            set
            {
                ;
            }
        }
        public Emu FontSize
        {
            get
            {
                foreach (var para in Items)
                {
                    foreach (var textblock in para)
                    {
                        return textblock.FontSize;
                    }
                }
                return Emu.FontSize1 * 18;
            }
            set
            {
                ;
            }
        }
        public string FontFamily
        {
            get
            {
                foreach (var para in Items)
                {
                    foreach (var textblock in para)
                    {
                        return textblock.FontFamily;
                    }
                }
                return "微软雅黑";
            }
            set
            {
                ;
            }
        }

        public TextParagraph New()
        {
            throw new NotImplementedException();
        }
    }
}
