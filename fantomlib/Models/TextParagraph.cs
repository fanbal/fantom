﻿namespace Fantom.Models
{
    public class TextParagraph : EchoList<TextBlock>
    {
        public TextBlock New()
        {
            throw new NotImplementedException();
        }

        public string ReadOnlyText
        {
            get
            {
                var temp = string.Empty;
                foreach (var item in this.Items)
                {
                    temp += item.Text;
                }
                return temp;
            }
        }
    }
}
