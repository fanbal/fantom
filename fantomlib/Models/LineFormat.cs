﻿
using DocumentFormat.OpenXml;

namespace Fantom.Models
{
    public class LineFormat : EchoBaseObject
    {
        //
        // 摘要:
        //     Compound Line Type
        public enum CompoundLineValues
        {
            //
            // 摘要:
            //     Single Line.
            //     When the item is serialized out as xml, its value is "sng".
            [EnumString("sng")]
            Single,
            //
            // 摘要:
            //     Double Lines.
            //     When the item is serialized out as xml, its value is "dbl".
            [EnumString("dbl")]
            Double,
            //
            // 摘要:
            //     Thick Thin Double Lines.
            //     When the item is serialized out as xml, its value is "thickThin".
            [EnumString("thickThin")]
            ThickThin,
            //
            // 摘要:
            //     Thin Thick Double Lines.
            //     When the item is serialized out as xml, its value is "thinThick".
            [EnumString("thinThick")]
            ThinThick,
            //
            // 摘要:
            //     Thin Thick Thin Triple Lines.
            //     When the item is serialized out as xml, its value is "tri".
            [EnumString("tri")]
            Triple
        }

        //
        // 摘要:
        //     Alignment Type
        public enum PenAlignmentValues
        {
            //
            // 摘要:
            //     Center Alignment.
            //     When the item is serialized out as xml, its value is "ctr".
            [EnumString("ctr")]
            Center,
            //
            // 摘要:
            //     Inset Alignment.
            //     When the item is serialized out as xml, its value is "in".
            [EnumString("in")]
            Insert
        }

        //
        // 摘要:
        //     End Line Cap
        public enum LineCapValues
        {
            //
            // 摘要:
            //     Round Line Cap.
            //     When the item is serialized out as xml, its value is "rnd".
            [EnumString("rnd")]
            Round,
            //
            // 摘要:
            //     Square Line Cap.
            //     When the item is serialized out as xml, its value is "sq".
            [EnumString("sq")]
            Square,
            //
            // 摘要:
            //     Flat Line Cap.
            //     When the item is serialized out as xml, its value is "flat".
            [EnumString("flat")]
            Flat
        }

        public class LineDashBase
        {

            public class LinePresetDash : LineDashBase
            {
                //
                // 摘要:
                //     Preset Line Dash Value
                public enum PresetLineDashType
                {
                    //
                    // 摘要:
                    //     Solid.
                    //     When the item is serialized out as xml, its value is "solid".
                    //[EnumString(("solid")]
                    Solid,
                    //
                    // 摘要:
                    //     Dot.
                    //     When the item is serialized out as xml, its value is "dot".
                    //[EnumString(("dot")]
                    Dot,
                    //
                    // 摘要:
                    //     Dash.
                    //     When the item is serialized out as xml, its value is "dash".
                    //[EnumString(("dash")]
                    Dash,
                    //
                    // 摘要:
                    //     Large Dash.
                    //     When the item is serialized out as xml, its value is "lgDash".
                    //[EnumString(("lgDash")]
                    LargeDash,
                    //
                    // 摘要:
                    //     Dash Dot.
                    //     When the item is serialized out as xml, its value is "dashDot".
                    //[EnumString(("dashDot")]
                    DashDot,
                    //
                    // 摘要:
                    //     Large Dash Dot.
                    //     When the item is serialized out as xml, its value is "lgDashDot".
                    //[EnumString(("lgDashDot")]
                    LargeDashDot,
                    //
                    // 摘要:
                    //     Large Dash Dot Dot.
                    //     When the item is serialized out as xml, its value is "lgDashDotDot".
                    //[EnumString(("lgDashDotDot")]
                    LargeDashDotDot,
                    //
                    // 摘要:
                    //     System Dash.
                    //     When the item is serialized out as xml, its value is "sysDash".
                    //[EnumString(("sysDash")]
                    SystemDash,
                    //
                    // 摘要:
                    //     System Dot.
                    //     When the item is serialized out as xml, its value is "sysDot".
                    //[EnumString(("sysDot")]
                    SystemDot,
                    //
                    // 摘要:
                    //     System Dash Dot.
                    //     When the item is serialized out as xml, its value is "sysDashDot".
                    //[EnumString(("sysDashDot")]
                    SystemDashDot,
                    //
                    // 摘要:
                    //     System Dash Dot Dot.
                    //     When the item is serialized out as xml, its value is "sysDashDotDot".
                    //[EnumString(("sysDashDotDot")]
                    SystemDashDotDot
                }

                public PresetLineDashType Type;

                internal static LinePresetDash BuildPresetDash(PresentationImportArgs importArgs)
                {
                    var xDash = importArgs.ArgElement as D.PresetDash;
                    var dash = new LinePresetDash();

                    dash.Type = (PresetLineDashType)xDash.Val.Value;

                    return dash;
                }

                internal override DocumentFormat.OpenXml.OpenXmlElement Export(PresentationExportArgs exportArgs)
                {
                    var xDash = new D.PresetDash();
                    xDash.Val = (D.PresetLineDashValues)Type;
                    return xDash;
                }
            }

            public class LineCustomDash : LineDashBase
            {
                public class DashStop
                {
                    public Emu DashLength;
                    public Emu SpaceLength;

                    internal static DashStop BuildDashStop(PresentationImportArgs importArgs)
                    {
                        var xDashStop = importArgs.ArgElement as D.DashStop;
                        var dashStop = new DashStop();
                        dashStop.DashLength = xDashStop.DashLength;
                        dashStop.SpaceLength = xDashStop.SpaceLength;
                        return dashStop;

                    }

                    internal static D.DashStop CreateDashStop(DashStop dashStop, PresentationExportArgs exportArgs)
                    {
                        var xDashStop = new D.DashStop();
                        xDashStop.DashLength = dashStop.DashLength.ToInt();
                        xDashStop.SpaceLength = dashStop.SpaceLength.ToInt();
                        return xDashStop;
                    }

                }

                public List<DashStop> DashStops;

                internal static LineCustomDash BuildCustomDash(PresentationImportArgs importArgs)
                {
                    var xDash = importArgs.ArgElement as D.CustomDash;
                    var dash = new LineCustomDash();

                    dash.DashStops = new List<DashStop>();

                    foreach (var xChild in xDash.Elements<D.DashStop>())
                    {
                        dash.DashStops.Add(DashStop.BuildDashStop(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                    }

                    return dash;
                }

                internal override DocumentFormat.OpenXml.OpenXmlElement Export(PresentationExportArgs exportArgs)
                {
                    var xDash = new D.CustomDash();
                    foreach (var dashStop in DashStops)
                    {
                        xDash.AppendExtend(DashStop.CreateDashStop(dashStop, exportArgs));
                    }
                    return xDash;
                }
            }

            internal virtual OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                return null;
            }

            internal static LineDashBase BuildLineDashEntry(PresentationImportArgs importArgs)
            {
                var xDash = importArgs.ArgElement;
                if (xDash is null) return null;
                if (xDash is D.CustomDash) return LineCustomDash.BuildCustomDash(importArgs);
                if (xDash is D.PresetDash) return LinePresetDash.BuildPresetDash(importArgs);
                throw new Exception();
            }

            internal static OX.OpenXmlElement CreateLineDashEntry(LineDashBase dash, PresentationExportArgs exportArgs)
            {
                if (dash is null) return null;
                return dash.Export(exportArgs);
            }

        }

        public class LineJoinBase
        {
            public class LineJoinRound : LineJoinBase
            {
                internal override DocumentFormat.OpenXml.OpenXmlElement Export(PresentationExportArgs exportArgs)
                {
                    return new D.Round();
                }
            }

            public class LineJoinBevel : LineJoinBase
            {
                internal override DocumentFormat.OpenXml.OpenXmlElement Export(PresentationExportArgs exportArgs)
                {
                    return new D.LineJoinBevel();
                }
            }
            public class LineJoinMiterProperties : LineJoinBase
            {
                public Emu Limit;
                internal override DocumentFormat.OpenXml.OpenXmlElement Export(PresentationExportArgs exportArgs)
                {
                    return new D.Miter() { Limit = Limit.ToInt() };
                }
            }

            virtual internal OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
            {
                return null;
            }

            internal static LineJoinBase BuildLineJoinEntry(PresentationImportArgs importArgs)
            {
                var element = importArgs.ArgElement;
                if (element is null) return null;
                LineJoinBase lineJoin = null;
                if (element is D.LineJoinBevel)
                {
                    lineJoin = new LineJoinBevel();
                }
                else if (element is D.Round)
                {
                    lineJoin = new LineJoinRound();
                }
                else if (element is D.Miter)
                {
                    lineJoin = new LineJoinMiterProperties() { Limit = (element as D.Miter).Limit };
                }
                else
                {
                    throw new Exception("未知 Join。");
                }
                return lineJoin;
            }

            internal static OX.OpenXmlElement CreateLineJoinEntry(LineJoinBase lineJoin, PresentationExportArgs exportArgs)
            {
                if (lineJoin is null) return null;
                return lineJoin.Export(exportArgs);
            }

        }

        public class LineEndProperties
        {
            //
            // 摘要:
            //     Line End Type
            public enum LineEndType
            {
                //
                // 摘要:
                //     None.
                //     When the item is serialized out as xml, its value is "none".
                //[EnumString("none")]
                None,
                //
                // 摘要:
                //     Triangle Arrow Head.
                //     When the item is serialized out as xml, its value is "triangle".
                //[EnumString("triangle")]
                Triangle,
                //
                // 摘要:
                //     Stealth Arrow.
                //     When the item is serialized out as xml, its value is "stealth".
                //[EnumString("stealth")]
                Stealth,
                //
                // 摘要:
                //     Diamond.
                //     When the item is serialized out as xml, its value is "diamond".
                //[EnumString("diamond")]
                Diamond,
                //
                // 摘要:
                //     Oval.
                //     When the item is serialized out as xml, its value is "oval".
                //[EnumString("oval")]
                Oval,
                //
                // 摘要:
                //     Arrow Head.
                //     When the item is serialized out as xml, its value is "arrow".
                //[EnumString("arrow")]
                Arrow
            }


            public enum LineEndSize
            {
                Small, Medium, Large
            }

            public LineEndType? Type;
            public LineEndSize? Width;
            public LineEndSize? Length;

            internal static LineEndProperties BuildLineEndProperties(PresentationImportArgs importArgs)
            {
                var xLineEndProperties = importArgs.ArgElement as D.LineEndPropertiesType;
                if (xLineEndProperties is null) return null;
                var lineEndProperties = new LineEndProperties();
                if (xLineEndProperties.Type is not null) lineEndProperties.Type = (LineEndType)xLineEndProperties.Type.Value;
                if (xLineEndProperties.Width is not null) lineEndProperties.Width = (LineEndSize)xLineEndProperties.Width.Value;
                if (xLineEndProperties.Length is not null) lineEndProperties.Length = (LineEndSize)xLineEndProperties.Length.Value;
                return lineEndProperties;
            }

            internal static T CreateLineEndProperties<T>(LineEndProperties lineEndProperties, PresentationExportArgs exportArgs)
                where T : D.LineEndPropertiesType, new()
            {
                if (lineEndProperties is null) return null;
                var xLineEndProperties = new T();

                if (lineEndProperties.Type is not null) xLineEndProperties.Type = (D.LineEndValues)lineEndProperties.Type;
                if (lineEndProperties.Width is not null) xLineEndProperties.Width = (D.LineEndWidthValues)lineEndProperties.Width;
                if (lineEndProperties.Length is not null) xLineEndProperties.Length = (D.LineEndLengthValues)lineEndProperties.Length;

                return xLineEndProperties;
            }

        }

        public FillBase Fill;
        public LineDashBase LineDash;
        public LineJoinBase JoinBase;

        public LineEndProperties HeadEnd;
        public LineEndProperties TailEnd;

        public Emu? Width;
        public LineCapValues? Cap;
        public CompoundLineValues? CompoundLine;
        public PenAlignmentValues? Alignment;
        internal static LineFormat BuildLineFormat(PresentationImportArgs importArgs)
        {
            var xLine = importArgs.ArgElement as D.LinePropertiesType;
            if (xLine is null) return null;
            var line = new LineFormat();
            line.Fill = FillBase.BuildFillEntry(xLine.FirstChild, new PresentationImportArgs(importArgs) { ArgElement = xLine.GetElement<D.NoFill, D.SolidFill, D.GradientFill, D.PatternFill>() });
            line.LineDash = LineDashBase.BuildLineDashEntry(new PresentationImportArgs(importArgs) { ArgElement = xLine.GetElement<D.PresetDash, D.CustomDash>() });
            line.JoinBase = LineJoinBase.BuildLineJoinEntry(new PresentationImportArgs(importArgs) { ArgElement = xLine.GetElement<D.Round, D.LineJoinBevel, D.Miter>() });
            line.HeadEnd = LineEndProperties.BuildLineEndProperties(new PresentationImportArgs(importArgs) { ArgElement = xLine.GetElement<D.HeadEnd>() });
            line.TailEnd = LineEndProperties.BuildLineEndProperties(new PresentationImportArgs(importArgs) { ArgElement = xLine.GetElement<D.TailEnd>() });

            if (xLine.Width is not null) line.Width = xLine.Width;
            if (xLine.CapType is not null) line.Cap = (LineCapValues)xLine.CapType.Value;
            if (xLine.CompoundLineType is not null) line.CompoundLine = (CompoundLineValues)xLine.CompoundLineType?.Value;
            if (xLine.Alignment is not null) line.Alignment = (PenAlignmentValues)xLine.Alignment?.Value;

            return line;
        }

        internal static D.Outline CreateLineFormat(LineFormat line, PresentationExportArgs exportArgs)
        {
            if (line is null) return null;
            var xLine = new D.Outline();
           
            if (line.Fill is not null)
                xLine.AppendExtend(FillBase.CreateFillEntry(line.Fill, exportArgs));

            xLine.AppendExtend(LineDashBase.CreateLineDashEntry(line.LineDash, exportArgs));
            xLine.AppendExtend(LineJoinBase.CreateLineJoinEntry(line.JoinBase, exportArgs));
            xLine.AppendExtend(LineEndProperties.CreateLineEndProperties<D.HeadEnd>(line.HeadEnd, exportArgs));
            xLine.AppendExtend(LineEndProperties.CreateLineEndProperties<D.TailEnd>(line.TailEnd, exportArgs));

            xLine.Width = line.Width.ToOX32();
            if (line.Cap is not null) xLine.CapType = (D.LineCapValues)line.Cap;
            if (line.CompoundLine is not null) xLine.CompoundLineType = (D.CompoundLineValues)line.CompoundLine;
            if (line.Alignment is not null) xLine.Alignment = (D.PenAlignmentValues)line.Alignment;

            return xLine;
        }



    }
}
