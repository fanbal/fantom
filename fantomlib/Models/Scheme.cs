﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Fantom.Models
{

    /// <summary>
    /// 颜色方案。
    /// </summary>
    public class ColorScheme : EchoBaseObject
    {

        public ColorBase this[ColorSchemeColorType type]
        {
            get
            {
                switch (type)
                {
                    case ColorSchemeColorType.Unknown:
                        throw new Exception("未知颜色");
                    case ColorSchemeColorType.Background1:
                        throw new Exception("Background1");
                    case ColorSchemeColorType.Text1:
                        throw new Exception("Text1");
                    case ColorSchemeColorType.Background2:
                        throw new Exception("Background2");
                    case ColorSchemeColorType.Text2:
                        throw new Exception("Text2");
                    case ColorSchemeColorType.Accent1:
                        return Accent1;
                    case ColorSchemeColorType.Accent2:
                        return Accent2;
                    case ColorSchemeColorType.Accent3:
                        return Accent3;
                    case ColorSchemeColorType.Accent4:
                        return Accent4;
                    case ColorSchemeColorType.Accent5:
                        return Accent5;
                    case ColorSchemeColorType.Accent6:
                        return Accent6;
                    case ColorSchemeColorType.Hyperlink:
                        return HyperLink;
                    case ColorSchemeColorType.FollowedHyperlink:
                        return FollowedHyperlink;
                    case ColorSchemeColorType.PhColor:
                        throw new Exception("PhColor");
                    case ColorSchemeColorType.Dark1:
                        return Dark1;
                    case ColorSchemeColorType.Light1:
                        return Light1;
                    case ColorSchemeColorType.Dark2:
                        return Dark2;
                    case ColorSchemeColorType.Light2:
                        return Light2;
                    default:
                        throw new Exception("不明颜色。");
                }
            }
            set
            {
                switch (type)
                {
                    case ColorSchemeColorType.Unknown:
                        throw new Exception("未知颜色");
                    case ColorSchemeColorType.Background1:
                        throw new Exception("Background1");
                    case ColorSchemeColorType.Text1:
                        throw new Exception("Text1");
                    case ColorSchemeColorType.Background2:
                        throw new Exception("Background2");
                    case ColorSchemeColorType.Text2:
                        throw new Exception("Text2");
                    case ColorSchemeColorType.Accent1:
                        Accent1 = value; break;

                    case ColorSchemeColorType.Accent2:
                        Accent2 = value; break;
                    case ColorSchemeColorType.Accent3:
                        Accent3 = value; break;
                    case ColorSchemeColorType.Accent4:
                        Accent4 = value; break;
                    case ColorSchemeColorType.Accent5:
                        Accent5 = value; break;
                    case ColorSchemeColorType.Accent6:
                        Accent6 = value; break;
                    case ColorSchemeColorType.Hyperlink:
                        HyperLink = value; break;
                    case ColorSchemeColorType.FollowedHyperlink:
                        FollowedHyperlink = value; break;
                    case ColorSchemeColorType.PhColor:
                        throw new Exception("PhColor");
                    case ColorSchemeColorType.Dark1:
                        Dark1 = value; break;
                    case ColorSchemeColorType.Light1:
                        Light1 = value; break;
                    case ColorSchemeColorType.Dark2:
                        Dark2 = value; break;
                    case ColorSchemeColorType.Light2:
                        Light2 = value; break;
                    default:
                        throw new Exception("不明颜色。");
                }
            }
        }

        /// <summary>
        /// 暗色1。
        /// </summary>
        public ColorBase Dark1 { get; set; }

        /// <summary>
        /// 暗色2。
        /// </summary>
        public ColorBase Dark2 { get; set; }

        /// <summary>
        /// 亮色1.
        /// </summary>
        public ColorBase Light1 { get; set; }

        /// <summary>
        /// 亮色2。
        /// </summary>
        public ColorBase Light2 { get; set; }

        public ColorBase Accent1 { get; set; }
        public ColorBase Accent2 { get; set; }
        public ColorBase Accent3 { get; set; }
        public ColorBase Accent4 { get; set; }
        public ColorBase Accent5 { get; set; }
        public ColorBase Accent6 { get; set; }
        public ColorBase HyperLink { get; set; }
        public ColorBase FollowedHyperlink { get; set; }


        internal static ColorScheme LoadColorScheme(D.ColorScheme scheme)
        {
            if (scheme == null) return null;
            var colorScheme = new ColorScheme();
            colorScheme.Name = scheme.Name;

            colorScheme.Accent1 = ColorBuilder.OpenXmlElementToColor(scheme.Accent1Color.FirstChild);
            colorScheme.Accent2 = ColorBuilder.OpenXmlElementToColor(scheme.Accent2Color.FirstChild);
            colorScheme.Accent3 = ColorBuilder.OpenXmlElementToColor(scheme.Accent3Color.FirstChild);
            colorScheme.Accent4 = ColorBuilder.OpenXmlElementToColor(scheme.Accent4Color.FirstChild);
            colorScheme.Accent5 = ColorBuilder.OpenXmlElementToColor(scheme.Accent5Color.FirstChild);
            colorScheme.Accent6 = ColorBuilder.OpenXmlElementToColor(scheme.Accent6Color.FirstChild);
            colorScheme.Dark1 = ColorBuilder.OpenXmlElementToColor(scheme.Dark1Color.FirstChild);
            colorScheme.Dark2 = ColorBuilder.OpenXmlElementToColor(scheme.Dark2Color.FirstChild);
            colorScheme.Light1 = ColorBuilder.OpenXmlElementToColor(scheme.Light1Color.FirstChild);
            colorScheme.Light2 = ColorBuilder.OpenXmlElementToColor(scheme.Light2Color.FirstChild);
            colorScheme.HyperLink = ColorBuilder.OpenXmlElementToColor(scheme.Hyperlink.FirstChild);
            colorScheme.FollowedHyperlink = ColorBuilder.OpenXmlElementToColor(scheme.FollowedHyperlinkColor.FirstChild);

            return colorScheme;
        }

        internal static D.ColorScheme CreateColorScheme(ColorScheme colorScheme)
        {
            if (colorScheme is null) return null;
            var xcolorScheme = new D.ColorScheme();

            xcolorScheme.AppendExtend(
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Accent1Color>(colorScheme.Accent1),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Accent2Color>(colorScheme.Accent2),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Accent3Color>(colorScheme.Accent3),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Accent4Color>(colorScheme.Accent4),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Accent5Color>(colorScheme.Accent5),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Accent6Color>(colorScheme.Accent6),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Dark1Color>(colorScheme.Dark1),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Dark2Color>(colorScheme.Dark1),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Light1Color>(colorScheme.Light1),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Light2Color>(colorScheme.Light2),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.Hyperlink>(colorScheme.HyperLink),
                ColorBuilder.CreateOpenXmlElementWithColorNode<D.FollowedHyperlinkColor>(colorScheme.FollowedHyperlink)
                );


            return xcolorScheme;
        }





    }

    /// <summary>
    /// 字体。
    /// </summary>
    public class TextFont
    {
        public string TypeFace { get; set; }
        public string Panose { get; set; }
        public string PitchFamily { get; set; }
        public byte CharSet { get; set; }

    }

    /// <summary>
    /// 字体集合。
    /// </summary>
    public class FontCollection
    {
        public TextFont Latin;
        public TextFont EA;
        public TextFont CS;
    }

    /// <summary>
    /// 字体方案。
    /// </summary>
    public class FontScheme : EchoBaseObject
    {
        public FontCollection MajorFont { get; internal set; }
        public FontCollection MinorFont { get; internal set; }
    }

    /// <summary>
    /// 格式方案。
    /// </summary>
    public class FormatScheme : EchoBaseObject
    {

        public class FillFormatList : List<FillFormatWrapper>
        {
            internal static FillFormatList Build(PresentationImportArgs importArgs)
            {
                var xFillList = importArgs.ArgElement as D.FillStyleList;
                var fillList = new FillFormatList();
                foreach (var xChild in xFillList.GetElements<D.NoFill, D.SolidFill, D.GradientFill, D.GroupFill, P.BlipFill, D.PatternFill>())
                {
                    fillList.Add(FillFormatWrapper.BuildFillFormat(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }
                return fillList;
            }

            internal static T Create<T>(FillFormatList fillList, PresentationExportArgs exportArgs) where T : OX.OpenXmlElement,new()
            {
                var xFillList = new T();

                foreach (var child in fillList)
                {
                    xFillList.AppendExtend(FillFormatWrapper.CreateFillFormat(child, exportArgs));
                }

                return xFillList;
            }
        }

        public class LineFormatList : List<LineFormat>
        {
            internal static LineFormatList Build(PresentationImportArgs importArgs)
            {
                var xFillList = importArgs.ArgElement as D.LineStyleList;
                var fillList = new LineFormatList();
                foreach (var xChild in xFillList.GetElements<D.NoFill, D.SolidFill, D.GradientFill, D.GroupFill, P.BlipFill, D.PatternFill>())
                {
                    fillList.Add(LineFormat.BuildLineFormat(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }
                return fillList;
            }

            internal static D.LineStyleList Create(LineFormatList fillList, PresentationExportArgs exportArgs)
            {
                var xFillList = new D.LineStyleList();

                foreach (var child in fillList)
                {
                    xFillList.AppendExtend(LineFormat.CreateLineFormat(child, exportArgs));
                }

                return xFillList;
            }
        }

        public class EffectStyle
        {
            public EffectProperties EffectProperties;
            public Scene3D Scene3D;
            public Shape3D Shape3D;

            internal static EffectStyle BuildEffectStyle(PresentationImportArgs importArgs)
            {
                var xEffectStyle = importArgs.ArgElement as D.EffectStyle;
                var effectStyle = new EffectStyle();
                effectStyle.EffectProperties = EffectProperties.BuildEffectProperties(new PresentationImportArgs(importArgs) { ArgElement = xEffectStyle.GetElement<D.EffectList, D.EffectDag>() });
                effectStyle.Scene3D = Scene3D.BuildScene3D(new PresentationImportArgs(importArgs) { ArgElement = xEffectStyle.GetElement<D.Scene3DType>() });
                effectStyle.Shape3D = Shape3D.BuildShape3D(new PresentationImportArgs(importArgs) { ArgElement = xEffectStyle.GetElement<D.Shape3DType>() });
                return effectStyle;
            }

            internal static D.EffectStyle CreateEffectStyle(EffectStyle effectStyle, PresentationExportArgs exportArgs)
            {
                var xEffectStyle = new D.EffectStyle();
                xEffectStyle.AppendExtend(EffectProperties.CreateEffectProperties(effectStyle.EffectProperties, exportArgs));
                xEffectStyle.AppendExtend(Scene3D.CreateScene3D(effectStyle.Scene3D, exportArgs));
                xEffectStyle.AppendExtend(Shape3D.CreateShape3D(effectStyle.Shape3D, exportArgs));

                return xEffectStyle;
            }

        }

        public class EffectList : List<EffectStyle>
        {
            internal static EffectList Build(PresentationImportArgs importArgs)
            {
                var xEffectList = importArgs.ArgElement as D.EffectStyleList;
                var effectList = new EffectList();
                foreach (var xChild in xEffectList.GetElement<D.EffectStyle>())
                {
                    effectList.Add(EffectStyle.BuildEffectStyle(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }
                return effectList;
            }

            internal static D.EffectStyleList CreateEffectList(EffectList effectList, PresentationExportArgs exportArgs)
            {
                var xEffectList = new D.EffectStyleList();

                foreach (var child in effectList)
                {
                    xEffectList.AppendExtend(EffectStyle.CreateEffectStyle(child, exportArgs));
                }

                return xEffectList;
            }

        }




        public FillFormatList Fills;
        public LineFormatList Lines;
        public EffectList Effects;
        public FillFormatList BackgroundFills;

        internal static FormatScheme BuildShapeStyle(PresentationImportArgs importArgs)
        {
            var xShapeStyle = importArgs.ArgElement as D.FormatScheme;
            if (xShapeStyle == null) return null;
            var shapeStyle = new FormatScheme();

            shapeStyle.Fills = FillFormatList.Build(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.FillStyleList });
            shapeStyle.Lines = LineFormatList.Build(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.LineStyleList });
            shapeStyle.Effects = EffectList.Build(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.EffectStyleList });
            shapeStyle.BackgroundFills = FillFormatList.Build(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.BackgroundFillStyleList });

            return shapeStyle;
        }

        internal static D.FormatScheme CreateShapeStyle(FormatScheme shapeStyle, PresentationExportArgs exportArgs)
        {
            if (shapeStyle is null) return null;
            var xShapeStyle = new D.FormatScheme();

            xShapeStyle.FillStyleList = FillFormatList.Create<D.FillStyleList>(shapeStyle.Fills, exportArgs);
            xShapeStyle.LineStyleList = LineFormatList.Create(shapeStyle.Lines, exportArgs);
            xShapeStyle.EffectStyleList = EffectList.CreateEffectList(shapeStyle.Effects, exportArgs);
            xShapeStyle.BackgroundFillStyleList = FillFormatList.Create<D.BackgroundFillStyleList>(shapeStyle.BackgroundFills, exportArgs);


            return xShapeStyle;
        }
    }

}
