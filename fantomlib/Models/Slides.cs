﻿using System.Linq;

namespace Fantom.Models
{
    public class Slides : EchoSelectionList<Slide>
    {
        #region properties

        public IEnumerable<Slide> SelectedSlides => from sld in this.Items where sld.IsSelected select sld;

        #endregion

        #region ctors

        #endregion

        #region methods

        public Slide New()
        {
            var sld = SlideBaseBuilder.BuildSlide();
            return sld;
        }

        internal override void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            foreach (var member in Items)
            {
                var counter2 = 1U;
                member.LoadPackIDMapper(member, ref counter2);
            }

        }

    

        #endregion
    }
}
