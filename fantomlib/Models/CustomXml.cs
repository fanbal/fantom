﻿using System.Xml.Linq;

namespace Fantom.Models
{

    /// <summary>
    /// 自定义Xml对象，用于安全可靠地存储用户的自定义信息。
    /// </summary>
    public class CustomXml : EchoBaseObject
    {
        public XDocument XDocument { get; set; }
    }
}
