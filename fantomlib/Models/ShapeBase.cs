﻿
namespace Fantom.Models
{

    /// <summary>
    /// 图形基类，派生类有<see cref="Shape"/>，<see cref="Connection"/>，<see cref="Group"/>，<see cref="Picture"/>
    /// </summary>
    public abstract class ShapeBase : EchoSelectableObject
    {
        #region static

        private static HashSet<Type> _shapeTypeHashSet = new HashSet<Type>()
        {
            typeof(P.Shape), typeof(P.GroupShape), typeof(P.Picture), typeof(P.ConnectionShape)
        };

        internal static bool IsShapeTypeX(OX.OpenXmlElement element)
        {
            return _shapeTypeHashSet.Contains(element.GetType());
        }

        #endregion

        public class ShapeBaseWrapper
        {
            #region properties



            public HyperLink? HyperLinkClick;

            public HyperLink? HyperLinkHover;


            public string? Description;

            public bool? Hidden;

            public string? Title;

            public PlaceHolder PlaceHolder;

            public MediaBase Media;

            public bool? IsPhoto;
            public bool? UserDrawn;

            public Emu? Left;
            public Emu? Top;
            public Emu? Width;
            public Emu? Height;

            public Emu? Rotation;
            public bool? FlipHorizontal;
            public bool? FlipVertical;

            public FillFormatWrapper Fill;

            public EffectProperties EffectProperties;

            public Scene3D Scene3D;

            #endregion

        }

        /// <summary>
        /// 图形类型。
        /// </summary>
        public virtual ShapeType Type => ShapeType.Unknown;


        public ShapeBaseWrapper BaseWrapper { get; internal set; } = new ShapeBaseWrapper();

        /// <summary>
        /// 图形锁。
        /// </summary>
        public LocksBase Locks { get; internal set; }

        /// <summary>
        /// 图形的左坐标。
        /// </summary>
        public double Left
        {
            get => BaseWrapper.Left is null ? 0d : BaseWrapper.Left.Value.ToPixelValue();
            set { BaseWrapper.Left = new Pixel(value).ToEmu(); RaiseEvent(); }
        }

        /// <summary>
        /// 图形的顶坐标。
        /// </summary>
        public double Top
        {
            get => BaseWrapper.Top is null ? 0d : BaseWrapper.Top.Value.ToPixelValue();
            set { BaseWrapper.Top = new Pixel(value).ToEmu(); RaiseEvent(); }
        }

        /// <summary>
        /// 图形的宽度。
        /// </summary>
        public double Width
        {
            get => BaseWrapper.Width is null ? 0d : BaseWrapper.Width.Value.ToPixelValue();
            set { BaseWrapper.Width = new Pixel(value).ToEmu(); RaiseEvent(); }
        }

        /// <summary>
        /// 图形的高度。
        /// </summary>
        public double Height
        {
            get => BaseWrapper.Height is null ? 0d : BaseWrapper.Height.Value.ToPixelValue();
            set { BaseWrapper.Height = new Pixel(value).ToEmu(); RaiseEvent(); }
        }

        /// <summary>
        /// 图形的旋转角度。
        /// </summary>
        public double Rotation
        {
            get => BaseWrapper.Rotation is null ? 0d : BaseWrapper.Rotation.Value.ToAngle();
            set { BaseWrapper.Rotation = Emu.FromAngle(value); RaiseEvent(); }
        }

        /// <summary>
        /// 图形填充。
        /// </summary>
        public FillFormat Fill => _fill;
        private FillFormat _fill;


        #region ctor

        public ShapeBase()
        {
            _fill = new FillFormat() { ShapeBase = this };
        }
        #endregion

        #region methods

        public override string ToString()
        {
            return $"Shape: {Name}, ID = {(Parent as Shapes).IndexOf(this)}";
        }


        internal EchoBaseObject GetSlidePartParent()
        {
            var sld = GetParent<Slide>();
            if (sld is not null) return sld;
            var layout = GetParent<SlideLayout>();
            if (layout is not null) return layout;
            var noteSlide = GetParent<NoteSlide>();
            if (noteSlide is not null) return noteSlide;
            var slidemaster = GetParent<SlideMaster>();
            if (slidemaster is not null) return slidemaster;
            var noteMaster = GetParent<NoteMaster>();
            if (noteMaster is not null) return noteMaster;
            throw new Exception($"无法知道当前图形 {Name} 所处画布对象。");
        }




        #endregion
    }


    public class Shape : ShapeBase
    {
        public class ShapeWrapper
        {
            public GeometryBase Geometry;
            public bool? IsTextBox;

            public LineFormat Line;
            public ShapeStyleWrapper Style;
            public Shape3D Shape3D;

            public TextBody TextBody;
        }

        public override ShapeType Type => ShapeType.Shape;
        public ShapeLocks ShapeLocks => base.Locks as ShapeLocks;


        public string Text
        {
            get
            {
                if (Wrapper.TextBody is null)
                    return string.Empty;
                else
                    return Wrapper.TextBody.ReadOnlyText;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public ShapeWrapper Wrapper { get; internal set; } = new ShapeWrapper();


    }

    public class Picture : ShapeBase
    {
        public class PictureWrapper
        {
            public GeometryBase Geometry;
            public PictureFill PictureFill;

            public LineFormat Line;
            public ShapeStyleWrapper Style;
            public Shape3D Shape3D;
        }

        public override ShapeType Type => ShapeType.Picture;
        public PictureLocks PictureLocks => base.Locks as PictureLocks;

        public PictureFill PictureFill
        {
            get => Wrapper.PictureFill;


        }


        public PictureWrapper Wrapper { get; internal set; } = new PictureWrapper();


        internal override void LoadPropertyRelationshipIDMap(EchoBaseObject mapperContainer, ref uint counter)
        {
            if(Wrapper.PictureFill is not null)
            {
                if(Wrapper.PictureFill.Media is not null)
                {
                    if (!mapperContainer._propertyRelationshipIDMapper.ContainsKey(Wrapper.PictureFill.Media))
                    {
                        counter++;
                        mapperContainer._propertyRelationshipIDMapper.Add(Wrapper.PictureFill.Media, counter);
                    }
                }
            }

           
        }




    }

    public class Connection : ShapeBase
    {
        public class ConnectionData
        {
            public uint ElementIndex;
            public uint Index;

            internal static ConnectionData BuildConnectionData(PresentationImportArgs importArgs)
            {
                D.ConnectionType xConnection = importArgs.ArgElement as D.ConnectionType;
                if (xConnection is null) return null;
                var connection = new ConnectionData();
                if (xConnection.Index is not null) connection.Index = xConnection.Index;
                if (xConnection.Id is not null) connection.ElementIndex = xConnection.Id;
                return connection;
            }

            internal static T CreateConnectionData<T>(ConnectionData connection, PresentationExportArgs exportArgs) where T : D.ConnectionType, new()
            {
                if (connection is null) return null;
                var xConnection = new T();
                xConnection.Index = connection.Index;
                xConnection.Id = connection.ElementIndex;
                return xConnection;
            }

        }

        public class ConnectionWrapper
        {
            public ConnectionData ConnectionStart;
            public ConnectionData ConnectionEnd;

            public GeometryBase Geometry;

            public LineFormat Line;
            public Shape3D Shape3D;
            public ShapeStyleWrapper Style;
        }

        public override ShapeType Type => ShapeType.Connection;

        public ConnectionShapeLocks ConnectionShapeLocks => base.Locks as ConnectionShapeLocks;

        public ConnectionWrapper Wrapper { get; internal set; } = new ConnectionWrapper();



    }

    public class Group : ShapeBase, IList<ShapeBase>
    {

        public class GroupWrapper
        {
            public Emu? ChOffX, ChOffY, ChExtX, ChExtY;
        }

        public ShapeBase this[int index] => Items[index];
        public ShapeBase this[string name] => Items[name];

        public Shapes Items;
        public override ShapeType Type => ShapeType.Group;
        public GroupShapeLocks GroupShapeLocks => base.Locks as GroupShapeLocks;

        public GroupWrapper Wrapper { get; internal set; } = new GroupWrapper();






        #region 实现接口
        public int Count => ((ICollection<ShapeBase>)Items).Count;

        public bool IsReadOnly => ((ICollection<ShapeBase>)Items).IsReadOnly;

        ShapeBase IList<ShapeBase>.this[int index] { get => ((IList<ShapeBase>)Items)[index]; set => ((IList<ShapeBase>)Items)[index] = value; }

        public int IndexOf(ShapeBase item)
        {
            return ((IList<ShapeBase>)Items).IndexOf(item);
        }

        public void Insert(int index, ShapeBase item)
        {
            ((IList<ShapeBase>)Items).Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            ((IList<ShapeBase>)Items).RemoveAt(index);
        }

        public void Add(ShapeBase item)
        {
            ((ICollection<ShapeBase>)Items).Add(item);
        }

        public void Clear()
        {
            ((ICollection<ShapeBase>)Items).Clear();
        }

        public bool Contains(ShapeBase item)
        {
            return ((ICollection<ShapeBase>)Items).Contains(item);
        }

        public void CopyTo(ShapeBase[] array, int arrayIndex)
        {
            ((ICollection<ShapeBase>)Items).CopyTo(array, arrayIndex);
        }

        public bool Remove(ShapeBase item)
        {
            return ((ICollection<ShapeBase>)Items).Remove(item);
        }

        public IEnumerator<ShapeBase> GetEnumerator()
        {
            return ((IEnumerable<ShapeBase>)Items).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Items).GetEnumerator();
        }

        #endregion

    }

}
