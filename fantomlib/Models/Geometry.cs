﻿
using Fantom.Helper;
using System.Linq;
using static Fantom.Models.GeometryBase.AdjustHandleBase;

namespace Fantom.Models
{
    public class GeometryBase
    {

        private static HashSet<Type> _geometryHashSet = new HashSet<Type>()
            {
                typeof(D.CloseShapePath), typeof(D.MoveTo)
            };

        internal static bool IsGeometryTypeX(OX.OpenXmlElement element)
        {
            return _geometryHashSet.Contains(element.GetType());
        }

        public class GeomRect
        {
            public string Left, Top, Right, Bottom;
            internal static GeomRect BuildGeomRect(PresentationImportArgs importArgs)
            {
                var xGeomRect = importArgs.ArgElement as D.Rectangle;
                if (xGeomRect is null) return null;
                var geomRect = new GeomRect();

                geomRect.Left = xGeomRect.Left;
                geomRect.Top = xGeomRect.Top;
                geomRect.Right = xGeomRect.Right;
                geomRect.Bottom = xGeomRect.Bottom;

                return geomRect;
            }


            internal static D.Rectangle CreateGeomRect(GeomRect geomRect, PresentationExportArgs exportArgs)
            {
                if (geomRect is null) return null;
                var xGeomRect = new D.Rectangle();

                xGeomRect.Left = geomRect.Left;
                xGeomRect.Top = geomRect.Top;
                xGeomRect.Right = geomRect.Right;
                xGeomRect.Bottom = geomRect.Bottom;

                return xGeomRect;
            }

        }

        public class GeomGuide
        {
            public string Name;
            public string Formula;
            internal static GeomGuide BuildGeomGuide(PresentationImportArgs importArgs)
            {
                var xGeomGuide = importArgs.ArgElement as D.ShapeGuide;
                if (xGeomGuide is null) return null;
                var geomGuide = new GeomGuide();
                geomGuide.Name = xGeomGuide.Name;
                geomGuide.Formula = xGeomGuide.Formula;
                return geomGuide;
            }


            internal static D.ShapeGuide CreateGeomGuide(GeomGuide geomGuide, PresentationExportArgs exportArgs)
            {
                if (geomGuide is null) return null;
                var xGeomGuide = new D.ShapeGuide();

                xGeomGuide.Name = geomGuide.Name;
                xGeomGuide.Formula = geomGuide.Formula;

                return xGeomGuide;
            }
        }

        public class GeomGuides : List<GeomGuide>
        {
            internal static GeomGuides BuildGeomGuideList(PresentationImportArgs importArgs)
            {
                var xGeomGuideList = importArgs.ArgElement as D.GeometryGuideListType;
                if (xGeomGuideList is null) return null;
                var geomGuideList = new GeomGuides();

                foreach (var xChild in xGeomGuideList.Elements<D.ShapeGuide>())
                {
                    geomGuideList.Add(GeomGuide.BuildGeomGuide(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }

                return geomGuideList;
            }

            internal static T CreateGeomGuideList<T>(GeomGuides geomGuides, PresentationExportArgs exportArgs) where T : D.GeometryGuideListType, new()
            {
                if (geomGuides is null) return null;
                var xGeomGuideList = new T();
                foreach (var child in geomGuides)
                {
                    xGeomGuideList.AppendExtend(GeomGuide.CreateGeomGuide(child, exportArgs));
                }
                return xGeomGuideList;
            }
        }

        public class ConnectionSite
        {
            public string X, Y;
            internal static ConnectionSite BuildConnectionSite(PresentationImportArgs importArgs)
            {
                var xConnectionSite = importArgs.ArgElement as D.ConnectionSite;
                if (xConnectionSite is null) return null;
                var connectionSite = new ConnectionSite();
                connectionSite.X = xConnectionSite.Position.X;
                connectionSite.Y = xConnectionSite.Position.Y;
                return connectionSite;
            }

            internal static D.ConnectionSite CreateConnectionSite(ConnectionSite connectionSites, PresentationExportArgs exportArgs)
            {
                if (connectionSites is null) return null;
                var xConnectionSite = new D.ConnectionSite();
                var xPosition = new D.Position() { X = connectionSites.X, Y = connectionSites.Y };
                xConnectionSite.Position = xPosition;
                return xConnectionSite;
            }
        }

        public class ConnectionSites : List<ConnectionSite>
        {
            internal static ConnectionSites BuildConnectionSiteList(PresentationImportArgs importArgs)
            {
                var xConnectionSiteList = new D.ConnectionSiteList();
                if (xConnectionSiteList is null) return null;
                var connectionSiteList = new ConnectionSites();
                foreach (var xChild in xConnectionSiteList.Elements<D.ConnectionSite>())
                {
                    connectionSiteList.Add(ConnectionSite.BuildConnectionSite(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }
                return connectionSiteList;
            }

            internal static D.ConnectionSiteList CreateConnectionSiteList(ConnectionSites connectionSites, PresentationExportArgs exportArgs)
            {
                var xConnectionSiteList = new D.ConnectionSiteList();
                if (connectionSites is null) return null;
                foreach (var child in connectionSites)
                {
                    xConnectionSiteList.AppendExtend(ConnectionSite.CreateConnectionSite(child, exportArgs));
                }

                return xConnectionSiteList;
            }
        }

        public class AdjustHandleBase
        {
            private static HashSet<Type> _adjustHandleHashSet = new HashSet<Type>()
            {
                typeof(D.CloseShapePath), typeof(D.MoveTo)
            };

            internal static bool IsAdjustHandleTypeX(OX.OpenXmlElement element)
            {
                return _adjustHandleHashSet.Contains(element.GetType());
            }

            public class XYAdjustHandle : AdjustHandleBase
            {
                public string X;
                public string Y;
                public string GeomGuideXName;
                public string GeomGuideYName;
                public string MinX;
                public string MinY;
                public string MaxX;
                public string MaxY;
                internal static XYAdjustHandle BuildAdjustHandle(PresentationImportArgs importArgs)
                {
                    var xAdjustHandle = importArgs.ArgElement as D.AdjustHandleXY;
                    if (xAdjustHandle is null) return null;
                    var adjustHandle = new XYAdjustHandle();

                    adjustHandle.X = xAdjustHandle.Position.X;
                    adjustHandle.Y = xAdjustHandle.Position.Y;
                    adjustHandle.GeomGuideXName = xAdjustHandle.XAdjustmentGuide;
                    adjustHandle.GeomGuideYName = xAdjustHandle.YAdjustmentGuide;
                    adjustHandle.MaxX = xAdjustHandle.MaxX;
                    adjustHandle.MinX = xAdjustHandle.MinX;
                    adjustHandle.MaxY = xAdjustHandle.MaxY;
                    adjustHandle.MinY = xAdjustHandle.MinY;

                    return adjustHandle;
                }

                internal static D.AdjustHandleXY CreateAdjustHandle(XYAdjustHandle adjustHandle, PresentationExportArgs exportArgs)
                {
                    if (adjustHandle is null) return null;
                    var xAdjustHandle = new D.AdjustHandleXY();

                    xAdjustHandle.Position = new D.Position() { X = adjustHandle.X, Y = adjustHandle.Y };
                    xAdjustHandle.XAdjustmentGuide = adjustHandle.GeomGuideXName;
                    xAdjustHandle.YAdjustmentGuide = adjustHandle.GeomGuideYName;
                    xAdjustHandle.MaxX = adjustHandle.MaxX;
                    xAdjustHandle.MaxY = adjustHandle.MaxY;
                    xAdjustHandle.MinX = adjustHandle.MinX;
                    xAdjustHandle.MinY = adjustHandle.MinY;

                    return xAdjustHandle;
                }
            }

            public class PolarAdjustHandle : AdjustHandleBase
            {
                public string X;
                public string Y;
                public string GeomGuideRName;
                public string GeomGuideAngName;
                public string MinR;
                public string MinAng;
                public string MaxR;
                public string MaxAng;

                internal static PolarAdjustHandle BuildAdjustHandle(PresentationImportArgs importArgs)
                {
                    
                    var xAdjustHandle = importArgs.ArgElement as D.AdjustHandlePolar;
                    if (xAdjustHandle is null) return null;
                    var adjustHandle = new PolarAdjustHandle();

                    adjustHandle.X = xAdjustHandle.Position.X;
                    adjustHandle.Y = xAdjustHandle.Position.Y;
                    adjustHandle.GeomGuideRName = xAdjustHandle.RadialAdjustmentGuide;
                    adjustHandle.GeomGuideAngName = xAdjustHandle.AngleAdjustmentGuide;
                    adjustHandle.MaxR = xAdjustHandle.MaxRadial;
                    adjustHandle.MinR = xAdjustHandle.MinRadial;
                    adjustHandle.MaxAng = xAdjustHandle.MaxAngle;
                    adjustHandle.MinAng = xAdjustHandle.MinAngle;

                    return adjustHandle;
                }

                internal static D.AdjustHandlePolar CreateAdjustHandle(PolarAdjustHandle adjustHandle, PresentationExportArgs exportArgs)
                {
                    if (adjustHandle is null) return null;
                    var xAdjustHandle = new D.AdjustHandlePolar();
                    
                    xAdjustHandle.Position = new D.Position() { X = adjustHandle.X, Y = adjustHandle.Y };
                    xAdjustHandle.RadialAdjustmentGuide = adjustHandle.GeomGuideRName;
                    xAdjustHandle.AngleAdjustmentGuide = adjustHandle.GeomGuideAngName;
                    xAdjustHandle.MaxRadial = adjustHandle.MaxR;
                    xAdjustHandle.MaxAngle = adjustHandle.MaxAng;
                    xAdjustHandle.MinRadial = adjustHandle.MinR;
                    xAdjustHandle.MinAngle = adjustHandle.MinAng;

                    return xAdjustHandle;
                }
            }

            internal static AdjustHandleBase BuildAdjustHandleEntry(PresentationImportArgs importArgs)
            {
                
                var adjustHandle = importArgs.ArgElement;
                if (adjustHandle is null) return null;
                if (adjustHandle is D.AdjustHandleXY) return XYAdjustHandle.BuildAdjustHandle(importArgs);
                else if (adjustHandle is D.AdjustHandlePolar) return PolarAdjustHandle.BuildAdjustHandle(importArgs);
                throw new Exception("AdjustHandle 未知节点信息导入。");
            }

            internal static OX.OpenXmlElement CreateAdjustHandleEntry(AdjustHandleBase adjustHandle, PresentationExportArgs exportArgs)
            {
                if (adjustHandle is null) return null;
                if (adjustHandle is XYAdjustHandle) return XYAdjustHandle.CreateAdjustHandle(adjustHandle as XYAdjustHandle, exportArgs);
                else if (adjustHandle is PolarAdjustHandle) return PolarAdjustHandle.CreateAdjustHandle(adjustHandle as PolarAdjustHandle, exportArgs);
                throw new Exception("AdjustHandle 未知节点信息导出。");
            }

        }


        public class AdjustHandles : List<AdjustHandleBase>
        {
            internal static AdjustHandles BuildAdjustHandles(PresentationImportArgs importArgs)
            {
                var xAdjustHandles = importArgs.ArgElement as D.AdjustHandleList;
                if (xAdjustHandles is null) return null;
                var adjustHandles = new AdjustHandles();

                foreach (var xChild in xAdjustHandles)
                {
                    if (AdjustHandleBase.IsAdjustHandleTypeX(xChild))
                        adjustHandles.Add(AdjustHandleBase.BuildAdjustHandleEntry(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }

                return adjustHandles;
            }

            internal static D.AdjustHandleList CreateAdjustHandles(AdjustHandles adjustHandles, PresentationExportArgs exportArgs)
            {
                if (adjustHandles is null) return null;
                var xAdjustHandles = new D.AdjustHandleList();

                foreach (var child in adjustHandles)
                {
                    xAdjustHandles.AppendExtend(AdjustHandleBase.CreateAdjustHandleEntry(child, exportArgs));
                }

                return xAdjustHandles;
            }
        }

        public class Path2D : List<SVGNodeBase>
        {

            public enum PathFillMode
            {
                None, Norm, Lighten, LightenLess, Darken, DarkenLess
            }

            public Emu? Width;
            public Emu? Height;
            public PathFillMode? FillMode;
            public bool? IsStroke;
            public bool? ExtrusionOK;

            internal static Path2D BuildPath2DList(PresentationImportArgs importArgs)
            {
                var xPath2D = importArgs.ArgElement as D.Path;
                if (xPath2D is null) return null;
                var path2D = new Path2D();

                foreach (var xChild in xPath2D)
                {
                    if (SVGNodeBase.IsSVGNodeTypeX(xChild))
                        path2D.Add(SVGNodeBase.BuildSVGNodeBaseEntry(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }

                path2D.Width = xPath2D.Width?.Value;
                path2D.Height = xPath2D.Height?.Value;
                if(xPath2D.Fill is not null) path2D.FillMode = (PathFillMode)xPath2D.Fill.Value;
                path2D.IsStroke = xPath2D.Stroke?.Value;
                path2D.ExtrusionOK = xPath2D.ExtrusionOk?.Value;

                return path2D;
            }

            internal static D.Path CreatePath2DList(Path2D path2D, PresentationExportArgs exportArgs)
            {
                if (path2D is null) return null;
                var xPath2D = new D.Path();

                foreach (var child in path2D)
                {
                    xPath2D.AppendExtend(SVGNodeBase.CreateSVGNodeBaseEntry(child, exportArgs));
                }

                xPath2D.Width = path2D.Width.ToOX64();
                xPath2D.Height = path2D.Height.ToOX64();
                if(path2D.FillMode is not null) xPath2D.Fill = (D.PathFillModeValues)path2D.FillMode;
                xPath2D.Stroke = path2D.IsStroke.ToOX();
                xPath2D.ExtrusionOk = path2D.ExtrusionOK.ToOX();

                return xPath2D;
            }
        }

        public class Path2Ds : List<Path2D>
        {
            internal static Path2Ds BuildPath2DList(PresentationImportArgs importArgs)
            {
                var xPath2DList = importArgs.ArgElement as D.PathList;
                if (xPath2DList is null) return null;
                var path2DList = new Path2Ds();

                foreach (var xChild in xPath2DList.Elements<D.Path>())
                {
                    path2DList.Add(Path2D.BuildPath2DList(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
                }

                return path2DList;
            }

            internal static D.PathList CreatePath2DList(Path2Ds path2DList, PresentationExportArgs exportArgs)
            {
                if (path2DList is null) return null;
                var xPath2DList = new D.PathList();

                foreach (var child in path2DList)
                {
                    xPath2DList.AppendExtend(Path2D.CreatePath2DList(child, exportArgs));
                }

                return xPath2DList;
            }
        }

        public class CustomGeometry : GeometryBase
        {
            public GeomGuides AdjustList;
            public GeomGuides GuideList;
            public AdjustHandles AdjustHandleList;
            public ConnectionSites ConnectionSiteList;
            public GeomRect Rectangle;
            public Path2Ds PathList;
            public CustomGeometry()
            {

            }

            public CustomGeometry(CustomGeometry geometry)
            {

            }

            internal static CustomGeometry BuildGeometry(PresentationImportArgs importArgs)
            {
                var xGeometry = importArgs.ArgElement as D.CustomGeometry;
                if (xGeometry is null) return null; 
                var geometry = new CustomGeometry();
                geometry.AdjustList = GeomGuides.BuildGeomGuideList(new PresentationImportArgs(importArgs) { ArgElement = xGeometry.AdjustValueList });
                geometry.GuideList = GeomGuides.BuildGeomGuideList(new PresentationImportArgs(importArgs) { ArgElement = xGeometry.ShapeGuideList });
                geometry.AdjustHandleList = AdjustHandles.BuildAdjustHandles(new PresentationImportArgs(importArgs) { ArgElement = xGeometry.AdjustHandleList });
                geometry.ConnectionSiteList = ConnectionSites.BuildConnectionSiteList(new PresentationImportArgs(importArgs) { ArgElement = xGeometry.ConnectionSiteList });
                geometry.Rectangle = GeomRect.BuildGeomRect(new PresentationImportArgs(importArgs) { ArgElement = xGeometry.Rectangle });
                geometry.PathList = Path2Ds.BuildPath2DList(new PresentationImportArgs(importArgs) { ArgElement = xGeometry.PathList });
                return geometry;
            }

            internal static D.CustomGeometry CreateGeometry(CustomGeometry geometry, PresentationExportArgs exportArgs)
            {
                if (geometry is null) return null;
                var xGeometry = new D.CustomGeometry();

                xGeometry.AdjustValueList = GeomGuides.CreateGeomGuideList<D.AdjustValueList>(geometry.AdjustList, exportArgs);
                xGeometry.ShapeGuideList = GeomGuides.CreateGeomGuideList<D.ShapeGuideList>(geometry.GuideList, exportArgs);
                xGeometry.AdjustHandleList = AdjustHandles.CreateAdjustHandles(geometry.AdjustHandleList, exportArgs);
                xGeometry.ConnectionSiteList = ConnectionSites.CreateConnectionSiteList(geometry.ConnectionSiteList, exportArgs);
                xGeometry.Rectangle = GeomRect.CreateGeomRect(geometry.Rectangle, exportArgs);
                xGeometry.PathList = Path2Ds.CreatePath2DList(geometry.PathList, exportArgs);

                return xGeometry;
            }

        }

        public class PresetGeometry : GeometryBase
        {
            public GeomGuides AdjustList;

            public ShapePresetType ShapePresetType;

            public PresetGeometry()
            {

            }

            public PresetGeometry(PresetGeometry geometry)
            {

            }

            internal static PresetGeometry BuildGeometry(PresentationImportArgs importArgs)
            {
                var xGeometry = importArgs.ArgElement as D.PresetGeometry;
                if (xGeometry is null) return null;
                var geometry = new PresetGeometry();
                geometry.AdjustList = GeomGuides.BuildGeomGuideList(new PresentationImportArgs(importArgs) { ArgElement = xGeometry.AdjustValueList });
                geometry.ShapePresetType = (ShapePresetType)xGeometry.Preset.Value;
                return geometry;
            }

            internal static D.PresetGeometry CreateGeometry(PresetGeometry geometry, PresentationExportArgs exportArgs)
            {
                if (geometry is null) return null;
                var xGeometry = new D.PresetGeometry();

                xGeometry.AdjustValueList = GeomGuides.CreateGeomGuideList<D.AdjustValueList>(geometry.AdjustList, exportArgs);

                xGeometry.Preset = (D.ShapeTypeValues)geometry.ShapePresetType;
                return xGeometry;
            }

        }


        internal static GeometryBase BuildGeometry(PresentationImportArgs importArgs)
        {
            var geometry = importArgs.ArgElement;
            if (geometry is null) return null;
            if (geometry is D.CustomGeometry) return CustomGeometry.BuildGeometry(importArgs);
            if (geometry is D.PresetGeometry) return PresetGeometry.BuildGeometry(importArgs);
            throw new Exception("Geometry 未知类型导入。");
        }

        internal static OX.OpenXmlElement CreateGeometry(GeometryBase geometry, PresentationExportArgs exportArgs)
        {
            if (geometry is null) return null;
            if (geometry is CustomGeometry) return CustomGeometry.CreateGeometry(geometry as CustomGeometry, exportArgs);
            if (geometry is PresetGeometry) return PresetGeometry.CreateGeometry(geometry as PresetGeometry, exportArgs);
            throw new Exception("Geometry 未知类型导出。");
        }

    }



}
