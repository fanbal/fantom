﻿namespace Fantom.Models
{
    public class PackageProperties
    {
        public string Category { get; set; }

        public string ContentStatus { get; set; }

        public string ContentType { get; set; }

        public DateTime? Created { get; set; }

        public string Creator { get; set; }

        public string Description { get; set; }

        public string Identifier { get; set; }

        public string Keywords { get; set; }

        public string Language { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastPrinted { get; set; }

        public DateTime? Modified { get; set; }

        public int Revision { get; set; }

        public string Subject { get; set; }

        public string Title { get; set; }

        public string Version { get; set; }

        internal PackageProperties() { }

        internal void SetPackageProperties(IOPK.PackageProperties prop)
        {
            prop.Category = Category;
            prop.ContentStatus = ContentStatus;
            prop.ContentType = ContentType;
            prop.Created = Created;
            prop.Creator = Creator;
            prop.Description = Description;
            prop.Identifier = Identifier;
            prop.Keywords = Keywords;
            prop.Language = Language;
            prop.LastModifiedBy = LastModifiedBy;
            prop.LastPrinted = LastPrinted;
            prop.Modified = Modified;
            prop.Revision = Revision.ToString();
            prop.Subject = Subject;
            prop.Title = Title;
            prop.Version = Version;
        }


        internal static PackageProperties GetPackageProperties(IOPK.PackageProperties iopkProp)
        {
            var prop = new PackageProperties();
            prop.Category = iopkProp.Category;
            prop.ContentStatus = iopkProp.ContentStatus;
            prop.ContentType = iopkProp.ContentType;
            prop.Created = iopkProp.Created;
            prop.Creator = iopkProp.Creator;
            prop.Description = iopkProp.Description;
            prop.Identifier = iopkProp.Identifier;
            prop.Keywords = iopkProp.Keywords;
            prop.Language = iopkProp.Language;
            prop.LastModifiedBy = iopkProp.LastModifiedBy;
            prop.LastPrinted = iopkProp.LastPrinted;
            prop.Modified = iopkProp.Modified;
            prop.Revision = int.Parse(iopkProp.Revision);
            prop.Subject = iopkProp.Subject;
            prop.Title = iopkProp.Title;
            prop.Version = iopkProp.Version;
            return prop;
        }
    }
}
