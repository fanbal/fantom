﻿
namespace Fantom.Models
{
    public class MediaSource : EchoBaseObject
    {
        public enum MediaSourceType
        {
            image_jpeg, image_png, Unknown
        }

        public MediaSourceType Type = MediaSourceType.Unknown;
        public string TypeString;

        public MediaSource(Uri uri, System.IO.Packaging.Package pack)
        {
            Uri = uri;

            var pk = pack.GetPart(Uri);

            Type = MediaSourceTypePrase(pk.ContentType);
            TypeString = pk.ContentType;


            var bytes = new List<byte>();
            var stream = pk.GetStream();
            int b = 0;

            while (b != -1)
            {
                b = stream.ReadByte();
                if (b == -1)
                {
                    break;
                }

                bytes.Add((byte)b);
            }
            stream.Dispose();

            _bytes = bytes.ToArray();
        }

        private MediaSourceType MediaSourceTypePrase(string type)
        {
            if (type == "image/jpeg")
                return MediaSourceType.image_jpeg;
            else
                return MediaSourceType.Unknown;
        }

        /// <summary>
        /// 创建图形部件，并在全局中注册该部件。
        /// </summary>
        /// <param name="exportArgs"></param>
        internal void CreatePart(PresentationExportArgs exportArgs)
        {
            var container = exportArgs.RelationshipMapperContainer;
            var part = exportArgs.Mapper[container];

            if (exportArgs.Mapper.ContainsKey(this))
            {
                var imagePart = exportArgs.Mapper[this] as PK.ImagePart;
                var rid = exportArgs.RelationshipMapperContainer.GetPropertyRelationshipIDString(this);
                part.AddPart<PK.ImagePart>(imagePart, rid);

            }
            else
            {
                PK.ImagePart imagePart = null;
                var rid = exportArgs.RelationshipMapperContainer.GetPropertyRelationshipIDString(this);

                if (part is PK.SlidePart) imagePart = (part as PK.SlidePart).AddImagePart(PK.ImagePartType.Jpeg, rid);
                if (part is PK.SlideLayoutPart) imagePart = (part as PK.SlideLayoutPart).AddImagePart(PK.ImagePartType.Jpeg, rid);
                if (part is PK.NotesSlidePart) imagePart = (part as PK.NotesSlidePart).AddImagePart(PK.ImagePartType.Jpeg, rid);
                if (part is PK.NotesMasterPart) imagePart = (part as PK.NotesMasterPart).AddImagePart(PK.ImagePartType.Jpeg, rid);
                if (part is PK.SlideMasterPart) imagePart = (part as PK.SlideMasterPart).AddImagePart(PK.ImagePartType.Jpeg, rid);




                //if (part is PK.NotesMasterPart) imagePart = (part as PK.SlideLayoutPart).AddImagePart(this.TypeString, rid);
                //if (part is PK.NotesMasterPart) imagePart = (part as PK.SlideLayoutPart).AddImagePart(this.TypeString, rid);
                if (imagePart is not null)
                {


                    using (var stream = imagePart.GetStream())
                    {
                        stream.Write(Bytes);
                    }


                    exportArgs.Mapper.TryAdd(this, imagePart);
                }
            }

        }



        public Uri Uri { get; internal set; }

        private byte[] _bytes;
        public byte[] Bytes => _bytes;





    }




    public abstract class MediaBase
    {

        public enum MediaBaseType
        {
            Unknown, AudioFromCDMedia, WaveAudioFileMedia, AudioFromFileMedia, VedioFromFileMedia, QuickTimeFileMedia,
        }

        public virtual MediaBaseType Type => MediaBaseType.Unknown;

        private HashSet<Type> _mediaTypeHashSet = new HashSet<Type>()
        {
            typeof(D.AudioFromCD), typeof(D.WaveAudioFile), typeof(D.QuickTimeFromFile), typeof(D.AudioFromFile), typeof(D.VideoFromFile)
        };

        internal bool IsMediaTypeX(OX.OpenXmlElement element)
        {
            return _mediaTypeHashSet.Contains(element.GetType());
        }


        internal static MediaBase LoadMediaEntry(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var xMedia = importArgs.ArgElement;
            if (xMedia is null) return null;
            if (xMedia is D.AudioFromCD)
            {
                return AudioFromCDMedia.LoadMedia(shape, importArgs);
            }
            else if (xMedia is D.WaveAudioFile)
            {
                return WaveAudioFileMedia.LoadMedia(shape, importArgs);
            }
            else if (xMedia is D.QuickTimeFromFile)
            {
                return QuickTimeFileMedia.LoadMedia(shape, importArgs);
            }
            else if (xMedia is D.AudioFromFile)
            {
                return AudioFromFileMedia.LoadMedia(shape, importArgs);
            }
            else if (xMedia is D.VideoFromFile)
            {
                return VedioFromFileMedia.LoadMedia(shape, importArgs);
            }
            throw new Exception("传入参数不是 Media 类型。");
        }

        internal static OX.OpenXmlElement CreateMediaEntry(MediaBase media, PresentationExportArgs exportArgs)
        {
            if (media is null) return null;
            if (media is AudioFromCDMedia) return AudioFromCDMedia.CreateMedia(media as AudioFromCDMedia, exportArgs);
            else if (media is WaveAudioFileMedia) return WaveAudioFileMedia.CreateMedia(media as WaveAudioFileMedia, exportArgs);
            else if (media is QuickTimeFileMedia) return QuickTimeFileMedia.CreateMedia(media as QuickTimeFileMedia, exportArgs);
            else if (media is AudioFromFileMedia) return AudioFromFileMedia.CreateMedia(media as AudioFromFileMedia, exportArgs);
            else if (media is VedioFromFileMedia) return VedioFromFileMedia.CreateMedia(media as VedioFromFileMedia, exportArgs);
            throw new Exception("不明媒体对象！");
        }

    }

    public class AudioFromCDMedia : MediaBase
    {
        public override MediaBaseType Type => MediaBaseType.AudioFromCDMedia;
        public Emu? StartTime;
        public Emu? EndTime;

        public AudioFromCDMedia()
        {

        }

        public AudioFromCDMedia(AudioFromCDMedia media)
        {

        }

        internal static AudioFromCDMedia LoadMedia(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var xMedia = importArgs.ArgElement as D.AudioFromCD;
            if (xMedia is null) return null;
            var media = new AudioFromCDMedia();
            shape.BaseWrapper.Media = media;
            media.StartTime = xMedia.StartTime.Time;
            media.EndTime = xMedia.EndTime.Time;
            return media;
        }

        internal static D.AudioFromCD CreateMedia(AudioFromCDMedia media, PresentationExportArgs exportArgs)
        {
            if (media is null) return null;
            var xMedia = new D.AudioFromCD()
            {
                StartTime = new D.StartTime() { Time = media.StartTime.ToOXU32() },
                EndTime = new D.EndTime() { Time = media.EndTime.ToOXU32() }
            };
            return xMedia;
        }

    }

    public class WaveAudioFileMedia : MediaBase
    {
        public override MediaBaseType Type => MediaBaseType.WaveAudioFileMedia;

        public string EmbedID;
        public string Name;

        public WaveAudioFileMedia()
        {

        }

        public WaveAudioFileMedia(WaveAudioFileMedia media)
        {

        }

        internal static WaveAudioFileMedia BuildMedia(PresentationImportArgs importArgs)
        {
            var xMedia = importArgs.ArgElement as D.EmbeddedWavAudioFileType;
            if (xMedia is null) return null;
            var media = new WaveAudioFileMedia();


            media.EmbedID = xMedia.Embed ?? null;
            media.Name = xMedia.Name ?? null;
            return media;
        }

        internal static WaveAudioFileMedia LoadMedia(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var media = BuildMedia(importArgs);
            shape.BaseWrapper.Media = media;
            return media;
        }

        internal static D.WaveAudioFile CreateMedia(WaveAudioFileMedia media, PresentationExportArgs exportArgs)
        {
            if (media is null) return null;
            var xMedia = new D.WaveAudioFile()
            {
                Embed = media.EmbedID ?? null,
                Name = media.Name ?? null
            };

            return xMedia;
        }

        internal static D.HyperlinkSound CreateMediaSound(WaveAudioFileMedia media, PresentationExportArgs exportArgs)
        {
            if (media is null) return null;
            var xMedia = new D.HyperlinkSound()
            {
                Embed = media.EmbedID ?? null,
                Name = media.Name ?? null
            };
            return xMedia;
        }


    }

    public class AudioFromFileMedia : MediaBase
    {

        public override MediaBaseType Type => MediaBaseType.AudioFromFileMedia;
        public string Link;

        public AudioFromFileMedia()
        {

        }

        public AudioFromFileMedia(AudioFromFileMedia media)
        {

        }

        internal static AudioFromFileMedia LoadMedia(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var xMedia = importArgs.ArgElement as D.AudioFromFile;
            if (xMedia is null) return null;
            var media = new AudioFromFileMedia();

            media.Link = xMedia.Link ?? null;
            shape.BaseWrapper.Media = media;
            return media;
        }

        internal static D.AudioFromFile CreateMedia(AudioFromFileMedia media, PresentationExportArgs exportArgs)
        {
            if (media is null) return null;
            var xMedia = new D.AudioFromFile()
            {
                Link = media.Link ?? null,
            };
            return xMedia;
        }

    }

    public class VedioFromFileMedia : MediaBase
    {

        public override MediaBaseType Type => MediaBaseType.VedioFromFileMedia;

        public string Link;

        public VedioFromFileMedia()
        {

        }

        public VedioFromFileMedia(VedioFromFileMedia media)
        {

        }

        internal static VedioFromFileMedia LoadMedia(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var xMedia = importArgs.ArgElement as D.VideoFromFile;
            if (xMedia is null) return null;
            var media = new VedioFromFileMedia();
            shape.BaseWrapper.Media = media;
            media.Link = xMedia.Link ?? null;
            return media;
        }

        internal static D.VideoFromFile CreateMedia(VedioFromFileMedia media, PresentationExportArgs exportArgs)
        {
            if (media is null) return null;
            var xMedia = new D.VideoFromFile()
            {
                Link = media.Link ?? null,
            };
            return xMedia;
        }

    }


    public class QuickTimeFileMedia : MediaBase
    {

        public override MediaBaseType Type => MediaBaseType.QuickTimeFileMedia;
        public string Link;

        public QuickTimeFileMedia()
        {

        }

        public QuickTimeFileMedia(QuickTimeFileMedia media)
        {

        }

        internal static QuickTimeFileMedia LoadMedia(ShapeBase shape, PresentationImportArgs importArgs)
        {
            var xMedia = importArgs.ArgElement as D.QuickTimeFromFile;
            if (xMedia is null) return null;
            var media = new QuickTimeFileMedia();
            shape.BaseWrapper.Media = media;
            media.Link = xMedia.Link ?? null;
            return media;
        }

        internal static D.QuickTimeFromFile CreateMedia(QuickTimeFileMedia media, PresentationExportArgs exportArgs)
        {
            if (media is null) return null;
            var xMedia = new D.QuickTimeFromFile()
            {
                Link = media.Link ?? null,
            };
            return xMedia;
        }

    }
}
