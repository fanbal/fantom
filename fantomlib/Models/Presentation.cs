﻿
using DocumentFormat.OpenXml.Bibliography;
using System.Diagnostics.Metrics;
using System.IO;
using System.Reflection;

namespace Fantom.Models
{
    public class Presentation : EchoSelectableObject
    {
        #region events
        /// <summary>
        /// 幻灯片激活时触发。
        /// </summary>
        public event PresentationEventHandler Actived;

        /// <summary>
        /// 幻灯片关闭前触发该对象。
        /// </summary>
        public event PresentationEventHandler BeforeClose;

        /// <summary>
        /// 演示文稿关闭后触发该事件。
        /// </summary>
        public event PresentationEventHandler AfterClose;


        public event PresentationEventHandler BeforeSave;
        public event PresentationEventHandler AfterSave;
        public event PresentationEventHandler AllowEdit;
        public event PresentationSizeChangeEventHandler SlideSizeChanged;
        public event PresentationSizeChangeEventHandler NoteSizeChanged;
        #endregion

        #region properties


        public bool IsActived { get; set; } = false;

        public string Author { get; set; }

        public bool AllowedEdit { get; set; } = true;

        public string Path { get; internal set; }


        [RelationshipItemExportType(RelationshipExportType.ExportItem)]
        public Slides Slides { get; internal set; } = new Slides();

        [RelationshipItemExportType(RelationshipExportType.ExportRoot)]
        public NoteMaster NoteMaster { get; internal set; } // TODO MoteMasters


        //[RelationshipItemExportType(RelationshipExportType.ExportItem)]
        //public Themes Themes { get; internal set; }


        [RelationshipItemExportType(RelationshipExportType.ExportItem)]
        public SlideMasters SlideMasters { get; internal set; } // TODO SlideMasters




        public uint RevisionCount { get; internal set; } = 0;


        private Size _slideSize = Size.Zero;
        public Size SlideSize
        {
            get => _slideSize;
            set
            {
                var oldsize = _slideSize;
                _slideSize = value;

                if (oldsize != value)
                {
                    SlideSizeChanged?.Invoke(this, new PresentationSizeChangeEventArgs(oldsize, value));
                    RaiseEvent();
                }

            }
        }

        private Size _noteSize = Size.Zero;
        public Size NoteSize
        {
            get => _noteSize;
            set
            {
                var oldsize = _slideSize;
                _noteSize = value;

                if (oldsize != value)
                {
                    NoteSizeChanged?.Invoke(this, new PresentationSizeChangeEventArgs(oldsize, value));
                    RaiseEvent();
                }

            }
        }

        public MediaManager MediaManager { get; set; }


        public PackageProperties PackageProperties { get; set; }


        #endregion

        #region ctors
        internal Presentation()
        {

        }

        #endregion

        #region methods
        internal override void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            PropertyMemberBatch((propInfo, member) =>
            {
                if (member is null) return;

                var counter2 = 1U;

                member._packIDMapper = new Dictionary<EchoBaseObject, uint>();
                member.LoadPackIDMapper(member, ref counter2);

            });
        }


        internal override void LoadPropertyRelationshipIDMap(EchoBaseObject mapperContainer, ref uint counter)
        {
            var type = this.GetType();
            mapperContainer._propertyRelationshipIDMapper = new Dictionary<EchoBaseObject, uint>();
            foreach (var prop in type.GetProperties())
            {
                if (IsEchoBaseObject(prop.PropertyType))
                {
                    var att = prop.GetCustomAttribute<RelationshipItemExportTypeAttribute>();
                    if (att is null) continue;
                    var member = prop.GetValue(this) as EchoBaseObject;

                    if (member is null) continue;

                    // 仅引用一层。
                    if (att.IsRef == true)
                    {
                        counter++;
                        mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                    }
                    else
                    {
                        if (att.RelationshipExport == RelationshipExportType.ExportItem)
                        {
                            foreach (EchoBaseObject item in member as IEnumerable)
                            {
                                counter++;
                                mapperContainer._propertyRelationshipIDMapper.Add(item, counter);
                            }
                        }
                        else if (att.RelationshipExport == RelationshipExportType.ExportBoth)
                        {
                            counter++;
                            mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                            member.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
                            foreach (EchoBaseObject item in member as IEnumerable)
                            {
                                counter++;
                                mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                            }
                        }
                        else // Root
                        {
                            counter++;
                            mapperContainer._propertyRelationshipIDMapper.Add(member, counter);
                        }
                    }



                }
            }

            counter++;
            mapperContainer._propertyRelationshipIDMapper.Add(SlideMasters[0].Theme, counter);

        }

        internal void ApplyPropertyRelationshipIDMap()
        {
            var presCounter = 0U;
            LoadPropertyRelationshipIDMap(this, ref presCounter);

            var type = this.GetType();
            foreach (var prop in type.GetProperties())
            {
                if (IsEchoBaseObject(prop.PropertyType))
                {
                    var att = prop.GetCustomAttribute<RelationshipItemExportTypeAttribute>();
                    if (att is null) continue;
                    var member = prop.GetValue(this) as EchoBaseObject;

                    if (member is null) continue;

                    // 仅引用一层。
                    if (att.IsRef == true) continue;

                    if (att.RelationshipExport == RelationshipExportType.ExportItem)
                    {
                        foreach (EchoBaseObject item in member as IEnumerable)
                        {
                            var itemCounter = 0U;
                            item._propertyRelationshipIDMapper = new Dictionary<EchoBaseObject, uint>();
                            item.LoadPropertyRelationshipIDMap(item, ref itemCounter);
                        }
                    }

                    else if (att.RelationshipExport == RelationshipExportType.ExportRoot) // Root
                    {
                        var memberCounter = 0U;
                        member._propertyRelationshipIDMapper = new Dictionary<EchoBaseObject, uint>();
                        member.LoadPropertyRelationshipIDMap(member, ref memberCounter);
                    }
                    else
                    {
                        throw new Exception($"无法正确为 {member.GetType()} 实例构建映射表。");
                    }
                }
            }
        }


        public override string ToString()
        {
            return GetType().Name;

        }
        public void Activate()
        {
            if (!IsActived)
            {
                var parent = Parent as Presentations;

                var cntPres = parent.CurrentPresentation;
                cntPres.IsActived = false;
                IsActived = true;
                parent.CurrentPresentation = this;
            }

            Actived?.Invoke(this, EventArgs.Empty);
        }

        public void Close()
        {
            BeforeClose?.Invoke(this, EventArgs.Empty);
            var parent = Parent as Presentations;
            if (IsActived)
            {
                parent.CurrentPresentation = null;
            }

            if (parent.Contains(this))
            {
                parent.Remove(this);
            }

            AfterClose?.Invoke(this, EventArgs.Empty);
        }

        public void Save()
        {
            if (RevisionCount == 0)
            {
                throw new ArgumentException("警告：新建演示文稿未指定存档目录。");
            }

            if (!AllowedEdit)
            {
                return;//只读下不进行存档
            }

            Save(Path);
        }

        public void Save(string saveAsPath)
        {
            if (!AllowedEdit && Path == saveAsPath)
            {
                return;
            }

            BeforeSave?.Invoke(this, EventArgs.Empty);

            PresentationBuilder.ExportEntry(this, saveAsPath);

        }

        public void PremitEdit()
        {
            if (AllowedEdit == false)
            {
                AllowedEdit = true;
                AllowEdit?.Invoke(this, EventArgs.Empty);
            }
        }


        public void ExportMarkdown(string path)
        {
            using (var filestream = new FileStream(path, FileMode.Create))
            {
                using (var markdownStream = new StreamWriter(filestream))
                {
                    ExportMarkdown(markdownStream);
                }

            }
        }

        public void ExportMarkdown(IO.Stream stream)
        {
            using (var markdownStream = new StreamWriter(stream))
            {
                ExportMarkdown(markdownStream);
            }
        }

        /// <summary>
        /// 导出 Markdown 文件。
        /// </summary>
        /// <param name="writer"></param>
        public void ExportMarkdown(IO.TextWriter writer)
        {
            if (Slides.Count == 0) return;
            var titleSlide = Slides[0];

            titleSlide.ExportMarkdown(writer, true);


            for (int i = 1; i < Slides.Count; i++)
            {
                var slide = Slides[i];
                slide.ExportMarkdown(writer, false);
            }


        }




        #endregion
    }
}
