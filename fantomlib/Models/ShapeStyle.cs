﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fantom.Models.ShapeStyleWrapper;

namespace Fantom.Models
{


    public class ShapeStyleWrapper
    {

        public class StyleMatrixReference
        {
            public ColorBase Color;
            public uint Index = 1;
            public FormatScheme scheme;
            internal static StyleMatrixReference BuildStyleMatrixReference(PresentationImportArgs importArgs)
            {
                var xStyleMatrixReference = importArgs.ArgElement as D.StyleMatrixReferenceType;
                if (xStyleMatrixReference is null) return null;
                var styleMatrixReference = new StyleMatrixReference();

                if (xStyleMatrixReference.Index is not null) styleMatrixReference.Index = xStyleMatrixReference.Index.Value;
                styleMatrixReference.Color = xStyleMatrixReference.GetColorFromNodeWithColor(importArgs.ColorLoadArgs);
                styleMatrixReference.scheme = importArgs.ColorLoadArgs.Theme.FormatScheme;
                return styleMatrixReference;
            }

            internal static T CreateStyleMatrixReference<T>(StyleMatrixReference styleMatrixReference, PresentationExportArgs exportArgs) where T : D.StyleMatrixReferenceType, new()
            {
                if (styleMatrixReference is null) return null;
                var xStyleMatrixReference = new T();
                xStyleMatrixReference.Index = styleMatrixReference.Index;
                xStyleMatrixReference.AppendExtend(ColorBuilder.ColorToOpenXmlElement(styleMatrixReference.Color));
                return xStyleMatrixReference;
            }
        }

        public class FontWithIndexReference
        {

            public enum FontCollectionIndexType
            {
                Major, Minor, None
            }

            public ColorBase Color;
            public FontCollectionIndexType IndexType;
            internal static FontWithIndexReference BuildFontReference(PresentationImportArgs importArgs)
            {
                var xFontReference = importArgs.ArgElement as D.FontReference;
                if (xFontReference is null) return null;
                var fontReference = new FontWithIndexReference();
                fontReference.Color = xFontReference.GetColorFromNodeWithColor(importArgs.ColorLoadArgs);
                fontReference.IndexType = (FontCollectionIndexType)xFontReference.Index.Value;
                return fontReference;
            }

            internal static D.FontReference CreateFontReference(FontWithIndexReference fontReference, PresentationExportArgs exportArgs)
            {
                if (fontReference is null) return null;
                var xFontReference = new D.FontReference();
                xFontReference.AppendExtend(ColorBuilder.ColorToOpenXmlElement(fontReference.Color));
                xFontReference.Index = (D.FontCollectionIndexValues)fontReference.IndexType;
                return xFontReference;
            }
        }


        public StyleMatrixReference LineReference;
        public StyleMatrixReference FillReference;
        public StyleMatrixReference EffectReference;
        public FontWithIndexReference FontReference;

        internal static ShapeStyleWrapper BuildShapeStyle(PresentationImportArgs importArgs)
        {
            var xShapeStyle = importArgs.ArgElement as P.ShapeStyle;
            if (xShapeStyle is null) return null;
            var shapeStyle = new ShapeStyleWrapper();

            shapeStyle.LineReference = StyleMatrixReference.BuildStyleMatrixReference(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.LineReference });
            shapeStyle.FillReference = StyleMatrixReference.BuildStyleMatrixReference(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.FillReference });
            shapeStyle.EffectReference = StyleMatrixReference.BuildStyleMatrixReference(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.EffectReference });
            shapeStyle.FontReference = FontWithIndexReference.BuildFontReference(new PresentationImportArgs(importArgs) { ArgElement = xShapeStyle.FontReference });

            return shapeStyle;
        }

        internal static P.ShapeStyle CreateShapeStyle(ShapeStyleWrapper shapeStyle, PresentationExportArgs exportArgs)
        {
            if (shapeStyle is null) return null;
            var xShapeStyle = new P.ShapeStyle();

            xShapeStyle.LineReference = StyleMatrixReference.CreateStyleMatrixReference<D.LineReference>(shapeStyle.LineReference, exportArgs);
            xShapeStyle.FillReference = StyleMatrixReference.CreateStyleMatrixReference<D.FillReference>(shapeStyle.FillReference, exportArgs);
            xShapeStyle.EffectReference = StyleMatrixReference.CreateStyleMatrixReference<D.EffectReference>(shapeStyle.EffectReference, exportArgs);
            xShapeStyle.FontReference = FontWithIndexReference.CreateFontReference(shapeStyle.FontReference, exportArgs);


            return xShapeStyle;
        }
    }
}
