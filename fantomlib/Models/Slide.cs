﻿using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Fantom.Models
{
    public class Slide : EchoSelectableObject, IHasTimeLine
    {

        #region properties

        public int Index => (Parent as Slides).IndexOf(this);

        [PackInnerRelationshipMember(HasItem = true, HasRoot = true, IsSpecial = true)]
        public Shapes Shapes { get; internal set; }


        [PackInnerRelationshipMember(HasItem = false, HasRoot = false)]
        public TimeLine TimeLine { get; set; } = null;


        [RelationshipItemExportType(RelationshipExportType.ExportRoot, IsRef = true)]
        public SlideLayout Layout { get; internal set; } = null;


        [RelationshipItemExportType(RelationshipExportType.ExportRoot)]
        public NoteSlide NoteSlide { get; internal set; } = null;

        [RelationshipItemExportType(RelationshipExportType.ExportRoot)]
        public CustomContainer CustomContainer { get; internal set; } = new CustomContainer();


        #endregion

        #region ctor

        internal Slide()
        {

        }

        #endregion

        #region methods
        public override string ToString()
        {
            return string.Format("SlideId: {0}", Index);
        }


        internal override void LoadPropertyRelationshipIDMap(EchoBaseObject mapperContainer, ref uint counter)
        {
            base.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
            Shapes.LoadPropertyRelationshipIDMap(mapperContainer, ref counter);
        }




        internal override void LoadPackIDMapper(EchoBaseObject mapperContainer, ref uint counter)
        {
            uint slideCounter = 0;
            _packIDMapper = new Dictionary<EchoBaseObject, uint>();
            //base.LoadPackIDMapper(this, ref slideCounter);

            Shapes.LoadPackIDMapper();

            uint noteCounter = 0;

            if (NoteSlide is not null)
            {
                NoteSlide._packIDMapper = new Dictionary<EchoBaseObject, uint>();
                NoteSlide.LoadPackIDMapper(NoteSlide, ref noteCounter);
            }



        }



        public void ExportMarkdown(IO.TextWriter writer, bool isCover)
        {
            foreach (var shape in Shapes)
            {
                if (shape is Shape)
                {
                    var cshape = shape as Shape;
                    if (cshape.Text.Trim().Replace(" ", "").Length < 2) continue;
                    if (cshape.BaseWrapper.PlaceHolder is not null)
                    {

                        if (cshape.BaseWrapper.PlaceHolder.Type == PlaceHolderType.Title || cshape.BaseWrapper.PlaceHolder.Type == PlaceHolderType.CenteredTitle)
                        {
                            if (isCover)
                            {
                                writer.WriteLine($"# {cshape.Text.Trim()}");
                            }

                            else
                            {
                                if (cshape.Wrapper.TextBody is not null)
                                    foreach (var para in cshape.Wrapper.TextBody)
                                    {
                                        writer.WriteLine();
                                        writer.WriteLine($"## {para.ReadOnlyText.Trim()}");
                                    }

                            }

                        }
                        else
                        {
                            if (cshape.Wrapper.TextBody is not null)
                                foreach (var para in cshape.Wrapper.TextBody)
                                {
                                    writer.WriteLine();
                                    writer.WriteLine($"{para.ReadOnlyText.Trim()}");
                                }
                        }

                    }
                }


            }
        }

        #endregion


    }
}
