﻿namespace Fantom.Interfaces
{
    /// <summary>
    /// 选择发生变化时的事件句柄。
    /// </summary>
    public delegate void SelectableEventHandler(ISelection sender, IEnumerable<ISelectable> changes);

}
