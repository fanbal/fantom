﻿namespace Fantom
{
    /// <summary>
    /// 演示文稿事件委托。
    /// </summary>
    public delegate void PresentationEventHandler(Presentation sender, EventArgs args);

    /// <summary>
    /// 演示文稿尺寸变化事件委托。
    /// </summary>
    public delegate void PresentationSizeChangeEventHandler(Presentation sender, PresentationSizeChangeEventArgs args);

    /// <summary>
    /// 演示文稿尺寸发生变化时触发该事件。
    /// </summary>
    public class PresentationSizeChangeEventArgs : EventArgs
    {
        private Size _originSize;

        /// <summary>
        /// 原来的尺寸。
        /// </summary>
        public Size OriginSize => _originSize;

        private Size _newSize;

        /// <summary>
        /// 全新的尺寸。
        /// </summary>
        public Size NewSize => _newSize;

        public PresentationSizeChangeEventArgs(Size oldSize, Size newSize)
        {
            _originSize = oldSize;
            _newSize = newSize;
        }
    }

}
