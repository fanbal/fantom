﻿namespace Fantom
{
    /// <summary>
    /// 演示文稿集合加载文档的事件委托。
    /// </summary>
    public delegate void PresentationsLoadingDocumentEventHandler(Presentations sender, PresentationLoadArguments args);

    /// <summary>
    /// 演示文稿集合一般事件委托。
    /// </summary>
    public delegate void PresentationsEventHandler(Presentations sender, EventArgs args);

    /// <summary>
    /// 演示文稿集合导入加载新文档时传递该对象。
    /// </summary>
    public class PresentationLoadArguments : EventArgs
    {
        /// <summary>
        /// 导入主体幻灯片之前存在的步骤数。
        /// </summary>
        private const int BEFORE_STEPS = 3;

        /// <summary>
        /// 表示导入时的进度。
        /// </summary>
        public int Step { get; private set; }

        /// <summary>
        /// 表示导入时的总进度。
        /// </summary>
        public int Steps { get; private set; }

        /// <summary>
        /// 相关信息。
        /// </summary>
        public string Info { get; private set; } = string.Empty;

        /// <summary>
        /// 表示导入时的状态。
        /// </summary>
        public PresentationsLoadingState LoadingState { get; private set; }

        public RaiseLoadingBundleArguments RaiseLoadingBundleArguments;

        public RaiseLoadingShapeArguments RaiseLoadingShapeArguments;

        public RaiseLoadingSlideArguments RaiseLoadingSlideArguments;

        public PresentationLoadArguments(PresentationsLoadingState loadingState, int steps, int step)
        {
            LoadingState = loadingState;
            Steps = steps;
            Step = step;
        }

        public PresentationLoadArguments(PresentationsLoadingState loadingState, int steps, int step, string info)
        {
            Info = info;
            LoadingState = loadingState;
            Steps = steps;
            Step = step;
        }
    }

}
