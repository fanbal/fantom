﻿

namespace Fantom.Expression
{
	class ProgramTest
	{


		static void ExpressionMain(string[] args)
		{

			var strarr = new string[]
			{
				"hello(hello(+5*1,0),1)",
				"hello(hello(1^2+5*100--10,10)%100+1*--------fanbal(fanbal(sin(1), pi), e),10)",
				"+2*4*5+10+cos(pi*10^2)++sin(hello(hello(1^2^2+5/7+10^(3*+100/(1000/3))--10,10)%100+1*+1-------fanbal(fanbal(sin(1), pi), e),10))",
				"hello      ( 1,       10)",
				"hello(1, )",
				"hello(1,1,1,1 )",
				"-------hello(1,1)",
				"********hello(1,1)",
				"1()",
			};

			foreach (var exp in strarr)
			{

				FormulaBuilder.CalculateExpression(
					exp, 3, null,null,
					out var list,
					out var root,
					out double rst,
					out int errPos);

				Console.WriteLine("原式：");
				Console.WriteLine(exp);

				Console.WriteLine("解析式：");
				FormulaBuilder.ShowExpressionMetaGroup(list, errPos);

				if (errPos == -1)
				{
					Console.WriteLine("表达式树：");
					FormulaBuilder.ShowExpressionTree(root, 0);
					Console.WriteLine();
					Console.WriteLine("结果：");
					Console.WriteLine(rst);
				}
				else
				{
					Console.WriteLine("表达式存在语法错误");
				}

				Console.WriteLine();
			}

			//Console.WriteLine(rst);
		}



	}
}
