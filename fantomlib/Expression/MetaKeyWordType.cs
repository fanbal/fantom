﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
	/// <summary>
	/// 表达式中关键字具体类型。
	/// </summary>
	public enum MetaKeyWordType : ulong
	{
		None, Unknown,

		StartOfSpace, EndOfSpace, SplitOfSpace,

		Add, Sub, Mul, Div, Pow, Mod,

		PI, E,

		rand,

		abs, sin, cos, tan, ceil, floor, rad, deg,

		max, min,

		custom,
		SysConst,
	}
}
