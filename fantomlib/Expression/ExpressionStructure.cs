﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
    /// <summary>
    /// 表达式结构，是一个已经建立了树结构的表达式，可使用该对象进行直接运算。
    /// </summary>
    public class ExpressionStructure
    {
        /// <summary>
        /// 已经分词了的表达式结果。
        /// </summary>
        public IList<MetaNode> MetaNodes { get;  }

        /// <summary>
        /// 计算。
        /// </summary>参与的变量值。
        /// <param name="value"></param>
        /// <returns></returns>
        public double Calculate(double value)
        {
            return value;
        }
    }
}
