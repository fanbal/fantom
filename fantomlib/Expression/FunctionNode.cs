﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
	/// <summary>
	/// 函数节点。
	/// </summary>
	public class FunctionNode
	{
		/// <summary>
		/// 名称。
		/// </summary>
		public string Name = "default";

		/// <summary>
		/// 参数数量。
		/// </summary>
		public int ParameterCount = 0;

		/// <summary>
		/// Fantom 表达式解析库的委托定义。
		/// </summary>
		/// <param name="num1"></param>
		/// <param name="num2"></param>
		/// <param name="num3"></param>
		/// <param name="num4"></param>
		/// <param name="num5"></param>
		/// <param name="num6"></param>
		/// <returns></returns>
		public delegate double FuncHandler(double num1, double num2, double num3, double num4, double num5, double num6);
		
		/// <summary>
		/// 函数具体实现。
		/// </summary>
		public FuncHandler Func;
	}
}
