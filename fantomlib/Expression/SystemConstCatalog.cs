﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Expression
{
    /// <summary>
    /// 系统变量
    /// </summary>
    public class SystemConstCatalog : Dictionary<string, double>
    {
        /// <summary>
        /// 默认的系统常量清单。
        /// </summary>
        public static SystemConstCatalog DefaultSystemConsts => _constdic;
        private static SystemConstCatalog _constdic = new SystemConstCatalog();

        public SystemConstCatalog()
        {
            AddRecord("ppt_x", 0f);
            AddRecord("ppt_y", 0f);
            AddRecord("ppt_r", 0f);
            AddRecord("ppt_w", 1f);
            AddRecord("ppt_h", 1f);
        }

        public void AddRecord(string name, double value)
        {
            if (!this.ContainsKey(name)) this.Add(name, value);
        }

        public void SetRecord(string name, double value)
        {
            if (this.ContainsKey(name)) this[name] = value;
        }

        public double GetRecord(string name)
        {
            if (this.ContainsKey(name)) return this[name];
            return 0f;
        }

        public bool HasRecord(string name)
        {
            return this.ContainsKey(name.ToLower());
        }


    }
}
