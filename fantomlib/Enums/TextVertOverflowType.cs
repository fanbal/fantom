﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Enums
{
    public enum TextAnchoringType
    {
        t, ctr, b, just, dist
    }

    public enum TextVertOverflowType
    {
        Overflow, Ellipsis, Clip
    }

    public enum TextHorzOverflowType
    {
        Overflow, Clip
    }

    public enum TextVerticalType
    {
        Horz, Vert, Vert270, WordArtVert, EaVert, mongolianVert, WordArtVertRtl
    }




}
