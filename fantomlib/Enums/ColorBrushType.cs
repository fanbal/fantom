﻿namespace Fantom
{
    /// <summary>
    /// 颜色类型。表示颜色画刷的类型，如系统颜色，色盘颜色等。
    /// </summary>
    public enum ColorBrushType
    {
        /// <summary>
        /// 未知。
        /// </summary>
        Unknown,

        /// <summary>
        /// 系统颜色。
        /// </summary>
        System,

        ///// <summary>
        ///// 色盘颜色。
        ///// </summary>
        //Schema,

        /// <summary>
        /// 常规颜色，此颜色无法引用。
        /// </summary>
        SRGB,

        /// <summary>
        /// 预设颜色。
        /// </summary>
        Preset,

        /// <summary>
        /// HSL模式。
        /// </summary>
        HSL,
    }
}
