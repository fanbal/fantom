﻿namespace Fantom
{
    public enum ZOrderCommandEnum
    {
        /// <summary>
        /// 前移一层。
        /// </summary>
        BringForward,

        /// <summary>
        /// 置为最顶。
        /// </summary>
        BringToFront,

        /// <summary>
        /// 后移一层。
        /// </summary>
        msoSendBackward,

        /// <summary>
        /// 置为最底。
        /// </summary>
        msoSendToBack,
    }
}
