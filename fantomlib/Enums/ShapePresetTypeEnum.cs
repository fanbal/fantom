﻿namespace Fantom
{
    public enum ShapePresetType
    {

        None = -1,

        Line = 0,

        LineInverse = 1,

        Triangle = 2,

        RightTriangle = 3,

        Rectangle = 4,

        Diamond = 5,

        Parallelogram = 6,

        Trapezoid = 7,

        NonIsoscelesTrapezoid = 8,

        Pentagon = 9,

        Hexagon = 10,

        Heptagon = 11,

        Octagon = 12,

        Decagon = 13,

        Dodecagon = 14,

        Star4 = 15,

        Star5 = 16,

        Star6 = 17,

        Star7 = 18,

        Star8 = 19,

        Star10 = 20,

        Star12 = 21,

        Star16 = 22,

        Star24 = 23,

        Star32 = 24,

        RoundRectangle = 25,

        Round1Rectangle = 26,

        Round2SameRectangle = 27,

        Round2DiagonalRectangle = 28,

        SnipRoundRectangle = 29,

        Snip1Rectangle = 30,

        Snip2SameRectangle = 31,

        Snip2DiagonalRectangle = 32,

        Plaque = 33,

        Ellipse = 34,

        Teardrop = 35,

        HomePlate = 36,

        Chevron = 37,

        PieWedge = 38,

        Pie = 39,

        BlockArc = 40,

        Donut = 41,

        NoSmoking = 42,

        RightArrow = 43,

        LeftArrow = 44,

        UpArrow = 45,

        DownArrow = 46,

        StripedRightArrow = 47,

        NotchedRightArrow = 48,

        BentUpArrow = 49,

        LeftRightArrow = 50,

        UpDownArrow = 51,

        LeftUpArrow = 52,

        LeftRightUpArrow = 53,

        QuadArrow = 54,

        LeftArrowCallout = 55,

        RightArrowCallout = 56,

        UpArrowCallout = 57,

        DownArrowCallout = 58,

        LeftRightArrowCallout = 59,

        UpDownArrowCallout = 60,

        QuadArrowCallout = 61,

        BentArrow = 62,

        UTurnArrow = 63,

        CircularArrow = 64,

        LeftCircularArrow = 65,

        LeftRightCircularArrow = 66,

        CurvedRightArrow = 67,

        CurvedLeftArrow = 68,

        CurvedUpArrow = 69,

        CurvedDownArrow = 70,

        SwooshArrow = 71,

        Cube = 72,

        Can = 73,

        LightningBolt = 74,

        Heart = 75,

        Sun = 76,

        Moon = 77,

        SmileyFace = 78,

        IrregularSeal1 = 79,

        IrregularSeal2 = 80,

        FoldedCorner = 81,

        Bevel = 82,

        Frame = 83,

        HalfFrame = 84,

        Corner = 85,

        DiagonalStripe = 86,

        Chord = 87,

        Arc = 88,

        LeftBracket = 89,

        RightBracket = 90,

        LeftBrace = 91,

        RightBrace = 92,

        BracketPair = 93,

        BracePair = 94,

        StraightConnector1 = 95,

        BentConnector2 = 96,

        BentConnector3 = 97,

        BentConnector4 = 98,

        BentConnector5 = 99,

        CurvedConnector2 = 100,

        CurvedConnector3 = 101,

        CurvedConnector4 = 102,

        CurvedConnector5 = 103,

        Callout1 = 104,

        Callout2 = 105,

        Callout3 = 106,

        AccentCallout1 = 107,

        AccentCallout2 = 108,

        AccentCallout3 = 109,

        BorderCallout1 = 110,

        BorderCallout2 = 111,

        BorderCallout3 = 112,

        AccentBorderCallout1 = 113,

        AccentBorderCallout2 = 114,

        AccentBorderCallout3 = 115,

        WedgeRectangleCallout = 116,

        WedgeRoundRectangleCallout = 117,

        WedgeEllipseCallout = 118,

        CloudCallout = 119,

        Cloud = 120,

        Ribbon = 121,

        Ribbon2 = 122,

        EllipseRibbon = 123,

        EllipseRibbon2 = 124,

        LeftRightRibbon = 125,

        VerticalScroll = 126,

        HorizontalScroll = 127,

        Wave = 128,

        DoubleWave = 129,

        Plus = 130,

        FlowChartProcess = 131,

        FlowChartDecision = 132,

        FlowChartInputOutput = 133,

        FlowChartPredefinedProcess = 134,

        FlowChartInternalStorage = 135,

        FlowChartDocument = 136,

        FlowChartMultidocument = 137,

        FlowChartTerminator = 138,

        FlowChartPreparation = 139,

        FlowChartManualInput = 140,

        FlowChartManualOperation = 141,

        FlowChartConnector = 142,

        FlowChartPunchedCard = 143,

        FlowChartPunchedTape = 144,

        FlowChartSummingJunction = 145,

        FlowChartOr = 146,

        FlowChartCollate = 147,

        FlowChartSort = 148,

        FlowChartExtract = 149,

        FlowChartMerge = 150,

        FlowChartOfflineStorage = 151,

        FlowChartOnlineStorage = 152,

        FlowChartMagneticTape = 153,

        FlowChartMagneticDisk = 154,

        FlowChartMagneticDrum = 155,

        FlowChartDisplay = 156,

        FlowChartDelay = 157,

        FlowChartAlternateProcess = 158,

        FlowChartOffpageConnector = 159,

        ActionButtonBlank = 160,

        ActionButtonHome = 161,

        ActionButtonHelp = 162,

        ActionButtonInformation = 163,

        ActionButtonForwardNext = 164,

        ActionButtonBackPrevious = 165,

        ActionButtonEnd = 166,

        ActionButtonBeginning = 167,

        ActionButtonReturn = 168,

        ActionButtonDocument = 169,

        ActionButtonSound = 170,

        ActionButtonMovie = 171,

        Gear6 = 172,

        Gear9 = 173,

        Funnel = 174,

        MathPlus = 175,

        MathMinus = 176,

        MathMultiply = 177,

        MathDivide = 178,

        MathEqual = 179,

        MathNotEqual = 180,

        CornerTabs = 181,

        SquareTabs = 182,

        PlaqueTabs = 183,

        ChartX = 184,

        ChartStar = 185,

        ChartPlus = 186
    }
}

namespace Fantom.Helper
{
    public static class ShapePresetHelper
    {
        // 具有单个节点的图形集合。
        private static HashSet<ShapePresetType> _oneAdjSet = new HashSet<ShapePresetType>()
        {
            ShapePresetType.RoundRectangle,
        };

        // 具有两个节点的图形集合。
        private static HashSet<ShapePresetType> _twoAdjSet = new HashSet<ShapePresetType>()
        {

        };

        private static HashSet<ShapePresetType> _threeAdjSet = new HashSet<ShapePresetType>()
        {

        };

        /// <summary>
        /// 返回指定图形对象的控点数。
        /// </summary>
        /// <param name="shapePresetType"></param>
        /// <returns>控点数目</returns>
        public static int GetAdjustCount(ShapePresetType shapePresetType)
        {
            if (_oneAdjSet.Contains(shapePresetType))
            {
                return 1;
            }
            else if (_twoAdjSet.Contains(shapePresetType))
            {
                return 2;
            }
            else if (_threeAdjSet.Contains(shapePresetType))
            {
                return 3;
            }

            return 0;
        }
    }
}