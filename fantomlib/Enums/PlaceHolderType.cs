﻿using DocumentFormat.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Enums
{
    public enum PlaceHolderType
    {

        [EnumString("title")]
        Title,

        [EnumString("body")]
        Body,

        [EnumString("ctrTitle")]
        CenteredTitle,

        [EnumString("subTitle")]
        SubTitle,

        [EnumString("dt")]
        DateAndTime,

        [EnumString("sldNum")]
        SlideNumber,

        [EnumString("ftr")]
        Footer,

        [EnumString("hdr")]
        Header,

        [EnumString("obj")]
        Object,

        [EnumString("chart")]
        Chart,

        [EnumString("tbl")]
        Table,

        [EnumString("clipArt")]
        ClipArt,

        [EnumString("dgm")]
        Diagram,

        [EnumString("media")]
        Media,


        [EnumString("sldImg")]
        SlideImage,
        
        [EnumString("pic")]
        Picture
    }

    class MyClass
    {
       
    }
}
