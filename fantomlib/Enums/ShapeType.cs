﻿namespace Fantom
{
    /// <summary>
    /// 图形的类型。
    /// </summary>
    public enum ShapeType
    {
        Unknown,

        /// <summary>
        /// 常规矢量图形类型。
        /// </summary>
        Shape,

        /// <summary>
        /// 组合图形类型，里面可内嵌常规矢量图形、连接图形与图片图形。
        /// </summary>
        Group,

        /// <summary>
        /// 连接符图形类型，一种特殊的线条图形。
        /// </summary>
        Connection,

        /// <summary>
        /// 图片类型。
        /// </summary>
        Picture,


    }
}
