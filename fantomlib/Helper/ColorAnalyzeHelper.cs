﻿using DocumentFormat.OpenXml;

namespace Fantom.Helper
{

    /// <summary>
    /// 颜色分析。
    /// </summary>
    public static class ColorAnalyzeHelper
    {

        /// <summary>
        /// 获得颜色对象。
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ColorBrushType GetColorBrushType(D.Color2Type element)
        {
            if (element.PresetColor is not null)
            {
                return ColorBrushType.Preset;
            }

            if (element.HslColor is not null)
            {
                return ColorBrushType.HSL;
            }

            if (element.RgbColorModelHex is not null)
            {
                return ColorBrushType.SRGB;
            }

            if (element.SystemColor is not null)
            {
                return ColorBrushType.System;
            }

            return ColorBrushType.Unknown;
        }

    }

    /// <summary>
    /// 主题色的解析。
    /// </summary>
    public static class ThemeColorTypeHelper
    {
        public static ColorSchemeColorType GetColorType(D.SchemeColorValues str)
        {
            switch (str)
            {
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Background1:
                    return ColorSchemeColorType.Light1;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Text1:
                    return ColorSchemeColorType.Dark2;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Background2:
                    return ColorSchemeColorType.Light2;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Text2:
                    return ColorSchemeColorType.Dark2;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Accent1:
                    return ColorSchemeColorType.Accent1;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Accent2:
                    return ColorSchemeColorType.Accent2;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Accent3:
                    return ColorSchemeColorType.Accent3;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Accent4:
                    return ColorSchemeColorType.Accent4;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Accent5:
                    return ColorSchemeColorType.Accent5;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Accent6:
                    return ColorSchemeColorType.Accent6;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Hyperlink:
                    return ColorSchemeColorType.Hyperlink;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.FollowedHyperlink:
                    return ColorSchemeColorType.FollowedHyperlink;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.PhColor:
                    return ColorSchemeColorType.Light2;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Dark1:
                    return ColorSchemeColorType.Dark1;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Light1:
                    return ColorSchemeColorType.Light1;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Dark2:
                    return ColorSchemeColorType.Dark2;
                case DocumentFormat.OpenXml.Drawing.SchemeColorValues.Light2:
                    return ColorSchemeColorType.Light2;
                default:
                    return ColorSchemeColorType.Unknown;
            }
          
    
        }
    }


}
