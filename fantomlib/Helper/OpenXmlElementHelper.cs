﻿using DocumentFormat.OpenXml.Math;
using DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Helper
{
    internal static class OpenXmlElementHelper
    {
        public static Emu Coordinate32ToPT(OX.OpenXmlElement ele)
        {
            return Emu.Degree1;
        }


        public static void AppendExtend(this OX.OpenXmlElement element, params OX.OpenXmlElement[] arr)
        {
            foreach (var item in arr)
            {
                if (item is null) continue;
                element.Append(item);
            }
        }

        public static OX.OpenXmlElement GetElement(this OX.OpenXmlElement root, params Type[] types)
        {
            return GetElements(root, types).FirstOrDefault();
        }

        public static IEnumerable<OX.OpenXmlElement> GetElements(this OX.OpenXmlElement root, params Type[] types)
        {
            var hashset = new HashSet<Type>(types);
            var list = new List<OX.OpenXmlElement>();
            if (root is null) return list;
            foreach (var xelem in root.Elements())
            {
                if (hashset.Contains(xelem.GetType())) list.Add(xelem);
            }
            return list;
        }

        public static IEnumerable<OX.OpenXmlElement> GetElements<T1>(this OX.OpenXmlElement root)
           where T1 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement
                where T14 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement
                where T14 : OX.OpenXmlElement
                where T15 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14), typeof(T15)); }
        public static IEnumerable<OX.OpenXmlElement> GetElements<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement
                where T14 : OX.OpenXmlElement
                where T15 : OX.OpenXmlElement
                where T16 : OX.OpenXmlElement

        { return GetElements(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14), typeof(T15), typeof(T16)); }


        public static OX.OpenXmlElement GetElement<T1>(this OX.OpenXmlElement root)
        where T1 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1)); }
        public static OX.OpenXmlElement GetElement<T1, T2>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement
                where T14 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement
                where T14 : OX.OpenXmlElement
                where T15 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14), typeof(T15)); }
        public static OX.OpenXmlElement GetElement<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this OX.OpenXmlElement root)
                where T1 : OX.OpenXmlElement
                where T2 : OX.OpenXmlElement
                where T3 : OX.OpenXmlElement
                where T4 : OX.OpenXmlElement
                where T5 : OX.OpenXmlElement
                where T6 : OX.OpenXmlElement
                where T7 : OX.OpenXmlElement
                where T8 : OX.OpenXmlElement
                where T9 : OX.OpenXmlElement
                where T10 : OX.OpenXmlElement
                where T11 : OX.OpenXmlElement
                where T12 : OX.OpenXmlElement
                where T13 : OX.OpenXmlElement
                where T14 : OX.OpenXmlElement
                where T15 : OX.OpenXmlElement
                where T16 : OX.OpenXmlElement

        { return GetElement(root, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14), typeof(T15), typeof(T16)); }



        //Function TRanges(count As Integer) As String
        //        Dim str As String: str = "T1"
        //        If count > 1 Then
        //                For i = 2 To count
        //                        str = str + ", T" & i
        //                Next
        //        End If
        //        TRanges = str
        //End Function

        //Function TypeOfRanges(count As Integer) As String
        //        Dim str As String: str = "typeof(T1)"
        //        If count > 1 Then
        //                For i = 2 To count
        //                        str = str + ", typeof(T" & i & ")"
        //                Next
        //        End If
        //        TypeOfRanges = str
        //End Function

        //Function WhereRanges(count As Integer) As String
        //        Dim str As String:
        //        For i = 1 To count
        //                str = str + "        where T" & i & ": OX.OpenXmlElement" + Chr(10)
        //        Next
        //        WhereRanges = str
        //End Function

        //Sub aaa()
        //Dim i As Integer
        //For i = 1 To 16
        //        Debug.Print CreateTBlock(i)
        //Next
        //'        Debug.Print TRanges(16)
        //'        Debug.Print TypeOfRanges(16)
        //'        Debug.Print WhereRanges(16)

        //End Sub

        //Function CreateTBlock(count As Integer) As String
        //         Dim len1 As String: len1 = "public static IEnumerable<OX.OpenXmlElement> GetElements<" + TRanges(count) + ">(this OX.OpenXmlElement root)" + Chr(10)
        //         Dim len2 As String: len2 = WhereRanges(count) + Chr(10)
        //        Dim len3 As String: len3 = "{ return GetElements(root, " + TypeOfRanges(count) + ");}"
        //        CreateTBlock = len1 + len2 + len3
        //End Function
    }
}
