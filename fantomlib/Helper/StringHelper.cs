﻿namespace Fantom
{

    /// <summary>
    /// 字符串辅助类，用于增加拓展方法。
    /// </summary>
    public static class StringHelper
    {
        public static string Shorten(this string str)
        {
            if (str.Length <= 15)
            {
                return str;
            }

            return str.Substring(0, 12) + "...";
        }

        public static OX.StringValue ToOX(this string str)
        {
            if (str is null) return null;
            //if (str == string.Empty) return null;
            return new OX.StringValue(str);
        }

    }
}
