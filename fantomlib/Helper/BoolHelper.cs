﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Helper
{

    /// <summary>
    /// 字符串辅助类，用于增加拓展方法。
    /// </summary>
    public static class BoolHelper
    {
        public static int ToInt(this bool obj)
        {
            if (obj)
                return 1;
            else
                return 0;
        }

        public static int ToInt(this bool? obj)
        {
            if (obj is null) return 0;
            return ToInt(obj.Value);
        }


        public static OX.BooleanValue ToOX(this bool? obj)
        {
            if (obj is null) return null;
            if (!obj.HasValue) return null;
            return new OX.BooleanValue(obj.Value);
        }


        public static bool IsTrue(this bool? obj)
        {
            if (obj is null) return false;
            if (!obj.HasValue) return false;
            return obj.Value;
        }

    }
}
