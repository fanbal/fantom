﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Helper
{
    internal static class EmuHelper
    {
        public static OX.Int64Value ToOX64(this Emu? emu)
        {
            if (emu is null) return null;
            if (!emu.HasValue) return null;
            return new OX.Int64Value(emu.Value.Value);
        }

        public static OX.Int32Value ToOX32(this Emu? emu)
        {
            if (emu is null) return null;
            if (!emu.HasValue) return null;
            return new OX.Int32Value(emu.Value.ToInt());
        }

        public static OX.UInt32Value ToOXU32(this Emu? emu)
        {
            if (emu is null) return null;
            if (!emu.HasValue) return null;
            return new OX.UInt32Value(emu.Value.ToUInt());
        }

        public static OX.UInt32Value ToOX(this uint? emu)
        {
            if (emu is null) return null;
            if (!emu.HasValue) return null;
            return new OX.UInt32Value(emu.Value);
        }

        public static OX.Int32Value ToOX(this int? emu)
        {
            if (emu is null) return null;
            if (!emu.HasValue) return null;
            return new OX.Int32Value(emu.Value);
        }
    }
}
