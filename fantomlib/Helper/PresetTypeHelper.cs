﻿namespace Fantom.Helper
{
    internal static class PresetTypeHelper
    {

        private static Dictionary<ShapePresetType, Emu[]> _typePresetAdjustmentsDic = new Dictionary<ShapePresetType, Emu[]>();




        private static void AddKey(ShapePresetType type, params Emu[] p)
        {
            if (p is null || p.Length == 0)
            {
                return;
            }

            _typePresetAdjustmentsDic.Add(type, p);
        }

        static PresetTypeHelper()
        {
            AddKey(ShapePresetType.RoundRectangle, 50000);
        }
    }
}
