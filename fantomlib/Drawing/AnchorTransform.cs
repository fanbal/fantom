﻿namespace Fantom.Drawing
{
    /// <summary>
    /// 锚点的信息。
    /// </summary>
    public class AnchorTransform
    {
        public double WidthRatio { get; set; }
        public double HeightRatio { get; set; }

        /// <summary>
        /// 此处为锚点真实的左坐标。所以与WPF以及PPT都需要重新转换。
        /// </summary>
        public Emu Left { get; set; }
        public Emu Top { get; set; }
        public AnchorTransform(double widthRatio, double heightRatio, Emu left, Emu top)
        {
            WidthRatio = widthRatio;
            HeightRatio = heightRatio;
            Left = left;
            Top = top;
        }

        public AnchorTransform(Emu left, Emu top) : this(0.5, 0.5, left, top)
        {

        }

        /// <summary>
        /// 默认的锚点配置。
        /// </summary>
        public static AnchorTransform Default = new AnchorTransform(0.5, 0.5, 0, 0);

        /// <summary>
        /// PPT传统的锚点配置。
        /// </summary>
        public static AnchorTransform Convention = new AnchorTransform(0, 0, 0, 0);

        public override string ToString()
        {
            return string.Format("Left: {0}, Top: {1}, WidthRatio: {2}, HeightRatio: {3}",
                Left.ToPixel(),
                Top.ToPixel(),
                Math.Round(WidthRatio, 4),
                Math.Round(HeightRatio, 4)
                );
        }

    }



}
