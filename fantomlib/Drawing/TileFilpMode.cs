﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Drawing
{
    /// <summary>
    /// 贴图模式
    /// </summary>
    public enum TileFilpMode
    {
        None, X, Y, XY
    }

}
