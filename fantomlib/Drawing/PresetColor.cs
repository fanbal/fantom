﻿namespace Fantom.Drawing
{
    /// <summary>
    /// 预设颜色。
    /// </summary>
    public sealed class PresetColor : TransformableColor
    {
        //
        // 摘要:
        //     Preset Color Value
        public enum PresetColorValues
        {
            //
            // 摘要:
            //     Alice Blue Preset Color.
            //     When the item is serialized out as xml, its value is "aliceBlue".
            AliceBlue = 0,
            //
            // 摘要:
            //     Antique White Preset Color.
            //     When the item is serialized out as xml, its value is "antiqueWhite".
            AntiqueWhite = 1,
            //
            // 摘要:
            //     Aqua Preset Color.
            //     When the item is serialized out as xml, its value is "aqua".
            Aqua = 2,
            //
            // 摘要:
            //     Aquamarine Preset Color.
            //     When the item is serialized out as xml, its value is "aquamarine".
            Aquamarine = 3,
            //
            // 摘要:
            //     Azure Preset Color.
            //     When the item is serialized out as xml, its value is "azure".
            Azure = 4,
            //
            // 摘要:
            //     Beige Preset Color.
            //     When the item is serialized out as xml, its value is "beige".
            Beige = 5,
            //
            // 摘要:
            //     Bisque Preset Color.
            //     When the item is serialized out as xml, its value is "bisque".
            Bisque = 6,
            //
            // 摘要:
            //     Black Preset Color.
            //     When the item is serialized out as xml, its value is "black".
            Black = 7,
            //
            // 摘要:
            //     Blanched Almond Preset Color.
            //     When the item is serialized out as xml, its value is "blanchedAlmond".
            BlanchedAlmond = 8,
            //
            // 摘要:
            //     Blue Preset Color.
            //     When the item is serialized out as xml, its value is "blue".
            Blue = 9,
            //
            // 摘要:
            //     Blue Violet Preset Color.
            //     When the item is serialized out as xml, its value is "blueViolet".
            BlueViolet = 10,
            //
            // 摘要:
            //     Brown Preset Color.
            //     When the item is serialized out as xml, its value is "brown".
            Brown = 11,
            //
            // 摘要:
            //     Burly Wood Preset Color.
            //     When the item is serialized out as xml, its value is "burlyWood".
            BurlyWood = 12,
            //
            // 摘要:
            //     Cadet Blue Preset Color.
            //     When the item is serialized out as xml, its value is "cadetBlue".
            CadetBlue = 13,
            //
            // 摘要:
            //     Chartreuse Preset Color.
            //     When the item is serialized out as xml, its value is "chartreuse".
            Chartreuse = 14,
            //
            // 摘要:
            //     Chocolate Preset Color.
            //     When the item is serialized out as xml, its value is "chocolate".
            Chocolate = 15,
            //
            // 摘要:
            //     Coral Preset Color.
            //     When the item is serialized out as xml, its value is "coral".
            Coral = 16,
            //
            // 摘要:
            //     Cornflower Blue Preset Color.
            //     When the item is serialized out as xml, its value is "cornflowerBlue".
            CornflowerBlue = 17,
            //
            // 摘要:
            //     Cornsilk Preset Color.
            //     When the item is serialized out as xml, its value is "cornsilk".
            Cornsilk = 18,
            //
            // 摘要:
            //     Crimson Preset Color.
            //     When the item is serialized out as xml, its value is "crimson".
            Crimson = 19,
            //
            // 摘要:
            //     Cyan Preset Color.
            //     When the item is serialized out as xml, its value is "cyan".
            Cyan = 20,
            //
            // 摘要:
            //     Dark Blue Preset Color.
            //     When the item is serialized out as xml, its value is "dkBlue".
            DarkBlue = 21,
            //
            // 摘要:
            //     Dark Cyan Preset Color.
            //     When the item is serialized out as xml, its value is "dkCyan".
            DarkCyan = 22,
            //
            // 摘要:
            //     Dark Goldenrod Preset Color.
            //     When the item is serialized out as xml, its value is "dkGoldenrod".
            DarkGoldenrod = 23,
            //
            // 摘要:
            //     Dark Gray Preset Color.
            //     When the item is serialized out as xml, its value is "dkGray".
            DarkGray = 24,
            //
            // 摘要:
            //     Dark Green Preset Color.
            //     When the item is serialized out as xml, its value is "dkGreen".
            DarkGreen = 25,
            //
            // 摘要:
            //     Dark Khaki Preset Color.
            //     When the item is serialized out as xml, its value is "dkKhaki".
            DarkKhaki = 26,
            //
            // 摘要:
            //     Dark Magenta Preset Color.
            //     When the item is serialized out as xml, its value is "dkMagenta".
            DarkMagenta = 27,
            //
            // 摘要:
            //     Dark Olive Green Preset Color.
            //     When the item is serialized out as xml, its value is "dkOliveGreen".
            DarkOliveGreen = 28,
            //
            // 摘要:
            //     Dark Orange Preset Color.
            //     When the item is serialized out as xml, its value is "dkOrange".
            DarkOrange = 29,
            //
            // 摘要:
            //     Dark Orchid Preset Color.
            //     When the item is serialized out as xml, its value is "dkOrchid".
            DarkOrchid = 30,
            //
            // 摘要:
            //     Dark Red Preset Color.
            //     When the item is serialized out as xml, its value is "dkRed".
            DarkRed = 31,
            //
            // 摘要:
            //     Dark Salmon Preset Color.
            //     When the item is serialized out as xml, its value is "dkSalmon".
            DarkSalmon = 32,
            //
            // 摘要:
            //     Dark Sea Green Preset Color.
            //     When the item is serialized out as xml, its value is "dkSeaGreen".
            DarkSeaGreen = 33,
            //
            // 摘要:
            //     Dark Slate Blue Preset Color.
            //     When the item is serialized out as xml, its value is "dkSlateBlue".
            DarkSlateBlue = 34,
            //
            // 摘要:
            //     Dark Slate Gray Preset Color.
            //     When the item is serialized out as xml, its value is "dkSlateGray".
            DarkSlateGray = 35,
            //
            // 摘要:
            //     Dark Turquoise Preset Color.
            //     When the item is serialized out as xml, its value is "dkTurquoise".
            DarkTurquoise = 36,
            //
            // 摘要:
            //     Dark Violet Preset Color.
            //     When the item is serialized out as xml, its value is "dkViolet".
            DarkViolet = 37,
            //
            // 摘要:
            //     Deep Pink Preset Color.
            //     When the item is serialized out as xml, its value is "deepPink".
            DeepPink = 38,
            //
            // 摘要:
            //     Deep Sky Blue Preset Color.
            //     When the item is serialized out as xml, its value is "deepSkyBlue".
            DeepSkyBlue = 39,
            //
            // 摘要:
            //     Dim Gray Preset Color.
            //     When the item is serialized out as xml, its value is "dimGray".
            DimGray = 40,
            //
            // 摘要:
            //     Dodger Blue Preset Color.
            //     When the item is serialized out as xml, its value is "dodgerBlue".
            DodgerBlue = 41,
            //
            // 摘要:
            //     Firebrick Preset Color.
            //     When the item is serialized out as xml, its value is "firebrick".
            Firebrick = 42,
            //
            // 摘要:
            //     Floral White Preset Color.
            //     When the item is serialized out as xml, its value is "floralWhite".
            FloralWhite = 43,
            //
            // 摘要:
            //     Forest Green Preset Color.
            //     When the item is serialized out as xml, its value is "forestGreen".
            ForestGreen = 44,
            //
            // 摘要:
            //     Fuchsia Preset Color.
            //     When the item is serialized out as xml, its value is "fuchsia".
            Fuchsia = 45,
            //
            // 摘要:
            //     Gainsboro Preset Color.
            //     When the item is serialized out as xml, its value is "gainsboro".
            Gainsboro = 46,
            //
            // 摘要:
            //     Ghost White Preset Color.
            //     When the item is serialized out as xml, its value is "ghostWhite".
            GhostWhite = 47,
            //
            // 摘要:
            //     Gold Preset Color.
            //     When the item is serialized out as xml, its value is "gold".
            Gold = 48,
            //
            // 摘要:
            //     Goldenrod Preset Color.
            //     When the item is serialized out as xml, its value is "goldenrod".
            Goldenrod = 49,
            //
            // 摘要:
            //     Gray Preset Color.
            //     When the item is serialized out as xml, its value is "gray".
            Gray = 50,
            //
            // 摘要:
            //     Green Preset Color.
            //     When the item is serialized out as xml, its value is "green".
            Green = 51,
            //
            // 摘要:
            //     Green Yellow Preset Color.
            //     When the item is serialized out as xml, its value is "greenYellow".
            GreenYellow = 52,
            //
            // 摘要:
            //     Honeydew Preset Color.
            //     When the item is serialized out as xml, its value is "honeydew".
            Honeydew = 53,
            //
            // 摘要:
            //     Hot Pink Preset Color.
            //     When the item is serialized out as xml, its value is "hotPink".
            HotPink = 54,
            //
            // 摘要:
            //     Indian Red Preset Color.
            //     When the item is serialized out as xml, its value is "indianRed".
            IndianRed = 55,
            //
            // 摘要:
            //     Indigo Preset Color.
            //     When the item is serialized out as xml, its value is "indigo".
            Indigo = 56,
            //
            // 摘要:
            //     Ivory Preset Color.
            //     When the item is serialized out as xml, its value is "ivory".
            Ivory = 57,
            //
            // 摘要:
            //     Khaki Preset Color.
            //     When the item is serialized out as xml, its value is "khaki".
            Khaki = 58,
            //
            // 摘要:
            //     Lavender Preset Color.
            //     When the item is serialized out as xml, its value is "lavender".
            Lavender = 59,
            //
            // 摘要:
            //     Lavender Blush Preset Color.
            //     When the item is serialized out as xml, its value is "lavenderBlush".
            LavenderBlush = 60,
            //
            // 摘要:
            //     Lawn Green Preset Color.
            //     When the item is serialized out as xml, its value is "lawnGreen".
            LawnGreen = 61,
            //
            // 摘要:
            //     Lemon Chiffon Preset Color.
            //     When the item is serialized out as xml, its value is "lemonChiffon".
            LemonChiffon = 62,
            //
            // 摘要:
            //     Light Blue Preset Color.
            //     When the item is serialized out as xml, its value is "ltBlue".
            LightBlue = 63,
            //
            // 摘要:
            //     Light Coral Preset Color.
            //     When the item is serialized out as xml, its value is "ltCoral".
            LightCoral = 64,
            //
            // 摘要:
            //     Light Cyan Preset Color.
            //     When the item is serialized out as xml, its value is "ltCyan".
            LightCyan = 65,
            //
            // 摘要:
            //     Light Goldenrod Yellow Preset Color.
            //     When the item is serialized out as xml, its value is "ltGoldenrodYellow".
            LightGoldenrodYellow = 66,
            //
            // 摘要:
            //     Light Gray Preset Color.
            //     When the item is serialized out as xml, its value is "ltGray".
            LightGray = 67,
            //
            // 摘要:
            //     Light Green Preset Color.
            //     When the item is serialized out as xml, its value is "ltGreen".
            LightGreen = 68,
            //
            // 摘要:
            //     Light Pink Preset Color.
            //     When the item is serialized out as xml, its value is "ltPink".
            LightPink = 69,
            //
            // 摘要:
            //     Light Salmon Preset Color.
            //     When the item is serialized out as xml, its value is "ltSalmon".
            LightSalmon = 70,
            //
            // 摘要:
            //     Light Sea Green Preset Color.
            //     When the item is serialized out as xml, its value is "ltSeaGreen".
            LightSeaGreen = 71,
            //
            // 摘要:
            //     Light Sky Blue Preset Color.
            //     When the item is serialized out as xml, its value is "ltSkyBlue".
            LightSkyBlue = 72,
            //
            // 摘要:
            //     Light Slate Gray Preset Color.
            //     When the item is serialized out as xml, its value is "ltSlateGray".
            LightSlateGray = 73,
            //
            // 摘要:
            //     Light Steel Blue Preset Color.
            //     When the item is serialized out as xml, its value is "ltSteelBlue".
            LightSteelBlue = 74,
            //
            // 摘要:
            //     Light Yellow Preset Color.
            //     When the item is serialized out as xml, its value is "ltYellow".
            LightYellow = 75,
            //
            // 摘要:
            //     Lime Preset Color.
            //     When the item is serialized out as xml, its value is "lime".
            Lime = 76,
            //
            // 摘要:
            //     Lime Green Preset Color.
            //     When the item is serialized out as xml, its value is "limeGreen".
            LimeGreen = 77,
            //
            // 摘要:
            //     Linen Preset Color.
            //     When the item is serialized out as xml, its value is "linen".
            Linen = 78,
            //
            // 摘要:
            //     Magenta Preset Color.
            //     When the item is serialized out as xml, its value is "magenta".
            Magenta = 79,
            //
            // 摘要:
            //     Maroon Preset Color.
            //     When the item is serialized out as xml, its value is "maroon".
            Maroon = 80,
            //
            // 摘要:
            //     Medium Aquamarine Preset Color.
            //     When the item is serialized out as xml, its value is "medAquamarine".
            MedAquamarine = 81,
            //
            // 摘要:
            //     Medium Blue Preset Color.
            //     When the item is serialized out as xml, its value is "medBlue".
            MediumBlue = 82,
            //
            // 摘要:
            //     Medium Orchid Preset Color.
            //     When the item is serialized out as xml, its value is "medOrchid".
            MediumOrchid = 83,
            //
            // 摘要:
            //     Medium Purple Preset Color.
            //     When the item is serialized out as xml, its value is "medPurple".
            MediumPurple = 84,
            //
            // 摘要:
            //     Medium Sea Green Preset Color.
            //     When the item is serialized out as xml, its value is "medSeaGreen".
            MediumSeaGreen = 85,
            //
            // 摘要:
            //     Medium Slate Blue Preset Color.
            //     When the item is serialized out as xml, its value is "medSlateBlue".
            MediumSlateBlue = 86,
            //
            // 摘要:
            //     Medium Spring Green Preset Color.
            //     When the item is serialized out as xml, its value is "medSpringGreen".
            MediumSpringGreen = 87,
            //
            // 摘要:
            //     Medium Turquoise Preset Color.
            //     When the item is serialized out as xml, its value is "medTurquoise".
            MediumTurquoise = 88,
            //
            // 摘要:
            //     Medium Violet Red Preset Color.
            //     When the item is serialized out as xml, its value is "medVioletRed".
            MediumVioletRed = 89,
            //
            // 摘要:
            //     Midnight Blue Preset Color.
            //     When the item is serialized out as xml, its value is "midnightBlue".
            MidnightBlue = 90,
            //
            // 摘要:
            //     Mint Cream Preset Color.
            //     When the item is serialized out as xml, its value is "mintCream".
            MintCream = 91,
            //
            // 摘要:
            //     Misty Rose Preset Color.
            //     When the item is serialized out as xml, its value is "mistyRose".
            MistyRose = 92,
            //
            // 摘要:
            //     Moccasin Preset Color.
            //     When the item is serialized out as xml, its value is "moccasin".
            Moccasin = 93,
            //
            // 摘要:
            //     Navajo White Preset Color.
            //     When the item is serialized out as xml, its value is "navajoWhite".
            NavajoWhite = 94,
            //
            // 摘要:
            //     Navy Preset Color.
            //     When the item is serialized out as xml, its value is "navy".
            Navy = 95,
            //
            // 摘要:
            //     Old Lace Preset Color.
            //     When the item is serialized out as xml, its value is "oldLace".
            OldLace = 96,
            //
            // 摘要:
            //     Olive Preset Color.
            //     When the item is serialized out as xml, its value is "olive".
            Olive = 97,
            //
            // 摘要:
            //     Olive Drab Preset Color.
            //     When the item is serialized out as xml, its value is "oliveDrab".
            OliveDrab = 98,
            //
            // 摘要:
            //     Orange Preset Color.
            //     When the item is serialized out as xml, its value is "orange".
            Orange = 99,
            //
            // 摘要:
            //     Orange Red Preset Color.
            //     When the item is serialized out as xml, its value is "orangeRed".
            OrangeRed = 100,
            //
            // 摘要:
            //     Orchid Preset Color.
            //     When the item is serialized out as xml, its value is "orchid".
            Orchid = 101,
            //
            // 摘要:
            //     Pale Goldenrod Preset Color.
            //     When the item is serialized out as xml, its value is "paleGoldenrod".
            PaleGoldenrod = 102,
            //
            // 摘要:
            //     Pale Green Preset Color.
            //     When the item is serialized out as xml, its value is "paleGreen".
            PaleGreen = 103,
            //
            // 摘要:
            //     Pale Turquoise Preset Color.
            //     When the item is serialized out as xml, its value is "paleTurquoise".
            PaleTurquoise = 104,
            //
            // 摘要:
            //     Pale Violet Red Preset Color.
            //     When the item is serialized out as xml, its value is "paleVioletRed".
            PaleVioletRed = 105,
            //
            // 摘要:
            //     Papaya Whip Preset Color.
            //     When the item is serialized out as xml, its value is "papayaWhip".
            PapayaWhip = 106,
            //
            // 摘要:
            //     Peach Puff Preset Color.
            //     When the item is serialized out as xml, its value is "peachPuff".
            PeachPuff = 107,
            //
            // 摘要:
            //     Peru Preset Color.
            //     When the item is serialized out as xml, its value is "peru".
            Peru = 108,
            //
            // 摘要:
            //     Pink Preset Color.
            //     When the item is serialized out as xml, its value is "pink".
            Pink = 109,
            //
            // 摘要:
            //     Plum Preset Color.
            //     When the item is serialized out as xml, its value is "plum".
            Plum = 110,
            //
            // 摘要:
            //     Powder Blue Preset Color.
            //     When the item is serialized out as xml, its value is "powderBlue".
            PowderBlue = 111,
            //
            // 摘要:
            //     Purple Preset Color.
            //     When the item is serialized out as xml, its value is "purple".
            Purple = 112,
            //
            // 摘要:
            //     Red Preset Color.
            //     When the item is serialized out as xml, its value is "red".
            Red = 113,
            //
            // 摘要:
            //     Rosy Brown Preset Color.
            //     When the item is serialized out as xml, its value is "rosyBrown".
            RosyBrown = 114,
            //
            // 摘要:
            //     Royal Blue Preset Color.
            //     When the item is serialized out as xml, its value is "royalBlue".
            RoyalBlue = 115,
            //
            // 摘要:
            //     Saddle Brown Preset Color.
            //     When the item is serialized out as xml, its value is "saddleBrown".
            SaddleBrown = 116,
            //
            // 摘要:
            //     Salmon Preset Color.
            //     When the item is serialized out as xml, its value is "salmon".
            Salmon = 117,
            //
            // 摘要:
            //     Sandy Brown Preset Color.
            //     When the item is serialized out as xml, its value is "sandyBrown".
            SandyBrown = 118,
            //
            // 摘要:
            //     Sea Green Preset Color.
            //     When the item is serialized out as xml, its value is "seaGreen".
            SeaGreen = 119,
            //
            // 摘要:
            //     Sea Shell Preset Color.
            //     When the item is serialized out as xml, its value is "seaShell".
            SeaShell = 120,
            //
            // 摘要:
            //     Sienna Preset Color.
            //     When the item is serialized out as xml, its value is "sienna".
            Sienna = 121,
            //
            // 摘要:
            //     Silver Preset Color.
            //     When the item is serialized out as xml, its value is "silver".
            Silver = 122,
            //
            // 摘要:
            //     Sky Blue Preset Color.
            //     When the item is serialized out as xml, its value is "skyBlue".
            SkyBlue = 123,
            //
            // 摘要:
            //     Slate Blue Preset Color.
            //     When the item is serialized out as xml, its value is "slateBlue".
            SlateBlue = 124,
            //
            // 摘要:
            //     Slate Gray Preset Color.
            //     When the item is serialized out as xml, its value is "slateGray".
            SlateGray = 125,
            //
            // 摘要:
            //     Snow Preset Color.
            //     When the item is serialized out as xml, its value is "snow".
            Snow = 126,
            //
            // 摘要:
            //     Spring Green Preset Color.
            //     When the item is serialized out as xml, its value is "springGreen".
            SpringGreen = 127,
            //
            // 摘要:
            //     Steel Blue Preset Color.
            //     When the item is serialized out as xml, its value is "steelBlue".
            SteelBlue = 128,
            //
            // 摘要:
            //     Tan Preset Color.
            //     When the item is serialized out as xml, its value is "tan".
            Tan = 129,
            //
            // 摘要:
            //     Teal Preset Color.
            //     When the item is serialized out as xml, its value is "teal".
            Teal = 130,
            //
            // 摘要:
            //     Thistle Preset Color.
            //     When the item is serialized out as xml, its value is "thistle".
            Thistle = 131,
            //
            // 摘要:
            //     Tomato Preset Color.
            //     When the item is serialized out as xml, its value is "tomato".
            Tomato = 132,
            //
            // 摘要:
            //     Turquoise Preset Color.
            //     When the item is serialized out as xml, its value is "turquoise".
            Turquoise = 133,
            //
            // 摘要:
            //     Violet Preset Color.
            //     When the item is serialized out as xml, its value is "violet".
            Violet = 134,
            //
            // 摘要:
            //     Wheat Preset Color.
            //     When the item is serialized out as xml, its value is "wheat".
            Wheat = 135,
            //
            // 摘要:
            //     White Preset Color.
            //     When the item is serialized out as xml, its value is "white".
            White = 136,
            //
            // 摘要:
            //     White Smoke Preset Color.
            //     When the item is serialized out as xml, its value is "whiteSmoke".
            WhiteSmoke = 137,
            //
            // 摘要:
            //     Yellow Preset Color.
            //     When the item is serialized out as xml, its value is "yellow".
            Yellow = 138,
            //
            // 摘要:
            //     Yellow Green Preset Color.
            //     When the item is serialized out as xml, its value is "yellowGreen".
            YellowGreen = 139,
            //
            // 摘要:
            //     darkBlue.
            //     When the item is serialized out as xml, its value is "darkBlue".
            //     This item is only available in Office 2010 and later.
            DarkBlue2010 = 140,
            //
            // 摘要:
            //     darkCyan.
            //     When the item is serialized out as xml, its value is "darkCyan".
            //     This item is only available in Office 2010 and later.
            DarkCyan2010 = 141,
            //
            // 摘要:
            //     darkGoldenrod.
            //     When the item is serialized out as xml, its value is "darkGoldenrod".
            //     This item is only available in Office 2010 and later.
            DarkGoldenrod2010 = 142,
            //
            // 摘要:
            //     darkGray.
            //     When the item is serialized out as xml, its value is "darkGray".
            //     This item is only available in Office 2010 and later.
            DarkGray2010 = 143,
            //
            // 摘要:
            //     darkGrey.
            //     When the item is serialized out as xml, its value is "darkGrey".
            //     This item is only available in Office 2010 and later.
            DarkGrey2010 = 144,
            //
            // 摘要:
            //     darkGreen.
            //     When the item is serialized out as xml, its value is "darkGreen".
            //     This item is only available in Office 2010 and later.
            DarkGreen2010 = 145,
            //
            // 摘要:
            //     darkKhaki.
            //     When the item is serialized out as xml, its value is "darkKhaki".
            //     This item is only available in Office 2010 and later.
            DarkKhaki2010 = 146,
            //
            // 摘要:
            //     darkMagenta.
            //     When the item is serialized out as xml, its value is "darkMagenta".
            //     This item is only available in Office 2010 and later.
            DarkMagenta2010 = 147,
            //
            // 摘要:
            //     darkOliveGreen.
            //     When the item is serialized out as xml, its value is "darkOliveGreen".
            //     This item is only available in Office 2010 and later.
            DarkOliveGreen2010 = 148,
            //
            // 摘要:
            //     darkOrange.
            //     When the item is serialized out as xml, its value is "darkOrange".
            //     This item is only available in Office 2010 and later.
            DarkOrange2010 = 149,
            //
            // 摘要:
            //     darkOrchid.
            //     When the item is serialized out as xml, its value is "darkOrchid".
            //     This item is only available in Office 2010 and later.
            DarkOrchid2010 = 150,
            //
            // 摘要:
            //     darkRed.
            //     When the item is serialized out as xml, its value is "darkRed".
            //     This item is only available in Office 2010 and later.
            DarkRed2010 = 151,
            //
            // 摘要:
            //     darkSalmon.
            //     When the item is serialized out as xml, its value is "darkSalmon".
            //     This item is only available in Office 2010 and later.
            DarkSalmon2010 = 152,
            //
            // 摘要:
            //     darkSeaGreen.
            //     When the item is serialized out as xml, its value is "darkSeaGreen".
            //     This item is only available in Office 2010 and later.
            DarkSeaGreen2010 = 153,
            //
            // 摘要:
            //     darkSlateBlue.
            //     When the item is serialized out as xml, its value is "darkSlateBlue".
            //     This item is only available in Office 2010 and later.
            DarkSlateBlue2010 = 154,
            //
            // 摘要:
            //     darkSlateGray.
            //     When the item is serialized out as xml, its value is "darkSlateGray".
            //     This item is only available in Office 2010 and later.
            DarkSlateGray2010 = 155,
            //
            // 摘要:
            //     darkSlateGrey.
            //     When the item is serialized out as xml, its value is "darkSlateGrey".
            //     This item is only available in Office 2010 and later.
            DarkSlateGrey2010 = 156,
            //
            // 摘要:
            //     darkTurquoise.
            //     When the item is serialized out as xml, its value is "darkTurquoise".
            //     This item is only available in Office 2010 and later.
            DarkTurquoise2010 = 157,
            //
            // 摘要:
            //     darkViolet.
            //     When the item is serialized out as xml, its value is "darkViolet".
            //     This item is only available in Office 2010 and later.
            DarkViolet2010 = 158,
            //
            // 摘要:
            //     lightBlue.
            //     When the item is serialized out as xml, its value is "lightBlue".
            //     This item is only available in Office 2010 and later.
            LightBlue2010 = 159,
            //
            // 摘要:
            //     lightCoral.
            //     When the item is serialized out as xml, its value is "lightCoral".
            //     This item is only available in Office 2010 and later.
            LightCoral2010 = 160,
            //
            // 摘要:
            //     lightCyan.
            //     When the item is serialized out as xml, its value is "lightCyan".
            //     This item is only available in Office 2010 and later.
            LightCyan2010 = 161,
            //
            // 摘要:
            //     lightGoldenrodYellow.
            //     When the item is serialized out as xml, its value is "lightGoldenrodYellow".
            //     This item is only available in Office 2010 and later.
            LightGoldenrodYellow2010 = 162,
            //
            // 摘要:
            //     lightGray.
            //     When the item is serialized out as xml, its value is "lightGray".
            //     This item is only available in Office 2010 and later.
            LightGray2010 = 163,
            //
            // 摘要:
            //     lightGrey.
            //     When the item is serialized out as xml, its value is "lightGrey".
            //     This item is only available in Office 2010 and later.
            LightGrey2010 = 164,
            //
            // 摘要:
            //     lightGreen.
            //     When the item is serialized out as xml, its value is "lightGreen".
            //     This item is only available in Office 2010 and later.
            LightGreen2010 = 165,
            //
            // 摘要:
            //     lightPink.
            //     When the item is serialized out as xml, its value is "lightPink".
            //     This item is only available in Office 2010 and later.
            LightPink2010 = 166,
            //
            // 摘要:
            //     lightSalmon.
            //     When the item is serialized out as xml, its value is "lightSalmon".
            //     This item is only available in Office 2010 and later.
            LightSalmon2010 = 167,
            //
            // 摘要:
            //     lightSeaGreen.
            //     When the item is serialized out as xml, its value is "lightSeaGreen".
            //     This item is only available in Office 2010 and later.
            LightSeaGreen2010 = 168,
            //
            // 摘要:
            //     lightSkyBlue.
            //     When the item is serialized out as xml, its value is "lightSkyBlue".
            //     This item is only available in Office 2010 and later.
            LightSkyBlue2010 = 169,
            //
            // 摘要:
            //     lightSlateGray.
            //     When the item is serialized out as xml, its value is "lightSlateGray".
            //     This item is only available in Office 2010 and later.
            LightSlateGray2010 = 170,
            //
            // 摘要:
            //     lightSlateGrey.
            //     When the item is serialized out as xml, its value is "lightSlateGrey".
            //     This item is only available in Office 2010 and later.
            LightSlateGrey2010 = 171,
            //
            // 摘要:
            //     lightSteelBlue.
            //     When the item is serialized out as xml, its value is "lightSteelBlue".
            //     This item is only available in Office 2010 and later.
            LightSteelBlue2010 = 172,
            //
            // 摘要:
            //     lightYellow.
            //     When the item is serialized out as xml, its value is "lightYellow".
            //     This item is only available in Office 2010 and later.
            LightYellow2010 = 173,
            //
            // 摘要:
            //     mediumAquamarine.
            //     When the item is serialized out as xml, its value is "mediumAquamarine".
            //     This item is only available in Office 2010 and later.
            MediumAquamarine2010 = 174,
            //
            // 摘要:
            //     mediumBlue.
            //     When the item is serialized out as xml, its value is "mediumBlue".
            //     This item is only available in Office 2010 and later.
            MediumBlue2010 = 175,
            //
            // 摘要:
            //     mediumOrchid.
            //     When the item is serialized out as xml, its value is "mediumOrchid".
            //     This item is only available in Office 2010 and later.
            MediumOrchid2010 = 176,
            //
            // 摘要:
            //     mediumPurple.
            //     When the item is serialized out as xml, its value is "mediumPurple".
            //     This item is only available in Office 2010 and later.
            MediumPurple2010 = 177,
            //
            // 摘要:
            //     mediumSeaGreen.
            //     When the item is serialized out as xml, its value is "mediumSeaGreen".
            //     This item is only available in Office 2010 and later.
            MediumSeaGreen2010 = 178,
            //
            // 摘要:
            //     mediumSlateBlue.
            //     When the item is serialized out as xml, its value is "mediumSlateBlue".
            //     This item is only available in Office 2010 and later.
            MediumSlateBlue2010 = 179,
            //
            // 摘要:
            //     mediumSpringGreen.
            //     When the item is serialized out as xml, its value is "mediumSpringGreen".
            //     This item is only available in Office 2010 and later.
            MediumSpringGreen2010 = 180,
            //
            // 摘要:
            //     mediumTurquoise.
            //     When the item is serialized out as xml, its value is "mediumTurquoise".
            //     This item is only available in Office 2010 and later.
            MediumTurquoise2010 = 181,
            //
            // 摘要:
            //     mediumVioletRed.
            //     When the item is serialized out as xml, its value is "mediumVioletRed".
            //     This item is only available in Office 2010 and later.
            MediumVioletRed2010 = 182,
            //
            // 摘要:
            //     dkGrey.
            //     When the item is serialized out as xml, its value is "dkGrey".
            //     This item is only available in Office 2010 and later.
            DarkGrey = 183,
            //
            // 摘要:
            //     dimGrey.
            //     When the item is serialized out as xml, its value is "dimGrey".
            //     This item is only available in Office 2010 and later.
            DimGrey = 184,
            //
            // 摘要:
            //     dkSlateGrey.
            //     When the item is serialized out as xml, its value is "dkSlateGrey".
            //     This item is only available in Office 2010 and later.
            DarkSlateGrey = 185,
            //
            // 摘要:
            //     grey.
            //     When the item is serialized out as xml, its value is "grey".
            //     This item is only available in Office 2010 and later.
            Grey = 186,
            //
            // 摘要:
            //     ltGrey.
            //     When the item is serialized out as xml, its value is "ltGrey".
            //     This item is only available in Office 2010 and later.
            LightGrey = 187,
            //
            // 摘要:
            //     ltSlateGrey.
            //     When the item is serialized out as xml, its value is "ltSlateGrey".
            //     This item is only available in Office 2010 and later.
            LightSlateGrey = 188,
            //
            // 摘要:
            //     slateGrey.
            //     When the item is serialized out as xml, its value is "slateGrey".
            //     This item is only available in Office 2010 and later.
            SlateGrey = 189
        }

        #region prop

        public PresetColorValues PresetType { get; private set; } 

        public string Name { get; private set; }

        #endregion

        #region static

        private static Dictionary<PresetColorValues, PresetColor> _colorDic = new Dictionary<PresetColorValues, PresetColor>();

        static PresetColor()
        {
            AddColor(PresetColorValues.Black, "000000");
            AddColor(PresetColorValues.White, "FFFFFF");
        }

        private static void AddColor(PresetColorValues type, byte red, byte green, byte blue)
        {
            _colorDic.Add(type, new PresetColor() { PresetType = type, _red = red, _green = green, _blue = blue });
        }

        private static void AddColor(PresetColorValues type, string hex)
        {

            var rgb = ColorConverter.HexToRGB(hex);

            _colorDic.Add(type, new PresetColor() { PresetType = type, _red = rgb[0], _green = rgb[1], _blue = rgb[2] });
        }


        /// <summary>
        /// 从相应名称中获得返回值。
        /// </summary>
        public static PresetColor FromPresetType(PresetColorValues type)
        {
            if (_colorDic.ContainsKey(type))
            {
                return _colorDic[type];
            }
            else
            {
                return _colorDic[PresetColorValues.Black];
            }
        }
        #endregion
    }
}
