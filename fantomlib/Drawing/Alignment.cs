﻿using O = DocumentFormat.OpenXml;

namespace Fantom.Drawing
{
    /// <summary>
    /// 对齐。
    /// </summary>
    public enum Alignment
    {
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Top Left ).
        //     When the item is serialized out as xml, its value is "tl".
        TopLeft = 0,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Top ).
        //     When the item is serialized out as xml, its value is "t".
        Top = 1,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Top Right ).
        //     When the item is serialized out as xml, its value is "tr".
        TopRight = 2,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Left ).
        //     When the item is serialized out as xml, its value is "l".
        Left = 3,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Center ).
        //     When the item is serialized out as xml, its value is "ctr".
        Center = 4,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Right ).
        //     When the item is serialized out as xml, its value is "r".
        Right = 5,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Bottom Left ).
        //     When the item is serialized out as xml, its value is "bl".
        BottomLeft = 6,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Bottom ).
        //     When the item is serialized out as xml, its value is "b".
        Bottom = 7,
        //
        // 摘要:
        //     Rectangle Alignment Enum ( Bottom Right ).
        //     When the item is serialized out as xml, its value is "br".
        BottomRight = 8
    }


    public static class AlignmentHelper
    {
        /// <summary>
        /// 从 OpenXml 的对齐中获得 Fantom 的对齐枚举。
        /// </summary>
        public static Alignment GetAlignment(O.EnumValue<D.RectangleAlignmentValues> al)
        {
            if (al is null)
            {
                return Alignment.Center;
            }

            return (Alignment)al.Value;
        }
    }

}
