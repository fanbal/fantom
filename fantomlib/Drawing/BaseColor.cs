﻿namespace Fantom.Drawing
{


    /// <summary>
    /// 由于 PPT 中存在诸多颜色类型，有些属于引用，有些则是值类型，为此，这里提供了一个有关颜色的基类，用于表示不同的颜色。
    /// </summary>
    public abstract class ColorBase
    {
        private static HashSet<Type> _colorTypeHashSet = new HashSet<Type>()
        {
            typeof(D.SystemColor), typeof(D.SchemeColor), typeof(D.HslColor), typeof(D.RgbColorModelHex), typeof(D.RgbColorModelPercentage), typeof(D.PresetColor)
        };

        internal static bool IsColorTypeX(OX.OpenXmlElement element)
        {
            return _colorTypeHashSet.Contains(element.GetType());
        }

        public enum ColorMode
        {
            RGB, HSL
        }

       


        #region rgb


        /// <summary>
        /// 红色通道，节点为a:red
        /// </summary>
        public virtual byte Red
        {
            get => _red;
        }
        protected byte _red = 0;




        /// <summary>
        /// 绿色通道，节点为a:green
        /// </summary>
        public virtual byte Green
        {
            get => _green;
        }
        protected byte _green = 0;



        /// <summary>
        /// 蓝色通道，节点为a:blue
        /// </summary>
        public virtual byte Blue
        {
            get => _blue;
        }
        protected byte _blue = 0;

        #endregion

        #region hsl

        /// <summary>
        /// 色相。
        /// </summary>
        public virtual double Hue => _hue;
        protected double _hue;

        /// <summary>
        /// 饱和度。
        /// </summary>
        public virtual double Saturation => _saturation;
        protected double _saturation;

        /// <summary>
        /// 亮度。
        /// </summary>
        public virtual double Lightness => _lightness;
        protected double _lightness;

        #endregion

        protected ColorBase()
        {

        }

        public ColorBase(string hex)
        {
            var rgb = ColorConverter.HexToRGB(hex);

            _red = rgb[0];
            _green = rgb[1];
            _blue = rgb[2];
        }


        public override string ToString()
        {
            return string.Format("RGB: ({0}, {1}, {2})",
                this.Red, this.Green, this.Blue);
        }


        public string ToHex()
        {
            return ColorConverter.RGBToHex(_red, _green, _blue);
        }


    }

    public static class Colors
    {
        public static SolidColor White = new SolidColor(255, 255, 255);
        public static SolidColor Black = new SolidColor(0, 0, 0);
    }

}
