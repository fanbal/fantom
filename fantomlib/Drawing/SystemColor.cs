﻿using DocumentFormat.OpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Drawing
{
    public class SystemColor : ColorBase
    {
        public enum SystemColorValues
        {
            //
            // 摘要:
            //     Scroll Bar System Color.
            //     When the item is serialized out as xml, its value is "scrollBar".
            [EnumString("scrollBar")]
            ScrollBar,
            //
            // 摘要:
            //     Background System Color.
            //     When the item is serialized out as xml, its value is "background".
            [EnumString("background")]
            Background,
            //
            // 摘要:
            //     Active Caption System Color.
            //     When the item is serialized out as xml, its value is "activeCaption".
            [EnumString("activeCaption")]
            ActiveCaption,
            //
            // 摘要:
            //     Inactive Caption System Color.
            //     When the item is serialized out as xml, its value is "inactiveCaption".
            [EnumString("inactiveCaption")]
            InactiveCaption,
            //
            // 摘要:
            //     Menu System Color.
            //     When the item is serialized out as xml, its value is "menu".
            [EnumString("menu")]
            Menu,
            //
            // 摘要:
            //     Window System Color.
            //     When the item is serialized out as xml, its value is "window".
            [EnumString("window")]
            Window,
            //
            // 摘要:
            //     Window Frame System Color.
            //     When the item is serialized out as xml, its value is "windowFrame".
            [EnumString("windowFrame")]
            WindowFrame,
            //
            // 摘要:
            //     Menu Text System Color.
            //     When the item is serialized out as xml, its value is "menuText".
            [EnumString("menuText")]
            MenuText,
            //
            // 摘要:
            //     Window Text System Color.
            //     When the item is serialized out as xml, its value is "windowText".
            [EnumString("windowText")]
            WindowText,
            //
            // 摘要:
            //     Caption Text System Color.
            //     When the item is serialized out as xml, its value is "captionText".
            [EnumString("captionText")]
            CaptionText,
            //
            // 摘要:
            //     Active Border System Color.
            //     When the item is serialized out as xml, its value is "activeBorder".
            [EnumString("activeBorder")]
            ActiveBorder,
            //
            // 摘要:
            //     Inactive Border System Color.
            //     When the item is serialized out as xml, its value is "inactiveBorder".
            [EnumString("inactiveBorder")]
            InactiveBorder,
            //
            // 摘要:
            //     Application Workspace System Color.
            //     When the item is serialized out as xml, its value is "appWorkspace".
            [EnumString("appWorkspace")]
            ApplicationWorkspace,
            //
            // 摘要:
            //     Highlight System Color.
            //     When the item is serialized out as xml, its value is "highlight".
            [EnumString("highlight")]
            Highlight,
            //
            // 摘要:
            //     Highlight Text System Color.
            //     When the item is serialized out as xml, its value is "highlightText".
            [EnumString("highlightText")]
            HighlightText,
            //
            // 摘要:
            //     Button Face System Color.
            //     When the item is serialized out as xml, its value is "btnFace".
            [EnumString("btnFace")]
            ButtonFace,
            //
            // 摘要:
            //     Button Shadow System Color.
            //     When the item is serialized out as xml, its value is "btnShadow".
            [EnumString("btnShadow")]
            ButtonShadow,
            //
            // 摘要:
            //     Gray Text System Color.
            //     When the item is serialized out as xml, its value is "grayText".
            [EnumString("grayText")]
            GrayText,
            //
            // 摘要:
            //     Button Text System Color.
            //     When the item is serialized out as xml, its value is "btnText".
            [EnumString("btnText")]
            ButtonText,
            //
            // 摘要:
            //     Inactive Caption Text System Color.
            //     When the item is serialized out as xml, its value is "inactiveCaptionText".
            [EnumString("inactiveCaptionText")]
            InactiveCaptionText,
            //
            // 摘要:
            //     Button Highlight System Color.
            //     When the item is serialized out as xml, its value is "btnHighlight".
            [EnumString("btnHighlight")]
            ButtonHighlight,
            //
            // 摘要:
            //     3D Dark System Color.
            //     When the item is serialized out as xml, its value is "3dDkShadow".
            [EnumString("3dDkShadow")]
            ThreeDDarkShadow,
            //
            // 摘要:
            //     3D Light System Color.
            //     When the item is serialized out as xml, its value is "3dLight".
            [EnumString("3dLight")]
            ThreeDLight,
            //
            // 摘要:
            //     Info Text System Color.
            //     When the item is serialized out as xml, its value is "infoText".
            [EnumString("infoText")]
            InfoText,
            //
            // 摘要:
            //     Info Back System Color.
            //     When the item is serialized out as xml, its value is "infoBk".
            [EnumString("infoBk")]
            InfoBack,
            //
            // 摘要:
            //     Hot Light System Color.
            //     When the item is serialized out as xml, its value is "hotLight".
            [EnumString("hotLight")]
            HotLight,
            //
            // 摘要:
            //     Gradient Active Caption System Color.
            //     When the item is serialized out as xml, its value is "gradientActiveCaption".
            [EnumString("gradientActiveCaption")]
            GradientActiveCaption,
            //
            // 摘要:
            //     Gradient Inactive Caption System Color.
            //     When the item is serialized out as xml, its value is "gradientInactiveCaption".
            [EnumString("gradientInactiveCaption")]
            GradientInactiveCaption,
            //
            // 摘要:
            //     Menu Highlight System Color.
            //     When the item is serialized out as xml, its value is "menuHighlight".
            [EnumString("menuHighlight")]
            MenuHighlight,
            //
            // 摘要:
            //     Menu Bar System Color.
            //     When the item is serialized out as xml, its value is "menuBar".
            [EnumString("menuBar")]
            MenuBar
        }

        public SystemColorValues Type;

        public string Name;
        public SystemColor(string hex) : base(hex)
        {

        }
    }
}
