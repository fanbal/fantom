﻿namespace Fantom.Drawing
{

    /// <summary>
    /// PowerPoint传统坐标锚点信息，用于文档导出。
    /// </summary>
    public struct PMLRenderTransform
    {
        /// <summary>
        /// 角度。
        /// </summary>
        public double Rotation;

        /// <summary>
        /// 左坐标。
        /// </summary>
        public Emu Left;

        /// <summary>
        /// 顶坐标。
        /// </summary>
        public Emu Top;

        /// <summary>
        /// 宽度。
        /// </summary>
        public Emu Width;

        /// <summary>
        /// 宽度。
        /// </summary>
        public Emu Height;

        /// <summary>
        /// 是否水平反向。
        /// </summary>
        public bool HorizontalFlap;

        /// <summary>
        /// 是否垂直反向。
        /// </summary>
        public bool VerticalFlap;
    }
}
