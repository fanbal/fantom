﻿namespace Fantom.Drawing
{
    /// <summary>
    /// 矩形的尺寸。
    /// </summary>
    public class RectSize
    {
        /// <summary>
        /// 矩形的宽度。
        /// </summary>
        public Emu Width;

        /// <summary>
        /// 矩形的高度。
        /// </summary>
        public Emu Height;

        public RectSize(Emu width, Emu height)
        {
            Width = width;
            Height = height;
        }

        public static RectSize Default = new RectSize(100, 100);

        public override string ToString()
        {
            return string.Format("Width: {0}, Height: {1}", Width.ToPixel(), Height.ToPixel());
        }

        public static RectSize FromPixel(double width, double height)
        {
            return new RectSize(new Pixel(width).ToEmu(), new Pixel(height).ToEmu());
        }

    }
}
