﻿namespace Fantom.Drawing
{
    public class BitmapColor : ColorBase
    {
        /// <summary>
        /// 媒体源。
        /// </summary>
        public MediaSource Media { get; set; }


        public BitmapColor(MediaSource media)
        {
            Media = media;
        }
    }
}
