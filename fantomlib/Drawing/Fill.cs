﻿
namespace Fantom.Drawing
{





    /// <summary>
    /// 填充格式，此处用于呈现填充的内容，派生类有<see cref="NoFill"/>，<see cref="SolidFill"/>，<see cref="GradientFill"/>，<see cref="PictureFill"/>，<see cref="GroupFill"/>，<see cref="PatternFill"/>。
    /// </summary>
    public abstract class FillBase
    {
        public enum FillType
        {
            Unknown, NoFill, SolidFill, GradientFill, PictureFill, GroupFill, PatternFill
        }

        public virtual FillType Type => FillType.Unknown;

        private static HashSet<Type> _fillTypeHashSet = new HashSet<Type>()
        {
            typeof(D.NoFill),
            typeof(P.BlipFill),
            typeof(D.GradientFill),
            typeof(D.SolidFill),
            typeof(D.GroupFill),
            typeof(D.PatternFill)
        };

        internal static Type[] _fillTypeArray = new Type[]{
            typeof(D.NoFill),
            typeof(P.BlipFill),
            typeof(D.GradientFill),
            typeof(D.SolidFill),
            typeof(D.GroupFill),
            typeof(D.PatternFill)
        };

        private static HashSet<Type> _lineTypeHashSet = new HashSet<Type>()
        {
            typeof(D.NoFill),
            typeof(D.GradientFill),
            typeof(D.SolidFill),
            typeof(D.PatternFill)
        };

        internal static bool IsFillTypeX(OX.OpenXmlElement element)
        {
            return _fillTypeHashSet.Contains(element.GetType());
        }


        internal static bool IsLineTypeX(OX.OpenXmlElement element)
        {
            return _lineTypeHashSet.Contains(element.GetType());
        }

        internal static FillBase BuildFillEntry(PresentationImportArgs importArgs)
        {
            return BuildFillEntry(importArgs.ArgElement, importArgs);
        }
        internal static FillBase BuildFillEntry(OX.OpenXmlElement element, PresentationImportArgs importArgs)
        {
            if (element is null) return null;
            if (element is D.NoFill)
            {
                return NoFill.BuildFill(element as D.NoFill, importArgs);
            }
            else if (element is P.BlipFill)
            {
                return PictureFill.BuildFill(element as P.BlipFill, importArgs);
            }
            else if (element is D.GradientFill)
            {
                return GradientFill.BuildFill(element as D.GradientFill, importArgs);
            }
            else if (element is D.SolidFill)
            {
                return SolidFill.BuildFill(element as D.SolidFill, importArgs);
            }
            else if (element is D.GroupFill)
            {
                throw new Exception("暂时无法解析组填充。");
            }
            else if (element is D.PatternFill)
            {
                throw new Exception("暂时无法解析模式填充。");
            }
            throw new Exception("未知填充对象。");
        }

        internal static OX.OpenXmlElement CreateFillEntry(FillBase fill, PresentationExportArgs exportArgs)
        {
            if (fill is null) return null;
            return fill.Export(exportArgs);
        }

        internal virtual OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
        {
            return null;
        }

    }


    /// <summary>
    /// 纯色填充。
    /// </summary>
    public sealed class SolidFill : FillBase
    {
        #region properties

        public override FillType Type => FillType.SolidFill;

        /// <summary>
        /// 颜色。
        /// </summary>
        public ColorBase Color { get; set; } = null;



        #endregion

        #region ctors

        public SolidFill(ColorBase color)
        {
            Color = color;
        }


        #endregion

        public override string ToString()
        {
            return "SolidFill: " + Color.ToString();
        }

        internal static SolidFill BuildFill(D.SolidFill element, PresentationImportArgs importArgs)
        {
            return new SolidFill(element.GetColorFromNodeWithColor(importArgs.ColorLoadArgs));
        }

        internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
        {
            return new D.SolidFill(ColorBuilder.ColorToOpenXmlElement(Color));
        }
    }

    /// <summary>
    /// 渐变填充。
    /// </summary>
    public sealed class GradientFill : FillBase
    {
        public override FillType Type => FillType.GradientFill;

        public class GradientStop
        {
            public Emu Position;
            public ColorBase Color;
        }

        public int RotationWithShape;


        public List<GradientStop> GradientStops => _gradientStops;
        private List<GradientStop> _gradientStops = new List<GradientStop>();


        internal static GradientFill BuildFill(D.GradientFill element, PresentationImportArgs importArgs)
        {
            return new GradientFill();
        }

        internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
        {
            //ColorBuilder.ColorToOpenXmlElement(Color)
            return new D.GradientFill();
        }

    }

    public sealed class PictureFill : FillBase
    {

        public override FillType Type => FillType.PictureFill;
        public class TileFormat
        {
            public Emu? HorizontalOffset, VerticalOffset, HorizontalRotio, VerticalRatio;
            public TileFilpMode? Filp;
            public RectAlignmentType? RectAlignment;

            internal static TileFormat BuildTileFormat(PresentationImportArgs importArgs)
            {
                var xTile = importArgs.ArgElement as D.Tile;
                if (xTile is null) return null;
                var tile = new TileFormat();
                tile.VerticalOffset = xTile.VerticalOffset?.Value;
                tile.HorizontalOffset = xTile.HorizontalOffset?.Value;
                tile.HorizontalRotio = xTile.HorizontalRatio?.Value;
                tile.VerticalRatio = xTile.VerticalRatio?.Value;
                if (xTile.Flip is not null) tile.Filp = (TileFilpMode)xTile.Flip.Value;
                if (xTile.Alignment is not null) tile.RectAlignment = (RectAlignmentType)xTile.Alignment.Value;
                return tile;
            }
            internal static D.Tile CreateTileFormat(TileFormat tile, PresentationExportArgs exportArgs)
            {
                if (tile is null) return null;
                var xTile = new D.Tile();
                xTile.VerticalOffset = tile.VerticalOffset.Value.Value;
                xTile.HorizontalOffset = tile.HorizontalOffset.Value.Value;
                xTile.HorizontalRatio = tile.HorizontalRotio.Value.ToInt();
                xTile.VerticalRatio = tile.VerticalRatio.Value.ToInt();
                xTile.Flip = (D.TileFlipValues)tile.Filp;
                xTile.Alignment = (D.RectangleAlignmentValues)tile.RectAlignment.Value;
                return xTile;
            }

        }

        //public string EmbedID;
        public MediaSource Media;

        public List<EffectBase> Effects = new List<EffectBase>();

        public RelativeRect SourceRect;

        public TileFormat Tile;

        public Emu? DPI;
        public bool? RotateWithShape;

        internal static PictureFill BuildPictureFill(PresentationImportArgs importArgs)
        {
            P.BlipFill xBlipFill = importArgs.ArgElement as P.BlipFill;
            if (xBlipFill is null) return null;
            var pictureFill = new PictureFill();

            if (xBlipFill.Dpi is not null) pictureFill.DPI = xBlipFill.Dpi;

            if (xBlipFill.RotateWithShape is not null) pictureFill.RotateWithShape = xBlipFill.RotateWithShape;

            pictureFill.Media = importArgs.MediaManager.LoadMedia(importArgs.RID2URLMapper[xBlipFill.Blip.Embed].Uri);



            foreach (var xChild in xBlipFill.Blip)
            {
                if (EffectBase.IsPictureEffectX(xChild))
                    pictureFill.Effects.Add(EffectBase.BuildEffectEntry(new PresentationImportArgs(importArgs) { ArgElement = xChild }));
            }

            pictureFill.SourceRect = RelativeRect.BuildRelativeRect(new PresentationImportArgs(importArgs) { ArgElement = xBlipFill.SourceRectangle });
            pictureFill.Tile = TileFormat.BuildTileFormat(new PresentationImportArgs(importArgs) { ArgElement = xBlipFill.GetFirstChild<D.Tile>() });
            return pictureFill;
        }



        internal static P.BlipFill CreateBlipFill(PictureFill pictureFill, PresentationExportArgs exportArgs)
        {
            if (pictureFill is null) return null;
            var xBilpFill = new P.BlipFill();

            //xBilpFill.Blip = new 
            xBilpFill.Blip = new D.Blip();
            xBilpFill.Blip.Embed = exportArgs.RelationshipMapperContainer.GetPropertyRelationshipIDString(pictureFill.Media);

            var hasMediaPart = exportArgs.MediaMapper.TryGetValue(pictureFill.Media, out var mediaPart);
            var hasContainerPart = exportArgs.Mapper.TryGetValue(exportArgs.RelationshipMapperContainer, out var slidePart);

            if(hasMediaPart == false || hasContainerPart == false)
            {
                pictureFill.Media.CreatePart(exportArgs);
           
            }

            
            if (pictureFill.DPI is not null) xBilpFill.Dpi = (DocumentFormat.OpenXml.UInt32Value)pictureFill.DPI.Value.Value;
            if (pictureFill.RotateWithShape is not null) xBilpFill.RotateWithShape = pictureFill.RotateWithShape;

            foreach (var effect in pictureFill.Effects)
            {
                xBilpFill.AppendExtend(effect.Export(exportArgs));
            }

            if (pictureFill.Tile is not null) xBilpFill.AppendExtend(TileFormat.CreateTileFormat(pictureFill.Tile, exportArgs));
            if (pictureFill.SourceRect is not null) xBilpFill.SourceRectangle = RelativeRect.CreateRelativeRect<D.SourceRectangle>(pictureFill.SourceRect, exportArgs);

            return xBilpFill;
        }


        internal static PictureFill BuildFill(P.BlipFill element, PresentationImportArgs importArgs)
        {
            return BuildPictureFill(new PresentationImportArgs(importArgs) { ArgElement = element });
        }

        internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
        {
            //ColorBuilder.ColorToOpenXmlElement(Color)
            return CreateBlipFill(this, null);
        }

    }


    /// <summary>
    /// 空填充。
    /// </summary>
    public sealed class NoFill : FillBase
    {
        public override FillType Type => FillType.NoFill;

        internal static NoFill BuildFill(D.NoFill element, PresentationImportArgs importArgs)
        {
            return new NoFill();
        }

        internal override OX.OpenXmlElement Export(PresentationExportArgs exportArgs)
        {
            //ColorBuilder.ColorToOpenXmlElement(Color)
            return new D.NoFill();
        }
    }

    public sealed class GroupFill : FillBase
    {
        public override FillType Type => FillType.GroupFill;
    }

    public sealed class PatternFill : FillBase
    {
        public override FillType Type => FillType.PatternFill;
    }
}
