﻿
using System.Linq;

namespace Fantom.Drawing
{
    /// <summary>
    /// 颜色导入相关方法集合。
    /// </summary>
    internal static class ColorBuilder
    {


        #region new methods

      
        /// <summary>
        /// 从节点中查找并提取颜色信息。
        /// </summary>
        public static ColorBase GetColorFromNodeWithColor(this OX.OpenXmlElement element, ColorLoadArgs loadArgs)
        {
            if (element is null) return null;
            foreach (var child in element.ChildElements)
            {
                if (child is D.RgbColorModelHex ||
                    child is D.RgbColorModelPercentage ||
                    child is D.SystemColor ||
                    child is D.HslColor ||
                    child is D.SchemeColor ||
                    child is D.PresetColor)
                {
                    return OpenXmlElementToColor(child, loadArgs);
                }
            }
            return null;
        }

        /// <summary>
        /// 导入颜色。
        /// </summary>
        public static ColorBase OpenXmlElementToColor(OX.OpenXmlElement color, ColorLoadArgs loadArgs)
        {
            if (color is null) return null; 
            if (color is D.RgbColorModelHex)
            {
                return SolidColor.FromHex((color as D.RgbColorModelHex).Val.Value);
            }
            else if (color is D.SchemeColor)
            {
                if (loadArgs.Theme is null) throw new Exception("未找到主题。");
                return SchemeColorElementToColor(color as D.SchemeColor, loadArgs.Theme);
            }
            else if (color is D.Blip)
            {
                if (loadArgs.Theme is null) throw new Exception("未找到主题。");
                return BitmapToColor(color as D.Blip, loadArgs);
            }
            else if (color is P.HslColor)
            {
                return GetHSLColorFromDXMLNode(color as P.HslColor);
            }
            else if (color is D.PresetColor)
            {
                return GetPresetColorFromDXMLNode(color as D.PresetColor);
            }
            else if (color is D.SystemColor)
            {
                return SystemColorToColor(color as D.SystemColor);
            }
            throw new Exception("不明颜色节点");

        }




        /// <summary>
        /// 导入颜色。
        /// </summary>
        public static ColorBase OpenXmlElementToColor(OX.OpenXmlElement color, Theme theme)
        {
            if (color is null) return null;
            if (color is D.RgbColorModelHex)
            {
                return SolidColor.FromHex((color as D.RgbColorModelHex).Val.Value);
            }
            else if (color is D.SchemeColor)
            {
                if (theme is null) throw new Exception("未找到主题。");
                return SchemeColorElementToColor(color as D.SchemeColor, theme);
            }

            else if (color is P.HslColor)
            {
                return GetHSLColorFromDXMLNode(color as P.HslColor);
            }
            else if (color is D.PresetColor)
            {
                return GetPresetColorFromDXMLNode(color as D.PresetColor);
            }
            else if (color is D.SystemColor)
            {
                return SystemColorToColor(color as D.SystemColor);
            }
            throw new Exception("不明颜色节点");

        }
        public static ColorBase OpenXmlElementToColor(OX.OpenXmlElement color)
        {
            if (color is null) return null;
            if (color is D.RgbColorModelHex)
            {
                return SolidColor.FromHex((color as D.RgbColorModelHex).Val.Value);
            }
            else if (color is P.HslColor)
            {
                return GetHSLColorFromDXMLNode(color as P.HslColor);
            }
            else if (color is D.PresetColor)
            {
                return GetPresetColorFromDXMLNode(color as D.PresetColor);
            }
            else if (color is D.SystemColor)
            {
                return SystemColorToColor(color as D.SystemColor);
            }
            throw new Exception("不明颜色节点");

        }






        /// <summary>
        /// 从节点中提取的图片相关数据。
        /// </summary>
        public static BitmapColor BitmapToColor(D.Blip color, ColorLoadArgs loadArgs)
        {
            var uris =
                from rp in loadArgs.SlidePart.Parts
                where color.Embed == rp.RelationshipId
                select rp.OpenXmlPart.Uri;

            var uri = uris.First();

            return new BitmapColor(loadArgs.MediaManager.LoadMedia(uri));
        }



        public static SystemColor SystemColorToColor(D.SystemColor xColor)
        {
            var color = new SystemColor(xColor.LastColor.InnerText) { Name = xColor.Val };
            return color;
        }


        /// <summary>
        /// 从节点中提取与之匹配的主题颜色。
        /// </summary>
        public static SchemeColor SchemeColorElementToColor(D.SchemeColor color, Theme theme)
        {
            var attr = color.Val.Value;
            var type = ThemeColorTypeHelper.GetColorType(attr);

            var xmod = color.GetFirstChild<D.LuminanceModulation>();
            var xoff = color.GetFirstChild<D.LuminanceOffset>();
            var xalp = color.GetFirstChild<D.Alpha>();
            var xshd = color.GetFirstChild<D.Shade>();

            var off = xoff is not null ? xoff.Val.Value : Emu.Num0;
            var mod = xmod is not null ? xmod.Val.Value : Emu.Num100;
            var alp = xalp is not null ? xalp.Val.Value : Emu.Num0;
            var shd = xshd is not null ? xshd.Val.Value : Emu.Num0;

            SchemeColor schemeColor = new SchemeColor(theme.ColorScheme[type], type);
            schemeColor.ColorScheme = theme.ColorScheme;
            schemeColor.LoadColorTransform(color);


            return schemeColor;
        }


        /// <summary>
        /// 从节点数据中提取正确的 HSL 变化值。
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static SolidColor GetHSLColorFromDXMLNode(P.HslColor color)
        {
            var hue = 255d * color.Hue.Value / Emu.TwoPi;
            var sat = 255d * color.Saturation.Value / Emu.TwoPi;
            var lum = 255d * color.Lightness.Value / Emu.TwoPi;
            return SolidColor.FromHSL(hue, sat, lum);
        }

        /// <summary>
        /// 从节点数据中提取预设颜色。
        /// </summary>
        public static PresetColor GetPresetColorFromDXMLNode(D.PresetColor color)
        {
            var c = PresetColor.FromPresetType((PresetColor.PresetColorValues)color.Val.Value);
            c.LoadColorTransform(color);
            return c;
        }


     
        /// <summary>
        /// 为主题色创建带有颜色的实例。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="color"></param>
        /// <returns></returns>
        public static T CreateOpenXmlElementWithColorNode<T>(ColorBase color) where T : OX.OpenXmlElement,new()
        {
            var type = new T();
            type.AppendExtend(ColorToOpenXmlElement(color));
            return type;
        }

        /// <summary>
        /// 颜色转换为 OpenXml 元素。
        /// </summary>
        /// <param name="color">颜色类型。</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static OX.OpenXmlElement ColorToOpenXmlElement(ColorBase color)
        {

            if (color is SolidColor)
            {
                var solidColor = color as SolidColor;
                switch (solidColor.Mode)
                {
                    case ColorBase.ColorMode.RGB:
                        return CreateSolidColor(solidColor);
                    case ColorBase.ColorMode.HSL:
                        return CreateHSLColor(solidColor);
                    default:
                        throw new Exception("不明模式的纯色。");
                }
            }
            else if (color is SchemeColor)
            {
                var schemeColor = color as SchemeColor;
                return CreateSchemeColor(schemeColor);
            }
            else if (color is SystemColor)
            {
                var systemColor = color as SystemColor;
                return CreateSystemColor(systemColor);
            }
            else if (color is BitmapColor)
            {
                var bitmapColor = color as BitmapColor;
                return CreateBlipFill(bitmapColor);
            }
            else if (color is PresetColor)
            {
                var presetColor = color as PresetColor;
                return CreatePresetColor(presetColor);
            }

            return null;
        }

        public static D.RgbColorModelHex CreateSolidColor(SolidColor solidColor)
        {
            var xsolid = new D.RgbColorModelHex();
            xsolid.Val = solidColor.ToHex();
            return xsolid;
        }

        public static D.SchemeColor CreateSchemeColor(SchemeColor schemeColor)
        {
            var xscheme = new D.SchemeColor();
            xscheme.Val = (D.SchemeColorValues)schemeColor.ThemeColorType;
            xscheme.Append(schemeColor.ExportColorTransform());
            return xscheme;
        }

        public static D.SystemColor CreateSystemColor(SystemColor systemColor)
        {
            var xsystem = new D.SystemColor();
            xsystem.LastColor = systemColor.ToHex();
            xsystem.Val = (D.SystemColorValues)systemColor.Type;
            return xsystem;
        }

        public static D.HslColor CreateHSLColor(SolidColor solidColor)
        {
            var xhsl = new D.HslColor();
            var hue = solidColor.Hue * Emu.TwoPi / 255d;
            var sat = solidColor.Saturation * Emu.TwoPi / 255d;
            var lum = solidColor.Lightness * Emu.TwoPi / 255d;
            xhsl.HueValue = (int)hue;
            xhsl.SatValue = (int)sat;
            xhsl.LumValue = (int)lum;

            return xhsl;
        }

        public static P.BlipFill CreateBlipFill(BitmapColor bitmapColor)
        {
            throw new Exception("没有实现对图片的导出。");
        }

        public static D.PresetColor CreatePresetColor(PresetColor presetColor)
        {
            var xPresetColor = new D.PresetColor();
            xPresetColor.Val = (D.PresetColorValues)presetColor.PresetType;
            xPresetColor.Append(presetColor.ExportColorTransform());
            return xPresetColor;
        }

        #endregion new methods

    }
}
