﻿namespace Fantom.Drawing
{
    //<a:solidFill>
    //	<a:schemeClr val = "accent1" >
    //		< a:lumMod val = "50000" />
    //		< a:alpha val = "50000" />
    //	</ a:schemeClr>
    //</a:solidFill>

    /// <summary>
    /// 纯色块的一阶引用。。
    /// </summary>
    public class SchemeColor : TransformableColor
    {
        #region properties

        private ColorBase _origin = null; // 源颜色。

        public ColorScheme ColorScheme;

        public override byte Red => _origin.Red;
        public override byte Blue => _origin.Blue;
        public override byte Green => _origin.Green;


        
        private bool _isDarken = true; // 是否为暗色调。

        /// <summary>
        /// 主题颜色类型。
        /// </summary>
        public ColorSchemeColorType ThemeColorType { get; private set; }

        #endregion

        #region ctors

        internal SchemeColor(ColorBase origin, ColorSchemeColorType themeColorType) 
        {
            _origin = origin;
            ThemeColorType = themeColorType;
            
        }

        /// <summary>
        /// 构造函数。
        /// </summary>
        /// <param name="origin"></param>
        private SchemeColor(ColorBase origin, Emu alpha, Emu lumMod, Emu lumOff, ColorSchemeColorType themeColorType)
        {
            if (origin is null)
            {
                throw new ArgumentNullException("SchemeColor中参数不允许为空值");
            }

            _origin = origin;

            var hsl = ColorConverter.RGBToHSL(_origin.Red, _origin.Green, _origin.Blue);


            Alpha = alpha;
            LumMod = lumMod;


            // 暗色调。
            if (lumOff == Emu.Num0)
            {
                _isDarken = true;
                hsl[2] *= (1d * lumMod.Value / Emu.Num100.Value);
            }
            else
            {
                _isDarken = false;
                hsl[2] = hsl[2] + (1 - hsl[2]) * 1d * lumOff.Value / Emu.Num100.Value;
            }


            var realRgb = ColorConverter.HSLToRGB2(hsl[0], hsl[1], hsl[2]);
            _red = realRgb[0];
            _green = realRgb[1];
            _blue = realRgb[2];

            ThemeColorType = themeColorType;
        }

        private SchemeColor(ColorBase origin, Emu alpha, Emu lumMod, ColorSchemeColorType themeColorType) :
            this(origin, alpha, lumMod, Emu.Num0, themeColorType)
        { }


        /// <summary>
        /// 拷贝构造函数。
        /// </summary>
        public SchemeColor(SchemeColor color, ColorSchemeColorType themeColorType)
        {
            if (color is null)
            {
                throw new ArgumentNullException("SchemeColor中参数不允许为空值");
            }

            ThemeColorType = themeColorType;
            _origin = color._origin;
        }

        #endregion


        #region static

     

        //public static SchemeColor FromLum(ColorBase origin, Emu alpha, Emu lumMod, Emu lumOff, ColorSchemeColorType themeColorType)
        //{
        //    var color = new SchemeColor();

        //    if (origin is null)
        //    {
        //        throw new ArgumentNullException("SchemeColor中参数不允许为空值");
        //    }

        //    color._origin = origin;

        //    var hsl = ColorConverter.RGBToHSL(origin.Red, origin.Green, origin.Blue);


        //    //color.Alpha = alpha;
        //    //color.LumMod = lumMod;


        //    // 暗色调。
        //    if (lumOff == Emu.Num0)
        //    {
        //        color._isDarken = true;
        //        hsl[2] *= (1d * lumMod.Value / Emu.Num100.Value);
        //    }
        //    else
        //    {
        //        color._isDarken = false;
        //        hsl[2] = hsl[2] + (1 - hsl[2]) * 1d * lumOff.Value / Emu.Num100.Value;
        //    }


        //    var realRgb = ColorConverter.HSLToRGB2(hsl[0], hsl[1], hsl[2]);
        //    color._red = realRgb[0];
        //    color._green = realRgb[1];
        //    color._blue = realRgb[2];

        //    color.ThemeColorType = themeColorType;

        //    return color;
        //}

        //public static SchemeColor FromShade(ColorBase origin, Emu alpha, Emu shade, ColorSchemeColorType themeColorType)
        //{
        //    var color = new SchemeColor();

        //    if (origin is null)
        //    {
        //        throw new ArgumentNullException("SchemeColor中参数不允许为空值");
        //    }

        //    color._origin = origin;

        //    var hsl = ColorConverter.RGBToHSL(origin.Red, origin.Green, origin.Blue);


        //    color.Alpha = alpha;
        //    color.Shade = shade;
        //    color._isDarken = true;

        //    hsl[2] *= (1d * shade.Value / Emu.Num100.Value);


        //    var realRgb = ColorConverter.HSLToRGB2(hsl[0], hsl[1], hsl[2]);
        //    color._red = realRgb[0];
        //    color._green = realRgb[1];
        //    color._blue = realRgb[2];

        //    color.ThemeColorType = themeColorType;

        //    return color;
        //}

        public override string ToString()
        {
            return string.Format("RGB: ({0}, {1}, {2})",
                this.Red, this.Green, this.Blue);
        }


        #endregion

    }
}
