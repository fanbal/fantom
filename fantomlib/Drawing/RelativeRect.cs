﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Drawing
{
    public class RelativeRect
    {
        public Emu? Left, Top, Right, Bottom;

        internal static RelativeRect BuildRelativeRect(PresentationImportArgs importArgs)
        {
            var xRelativeRect = importArgs.ArgElement as D.RelativeRectangleType;
            if (xRelativeRect is null) return null;
            var relativeRect = new RelativeRect();
            relativeRect.Left = xRelativeRect.Left?.Value;
            relativeRect.Bottom = xRelativeRect.Bottom?.Value;
            relativeRect.Right = xRelativeRect.Right?.Value;
            relativeRect.Top = xRelativeRect.Top?.Value;
            return relativeRect;
        }

        internal static T CreateRelativeRect<T>(RelativeRect relativeRect ,PresentationExportArgs exportArgs)where T : D.RelativeRectangleType,new()
        {
            if (relativeRect is null) return null;
            var xRelativeRect = new T();
            xRelativeRect.Left = relativeRect.Left.ToOX32();
            xRelativeRect.Bottom = relativeRect.Bottom.ToOX32();
            xRelativeRect.Right = relativeRect.Right.ToOX32();
            xRelativeRect.Top = relativeRect.Top.ToOX32();

            return xRelativeRect;
        }

    }
}
