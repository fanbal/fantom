﻿namespace Fantom.Drawing
{
    /// <summary>
    /// 纯色可透明颜色。
    /// </summary>
    public sealed class SolidColor : TransformableColor
    {
        /// <summary>
        /// 颜色类型。
        /// </summary>
        public ColorMode Mode;

        #region ctors

        public SolidColor(double hue, double sat, double light)
        {
            _hue = hue; _saturation = sat; _lightness = light; Mode = ColorMode.HSL;
        }

        public SolidColor(byte red, byte green, byte blue)
        {
            _red = red; _green = green; _blue = blue; Mode = ColorMode.RGB;
        }

        public SolidColor(SolidColor color)
        {
            _red = color._red;
            _green = color._green;
            _blue = color._blue;

            Mode = color.Mode;
        }

        public SolidColor(string hex) : base(hex) { }

        #endregion

        #region static

        public static SolidColor FromHex(string hex)
        {
            int[] hexArr = new int[6];
            for (int i = 0; i < 6; i++)
            {
                if (char.IsNumber(hex[i]))
                {
                    hexArr[i] = hex[i] - '0';
                }
                else if (char.IsUpper(hex[i]))
                {
                    hexArr[i] = hex[i] - 'A' + 10;
                }
                else
                {
                    hexArr[i] = hex[i] - 'a' + 10;
                }
            }
            return new SolidColor(
                (byte)(hexArr[0] * 16 + hexArr[1]),
                (byte)(hexArr[2] * 16 + hexArr[3]),
                (byte)(hexArr[4] * 16 + hexArr[5]));
        }

        public static SolidColor FromRGB(byte red, byte green, byte blue)
        {
            return new SolidColor(red, green, blue);
        }

        public static SolidColor FromHSL(double hue, double sat, double light)
        {
            return new SolidColor(hue, sat, light);
        }

        #endregion

    }
}
