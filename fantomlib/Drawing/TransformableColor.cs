﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Drawing
{
    public abstract class TransformableColor : ColorBase
    {


        public Emu? Tint;

        public Emu? Shade;

        public bool? Complement;

        public bool? Inverse;

        public bool? GrayScale;

        /// <summary>
        /// 透明度，节点为a:alpha
        /// </summary>
        public Emu? Alpha;

        public Emu? AlphaOff;

        public Emu? AlphaMod;

        public Emu? _Hue;

        public Emu? HueOff;

        public Emu? HueMod;

        public Emu? Sat;

        public Emu? SatOff;
        public Emu? SatMod;
        public Emu? Lum;
        public Emu? LumOff;
        public Emu? LumMod;
        public Emu? _Red;
        public Emu? RedOff;
        public Emu? RedMod;
        public Emu? _Green;
        public Emu? GreenOff;
        public Emu? GreenMod;
        public Emu? _Blue;
        public Emu? BlueOff;
        public Emu? BlueMod;
        public bool? Gamma;
        public bool? InvGamma;

        public TransformableColor()
        {

        }

        public TransformableColor(string hex) : base(hex)
        {

        }

        public void LoadColorTransform(OX.OpenXmlElement element)
        {
            foreach (var xChild in element)
            {
                if (xChild is D.Tint)
                    Tint = (xChild as D.Tint).Val;
                if (xChild is D.Shade)
                    Shade = (xChild as D.Shade).Val;
                if (xChild is D.Complement)
                    Complement = true;
                if (xChild is D.Inverse)
                    Inverse = true;
                if (xChild is D.Grayscale)
                    GrayScale = true;
                if (xChild is D.Alpha)
                    Alpha = (xChild as D.Alpha).Val;
                if (xChild is D.AlphaOffset)
                    AlphaOff = (xChild as D.AlphaOffset).Val;
                if (xChild is D.AlphaModulation)
                    AlphaMod = (xChild as D.AlphaModulation).Val;
                if (xChild is D.Hue)
                    _Hue = (xChild as D.Hue).Val;
                if (xChild is D.HueOffset)
                    HueOff = (xChild as D.HueOffset).Val;
                if (xChild is D.HueModulation)
                    HueMod = (xChild as D.HueModulation).Val;
                if (xChild is D.Saturation)
                    Sat = (xChild as D.Saturation).Val;
                if (xChild is D.SaturationOffset)
                    SatOff = (xChild as D.SaturationOffset).Val;
                if (xChild is D.SaturationModulation)
                    SatMod = (xChild as D.SaturationModulation).Val;
                if (xChild is D.Luminance)
                    Lum = (xChild as D.Luminance).Val;
                if (xChild is D.LuminanceOffset)
                    LumOff = (xChild as D.LuminanceOffset).Val;
                if (xChild is D.LuminanceModulation)
                    LumMod = (xChild as D.LuminanceModulation).Val;
                if (xChild is D.Red)
                    _Red = (xChild as D.Red).Val;
                if (xChild is D.Green)
                    _Green = (xChild as D.Green).Val;
                if (xChild is D.Blue)
                    _Blue = (xChild as D.Blue).Val;

                if (xChild is D.RedOffset)
                    RedOff = (xChild as D.RedOffset).Val;
                if (xChild is D.GreenOffset)
                    GreenOff = (xChild as D.GreenOffset).Val;
                if (xChild is D.BlueOffset)
                    BlueOff = (xChild as D.BlueOffset).Val;


                if (xChild is D.RedModulation)
                    RedMod = (xChild as D.RedModulation).Val;
                if (xChild is D.GreenModulation)
                    GreenMod = (xChild as D.GreenModulation).Val;
                if (xChild is D.BlueModulation)
                    BlueMod = (xChild as D.BlueModulation).Val;

                if (xChild is D.Gamma)
                    Gamma = true;

                if (xChild is D.InverseGamma)
                    InvGamma = true;

            }
        }

        public OX.OpenXmlElement[] ExportColorTransform()
        {

            var list = new List<OX.OpenXmlElement>();

            if (Tint is not null) list.Add(new D.Tint() { Val = Tint.ToOX32() });
            if (Shade is not null) list.Add(new D.Tint() { Val = Shade.ToOX32() });
            if (Complement is not null && Complement.Value == true) list.Add(new D.Complement());
            if (Inverse is not null && Inverse.Value == true) list.Add(new D.Inverse());
            if (GrayScale is not null && GrayScale.Value == true) list.Add(new D.Grayscale() );

            if (Alpha is not null) list.Add(new D.Alpha() { Val = Alpha.ToOX32() });
            if (AlphaOff is not null) list.Add(new D.AlphaOffset() { Val = AlphaOff.ToOX32() });
            if (AlphaMod is not null) list.Add(new D.AlphaModulation() { Val = AlphaMod.ToOX32() });

            if (_Hue is not null) list.Add(new D.Hue() { Val = _Hue.ToOX32() });
            if (HueOff is not null) list.Add(new D.HueOffset() { Val = HueOff.ToOX32() });
            if (HueMod is not null) list.Add(new D.HueModulation() { Val = HueMod.ToOX32() });

            if (Sat is not null) list.Add(new D.Saturation() { Val = Sat.ToOX32() });
            if (SatOff is not null) list.Add(new D.SaturationOffset() { Val = SatOff.ToOX32() });
            if (SatMod is not null) list.Add(new D.SaturationModulation() { Val = SatMod.ToOX32() });

            if (Lum is not null) list.Add(new D.Luminance() { Val = Lum.ToOX32() });
            if (LumOff is not null) list.Add(new D.LuminanceOffset() { Val = LumOff.ToOX32() });
            if (LumMod is not null) list.Add(new D.LuminanceModulation() { Val = LumMod.ToOX32() });

            if (_Red is not null) list.Add(new D.Red() { Val = _Red.ToOX32() });
            if (RedOff is not null) list.Add(new D.RedOffset() { Val = RedOff.ToOX32() });
            if (RedMod is not null) list.Add(new D.RedModulation() { Val = RedMod.ToOX32() });
            if (_Green is not null) list.Add(new D.Green() { Val = _Green.ToOX32() });
            if (GreenOff is not null) list.Add(new D.GreenOffset() { Val = GreenOff.ToOX32() });
            if (GreenMod is not null) list.Add(new D.GreenModulation() { Val = GreenMod.ToOX32() });

            if (_Blue is not null) list.Add(new D.Blue() { Val = _Blue.ToOX32() });
            if (BlueOff is not null) list.Add(new D.BlueOffset() { Val = BlueOff.ToOX32() });
            if (BlueMod is not null) list.Add(new D.BlueModulation() { Val = BlueMod.ToOX32() });

            if (Gamma is not null && Gamma.Value == true) list.Add(new D.Gamma());
            if (InvGamma is not null && InvGamma.Value == true) list.Add(new D.InverseGamma());


            return list.ToArray();
        }

    }
}
