﻿namespace Fantom
{
    /// <summary>
    /// 可选中对象的集合。
    /// </summary>
    public interface ISelection : IEnumerable<ISelectable>
    {

        /// <summary>
        /// 选择集合发生更改时触发该事件。
        /// </summary>
        event SelectionEventHandler OnSelectionChanged;

        /// <summary>
        /// 选中集合。
        /// </summary>
        IEnumerable<ISelectable> Selection { get; }


        /// <summary>
        /// 覆盖原选择集合的重新选择。
        /// </summary>
        /// <param name="objRange">下标集合。</param>
        void Select(IEnumerable<ISelectable> objRange);

        /// <summary>
        /// 附加原集合的选择。
        /// </summary>
        /// <param name="idRange">下标集合。</param>
        void SelectAdditional(IEnumerable<ISelectable> objRange);

        /// <summary>
        /// 覆盖原选择集合的重新选择。
        /// </summary>
        /// <param name="objRange">下标。</param>
        void Select(ISelectable obj);

        /// <summary>
        /// 附加原选择集合的选择。
        /// </summary>
        /// <param name="obj">下标。</param>
        void SelectAdditional(ISelectable obj);

        /// <summary>
        /// 释放选择。
        /// </summary>
        /// <param name="obj"></param>
        void UnSelect(ISelectable obj);

        /// <summary>
        /// 释放原选择集合中的指定元素。
        /// </summary>
        /// <param name="objRange"></param>
        void UnSelect(IEnumerable<ISelectable> objRange);

        /// <summary>
        /// 全选。
        /// </summary>
        void SelectAll();

        /// <summary>
        /// 全部释放。
        /// </summary>
        void UnSelectAll();
    }
}
