﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fantom.Interfaces
{
    internal interface IHasTimeLine
    {
        TimeLine TimeLine { get; internal set; }
    }
}
